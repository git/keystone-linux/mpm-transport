# Copyright (C) 2013-2015 Texas Instruments Incorporated - http://www.ti.com/
#
#
#  Redistribution and use in source and binary forms, with or without
#  modification, are permitted provided that the following conditions
#  are met:
#
#    Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
#    Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the
#    distribution.
#
#    Neither the name of Texas Instruments Incorporated nor the names of
#    its contributors may be used to endorse or promote products derived
#    from this software without specific prior written permission.
#
#  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
#  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
#  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
#  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
#  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
#  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
#  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
#  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
#  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
#  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
#  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

MPM_ROOT = $(shell pwd)
MPM_SRC = $(MPM_ROOT)/src
export MPM_LIB = $(MPM_ROOT)/lib
export MPM_BIN ?= $(MPM_ROOT)/bin
export MPM_INC = $(MPM_ROOT)/include

ifeq ($(BUILD_LOCAL),true)
ifdef LOCAL_SYSROOT
SYSROOT_OPT = --sysroot=$(LOCAL_SYSROOT)
STAGING_KERNEL_DIR = $(LOCAL_SYSROOT)/usr/src/kernel
endif
ARCH = arm
CROSS_COMPILE ?= arm-linux-gnueabihf-
ARFLAGS = crus
LDFLAGS ?= -Wl,--hash-style=gnu
CFLAGS ?= -march=armv7-a -marm -mthumb-interwork -mfloat-abi=hard -mfpu=neon -mtune=cortex-a15 -O3
EXTRA_CFLAGS = -g -ggdb2 -D_GNU_SOURCE -DARCH_$(ARCH) -Wall
CC = $(CROSS_COMPILE)gcc $(SYSROOT_OPT) $(CFLAGS) $(EXTRA_CFLAGS)
CC += -I$(STAGING_KERNEL_DIR)/include
AR = $(CROSS_COMPILE)ar
endif

DEVKIT_INC = $(PDK_INSTALL_PATH)
DEVKIT_LIB = $(PDK_INSTALL_PATH)/../lib/
DEVICE ?= k2h
DEVICE_UPPER = `echo $(DEVICE) | tr a-z A-Z`
INTERNALDEFS += -D__ARMv7 -DDEVICE_$(DEVICE_UPPER) -D_VIRTUAL_ADDR_SUPPORT -D__LINUX_USER_SPACE -D_LITTLE_ENDIAN=1 -DMAKEFILE_BUILD -DINTERNAL_LINKING_RAM -D_GNU_SOURCE

ifeq ($(HYPLNK_TRANSPORT),true)
INTERNALDEFS += -DHYPLNK_TRANSPORT
endif

ifeq ($(SRIO_TRANSPORT),true)
INTERNALDEFS += -DSRIO_TRANSPORT
endif

TARGET_PLATFORM = $(ARCH)
LDFLAGS += -pthread -lrt

export TARGET_PLATFORM
export LDFLAGS
export ARFLAGS
export CC
export AR
export LD
export INTERNALDEFS
export DEVKIT_INC
export DEVKIT_LIB
export DEVICE

MAKEFLAGS += --no-print-directory

export MPM_UTILS = $(MPM_SRC)/utils
export MPM_COMMON = $(MPM_SRC)/common
export MPM_TRANSPORT  = $(MPM_SRC)/transport

export MPMTRANSPORT_LIB = libmpmtransport.a
export MPMTRANSPORT_LIB_SO = libmpmtransport.so
export MPMTRANSPORT_LIB_OPT = -lmpmtransport

cJSON_DIR = $(MPM_UTILS)/cJSON
EDMA3_DIR = $(MPM_UTILS)/edma3

.PHONY: all build clean distclean jtest mkdir transport

all: transport

mkdir:
	mkdir -p $(MPM_LIB)

transport: mkdir
	make -C $(MPM_TRANSPORT) all

transport_clean:
	make -C $(MPM_TRANSPORT) clean

clean: transport_clean
	rm -rf $(MPM_LIB)

jtest: $(cJSON_DIR)/cJSON.c $(cJSON_DIR)/test.c
	$(CC) -g -o $@ $^ -lm

distclean: clean
	rm -rf $(cJSON_DIR)/*.o jtest

sysroot_install:
	cp lib/* $(LOCAL_SYSROOT)/usr/lib
	cp include/* $(LOCAL_SYSROOT)/usr/includes

installbin:
	install -d $(INSTALL_BIN_BASE_DIR)
	install -c -m 755 $(BASE_DIR)/src/transport/test/sharedmem/mpm_transport_test.out $(INSTALL_BIN_BASE_DIR)/mpm_transport_test.out
	install -c -m 755 $(BASE_DIR)/src/transport/test/qmss/qmss_arm_mt/mpm_transport_qmss_arm_mt.out $(INSTALL_BIN_BASE_DIR)/mpm_transport_qmss_arm_mt.out
ifeq ($(SRIO_TRANSPORT),true)
	install -c -m 755 $(BASE_DIR)/src/transport/test/srio/srio_arm_mt/mpm_transport_srio_arm_mt.out $(INSTALL_BIN_BASE_DIR)/mpm_transport_srio_arm_mt.out
endif
ifeq ($(HYPLNK_TRANSPORT),true)
	install -c -m 755 $(BASE_DIR)/src/transport/test/hyplnk/hyplnk_loopback/mpm_transport_hyplnk_loopback.out $(INSTALL_BIN_BASE_DIR)/mpm_transport_hyplnk_loopback.out
	install -c -m 755 $(BASE_DIR)/src/transport/test/hyplnk/hyplnk_remote/mpm_transport_hyplnk_remote.out $(INSTALL_BIN_BASE_DIR)/mpm_transport_hyplnk_remote.out
	install -c -m 755 $(BASE_DIR)/src/transport/test/hyplnk/hyplnk_loopback_dma/mpm_transport_hyplnk_loopback_dma.out $(INSTALL_BIN_BASE_DIR)/mpm_transport_hyplnk_loopback_dma.out
	install -c -m 755 $(BASE_DIR)/src/transport/test/hyplnk/hyplnk_loopback64/mpm_transport_hyplnk_loopback64.out $(INSTALL_BIN_BASE_DIR)/mpm_transport_hyplnk_loopback64.out
endif
