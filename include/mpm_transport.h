/*
 * Copyright (C) 2013-2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef __MPM_TRANSPORT_H__
#define __MPM_TRANSPORT_H__

#include <stdint.h>
#include <stdbool.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <sys/time.h>

#define DEFAULT_CONFIG_FILE_NAME "/etc/mpm/mpm_config.json"

/**
 * @brief MPM Transport Handle returned to user
 *
 * This is the handle passed back to the user from mpm_transport_open()
 * and contains the transport-related information to use the rest of the
 * mpm-transport API with.
 */
typedef void * mpm_transport_h;

/**
 * @brief Transaction handle returned after a DMA operation
 *
 * This handle is returned when invoking the put()/get() API to use
 * EDMA3 for a transfer. This transaction handle contains information
 * that allows you to later check when the transaction has finished
 */
typedef void * mpm_transport_trans_h;

#define MPM_TRANSPORT_TRANS_FAIL ((mpm_transport_trans_h) (-1))

/**
 * @brief Description of packet type to be used
 */
typedef enum {
	packet_addr_type_NONE = 0,
	packet_addr_type_QMSS = 1,
	packet_addr_type_SRIO_TYPE9 = 2,
	packet_addr_type_SRIO_TYPE11 = 3
} packet_addr_type;

typedef enum {
	MPM_TRANSPORT_INTERFACE_MODE_TX_AND_RX=0,
	MPM_TRANSPORT_INTERFACE_MODE_TX_ONLY=1
} interface_mode_t;

/**
 * @brief Options for how to wait for a packet
 */
typedef enum {
	/** The default option may vary on transport mode
	 *  If param is NULL, this option will be used */
	packet_recv_wait_DEFAULT,
	/** Signals to wait on an interrupt forever */
	packet_recv_BLOCKING_INTERRUPT,
	/** Wait for a message for a specific amount of time */
	packet_recv_TIMEOUT
} packet_recv_wait_opt;

/**
 * @brief Options for what buffer a packet being sent should use
 */
typedef enum {
	/** The default option may vary on transport mode
	 *  If param is NULL, this option will be used */
	packet_send_BUFFER_DEFAULT,
	/** API will memcpy user information to the descriptor buffer
	 *  This requires preallocated buffer for each descriptor */
	packet_send_MEMCPY_BUFFER,
	/** API will point the descriptor to a specified buffer addr */
	packet_send_LINK_BUFFER_POINTER
} packet_send_buffer_opt;

/**
 * @brief Options for mpm_transport_read()
 */
typedef struct mpm_transport_read_tag {
	/** Placeholder */
	uint32_t dummy;
} mpm_transport_read_t;

/**
 * @brief Options for mpm_transport_write()
 */
typedef struct mpm_transport_write_tag {
	/** Placeholder */
	uint32_t dummy;
} mpm_transport_write_t;

/**
 * @brief Options for sending a packet
 */
typedef struct mpm_transport_send_tag {
	/** Options for the send buffer */
	packet_send_buffer_opt buf_opt;
} mpm_transport_send_t;

/**
 * @brief Options for receiving a packet
 */
typedef struct mpm_transport_recv_tag {
	/** Options for waiting for a packet */
	packet_recv_wait_opt wait_opt;
} mpm_transport_recv_t;

/**
 * @brief Structure for creating a memory map
 *
 * This structure will tell MPM Transport API how to use
 * mmap. Please see mmap man page for options.
 */
typedef struct mpm_transport_mmap_tag {
	/** Refers to the 'prot' field in mmap and configures
	 *  the desired protection of the mapping */
	int mmap_prot;
	/** Refers to the 'flags' field in mmap and provides
	 *  additional options on opening a memory map */
	int mmap_flags;
} mpm_transport_mmap_t;

/**
 * @brief QMSS packet related information
 */
typedef struct mpm_transport_packet_addr_qmss_tag {
	/** Flow number to send this packet to */
	uint32_t flowId;
} mpm_transport_packet_addr_qmss_t;

/**
 * @brief SRIO Type11 transport related information
 *
 * Refer to SRIO user guide for additional information on
 * these fields.
 */
typedef struct mpm_transport_packet_srio_type11_tag {
	/** Defines 8-bit or 16-bit ID */
	int tt;
	/** Identification of the SRIO endpoint */
	uint16_t id;
	/** mailbox to tie with identification */
	int mailbox;
	/** letter to tie with identification */
	int letter;
	/** defines segment size usage */
	int segmap;
} mpm_transport_packet_srio_type11_tag;

/**
 * @brief SRIO Type9 transport related information
 *
 * Refer to SRIO user guide for additional information on
 * these fields.
 */
typedef struct mpm_transport_packet_srio_type9_tag {
	/** Defines 8-bit or 16-bit ID */
	int tt;
	/** Identification of the SRIO endpoint */
	uint16_t id;
	/** cos for type9 */
	int cos;
	/** stream ID for type 9 */
	int streamId;
} mpm_transport_packet_srio_type9_tag;

/**
 * @brief SRIO related information for open
 */
typedef struct mpm_transport_packet_addr_srio_tag {
	/** Type of SRIO packet. Can only be Type11 or Type 9*/
	packet_addr_type type;
	/** Specific Type11 or Type9 information */
	union {
		mpm_transport_packet_srio_type11_tag type11;
		mpm_transport_packet_srio_type9_tag type9;
	};
	/** QMSS memory region to use. This is an optional field
	 *  for future development to assign SRIO a pre-existing
	 *  memory region to use instead of creating a new region. */
	int memReg;
	/** SRIO peripheral initiation function pointer. This function
	 *  is called during mpm_transport_open() if populated. This field
	 *  can be NULL if you do not want this handle to initialize SRIO */
	int (*deviceInit) (void *initCfg, void *srioAddr, uint32_t serdesAddr);
	/* Additional parameter for the SRIO device init function */
	void *initCfg;
	/** SRIO peripheral teardown function pointer. This function
	 *  is called during mpm_transport_close() if populated. This field
	 *  can be NULL if you do not want this handle to teardown SRIO */
	int (*deviceDeinit) (void *deinitCfg, void *srioAddr);
	/* Additional parameter for the SRIO device deinit function */
	void *deinitCfg;
} mpm_transport_srio_init_t;

/**
 * @brief Structure for packets supported by MPM Transport
 */
typedef struct mpm_transport_packet_addr_tag {
	/** The type of packet this is */
	packet_addr_type addr_type;
	/** Current supported packet-based transports */
	union {
		mpm_transport_packet_addr_qmss_t qmss;
		union {
			mpm_transport_packet_srio_type11_tag type11;
			mpm_transport_packet_srio_type9_tag type9;
		} srio;
	} addr;
} mpm_transport_packet_addr_t;

/**
 * @brief QMSS related information for open
 *
 * flowId is returned to user. MPM Transport API will use
 * the next available flow possible. User can get this flow
 * number by reading this flowId entry that was passed in as
 * part of the config for mpm_transport_open().
 */
typedef struct mpm_transport_qmss_init_tag {
	/** flow id allocated by MPM Transport and returned to user */
	uint32_t flowId;
	/** Signifies if you want to init QMSS. Needs to be done per process
	 *  but not necessarily need to be done by MPM Transport */
	int qmss_init;
} mpm_transport_qmss_init_t;

/**
 * @brief RM related information for open
 */
typedef struct mpm_transport_rm_init_tag {
	/** RM client service handle to use. Needs to be set up
	 *  by user */
	void * rm_client_handle;
} mpm_transport_rm_init_t;

/**
 * @brief Information for opening a MPM Transport handle
 */
typedef struct mpm_transport_open_tag {
	/** open mode for sharedmem */
	mode_t open_mode;
	/** Time out value used for waiting for hyperlink connection */
	uint32_t msec_timeout;
	/** Flag to init hyperlink SERDES */
	int serdes_init;
	/** Flag to indicate Interface mode */
	interface_mode_t interface_mode;
	/** Resource Manager information passed to handle */
	mpm_transport_rm_init_t rm_info;
	/* Specific transport related information */
	struct {
		mpm_transport_qmss_init_t qmss;
		mpm_transport_srio_init_t srio;
	} transport_info;
} mpm_transport_open_t;

/**
 *  @b Function mpm_transport_packet_send
 *  @n Pushes a message of size len from pointer buf to the destination
 *     specified by addr_info
 *
 *  @retval
 *      Success - returns 0
 *  @retval
 *      Failure - returns value less than 0 (negative integer)
 *
 */
int mpm_transport_packet_send(mpm_transport_h h, char **buf, int *len, 
	mpm_transport_packet_addr_t *addr_info, mpm_transport_send_t *cfg);

/**
 *  @b Function mpm_transport_packet_recv
 *  @n Pops information from the packet_addr_t binded to handle h. Data is
 *     copied to buf, of size len.
 *
 *     Valid for SRIO recv: The source address from which the data was
 *     received is returned in the src_addr structure.
 *
 *	@retval
 *		Success - returns 0
 *	@retval
 *		Failure - returns value less than 0 (negative integer)
 *
 */
int mpm_transport_packet_recv(mpm_transport_h h, char **buf, int *len,
							  mpm_transport_packet_addr_t *src_addr,
							  mpm_transport_recv_t *cfg);

/**
 *  @b Function mpm_transport_peripheral_enable
 *  @n Sets up the transport portion defined by slave_name.
 *
 *  @param[in] slave_name
 *      The name of the slave to open. This function will parse for its 
 *      transport config
 *
 *  @param[in] cfg
 *      This parameter specifies the open mode to open the file descriptors
 *      Example:
 *          mpm_transport_open_t cfg = {
 *              .open_mode	= (O_SYNC|O_RDWR),
 *          };
 *
 *  @retval
 *      Success - returns 0
 *  @retval
 *      Failure - returns value less than 0 (negative integer)
 *
 */
int mpm_transport_peripheral_enable(char *slave_name, mpm_transport_open_t *cfg);

/**
 *  @b Function mpm_transport_peripheral_disable
 *  @n Disable the transport hardware defined by slave_name.
 *
 *  @param[in] slave_name
 *      The name of the slave to close. This function will parse for
 *      its transport config
 *
 *  @retval
 *      Success - returns 0
 *	@retval
 *      Failure - returns value less than 0 (negative integer)
 *
 */
int mpm_transport_peripheral_disable(char *slave_name);

/**
 *  @b Description: Function mpm_transport_open
 *  @n Open mpm transport connection
 *
 *
 *  @param[in] slave_name
 *             Should match the name in config file.
 *  @param[in] cfg
 *             Runtime configuration needed for this device
 *  @retval
 *      mpm transport handle 
 *  @retval
 *      Failure - returns NULL on error
 *
 *  @pre
 *  @post
 */
mpm_transport_h mpm_transport_open(char *slave_name, mpm_transport_open_t *cfg);

/**
 *  @b Description: Function mpm_transport_close
 *  @n Close mpm transport connection
 *
 *
 *  @param[in] h
 *      mpm transport handle
 *
 *  @pre
 *  @post
 */
void mpm_transport_close(mpm_transport_h h);

/**
 *  @b Description: Function mpm_transport_read
 *  @n Read from specified address on slave device into local buffer
 *
 *
 *  @param[in] h
 *      mpm transport handle
 *  @param[in] addr
 *      Address to read from
 *  @param[in] length
 *      Number of bytes to read
 *  @param[in] buf
 *      Buffer for storing read bytes
 *  @param[in] cfg
 *      Configuration for the read operation
 *  @retval
 *      Success - returns 0
 *  @retval
 *      Failure - returns error code less than 0 (negative integer)
 *
 *  @pre  call to mpm_transport_open is needed before calling this routine
 *  @post
 */
int mpm_transport_read(mpm_transport_h h, uint32_t addr, uint32_t length,
	char *buf, mpm_transport_read_t *cfg);

/**
 *  @b Description: Function mpm_transport_write
 *  @n Write to specified address on slave device from local buffer
 *
 *
 *  @param[in] h
 *      mpm transport handle
 *  @param[in] addr
 *      Address to write
 *  @param[in] length
 *      Number of bytes to write
 *  @param[in] buf
 *      Buffer with bytes to write
 *  @param[in] cfg
 *      Configuration for the write operation
 *  @retval
 *      Success - returns 0
 *  @retval
 *      Failure - returns error code less than 0 (negative integer)
 *
 *  @pre  call to mpm_transport_open is needed before calling this routine
 *  @post
 */
int mpm_transport_write(mpm_transport_h h, uint32_t addr, uint32_t length,
	char *buf, mpm_transport_write_t *cfg);

/**
 *  @b Description: Function mpm_transport_read64
 *  @n Read from specified address on slave device into local buffer
 *
 *
 *  @param[in] h
 *      mpm transport handle
 *  @param[in] addr
 *      64 bit Address to read from
 *  @param[in] length
 *      Number of bytes to read
 *  @param[in] buf
 *      Buffer for storing read bytes
 *  @param[in] cfg
 *      Configuration for the read operation
 *  @retval
 *      Success - returns 0
 *  @retval
 *      Failure - returns error code less than 0 (negative integer)
 *
 *  @pre  call to mpm_transport_open is needed before calling this routine
 *  @post
 */
int mpm_transport_read64(mpm_transport_h h, uint64_t addr, uint32_t length,
	char *buf, mpm_transport_read_t *cfg);

/**
 *  @b Description: Function mpm_transport_write64
 *  @n Write to specified address on slave device from local buffer
 *
 *
 *  @param[in] h
 *      mpm transport handle
 *  @param[in] addr
 *      64 bit Address to write
 *  @param[in] length
 *      Number of bytes to write
 *  @param[in] buf
 *      Buffer with bytes to write
 *  @param[in] cfg
 *      Configuration for the write operation
 *  @retval
 *      Success - returns 0
 *  @retval
 *      Failure - returns error code less than 0 (negative integer)
 *
 *  @pre  call to mpm_transport_open is needed before calling this routine
 *  @post
 */
int mpm_transport_write64(mpm_transport_h h, uint64_t addr, uint32_t length,
	char *buf, mpm_transport_write_t *cfg);

/**
 *  @b Description: Function mpm_transport_mmap
 *  @n Creates a mmap for the memory location and returns virtual address
 *
 *
 *  @param[in] h
 *      mpm transport handle
 *  @param[in] addr
 *      Address to mmap
 *  @param[in] length
 *      length of mmap
 *  @param[in] cfg
 *      Configuration for the mmap operation
 *  @retval
 *      mmaped Virtual address 
 *  @retval
 *      Failure - returns MAP_FAILED: (void *) (-1)
 *
 *  @pre  call to mpm_transport_open is needed before calling this routine
 *  @post
 */
void *mpm_transport_mmap(mpm_transport_h h, uint32_t addr, uint32_t length,
	mpm_transport_mmap_t *cfg);

/**
 *  @b Description: Function mpm_transport_munmap
 *  @n Undoes the previous mmap
 *
 *
 *  @param[in] h
 *      mpm transport handle
 *  @param[in] va
 *      Virtual address of previous mmap
 *  @param[in] length
 *      length of previous mmap
 *  @param[in] cfg
 *      Configuration for the mmap operation
 *  @retval
 *      Success - returns 0
 *  @retval
 *      Failure - returns error code less than 0 (negative integer)
 *
 *  @pre  call to mpm_transport_open is needed before calling this routine
 *  @post
 */
int mpm_transport_munmap(mpm_transport_h h, void *va, uint32_t length);

/**
 *  @b Description: Function mpm_transport_mmap64
 *  @n Creates mmap for the 64 bit memory location and returns virtual address
 *
 *
 *  @param[in] h
 *      mpm transport handle
 *  @param[in] addr
 *      64 bit Address to mmap
 *  @param[in] length
 *      length of mmap
 *  @param[in] cfg
 *      Configuration for the mmap operation
 *  @retval
 *      mmaped Virtual address 
 *  @retval
 *      Failure - returns MAP_FAILED: (void *) (-1)
 *
 *  @pre  call to mpm_transport_open is needed before calling this routine
 *  @post
 */
void *mpm_transport_mmap64(mpm_transport_h h, uint64_t addr, uint32_t length,
	mpm_transport_mmap_t *cfg);

/**
 *  @b Description: Function mpm_transport_munmap64
 *  @n Undoes the previous 64 bit mmap
 *
 *
 *  @param[in] h
 *      mpm transport handle
 *  @param[in] va
 *      Virtual address of previous mmap
 *  @param[in] length
 *      length of previous mmap
 *  @param[in] cfg
 *      Configuration for the mmap operation
 *  @retval
 *      Success - returns 0
 *  @retval
 *      Failure - returns error code less than 0 (negative integer)
 *
 *  @pre  call to mpm_transport_open is needed before calling this routine
 *  @post
 */
int mpm_transport_munmap64(mpm_transport_h h, void *va, uint32_t length);

/**
 *  @b Description: Function mpm_transport_get_initiate
 *  @n Initiate DMA transfer from remote address to local address
 *
 *
 *  @param[in] h
 *      mpm transport handle
 *  @param[in] from_addr
 *      Remote address to DMA from
 *  @param[in] to_addr
 *      Local address to DMA to
 *  @param[in] length
 *      length of transfer
 *  @param[in] is_blocking
 *      If 1, waits for the transfer to be complete. 
 *      If 0, Initiates transfer and returns transaction handle
 *  @param[in] cfg
 *      Configuration for read operation
 *  @retval
 *      Success - returns transaction handle mpm_transport_trans_h 
 *              - or NULL if is_blocking is set
 *  @retval
 *      Failure - MPM_TRANSPORT_TRANS_FAIL
 *
 *  @pre  call to mpm_transport_open is needed before calling this routine
 *  @post
 */
mpm_transport_trans_h mpm_transport_get_initiate(mpm_transport_h h,
	uint32_t from_addr, uint32_t to_addr, uint32_t length, bool is_blocking,
	mpm_transport_read_t *cfg);

/**
 *  @b Description: Function mpm_transport_put_initiate
 *  @n Initiate DMA transfer from local address to remote address
 *
 *
 *  @param[in] h
 *      mpm transport handle
 *  @param[in] to_addr
 *      Remote address to DMA to
 *  @param[in] from_addr
 *      Local address to DMA from
 *  @param[in] length
 *      length of transfer
 *  @param[in] is_blocking
 *      If 1, waits for the transfer to be complete and returns NULL 
 *      If 0, Initiates transfer and returns transaction handle
 *  @param[in] cfg
 *      Configuration for write operation
 *  @retval
 *      Success - returns transaction handle mpm_transport_trans_h 
 *              - or NULL if is_blocking is set
 *  @retval
 *      Failure - MPM_TRANSPORT_TRANS_FAIL
 *
 *  @pre  call to mpm_transport_open is needed before calling this routine
 *  @post
 */
mpm_transport_trans_h mpm_transport_put_initiate(mpm_transport_h h,
	uint32_t to_addr, uint32_t from_addr, uint32_t length, bool is_blocking,
	mpm_transport_write_t *cfg);

/**
 *  @b Description: Function mpm_transport_get_initiate_linked
 *  @n Initiate multiple linked DMA transfers from remote addresses
 *     to local addresses
 *
 *
 *  @param[in] h
 *      mpm transport handle
 *  @param[in] num_links
 *      number of links
 *  @param[in] from_addr
 *      Array of Remote addresses to DMA from
 *  @param[in] to_addr
 *      Array of Local addresses to DMA to
 *  @param[in] length
 *      Array of length of transfers
 *  @param[in] is_blocking
 *      If 1, waits for the transfer to be complete. 
 *      If 0, Initiates transfer and returns transaction handle
 *  @param[in] cfg
 *      Configuration for read operation
 *  @retval
 *      Success - returns transaction handle mpm_transport_trans_h 
 *              - or NULL if is_blocking is set
 *  @retval
 *      Failure - MPM_TRANSPORT_TRANS_FAIL
 *
 *  @pre  call to mpm_transport_open is needed before calling this routine
 *  @post
 */
mpm_transport_trans_h mpm_transport_get_initiate_linked(mpm_transport_h h,
	uint32_t num_links, uint32_t from_addr[], uint32_t to_addr[],
	uint32_t length[], bool is_blocking, mpm_transport_read_t *cfg);

/**
 *  @b Description: Function mpm_transport_put_initiate_linked
 *  @n Initiate multiple linked DMA transfers from local addresses to 
 *     remote addresses
 *
 *
 *  @param[in] h
 *      mpm transport handle
 *  @param[in] to_addr
 *      Array of Remote addresses to DMA to
 *  @param[in] from_addr
 *      Array of Local address to DMA from
 *  @param[in] length
 *      Array of length of transfers
 *  @param[in] is_blocking
 *      If 1, waits for the transfer to be complete and returns NULL 
 *      If 0, Initiates transfer and returns transaction handle
 *  @param[in] cfg
 *      Configuration for write operation
 *  @retval
 *      Success - returns transaction handle mpm_transport_trans_h 
 *              - or NULL if is_blocking is set
 *  @retval
 *      Failure - MPM_TRANSPORT_TRANS_FAIL
 *
 *  @pre  call to mpm_transport_open is needed before calling this routine
 *  @post
 */
mpm_transport_trans_h mpm_transport_put_initiate_linked(mpm_transport_h h,
	uint32_t num_links, uint32_t to_addr[], uint32_t from_addr[],
	uint32_t length[], bool is_blocking, mpm_transport_write_t *cfg);

/**
 *  @b Description: Function mpm_transport_transfer_check
 *  @n Checks for DMA transfer completion and clears pending interrupt
 *     if complete.
 *
 *
 *  @param[in] h
 *      mpm transport handle
 *  @param[in] th
 *      Transaction handle of previously initiated transfer
 *  @retval
 *      Success  0 : If DMA transfer still in progress
 *               1 : If DMA transfer is complete
 *  @retval
 *      Failure - returns error code less than 0 (negative integer)
 *
 *  @pre  call to put/get initiate functions needed before calling this routine
 *  @post
 */
int mpm_transport_transfer_check(mpm_transport_h h, mpm_transport_trans_h th);

/**
 *  @b Description: Function mpm_transport_get_initiate64
 *  @n Initiate DMA transfer from remote address to local address
 *     (Supports 64 bit addresses)
 *
 *
 *  @param[in] h
 *      mpm transport handle
 *  @param[in] from_addr
 *      64 bit Remote address to DMA from
 *  @param[in] to_addr
 *      64 bit Local address to DMA to
 *  @param[in] length
 *      length of transfer
 *  @param[in] is_blocking
 *      If 1, waits for the transfer to be complete. 
 *      If 0, Initiates transfer and returns transaction handle
 *  @param[in] cfg
 *      Configuration for read operation
 *  @retval
 *      Success - returns transaction handle mpm_transport_trans_h 
 *              - or NULL if is_blocking is set
 *  @retval
 *      Failure - MPM_TRANSPORT_TRANS_FAIL
 *
 *  @pre  call to mpm_transport_open is needed before calling this routine
 *  @post
 */
mpm_transport_trans_h mpm_transport_get_initiate64(mpm_transport_h h,
	uint64_t from_addr, uint64_t to_addr, uint32_t length, bool is_blocking,
	mpm_transport_read_t *cfg);

/**
 *  @b Description: Function mpm_transport_put_initiate64
 *  @n Initiate DMA transfer from local address to remote address
 *     (Supports 64 bit addresses)
 *
 *
 *  @param[in] h
 *      mpm transport handle
 *  @param[in] to_addr
 *      64 bit Remote address to DMA to
 *  @param[in] from_addr
 *      64 bit Local address to DMA from
 *  @param[in] length
 *      length of transfer
 *  @param[in] is_blocking
 *      If 1, waits for the transfer to be complete and returns NULL 
 *      If 0, Initiates transfer and returns transaction handle
 *  @param[in] cfg
 *      Configuration for write operation
 *  @retval
 *      Success - returns transaction handle mpm_transport_trans_h 
 *              - or NULL if is_blocking is set
 *  @retval
 *      Failure - MPM_TRANSPORT_TRANS_FAIL
 *
 *  @pre  call to mpm_transport_open is needed before calling this routine
 *  @post
 */
mpm_transport_trans_h mpm_transport_put_initiate64(mpm_transport_h h,
	uint64_t to_addr, uint64_t from_addr, uint32_t length, bool is_blocking,
	mpm_transport_write_t *cfg);

/**
 *  @b Description: Function mpm_transport_phys_map
 *  @n Creates a mmap for the memory location and returns physical address
 *
 *
 *  @param[in] h
 *      mpm transport handle
 *  @param[in] addr
 *      Address to mmap
 *  @param[in] length
 *      length of mmap
 *  @param[in] cfg
 *      Configuration for the mmap operation
 *  @retval
 *      mmaped Physical address
 *  @retval
 *      Failure - returns 0
 *
 *  @pre  call to mpm_transport_open is needed before calling this routine
 *  @post
 */
uint32_t mpm_transport_phys_map(mpm_transport_h h, uint32_t addr,
	uint32_t length, mpm_transport_mmap_t *cfg);

/**
 *  @b Description: Function mpm_transport_phys_map64
 *  @n Creates a mmap for the memory location and returns physical address
 *
 *
 *  @param[in] h
 *      mpm transport handle
 *  @param[in] addr
 *      64 bit address to mmap
 *  @param[in] length
 *      length of mmap
 *  @param[in] cfg
 *      Configuration for the mmap operation
 *  @retval
 *      mmaped Physical address
 *  @retval
 *      Failure - returns 0
 *
 *  @pre  call to mpm_transport_open is needed before calling this routine
 *  @post
 */
uint32_t mpm_transport_phys_map64(mpm_transport_h h, uint64_t addr, uint32_t length, mpm_transport_mmap_t *cfg);

/**
 *  @b Description: Function mpm_transport_get_event_fd
 *  @n Returns the file descriptor to be used with read/select
 *     to wait on interrupt events
 *
 *  @param[in] transport_handle
 *      Transport handle
 *  @param[in] event_bitmap
 *             bit map of events. currently ignored. for future use.
 *  @retval
 *      file descriptor : positive integer
 *  @retval
 *      Failure - returns value less than 0 (negative integer)
 *  @pre  call to mpm_transport_open is needed before calling this routine
 *  @post
 *
 */
int mpm_transport_get_event_fd(mpm_transport_h transport_handle, uint32_t event_bitmap);

/**
 *  @b Description: Function mpm_transport_read_and_ack_interrupt_event
 *  @n Routine read and ack the interrupt event occurred with the peripheral
 *
 *
 *  @param[in]  transport_handle       Transport handle
 *  @param[in]  event_bitmap           Bitmap of events to ack
 *  @param[out] reported_event_bitmap  reported bitmap of events triggering interrupt
 *
 *  @retval
 *      Success - returns 0
 *  @retval
 *      Failure - returns value less than 0 (negative integer)
 *
 *  @pre  call to mpm_transport_open is needed before calling this routine
 *  @post
 */
int mpm_transport_read_and_ack_interrupt_event (mpm_transport_h transport_handle,
		uint32_t event_bitmap, uint32_t *reported_event_bitmap);

/**
 *  @b Description: Function mpm_transport_wait_for_interrupt_event
 *  @n Routine waits for the device interrupt to occur. Blocking call:
 *     sleep till either interrupt occurs or wait time expires
 *     ack the interrupt with the peripheral and returns event bitmap
 *
 *  @param[in] transport_handle  Transport handle
 *  @param[in] wait_time               Time out
 *  @param[in] event_bitmap
 *             bit map of events. Currently ignored, for future use.
 *  @param[out] reported_event_bitmap
 *              reported bitmap of events triggering interrupt
 *  @retval
 *      Success - returns 0
 *  @retval
 *      Failure - returns value less than 0 (negative integer)
 *
 *  @pre  call to mpm_transport_open is needed before calling this routine
 *  @post
 */
int mpm_transport_wait_for_interrupt_event(mpm_transport_h transport_handle,
		uint32_t event_bitmap, struct timeval *wait_time, uint32_t *reported_event_bitmap );

/**
 *  @b Description: Function mpm_transport_generate_interrupt_event
 *  @n Generate interrupt event specified by the bitmap
 *
 *
 *  @param[in] transport_handle  Transport handle
 *  @param[in] event_bitmap      Bitmap of events triggering interrupt
 *
 *  @retval
 *      Success - returns 0
 *  @retval
 *      Failure - returns value less than 0 (negative integer)
 *
 *  @pre  call to mpm_transport_open is needed before calling this routine
 *  @post
 */
int mpm_transport_generate_interrupt_event (mpm_transport_h transport_handle,
		uint32_t event_bitmap);

#endif
