/*
 * Copyright (C) 2013-2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <malloc.h>
#include <errno.h>
#include <dirent.h>

#include "mpm_transport_sharedmem.h"
#include <syslog.h>

#include "uio_module_drv.h"

#define MAX_DEVICE_NAME_LEN 32
#define MAX_FILE_NAME_LENGTH 256
#define MAX_PARAM_VAL_LENGTH 32
#define HEXA_DECIMAL_BASE 16

#define SYSFS_ENTRY_PREFIX "/sys/class/misc"

static int
mpm_transport_get_mem_details (mpm_transport_cfg_t *sp,
	uint32_t laddr, uint32_t size, uint32_t *base_address, uint32_t *offset,
	int *index)
{
	int i;

	for (i = 0; i < sp->num_mmap; i++) {

		if (sp->mmap[i].local_addr <= laddr &&
			sp->mmap[i].local_addr + sp->mmap[i].length >= laddr + size) {
			*offset = laddr - sp->mmap[i].local_addr;
			*base_address = sp->mmap[i].local_addr;
			*index = i;
			return 0;
		}
	}

	for (i = 0; i < sp->num_mmap; i++) {
		if (sp->mmap[i].global_addr <= laddr &&
			sp->mmap[i].global_addr + sp->mmap[i].length >= laddr + size) {
			*offset = laddr-sp->mmap[i].global_addr;
			*base_address = sp->mmap[i].global_addr;
			*index = i;
			return 0;
		}
	}

	mpm_printf(1, "can't find global address from local address 0x%x size %d in cfg for %s\n",
		laddr, size, sp->name);
	return -1;
}

/* Function finds the mapping index based on the starting address */
static int mpm_transport_find_map_index(
	mpm_transport_sharedmem_t *td, int fd_index, uint64_t global_start_addr,
	uint32_t length, int start_block)
{

	int k;
#ifdef DEBUG
	mpm_printf(1, "DEBUG: fd_index %d, global_start_addr 0x%x, length 0x%x num_mem_blocks %d start_block %d\n",
		fd_index, (uint32_t)global_start_addr, length,
		td->fd_mem.fd_mem_entry[fd_index].num_mem_blocks, start_block);
#endif
	/* Find the mem_block region that is within the range of configured region */

	for (k = start_block; k < td->fd_mem.fd_mem_entry[fd_index].num_mem_blocks; k++) {
#ifdef DEBUG
		mpm_printf(1, "DEBUG: k %d, base addr 0x%x: length 0x%x\n",
			k, td->fd_mem.fd_mem_entry[fd_index].mem_block[k].base,
			td->fd_mem.fd_mem_entry[fd_index].mem_block[k].length);
#endif
		if((global_start_addr <= td->fd_mem.fd_mem_entry[fd_index].mem_block[k].base)
			&&
			((global_start_addr + length)
			  >= td->fd_mem.fd_mem_entry[fd_index].mem_block[k].base)) {
#ifdef DEBUG
			mpm_printf(1, "DEBUG: Success map_index %d, global_start_addr 0x%x: fd_index %d\n",
				k, (uint32_t)global_start_addr, fd_index);
#endif
			return k;
		}
	}
	return -1;

}
static int update_mem_entries(mpm_transport_cfg_t *sp,
	mpm_transport_sharedmem_t *td, int section_index,
	int fd_index, int map_index)
{
	int map_index2;	
#ifdef DEBUG
	mpm_printf(1, "DEBUG: section_index:%d, map_index %d,  original config: Global addr: 0x%x, length 0x%x, : device Base 0x%x, length 0x%x\n",
		section_index, map_index, (uint32_t)sp->mmap[section_index].global_addr,
		(uint32_t)sp->mmap[section_index].length,
		(uint32_t)td->fd_mem.fd_mem_entry[fd_index].mem_block[map_index].base,
		(uint32_t)td->fd_mem.fd_mem_entry[fd_index].mem_block[map_index].length);
#endif

	map_index2 = map_index;
	do {
		/* Also check for additional sections within the range
		   and add as additional entries */
		/* Now find the matching map section */
		map_index2 = mpm_transport_find_map_index(td, fd_index,
			sp->mmap[section_index].global_addr,
			sp->mmap[section_index].length, map_index2+1);
		if (map_index2 < 0)
			break;
		if (sp->num_mmap >= (MPM_MAX_MMAPS-1)) {
			mpm_printf(1, "num mmaps exceeded%d\n");
			return MPM_TRANSPORT_SHM_OPEN_ERR_BLOCKS_EXCEED_MAX;
		
		}
		sp->mmap[sp->num_mmap].local_addr =  sp->mmap[section_index].local_addr 
			+ (td->fd_mem.fd_mem_entry[fd_index].mem_block[map_index2].base
			   - sp->mmap[section_index].global_addr
			  );
		if ((td->fd_mem.fd_mem_entry[fd_index].mem_block[map_index2].base
		      + td->fd_mem.fd_mem_entry[fd_index].mem_block[map_index2].length)
		    > (sp->mmap[section_index].global_addr + sp->mmap[section_index].length)) {
			sp->mmap[sp->num_mmap].length
				= (sp->mmap[section_index].global_addr + sp->mmap[section_index].length)
				   - td->fd_mem.fd_mem_entry[fd_index].mem_block[map_index2].base;
		} else {
			sp->mmap[sp->num_mmap].length
				= td->fd_mem.fd_mem_entry[fd_index].mem_block[map_index2].length;
		}

		sp->mmap[sp->num_mmap].global_addr
			=  td->fd_mem.fd_mem_entry[fd_index].mem_block[map_index2].base;
		strcpy(sp->mmap[sp->num_mmap].devicename, sp->mmap[section_index].devicename);
#ifdef DEBUG
		mpm_printf(1, "DEBUG: local_addr 0x%x global addr 0x%x length 0x%x \n",
			(uint32_t)sp->mmap[sp->num_mmap].local_addr,
			(uint32_t)sp->mmap[sp->num_mmap].global_addr,
			(uint32_t)sp->mmap[sp->num_mmap].length);
#endif
		sp->num_mmap++;
#ifdef DEBUG
		mpm_printf(1, "DEBUG: Added one more entry %d: \n", sp->num_mmap);
#endif
	} while (1);

	/* Need to also update the configured addresses to restrict to what
	   is available in sysfs or device tree entry */
	/* Override local address with update based on global address */
	sp->mmap[section_index].local_addr = sp->mmap[section_index].local_addr 
		+ (td->fd_mem.fd_mem_entry[fd_index].mem_block[map_index].base
		   - sp->mmap[section_index].global_addr);
	if ((td->fd_mem.fd_mem_entry[fd_index].mem_block[map_index].base
	      + td->fd_mem.fd_mem_entry[fd_index].mem_block[map_index].length)
	    > (sp->mmap[section_index].global_addr + sp->mmap[section_index].length)) {
		sp->mmap[section_index].length
			= (sp->mmap[section_index].global_addr + sp->mmap[section_index].length)
			   - td->fd_mem.fd_mem_entry[fd_index].mem_block[map_index].base;
	} else {
		/* Override length to length in sysfs or device tree entry */
		sp->mmap[section_index].length
			= td->fd_mem.fd_mem_entry[fd_index].mem_block[map_index].length;
	}
	/* Override global address with available address */
	sp->mmap[section_index].global_addr
		= td->fd_mem.fd_mem_entry[fd_index].mem_block[map_index].base;

#ifdef DEBUG
	mpm_printf(1, "DEBUG: Updating: local_addr 0x%x global addr 0x%x length 0x%x \n",
		(uint32_t)sp->mmap[section_index].local_addr,
		(uint32_t)sp->mmap[section_index].global_addr,
		(uint32_t)sp->mmap[section_index].length);
#endif
	return 0;
}

/* Find the device_tree _name that matches the device_name */
int get_dev_tree_name(char *dev_name, char *dev_tree_name)  {

	DIR *dir = 0;
	struct dirent *entry = 0;
	char *ret_ptr;

	dir = opendir("/proc/device-tree/soc");
	if (!dir) {
		perror("readdir /proc/device-tree/soc");
		return -1;
	}

	while ((entry = readdir(dir)) != NULL) {
		ret_ptr = strstr (entry->d_name, dev_name);
		if ((ret_ptr == NULL) &&  (ret_ptr != entry->d_name)) {
			continue;
		}
		strcpy(dev_tree_name, entry->d_name);
		closedir(dir);
		return 0;
	}
	if (dir) closedir(dir);
	return -1;
}

int mpm_transport_sharedmem_open(mpm_transport_cfg_t *sp, mpm_transport_open_t *ocfg)
{
	int i, j, k;
	mpm_transport_sharedmem_t *td;
	int fd;
	char filename[MAX_FILE_NAME_LENGTH];
	char dev_tree_name[MAX_FILE_NAME_LENGTH];
	char hexstring[MAX_PARAM_VAL_LENGTH];
	off_t  fileSize;
	struct stat statbuf;
	char   *scratch;
	char   *devname;
	int map_index;
	int ret;

	td = calloc(1, sizeof(mpm_transport_sharedmem_t));
	if (!td) {
		mpm_printf(1, "can't allocate memory for transport data \n");
		return MPM_TRANSPORT_SHM_OPEN_ERR_CALLOC;
	}
	td->fd_mem.num_fds = 0;
	for (i = 0; i < sp->num_mmap; i++) {
		/* check if fd for same device name is already opened */
		for (j = 0; j < td->fd_mem.num_fds; j++) {
#ifdef DEBUG
			mpm_printf(1, "DEBUG: string compare i:%d, j:%d, %s : %s\n",
				i, j, sp->mmap[i].devicename,
				td->fd_mem.fd_mem_entry[j].devicename);
#endif
			if (!strncmp(sp->mmap[i].devicename,
				td->fd_mem.fd_mem_entry[j].devicename,
				MAX_DEVICE_NAME_LEN)) {
				td->fd_index[i] = j;
				map_index = 
					mpm_transport_find_map_index(
						td, j,
						sp->mmap[i].global_addr,
						sp->mmap[i].length, 0);
				if ( map_index < 0 ) {
					mpm_printf(1, "Could not find map index for address %llx error map_index %d\n",
						sp->mmap[i].global_addr, map_index);
					return MPM_TRANSPORT_SHM_OPEN_ERR_MAP_INDEX;

				}
				if (((uint32_t)sp->mmap[i].global_addr
				     != (uint32_t)td->fd_mem.fd_mem_entry[j].mem_block[map_index].base)
				    || ((uint32_t)sp->mmap[i].length != (uint32_t)td->fd_mem.fd_mem_entry[j].mem_block[map_index].length)) {
					ret = update_mem_entries(sp, td, i, j,  map_index); 
					if (ret < 0)
						return ret;
				}
				td->map_index[i] = map_index;
				break;
			}
		}
		if (j < td->fd_mem.num_fds) {
			continue;
		}
		td->fd_mem.fd_mem_entry[td->fd_mem.num_fds].fd =
			open(sp->mmap[i].devicename, (O_RDWR | O_SYNC));
		if (td->fd_mem.fd_mem_entry[td->fd_mem.num_fds].fd < 0) {
			mpm_printf(1, "Failed to open %s :Error (%s)",
				sp->mmap[i].devicename, strerror(errno));
			free(td);
			return MPM_TRANSPORT_SHM_OPEN_ERR_DEV_OPEN;
		}
		td->fd_index[i] = td->fd_mem.num_fds;
		devname = strrchr(sp->mmap[i].devicename, '/')+1;

		k = 0;
		/* Check first if there are sysfs entries for the memory */
		snprintf(filename, MAX_FILE_NAME_LENGTH,
			SYSFS_ENTRY_PREFIX"/%s/memory%d/addr", devname, k);

		if ((fd = open (filename, O_RDONLY)) != -1) {

			/* Found sysfs entry : Read memory information */
			do {
				ret = read(fd, hexstring, MAX_PARAM_VAL_LENGTH);
				if ( ret <= 0 ) {
					mpm_printf (1, "Failed to read %d bytes from %s: ret %d\n",
						(int) MAX_PARAM_VAL_LENGTH, filename, ret);
					return MPM_TRANSPORT_SHM_OPEN_ERR_READ;
				}
				close(fd);
				/* And populate mem_block base information */
				td->fd_mem.fd_mem_entry[td->fd_mem.num_fds].mem_block[k].base
					= (uint32_t)strtoul(hexstring, NULL, HEXA_DECIMAL_BASE);
#ifdef DEBUG
				mpm_printf(1, "Recording addr 0x%x, ",
					td->fd_mem.fd_mem_entry[td->fd_mem.num_fds].mem_block[k].base);
#endif
				snprintf(filename, MAX_FILE_NAME_LENGTH,
					SYSFS_ENTRY_PREFIX"/%s/memory%d/size",
					devname, k);
				fd = open(filename, O_RDONLY);
				if ( fd == -1 ) {
					mpm_printf(1, "\nFailed to open \"%s\" err=%s\n",
						filename, strerror(errno));
					return MPM_TRANSPORT_SHM_OPEN_ERR_MEM_FILE_OPEN;
				}

				ret = read(fd, hexstring, MAX_PARAM_VAL_LENGTH);
				if ( ret <= 0 ) {
					mpm_printf (1, "\nFailed to read %d bytes from %s: ret %d\n",
						(int) MAX_PARAM_VAL_LENGTH, filename, ret);
					return MPM_TRANSPORT_SHM_OPEN_ERR_READ;
				}
				close(fd);
				td->fd_mem.fd_mem_entry[td->fd_mem.num_fds].mem_block[k].length
					= (uint32_t)strtoul(hexstring, NULL, HEXA_DECIMAL_BASE);
#ifdef DEBUG
				mpm_printf(1, "Recording length 0x%x\n",
					td->fd_mem.fd_mem_entry[td->fd_mem.num_fds].mem_block[k].length);
#endif
				k++;
				if ( k >= MPM_MAX_MEM_ENTRIES) {
					mpm_printf(1, "\nNumber of mem entries: %d exceed max allowed %d\n",
						k, MPM_MAX_MEM_ENTRIES);
					return MPM_TRANSPORT_SHM_OPEN_ERR_BLOCKS_EXCEED_MAX;
				}
				snprintf(filename, MAX_FILE_NAME_LENGTH,
					SYSFS_ENTRY_PREFIX"/%s/memory%d/addr",
					devname, k);
			} while ((fd = open(filename, O_RDONLY)) != -1 );
			td->fd_mem.fd_mem_entry[td->fd_mem.num_fds].num_mem_blocks = k;
				
		} else {
#ifdef DEBUG
			mpm_printf(1, "\nFailed to open \"%s\" err=%s\n",
					filename, strerror(errno));
#endif
			ret = get_dev_tree_name(devname, dev_tree_name);
			if (ret) {
				mpm_printf(1, "Failed to find dev tree entry for \"%s\" err=%d\n",
					filename, ret);
				return MPM_TRANSPORT_SHM_OPEN_ERR_MEM_FILE_OPEN;
			}
			/* Check device tree and record the index of the mmap */
			snprintf(filename, MAX_FILE_NAME_LENGTH, "/proc/device-tree/soc/%s/reg", dev_tree_name);

			if ((fd = open(filename, O_RDONLY)) == -1)
			{
				mpm_printf(1, "Failed to open \"%s\" err=%s\n",
					filename, strerror(errno));
				return MPM_TRANSPORT_SHM_OPEN_ERR_MEM_FILE_OPEN;
			}
			if (fstat (fd, &statbuf) == -1)
			{
				mpm_printf (1, "Failed to find size of %s (%s)\n",
					filename, strerror(errno));
				return MPM_TRANSPORT_SHM_OPEN_ERR_FIND_FILE_SIZE;
			}
			fileSize = statbuf.st_size;
			td->fd_mem.fd_mem_entry[td->fd_mem.num_fds].num_mem_blocks =
				fileSize / sizeof(mem_block_t);
			scratch =
				malloc (sizeof(mem_block_t) * td->fd_mem.fd_mem_entry[td->fd_mem.num_fds].num_mem_blocks);
			if (!td->fd_mem.fd_mem_entry[td->fd_mem.num_fds].num_mem_blocks  || !scratch)
			{
				mpm_printf (1, "Failed to malloc size blocks for %d\n",
					td->fd_mem.fd_mem_entry[td->fd_mem.num_fds].num_mem_blocks);
				return MPM_TRANSPORT_SHM_OPEN_ERR_MALLOC;
			}
			if ( read (fd, scratch, fileSize) != fileSize)
			{
				mpm_printf (1, "Failed to read %d bytes from %s\n",
							 (int)fileSize, filename);
				return MPM_TRANSPORT_SHM_OPEN_ERR_READ;
			}
			close (fd);
			if ( td->fd_mem.fd_mem_entry[td->fd_mem.num_fds].num_mem_blocks > MPM_MAX_MEM_ENTRIES) {
				mpm_printf (1, "Number of blocks %d exceeds max %d\n",
					td->fd_mem.fd_mem_entry[td->fd_mem.num_fds].num_mem_blocks, MPM_MAX_MEM_ENTRIES );
				return MPM_TRANSPORT_SHM_OPEN_ERR_BLOCKS_EXCEED_MAX;
			}
			for (k = 0; k < td->fd_mem.fd_mem_entry[td->fd_mem.num_fds].num_mem_blocks; k++)
			{
				 /* Swizzle from big endian in proc */
				 td->fd_mem.fd_mem_entry[td->fd_mem.num_fds].mem_block[k].base =
					 (scratch[k*8 + 0] << 24) |
					 (scratch[k*8 + 1] << 16) |
					 (scratch[k*8 + 2] <<  8) |
					 (scratch[k*8 + 3]      );
				 td->fd_mem.fd_mem_entry[td->fd_mem.num_fds].mem_block[k].length =
					 (scratch[k*8 + 4] << 24) |
					 (scratch[k*8 + 5] << 16) |
					 (scratch[k*8 + 6] <<  8) |
					 (scratch[k*8 + 7]      );
#ifdef DEBUG
				mpm_printf(1, " DEBUG: Index %d: Base %x, length %x\n", k,
					td->fd_mem.fd_mem_entry[td->fd_mem.num_fds].mem_block[k].base,
					td->fd_mem.fd_mem_entry[td->fd_mem.num_fds].mem_block[k].length);
#endif
			}
			free (scratch);
		}
		/* Now find the matching map section */
		map_index = mpm_transport_find_map_index(td, td->fd_mem.num_fds,
				sp->mmap[i].global_addr, sp->mmap[i].length, 0);
		if ( map_index < 0 ) {
			mpm_printf(1, "Could not find map index for address 0x%llx error: map_index %d\n",
				sp->mmap[i].global_addr, map_index);
			return MPM_TRANSPORT_SHM_OPEN_ERR_MAP_INDEX;
		}
		if ((sp->mmap[i].global_addr
		     != (td->fd_mem.fd_mem_entry[td->fd_mem.num_fds].mem_block[map_index].base))
		    || (sp->mmap[i].length
		        != td->fd_mem.fd_mem_entry[td->fd_mem.num_fds].mem_block[map_index].length)) {
			ret = update_mem_entries(sp, td, i, td->fd_mem.num_fds, map_index);
			if (ret < 0)
				return ret;
		}

		td->map_index[i] = map_index;
		td->fd_mem.fd_mem_entry[td->fd_mem.num_fds].devicename = 
			sp->mmap[i].devicename;

		td->fd_mem.num_fds++;
		if ( td->fd_mem.num_fds >= MPM_MAX_NUM_FDS ) {
			mpm_printf (1, "Number of fds %d exceeds max fds %d\n",
				td->fd_mem.num_fds, MPM_MAX_NUM_FDS );
			return MPM_TRANSPORT_SHM_OPEN_ERR_EXCEED_NUM_FDS;
		}
	}

	sp->td = (void *)td;
	return 0;

}

void *mpm_transport_sharedmem_mmap(mpm_transport_cfg_t *sp, uint32_t addr, uint32_t length, mpm_transport_mmap_t *mcfg)
{
	int index;
	int user_index;
	uint32_t offset, base_address, base_correction;
	int pg_offset, mmap_length, min_len;
	int page_size = getpagesize();
	mpm_transport_sharedmem_t *td = (mpm_transport_sharedmem_t *) sp->td;

	/* Find a free mmap index to use */
	for (user_index = 0; user_index < MPM_MAX_USER_MMAPS; user_index++) {
		if (!td->mmap_user[user_index].addr) {
			break;
		}
	}

	if (user_index >= MPM_MAX_USER_MMAPS) {
		mpm_printf(1, "exceeded maximum user mmap limits (%d)\n",
				MPM_MAX_USER_MMAPS);
		return MAP_FAILED;
	}

	if(mpm_transport_get_mem_details(sp, addr, length,
			&base_address, &offset, &index)) {
		mpm_printf(1, "can't find index and offset for address 0x%x\n", addr);
		return MAP_FAILED;
	}
	base_correction = (base_address & (page_size - 1));
	pg_offset = offset & (~((page_size<< UIO_MODULE_DRV_MAP_OFFSET_SHIFT) - 1));

	mmap_length = length + offset - pg_offset + base_correction;

	td->mmap_user[user_index].addr = mmap(NULL, mmap_length,
		mcfg->mmap_prot, mcfg->mmap_flags,
		td->fd_mem.fd_mem_entry[td->fd_index[index]].fd,
		((pg_offset) + (td->map_index[index] * page_size)));
	if (td->mmap_user[user_index].addr == MAP_FAILED) {
		mpm_printf(1, "can't mmap for the address 0x%x with length"
			" 0x%x (err: %s)\n",
 			addr, length, strerror(errno));
		return MAP_FAILED;
	}
	td->mmap_user[user_index].size = mmap_length;
	td->mmap_user[user_index].addr_usr = td->mmap_user[user_index].addr
		+ (offset - pg_offset) + base_correction;

	return (td->mmap_user[user_index].addr_usr);
}

int mpm_transport_sharedmem_munmap(mpm_transport_cfg_t *sp, void *va, uint32_t length)
{
	int user_index;
	mpm_transport_sharedmem_t *td = (mpm_transport_sharedmem_t *) sp->td;

	for (user_index = 0; user_index < MPM_MAX_USER_MMAPS; user_index++) {
		if (td->mmap_user[user_index].addr_usr == va) {
			break;
		}
	}

	if (user_index >= MPM_MAX_USER_MMAPS) {
		mpm_printf(1, "can't find mapping to unmap\n",
 			MPM_MAX_USER_MMAPS);
		return -1;
	}

	if (length > td->mmap_user[user_index].size) {
		mpm_printf(1, "invalid length %d, it must be <= %d\n",
 			length, td->mmap_user[user_index].size);
	}

	munmap(td->mmap_user[user_index].addr, td->mmap_user[user_index].size);

	td->mmap_user[user_index].addr = 0;
	td->mmap_user[user_index].size = 0;
	return 0;
}

static void *mpm_transport_sharedmem_rw_map (mpm_transport_cfg_t *sp, uint32_t addr, uint32_t length, int *index)
{
	uint32_t offset, base_address, base_correction;
	int mmap_length, min_len;
	int page_size = getpagesize();
	int ret_val;
	mpm_transport_sharedmem_t *td = (mpm_transport_sharedmem_t *) sp->td;

	if(mpm_transport_get_mem_details(sp, addr, length, &base_address,
			&offset,  index)) {
		mpm_printf(1, "can't find global address for local address 0x%x\n", addr);
		return 0;
	}

	base_correction = (base_address & (page_size - 1));
	mmap_length = sp->mmap[*index].length + base_correction;

	mpm_printf(1, "Debug: Base Address 0x%x, index %d,  offset 0x%x, base_correction 0x%x, mmap_length 0x%x \n",
		base_address, *index, offset, base_correction, mmap_length);
	if (!td->mmap_rw[*index].addr) {
		td->mmap_rw[*index].addr = mmap(NULL, mmap_length, (PROT_READ|PROT_WRITE),
				MAP_SHARED, td->fd_mem.fd_mem_entry[td->fd_index[*index]].fd,
				(td->map_index[*index] * page_size));
		if (td->mmap_rw[*index].addr == MAP_FAILED) {
			td->mmap_rw[*index].addr = 0;
			mpm_printf(1, "can't mmap for the address 0x%x with length 0x%x (err: %s)\n",
	 			addr, length, strerror(errno));
			return 0;
		}

		td->mmap_rw[*index].size = sp->mmap[*index].length;
		ret_val = pthread_mutex_init(&td->mutex_rw[*index], NULL);
		if (ret_val != 0) {
			mpm_printf(1, " Unable to init mutex");
			return 0;
		}
	}
	td->mmap_rw[*index].addr_usr = td->mmap_rw[*index].addr
		+ offset + base_correction;

	return (td->mmap_rw[*index].addr_usr);
}

int mpm_transport_sharedmem_read(mpm_transport_cfg_t *sp, uint32_t addr, uint32_t length, char *buf, mpm_transport_read_t *rcfg)
{
	int index;
	void *mmap_addr;
	mpm_transport_sharedmem_t *td = (mpm_transport_sharedmem_t *) sp->td;

	mmap_addr = mpm_transport_sharedmem_rw_map (sp, addr, length, &index);

	if (!mmap_addr) {
		mpm_printf(1, "can't map for address 0x%x\n", addr);
		return -1;
	}
	pthread_mutex_lock(&td->mutex_rw[index]);
	memcpy(buf, mmap_addr, length);
	pthread_mutex_unlock(&td->mutex_rw[index]);
	return 0;
}

int mpm_transport_sharedmem_write(mpm_transport_cfg_t *sp, uint32_t addr, uint32_t length, char *buf, mpm_transport_write_t *cfg)
{
	int index;
	void *mmap_addr;
	mpm_transport_sharedmem_t *td = (mpm_transport_sharedmem_t *) sp->td;

	mmap_addr = mpm_transport_sharedmem_rw_map (sp, addr, length, &index);
	if (!mmap_addr) {
		mpm_printf(1, "can't map for address 0x%x\n", addr);
		return -1;
	}
	pthread_mutex_lock(&td->mutex_rw[index]);
	memcpy(mmap_addr, buf, length);
	pthread_mutex_unlock(&td->mutex_rw[index]);
	return 0;
}

void mpm_transport_sharedmem_close(mpm_transport_cfg_t *sp)
{
	int i, j;
	mpm_transport_sharedmem_t *td = (mpm_transport_sharedmem_t *) sp->td;

	if (!td) {
		return;
	}

	for (i = 0; i < MPM_MAX_USER_MMAPS; i++) {
		if(td->mmap_user[i].addr) {
			munmap(td->mmap_user[i].addr, td->mmap_user[i].size);
			td->mmap_user[i].addr = 0;
		}
	}
	for (i = 0; i < MPM_MAX_MMAPS; i++) {
		if(td->mmap_rw[i].addr) {
			munmap(td->mmap_rw[i].addr, td->mmap_rw[i].size);
			pthread_mutex_destroy(&td->mutex_rw[i]);
			td->mmap_rw[i].addr = 0;
		}
	}
	for (i = 0; i < td->fd_mem.num_fds; i++) {
		if (td->fd_mem.fd_mem_entry[i].fd) {
			fsync(td->fd_mem.fd_mem_entry[i].fd);
			close(td->fd_mem.fd_mem_entry[i].fd);
			td->fd_mem.fd_mem_entry[i].fd = 0;
		}
	}

	if (td) free(td);
}
