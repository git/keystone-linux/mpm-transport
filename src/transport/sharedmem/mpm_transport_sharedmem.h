/*
 * Copyright (C) 2013-2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef __MPM_TRANSPORT_SHAREDMEM_H__
#define __MPM_TRANSPORT_SHAREDMEM_H__

#include <pthread.h>
#include "mpm_transport_cfg.h"

#define MPM_MAX_USER_MMAPS	(16)
#define MPM_MAX_MEM_ENTRIES	4
#define MPM_MAX_NUM_FDS 8

#define MPM_TRANSPORT_SHM_OPEN_ERR_CALLOC    		-100
#define MPM_TRANSPORT_SHM_OPEN_ERR_DEV_OPEN   		-101
#define MPM_TRANSPORT_SHM_OPEN_ERR_MEM_FILE_OPEN	-102
#define MPM_TRANSPORT_SHM_OPEN_ERR_FIND_FILE_SIZE	-103
#define MPM_TRANSPORT_SHM_OPEN_ERR_MALLOC		-104
#define MPM_TRANSPORT_SHM_OPEN_ERR_READ			-105
#define MPM_TRANSPORT_SHM_OPEN_ERR_BLOCKS_EXCEED_MAX	-106
#define MPM_TRANSPORT_SHM_OPEN_ERR_MAP_INDEX		-107
#define MPM_TRANSPORT_SHM_OPEN_ERR_EXCEED_NUM_FDS	-108

typedef struct {
	uint32_t base;
	uint32_t length;
} mem_block_t;

typedef struct {
	int	fd;
	char	*devicename;
	int     num_mem_blocks;
	mem_block_t mem_block[MPM_MAX_MEM_ENTRIES];
} fd_mem_entry_t;

typedef struct {
	int 	num_fds;
	fd_mem_entry_t fd_mem_entry[MPM_MAX_NUM_FDS];
} fd_mem_t;

typedef struct mpm_transport_sharedmem_tag {
	int 	fd_index[MPM_MAX_MMAPS];
	int	map_index[MPM_MAX_MMAPS];
	fd_mem_t fd_mem;
	mmap_data_t mmap_rw[MPM_MAX_MMAPS];
	pthread_mutex_t mutex_rw[MPM_MAX_MMAPS];
	mmap_data_t mmap_user[MPM_MAX_USER_MMAPS];
} mpm_transport_sharedmem_t;

int mpm_transport_sharedmem_open(mpm_transport_cfg_t *sp, mpm_transport_open_t *ocfg);

int mpm_transport_sharedmem_read(mpm_transport_cfg_t *sp, uint32_t addr, uint32_t length, char *buf, mpm_transport_read_t *rcfg);

int mpm_transport_sharedmem_write(mpm_transport_cfg_t *sp, uint32_t addr, uint32_t length, char *buf, mpm_transport_write_t *wcfg);

void *mpm_transport_sharedmem_mmap(mpm_transport_cfg_t *sp, uint32_t addr, uint32_t length, mpm_transport_mmap_t *mcfg);

int mpm_transport_sharedmem_munmap(mpm_transport_cfg_t *sp, void *va, uint32_t length);

void mpm_transport_sharedmem_close(mpm_transport_cfg_t *sp);

#endif
