/*
 * Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef __MPM_TRANSPORT_QMSS_H__
#define __MPM_TRANSPORT_QMSS_H__

#include "mpm_transport_qmss_interface.h"
#include "mpm_transport_cfg.h"

/* This structure defines the transport specific data for QMSS */
typedef struct mpm_transport_qmss_tag {

	/* CPPI handle for DMA transactions */
	Cppi_Handle cppiHnd;

	/* CPPI RX Channel */
	Cppi_ChHnd rxChHnd;

	/* CPPI TX Channel */
	Cppi_ChHnd txChHnd;

	/* CPPI Flow */
	Cppi_FlowHnd flowHnd;

	/* Name for the TX free queue handle
		To be requested from RM */
	char txFqName[MPM_MAX_NAME_LENGTH];

	/* Free queue handle, to pop descriptors from
		Descriptors are used for sending */
	Qmss_QueueHnd txFqHnd;

	/* Transmit handle, to send descriptors to remote
		flows */
	Qmss_QueueHnd txqHnd;

	/* Name for the RX free queue handle
		To be requested from RM */
	char rxFqName[MPM_MAX_NAME_LENGTH];

	/* Free queue handle, to pop descriptors from
		Descriptors are used for receiving */
	Qmss_QueueHnd rxFqHnd;

	/* Receive handle, to pop descriptors from
		Will read payload if necessary */
	Qmss_QueueHnd rxqHnd;

	/* File descriptor for receive queue
		Using QPEND queues - can read this for interrupts when packet arrives */
	int rxFd;

	/* Name for the memory region info opened in JSON */
	char regionName[MPM_MAX_NAME_LENGTH];

	/* QMSS memory region information */
	Qmss_Result memReg;

	/* Maximum number of descriptors to initialize QMSS with */
	int maxDescNum;

	/* Size of tx buff */
	int txBufSize;

	/* Size of tx descriptor */
	int txDescSize;

	/* CPDMA FIFO write depth */
	uint8_t cppiWriteFifoDepth;

	/* CPDMA timeout count */
	uint16_t cppiTimeoutCount;

	/* Flag for initializing QMSS */
	int qmssInit;

} mpm_transport_qmss_t;

int mpm_transport_qmss_init(mpm_transport_cfg_t *sp, mpm_transport_open_t *ocfg);
Qmss_QueueHnd mpm_transport_qmss_init_desc(mpm_transport_cfg_t *sp, char *name, mpm_transport_qmss_desc_info_t *params);
int mpm_transport_qmss_insert_mem_region(mpm_transport_cfg_t *sp, mpm_transport_qmss_mem_region_info_t *params);
int mpm_transport_qmss_open(mpm_transport_cfg_t *sp, mpm_transport_open_t *ocfg);
void mpm_transport_qmss_close(mpm_transport_cfg_t *sp);
int mpm_transport_qmss_packet_send(mpm_transport_cfg_t *sp, char **buf, int *len, mpm_transport_packet_addr_t *addr_info, mpm_transport_send_t *cfg);
int mpm_transport_qmss_packet_recv(mpm_transport_cfg_t *sp, char **buf, int *len, mpm_transport_recv_t *cfg);

#endif
