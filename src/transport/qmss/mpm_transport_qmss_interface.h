/*
 * Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef __MPM_TRANSPORT_QMSS_INTERFACE_H__
#define __MPM_TRANSPORT_QMSS_INTERFACE_H__

#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include <pthread.h>

/* CPPI LLD include */
#include <ti/drv/cppi/cppi_drv.h>
#include <ti/drv/cppi/cppi_desc.h>

/* QMSS LLD include */
#include <ti/drv/qmss/qmss_drv.h>

#include "uioutils.h"
#include "mpm_transport_cfg.h"

/* Physical address of the memory pool */
extern off_t fw_mem_start_phy;
/* virtual address of the memory pool */
extern uint8_t *fw_mem_start;

/* Virtual pointers for hardware */
void *fw_qmssCfgVaddr;
void *fw_qmssDataVaddr;
void *fw_srioCfgVaddr;
void *fw_srioSerdesCfgVaddr;

/* Physical address map & size for various subsystems */
#define QMSS_CFG_BASE_ADDR         CSL_QMSS_CFG_BASE
#define QMSS_CFG_BLK_SZ            (0x00100000)
#define QMSS_DATA_BASE_ADDR        CSL_QMSS_DATA_BASE
#define QMSS_DATA_BLK_SZ           (0x00100000)
#define SRIO_CFG_BASE_ADDR         CSL_SRIO_CFG_REGS
#define SRIO_CFG_BLK_SZ            (0x00040000)
#define SRIO_SERDES_CFG_BASE_ADDR  CSL_SRIO_SERDES_CFG_REGS
#define SRIO_SERDES_CFG_BLK_SZ     (0x00002000)

/* Amount of CMA/CMEM needed */
#define FW_QMSS_CMEM_SIZE (1024*1024)

/* Cache line size */
#define CACHE_LINESZ 64

/* Masking macro for QMSS descriptor */
#define QMSS_DESC_PTR(desc) ((uint32_t)desc & 0xFFFFFFF0)

/* This structure defines the init params for descriptors in a queue
	This information is parsed via the JSON file */
typedef struct mpm_transport_qmss_desc_info_tag {

	/* Queue number to open. If qid == -1, it will use next available */
	int qid;

	/* Number of descriptors */
	int numDesc;

	/* Size of buffer to tie to each descriptor.
		If bufSize == 0, assumes you will pass in buffer to use on send/recv */
	int sizeBuf;

} mpm_transport_qmss_desc_info_t;


/* This structure defines the init params for a memory region
	This information is parsed via the JSON file */
typedef struct mpm_transport_qmss_mem_region_info_tag {

	/* Number of descriptors in this memory region */
	int regionDescNum;

	/* size of descriptors */
	int regionDescSize;

	/* region number to use. If regionnum == -1, it will use the next available */
	int regionNum;

	/* Start index of region */
	int regionStartIdx;

} mpm_transport_qmss_mem_region_info_t;

int mpm_transport_open_cppi_qmss_cpdma(Cppi_Handle *cHnd, uint8_t cppiWriteFifoDepth, uint16_t cppiTimeoutCount);
int32_t mpm_transport_init_qm(mpm_transport_cfg_t *sp, int maxDescNum);
int32_t mpm_transport_init_cppi(mpm_transport_cfg_t *sp);
int mpm_transport_qmss_open_qpend_and_fd(const char *basename, Qmss_QueueHnd *rxQueueHnd);
int mpm_transport_qmss_open_infra_triplet(Cppi_Handle cppiHnd,
										  Cppi_ChHnd *pTxChHnd,
										  Cppi_ChHnd *pRxChHnd,
										  Qmss_QueueHnd *pTxQHnd);
#endif