/*
 * Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <malloc.h>
#include <errno.h>

#include "mpm_transport_qmss_interface.h"
#include "mpm_transport_qmss.h"
#include "fw_mem_allocator.h"

int mpm_transport_qmss_init(mpm_transport_cfg_t *sp, mpm_transport_open_t *ocfg)
{
	mpm_transport_qmss_t *td = (mpm_transport_qmss_t *) sp->td;
	off_t pCMemBase = 0;
	if (ocfg->rm_info.rm_client_handle == NULL) {
		mpm_printf(1, "mpm_transport_qmss_init(): RM client handle missing!\n");
		return -1;
	}
	sp->rmClientServiceHandle = (Rm_ServiceHandle *) ocfg->rm_info.rm_client_handle;
	td->qmssInit = ocfg->transport_info.qmss.qmss_init;

	if (fw_memAllocInit(&pCMemBase,
						FW_QMSS_CMEM_SIZE) == fw_FALSE) {
		mpm_printf(1, "mpm_transport_qmss_init(): fw_memAllocInit failed\n");
		return (-1);
	}

	/* Create virtual memory maps */
	/* QMSS CFG Regs */
	if (!fw_qmssCfgVaddr) {
		fw_qmssCfgVaddr = fw_memMap((void*)QMSS_CFG_BASE_ADDR,
											QMSS_CFG_BLK_SZ);
		if (!fw_qmssCfgVaddr)
		{
			mpm_printf(1, "mpm_transport_qmss_init(): Failed to fw_memMap QMSS CFG registers\n");
			return (-1);
		}
	}

	/* QMSS DATA Regs */
	if (!fw_qmssDataVaddr) {
		fw_qmssDataVaddr = fw_memMap((void*)QMSS_DATA_BASE_ADDR,
											QMSS_DATA_BLK_SZ);
		if (!fw_qmssDataVaddr)
		{
			mpm_printf(1, "mpm_transport_qmss_init(): Failed to fw_memMap QMSS DATA registers\n");
			return (-1);
		}
	}

	/* Setup QMSS and initialize CPPI */
	if (td->qmssInit){
		if (mpm_transport_init_qm(sp, td->maxDescNum)) {
			mpm_printf (1, "mpm_transport_qmss_init(): mpm_transport_init_qm returned error, exiting\n");
			return (-1);
		}
	}

	if (mpm_transport_init_cppi(sp)) {
		mpm_printf (1, "initQm: initCppi failed\n");
		return (-1);
	}

	if (mpm_transport_open_cppi_qmss_cpdma(&(td->cppiHnd), td->cppiWriteFifoDepth, td->cppiTimeoutCount)) {
		mpm_printf (1, "mpm_transport_qmss_init(): mpm_transport_open_cppi_qmss_cpdma returned error, exiting\n");
		return (-1);
	}

	return 0;
}

Qmss_QueueHnd mpm_transport_qmss_init_desc(mpm_transport_cfg_t *sp, char *name, mpm_transport_qmss_desc_info_t *params)
{
	mpm_transport_qmss_t *td = (mpm_transport_qmss_t *) sp->td;
	Cppi_DescCfg descCfg;
	Qmss_QueueHnd tmpQ;
	Cppi_Desc *desc;
	uint8_t *descPtr;
	uint32_t numAllocated;
	int i;

	mpm_printf(2, "mpm_transport_qmss_init_desc() got params:"
		"\n\tname = %s"
		"\n\tqid = %d"
		"\n\tnumDesc = %d"
		"\n\tsizeBuf = %d\n", name, params->qid, params->numDesc, params->sizeBuf);

	/* Setup the descriptors for transmit free queue */
	memset(&descCfg, 0, sizeof(descCfg));
	descCfg.memRegion = (Qmss_MemRegion)td->memReg;
	descCfg.descNum = params->numDesc;
	descCfg.destQueueNum = params->qid;
	descCfg.queueType = Qmss_QueueType_GENERAL_PURPOSE_QUEUE;
	descCfg.initDesc = Cppi_InitDesc_INIT_DESCRIPTOR;
	descCfg.descType = Cppi_DescType_HOST;
	descCfg.epibPresent = Cppi_EPIB_NO_EPIB_PRESENT;

	/* Descriptor should be recycled to TX completion queue */
	descCfg.returnQueue.qMgr = QMSS_PARAM_NOT_SPECIFIED;
	descCfg.returnQueue.qNum = QMSS_PARAM_NOT_SPECIFIED;

	/* Initialize the descriptors and push to free Queue */
	if ((tmpQ = Cppi_initDescriptor (&descCfg, &numAllocated)) < 0)
	{
		mpm_printf(1, "mpm_transport_qmss_init_desc(): Error initing descriptors\n");
		return (Qmss_QueueHnd) NULL;
	}

	/* If buffer size specify, allocate memory... */
	if (params->sizeBuf) {
		descPtr = (uint8_t *)fw_memAlloc((params->sizeBuf * params->numDesc), CACHE_LINESZ);
		if (descPtr == NULL) {
		printf ("mpm_transport_qmss_init_desc(): Failed to fw_memAlloc descPtr\n");
		return (Qmss_QueueHnd) NULL;
		}
		for (i = 0; i<numAllocated; i++) {
			if ((desc = (Cppi_Desc *) Qmss_queuePop(tmpQ)) == NULL) {
				mpm_printf(1, "mpm_transport_qmss_init_desc(): error grabbing a packet from rx fq %d\n", tmpQ);
				return (Qmss_QueueHnd) NULL;
			}
			/* Alignment */
			desc = (Cppi_Desc *) QMSS_DESC_PTR(desc);
		Cppi_setData (Cppi_DescType_HOST, desc, descPtr + (i * params->sizeBuf), params->sizeBuf);
		Cppi_setOriginalBufInfo (Cppi_DescType_HOST, desc, descPtr + (i * params->sizeBuf), params->sizeBuf);
		Qmss_queuePushDesc (tmpQ, (uint32_t *) desc);
		}
	}

	return tmpQ;
}

int mpm_transport_qmss_insert_mem_region(mpm_transport_cfg_t *sp, mpm_transport_qmss_mem_region_info_t *params)
{
	mpm_transport_qmss_t *td = (mpm_transport_qmss_t *) sp->td;
	Qmss_MemRegInfo memInfo;
	uint8_t *descPtr;

	descPtr = (uint8_t *)fw_memAlloc((params->regionDescNum * params->regionDescSize), CACHE_LINESZ);
	if (descPtr == NULL) {
		mpm_printf (1, "mpm_transport_qmss_init_mem_region(): Failed to allocate descriptors\n");
		return (-1);
	}
	mpm_printf(2, "mpm_transport_qmss_insert_mem_region() got params:"
		"\n\tdesc size = %d"
		"\n\tdesc num = %d"
		"\n\tregion num = %d"
		"\n\tstart index = %d\n", params->regionDescSize, params->regionDescNum, params->regionNum, params->regionStartIdx);

	/* Setup memory region for descriptors */
	memset(&memInfo, 0, sizeof(memInfo));
	memset ((void *) descPtr, 0, params->regionDescNum * params->regionDescSize);
	memInfo.descBase = (uint32_t *) descPtr; //l2_global_address ((uint32_t) syncDesc);
	memInfo.descSize = params->regionDescSize;
	memInfo.descNum = params->regionDescNum;
	memInfo.manageDescFlag = Qmss_ManageDesc_MANAGE_DESCRIPTOR;
	memInfo.memRegion = params->regionNum;
	memInfo.startIndex = params->regionStartIdx;
	td->memReg = Qmss_insertMemoryRegion (&memInfo);
	if (td->memReg < QMSS_SOK) {
		mpm_printf(1, "mpm_transport_qmss_init_mem_region(): Error inserting region %d\n", td->memReg);
		return -1;
	}
	td->txDescSize = params->regionDescSize;
	return 0;
}

int mpm_transport_qmss_open(mpm_transport_cfg_t *sp, mpm_transport_open_t *ocfg)
{
	mpm_transport_qmss_t *td = (mpm_transport_qmss_t *) sp->td;
	uint8_t isAllocated;
	Cppi_RxFlowCfg rxFlowCfg;
	Qmss_Queue queInfo;

	td->rxFd = mpm_transport_qmss_open_qpend_and_fd("qpend", &(td->rxqHnd));
	if (td->rxFd < 0) {
		mpm_printf(1, "mpm_transport_qmss_open(): Error obtaining qpend queue and fd\n");
		return -1;
	}

	mpm_printf(2, "mpm_transport_qmss_open(): opened qpend %d\n", td->rxqHnd);

	if (mpm_transport_qmss_open_infra_triplet(td->cppiHnd, &(td->txChHnd),
			&(td->rxChHnd), &(td->txqHnd)) < 0)
	{
		mpm_printf(1, "mpm_transport_qmss_open(): "
			"Error when allocating the QMSS infrastructure triplet\n");
		return -1;
	}

	/* Setup Rx flow parameters */
	memset ((void *) &rxFlowCfg, 0, sizeof (Cppi_RxFlowCfg));

	/* Configure flow 0 */
	rxFlowCfg.flowIdNum = CPPI_PARAM_NOT_SPECIFIED;
	/* Get queue manager and queue number from handle */
	queInfo = Qmss_getQueueNumber (td->rxqHnd);
	rxFlowCfg.rx_dest_qnum = queInfo.qNum;
	rxFlowCfg.rx_dest_qmgr = queInfo.qMgr;
	rxFlowCfg.rx_sop_offset = 0;
	rxFlowCfg.rx_desc_type = Cppi_DescType_HOST;

	/* Get queue manager and queue number from handle */
	queInfo = Qmss_getQueueNumber (td->rxFqHnd);
	rxFlowCfg.rx_fdq0_sz0_qnum = queInfo.qNum;
	rxFlowCfg.rx_fdq0_sz0_qmgr = queInfo.qMgr;

	/* Configure Rx flow */
	td->flowHnd = (Cppi_FlowHnd) Cppi_configureRxFlow (td->cppiHnd, &rxFlowCfg, &isAllocated);
	if (td->flowHnd == NULL)
	{
		mpm_printf(1, "mpm_transport_qmss_open(): Error opening flow\n");
		return -1;
	}

	/* Enable transmit channel */
	if (Cppi_channelEnable (td->txChHnd) != CPPI_SOK)
	{
		mpm_printf(1, "mpm_transport_qmss_open(): Error enabling tx channel\n");
		return -1;
	}

	/* Enable receive channel */
	if (Cppi_channelEnable (td->rxChHnd) != CPPI_SOK)
	{
		mpm_printf(1, "mpm_transport_qmss_open(): Error enabling rx channel\n");
		return -1;
	}

	ocfg->transport_info.qmss.flowId = Cppi_getFlowId(td->flowHnd);

	return 0;
}

void mpm_transport_qmss_close(mpm_transport_cfg_t *sp)
{
	mpm_transport_qmss_t *td = (mpm_transport_qmss_t *) sp->td;
	Qmss_Result result;

	/* Disable Tx channel */
	if ((result = Cppi_channelDisable (td->txChHnd)) != CPPI_SOK)
	{
		mpm_printf (1, "mpm_transport_qmss_close(): Disabling Tx channel error code : %d\n", result);
	}

	/* Disable Rx channel */
	if ((result = Cppi_channelDisable (td->rxChHnd)) != CPPI_SOK)
	{
		mpm_printf (1, "mpm_transport_qmss_close(): Disabling Rx channel error code : %d\n", result);
	}

	/* Close Tx channel */
	if ((result = Cppi_channelClose (td->txChHnd)) != CPPI_SOK)
	{
		mpm_printf (1, "mpm_transport_qmss_close(): Closing Tx channel error code : %d\n", result);
	}

	/* Close Rx channel */
	if ((result = Cppi_channelClose (td->rxChHnd)) != CPPI_SOK)
	{
		mpm_printf (1, "mpm_transport_qmss_close(): Closing Rx channel error code : %d\n", result);
	}

	/* Close RX flow */
	if ((result = Cppi_closeRxFlow (td->flowHnd)) != CPPI_SOK)
	{
		mpm_printf (1, "mpm_transport_qmss_close(): Closing rx flow error code : %d\n", result);
	}

	close(td->rxFd);

	Cppi_close(td->cppiHnd);

	mpm_printf(2,"mpm_transport_qmss_close(): Exit queue count for td->txFqHnd: %d\n",Qmss_getQueueEntryCount(td->txFqHnd));
	mpm_printf(2,"mpm_transport_qmss_close(): Exit queue count for td->txqHnd: %d\n",Qmss_getQueueEntryCount(td->txqHnd));
	mpm_printf(2,"mpm_transport_qmss_close(): Exit queue count for td->rxFqHnd: %d\n",Qmss_getQueueEntryCount(td->rxFqHnd));
	mpm_printf(2,"mpm_transport_qmss_close(): Exit queue count for td->rxqHnd: %d\n",Qmss_getQueueEntryCount(td->rxqHnd));
	Qmss_queueEmpty(td->txFqHnd);
	Qmss_queueEmpty(td->txqHnd);
	Qmss_queueEmpty(td->rxFqHnd);
	Qmss_queueClose(td->txFqHnd);
	Qmss_queueClose(td->txqHnd);
	Qmss_queueClose(td->rxFqHnd);
	Qmss_queueClose(td->rxqHnd);

	if ((result = Qmss_removeMemoryRegion(td->memReg, 0)) != QMSS_SOK) {
		mpm_printf(1,"mpm_transport_qmss_close(): Error removing region\n");
	}

	if (td->qmssInit) {
		Cppi_exit();
		if ((result = Qmss_exit()) != QMSS_SOK) {
			mpm_printf(1,"mpm_transport_qmss_close(): Error qmss exit\n");
		}
	}

	fw_memAllocExit ();

	free(td);
}


int mpm_transport_qmss_packet_send(mpm_transport_cfg_t *sp, char **buf, int *len, mpm_transport_packet_addr_t *addr_info, mpm_transport_send_t *cfg)
{
	mpm_transport_qmss_t *td = (mpm_transport_qmss_t *) sp->td;
	Cppi_Desc *desc;
	int flow_id = ((addr_info->addr)).qmss.flowId;
	int ret = 0;
	uint32_t buffLen;
	uint8_t *tempBuf;
	Cppi_DescTag tag;
	packet_send_buffer_opt bufferOption;

	if (addr_info->addr_type != packet_addr_type_QMSS) {
		mpm_printf(1, "mpm_transport_qmss_packet_send(): invalid send address type! only Qmss type supported\n");
		return -1;
	}
	if (flow_id <= 0) {
		mpm_printf(1, "mpm_transport_qmss_packet_send(): invalid flow id %d\n", flow_id);
		return -1;
	}
	if ((desc = (Cppi_Desc *) Qmss_queuePop(td->txFqHnd)) == NULL) {
		mpm_printf(1, "mpm_transport_qmss_packet_send(): error grabbing a packet from fq %d\n", td->txFqHnd);
		return -1;
	}

	/* Alignment */
	desc = (Cppi_Desc *) QMSS_DESC_PTR(desc);

	/* Grab the packet's buffer into tempBuf */
	Cppi_getOriginalBufInfo(Cppi_DescType_HOST, desc, &tempBuf, &buffLen);

	if (cfg == NULL) {
		bufferOption = packet_send_BUFFER_DEFAULT;
	}
	else {
		bufferOption = cfg->buf_opt;
	}

	if (tempBuf == NULL) {
		if (bufferOption == packet_send_MEMCPY_BUFFER) {
			mpm_printf(1, "mpm_transport_qmss_packet_send(): no buffer attached to desc, can't use memcpy\n");
			return -1;
		}
	}

	if ( (bufferOption == packet_send_MEMCPY_BUFFER) || (bufferOption == packet_send_BUFFER_DEFAULT)) {
		if (*len > buffLen) {
			mpm_printf(1, "mpm_transport_qmss_packet_send(): Error, tried to send more data than buffer size!\n");
			return -1;
		}
		else {
			memcpy(tempBuf, *buf, *len);
			Cppi_setDataLen(Cppi_DescType_HOST, desc, *len);
		}
	}
	else if (bufferOption == packet_send_LINK_BUFFER_POINTER) {
		Cppi_setData(Cppi_DescType_HOST, desc, (uint8_t *) (*buf), *len);
	}

	/* Set tag information - this selects the flow */
	tag.destTagLo = 0;
	tag.destTagHi = 0;
	tag.srcTagLo = flow_id;
	tag.srcTagHi = 0;
	Cppi_setTag(Cppi_DescType_HOST, desc, &tag);

	/* Set packet length */
	Cppi_setPacketLen(Cppi_DescType_HOST, desc, *len);

	Qmss_queuePushDescSize (td->txqHnd, (uint32_t *) desc, td->txDescSize);

	/* Return information to user if necessary */
	if ( (bufferOption == packet_send_MEMCPY_BUFFER) || (bufferOption == packet_send_BUFFER_DEFAULT)) {
		// No need to signal user if we're doing memcpy
		*buf = NULL;
	}
	/*
	else {
		// Check completion queue and free up a block if there's any
	}*/

	return ret;
}

int mpm_transport_qmss_packet_recv(mpm_transport_cfg_t *sp, char **buf, int *len, mpm_transport_recv_t *cfg)
{
	mpm_transport_qmss_t *td = (mpm_transport_qmss_t *) sp->td;
	int ret = 0;
	uint32_t count;
	uint32_t interrupt = 1;
	Cppi_Desc *desc;
	uint32_t buffLen;
	uint8_t *tempBuf;
	packet_recv_wait_opt opt;

	if (cfg == NULL) {
		opt = packet_recv_wait_DEFAULT;
	}
	else {
		opt = cfg->wait_opt;
	}

	if ((opt == packet_recv_wait_DEFAULT) || (opt == packet_recv_BLOCKING_INTERRUPT)) {
		if (write(td->rxFd, &interrupt, sizeof(interrupt)) != sizeof(interrupt)) {
			mpm_printf(1, "mpm_transport_qmss_packet_recv(): unable to enable interrupt\n");
		}
		else if ( (read (td->rxFd, &count, sizeof(count))) != sizeof(count))
		{
			mpm_printf(1, "mpm_transport_qmss_packet_recv(): unable to read on block for interrupt\n");
		}
		desc = Qmss_queuePop(td->rxqHnd);
	}
	else {
		mpm_printf(1, "mpm_transport_qmss_packet_recv(): Invalid wait option!\n");
		return -1;
	}

	if (desc == NULL) {
		mpm_printf(1, "mpm_transport_qmss_packet_recv(): no packet in receive queue\n");
		return -1;
	}

	/* Alignment */
	desc = (Cppi_Desc *) QMSS_DESC_PTR(desc);

	/* Grab the packet's buffer into tempBuf */
	Cppi_getData(Cppi_DescType_HOST, desc, &tempBuf, &buffLen);

	if (*buf == NULL) {
		*len = buffLen;
		*buf = (char *) tempBuf;
		// PUSH TO RX COMPLETE Q
	}
	else {
		if (*len < buffLen) {
			mpm_printf(1, "mpm_transport_qmss_packet_recv(): "
					   "Error, received %d bytes but "
					   "provided buffer only has %d bytes!\n", buffLen, *len);
			return -1;
		}
		else {
			memcpy(*buf, tempBuf, buffLen);
			*len = buffLen;
		}

		/* Pushing descriptor back to recycle queue */
		Qmss_queuePushDesc(td->rxFqHnd, desc);
	}

	return ret;
}
