/*
 * Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <malloc.h>
#include <errno.h>
#include <dirent.h>

/* QMSS Types includes */
#include <stdint.h>
#include <stdlib.h>

/* QMSS includes */
#include <ti/drv/qmss/qmss_qm.h>
#include <ti/drv/qmss/qmss_osal.h>

/* CSL RL includes */
#include <ti/csl/cslr_device.h>
#include <ti/csl/cslr_qm_config.h>
#include <ti/csl/cslr_qm_descriptor_region_config.h>
#include <ti/csl/cslr_qm_queue_management.h>
#include <ti/csl/cslr_qm_queue_status_config.h>
#include <ti/csl/cslr_qm_intd.h>
#include <ti/csl/cslr_pdsp.h>
#include <ti/csl/csl_qm_queue.h>
 
/* CPPI includes */
#include <ti/drv/cppi/cppi_drv.h>

/* CSL RL includes */
#include <ti/csl/cslr_device.h>
#include <ti/csl/cslr_cppidma_global_config.h>
#include <ti/csl/cslr_cppidma_rx_channel_config.h>
#include <ti/csl/cslr_cppidma_rx_flow_config.h>
#include <ti/csl/cslr_cppidma_tx_channel_config.h>
#include <ti/csl/cslr_cppidma_tx_scheduler_config.h>
#include <ti/csl/csl_cppi.h>

#include "mpm_transport_qmss_interface.h"

extern Qmss_GlobalConfigParams qmssGblCfgParams;
extern Cppi_GlobalConfigParams cppiGblCfgParams;

uint32_t mpm_transport_cpdma_ctl;
#define MPM_QM1_CPDMA_SET (1<<0)
#define MPM_QM2_CPDMA_SET (1<<1)
#define MPM_SRIO_CPDMA_SET (1<<2)


/******************************************************************************
* Macro to convert to IP Register Virtual Address from a mapped base Virtual Address
* Input: virtBaseAddr: Virtual base address mapped using mmap for IP
*        phyBaseAddr: Physical base address for the IP
*        phyRegAddr:  Physical register address
******************************************************************************/
static inline void* FW_GET_REG_VADDR (void * virtBaseAddr, uint32_t phyBaseAddr, uint32_t phyRegAddr)
{
	return((void *)((uint8_t *)virtBaseAddr + (phyRegAddr - phyBaseAddr)));
}

/** ============================================================================
 *   @n@b mpm_transport_init_qm
 *
 *   @b Description
 *   @n This API initializes the QMSS LLD on core 0 only.
 *
 *   @param[in]
 *   @n None
 *
 *   @return    int32_t
 *              -1      -   Error
 *              0       -   Success
 * =============================================================================
 */
int32_t mpm_transport_init_qm(mpm_transport_cfg_t *sp, int maxDescNum)
{
	int32_t                     result;
	Qmss_InitCfg                qmssInitConfig;
	Qmss_GlobalConfigParams     fw_qmssGblCfgParams;
	uint32_t                    count;

	/* Initialize QMSS */
	memset (&qmssInitConfig, 0, sizeof (Qmss_InitCfg));

	/* Set up QMSS configuration */

	/* Use internal linking RAM */
	qmssInitConfig.linkingRAM0Base  = 0;
	qmssInitConfig.linkingRAM0Size  = 0;
	qmssInitConfig.linkingRAM1Base  = 0;
	qmssInitConfig.maxDescNum       = maxDescNum;

	/* Bypass hardware initialization as it is done within Kernel */
	qmssInitConfig.qmssHwStatus     =   QMSS_HW_INIT_COMPLETE;

	fw_qmssGblCfgParams = qmssGblCfgParams;

	/* Convert address to Virtual address */
	for(count=0;count < fw_qmssGblCfgParams.maxQueMgrGroups;count++)
	{

		fw_qmssGblCfgParams.groupRegs[count].qmConfigReg =
			FW_GET_REG_VADDR(fw_qmssCfgVaddr,QMSS_CFG_BASE_ADDR,(uint32_t)fw_qmssGblCfgParams.groupRegs[count].qmConfigReg);

		fw_qmssGblCfgParams.groupRegs[count].qmDescReg =
			FW_GET_REG_VADDR(fw_qmssCfgVaddr,QMSS_CFG_BASE_ADDR,(uint32_t)fw_qmssGblCfgParams.groupRegs[count].qmDescReg);

		fw_qmssGblCfgParams.groupRegs[count].qmQueMgmtReg =
			FW_GET_REG_VADDR(fw_qmssCfgVaddr,QMSS_CFG_BASE_ADDR,(uint32_t)fw_qmssGblCfgParams.groupRegs[count].qmQueMgmtReg);

		fw_qmssGblCfgParams.groupRegs[count].qmQueMgmtProxyReg =
			FW_GET_REG_VADDR(fw_qmssCfgVaddr,QMSS_CFG_BASE_ADDR,(uint32_t)fw_qmssGblCfgParams.groupRegs[count].qmQueMgmtProxyReg);

		fw_qmssGblCfgParams.groupRegs[count].qmQueStatReg =
			FW_GET_REG_VADDR(fw_qmssCfgVaddr,QMSS_CFG_BASE_ADDR,(uint32_t)fw_qmssGblCfgParams.groupRegs[count].qmQueStatReg);

		fw_qmssGblCfgParams.groupRegs[count].qmStatusRAM =
			FW_GET_REG_VADDR(fw_qmssCfgVaddr,QMSS_CFG_BASE_ADDR,(uint32_t)fw_qmssGblCfgParams.groupRegs[count].qmStatusRAM);

		fw_qmssGblCfgParams.groupRegs[count].qmQueMgmtDataReg =
			FW_GET_REG_VADDR(fw_qmssDataVaddr,QMSS_DATA_BASE_ADDR,(uint32_t)fw_qmssGblCfgParams.groupRegs[count].qmQueMgmtDataReg);

		fw_qmssGblCfgParams.groupRegs[count].qmQueMgmtProxyDataReg = NULL; /* not supported on k2 hardware, and not used by lld */
	}

	for(count=0;count < QMSS_MAX_INTD;count++)
	{
		fw_qmssGblCfgParams.regs.qmQueIntdReg[count] =
			FW_GET_REG_VADDR(fw_qmssCfgVaddr,QMSS_CFG_BASE_ADDR,(uint32_t)fw_qmssGblCfgParams.regs.qmQueIntdReg[count]);
	}

	for(count=0;count < QMSS_MAX_PDSP;count++)
	{
		fw_qmssGblCfgParams.regs.qmPdspCmdReg[count] =
			FW_GET_REG_VADDR(fw_qmssCfgVaddr,QMSS_CFG_BASE_ADDR,(uint32_t)fw_qmssGblCfgParams.regs.qmPdspCmdReg[count]);

		fw_qmssGblCfgParams.regs.qmPdspCtrlReg[count] =
			FW_GET_REG_VADDR(fw_qmssCfgVaddr,QMSS_CFG_BASE_ADDR,(uint32_t)fw_qmssGblCfgParams.regs.qmPdspCtrlReg[count]);

		fw_qmssGblCfgParams.regs.qmPdspIRamReg[count] =
			FW_GET_REG_VADDR(fw_qmssCfgVaddr,QMSS_CFG_BASE_ADDR,(uint32_t)fw_qmssGblCfgParams.regs.qmPdspIRamReg[count]);
	}

	fw_qmssGblCfgParams.regs.qmLinkingRAMReg =
		FW_GET_REG_VADDR(fw_qmssCfgVaddr,QMSS_CFG_BASE_ADDR,(uint32_t)fw_qmssGblCfgParams.regs.qmLinkingRAMReg);

	fw_qmssGblCfgParams.regs.qmBaseAddr =
		FW_GET_REG_VADDR(fw_qmssCfgVaddr,QMSS_CFG_BASE_ADDR,(uint32_t)fw_qmssGblCfgParams.regs.qmBaseAddr);

	if (sp->rmClientServiceHandle) {
		fw_qmssGblCfgParams.qmRmServiceHandle = sp->rmClientServiceHandle;
	}

	/* Initialize the Queue Manager */
	result = Qmss_init (&qmssInitConfig, &fw_qmssGblCfgParams);
	if (result != QMSS_SOK)
	{
		mpm_printf (1, "initQmss: Error initializing Queue Manager SubSystem, Error code : %d\n", result);
		return -1;
	}

	/* Start Queue manager on this core */
	result = Qmss_start ();
	if (result != QMSS_SOK)
	{
		mpm_printf (1, "initQmss: Error starting Queue Manager SubSystem, Error code : %d\n", result);
		return -1;
	}

	/* Queue Manager Initialization Done */
	return 0;
}

/** ============================================================================
 *   @n@b mpm_transport_init_cppi
 *
 *   @b Description
 *   @n This API initializes the CPPI LLD and opens the QMSS CPDMA
 *
 *   @param[out]
 *   @n cppi handle
 *
 *   @return    int32_t
 *              -1      -   Error
 *              0       -   Success
 * =============================================================================
 */
int32_t mpm_transport_init_cppi(mpm_transport_cfg_t *sp)
{
	int32_t                     result;
	Cppi_GlobalConfigParams     fw_cppiGblCfgParams;
	Cppi_StartCfg               cppiStartCfg;

	fw_cppiGblCfgParams = cppiGblCfgParams;

	if ( (!(mpm_transport_cpdma_ctl & MPM_QM1_CPDMA_SET)) && (fw_qmssCfgVaddr != 0) ) {
		/* Convert Physical address to Virtual address for LLD access */
		/* QMSS CPDMA regs */
		fw_cppiGblCfgParams.cpDmaCfgs[Cppi_CpDma_QMSS_CPDMA].gblCfgRegs =
			FW_GET_REG_VADDR(fw_qmssCfgVaddr,QMSS_CFG_BASE_ADDR, (uint32_t) fw_cppiGblCfgParams.cpDmaCfgs[Cppi_CpDma_QMSS_CPDMA].gblCfgRegs);
	
		fw_cppiGblCfgParams.cpDmaCfgs[Cppi_CpDma_QMSS_CPDMA].txChRegs =
			FW_GET_REG_VADDR(fw_qmssCfgVaddr,QMSS_CFG_BASE_ADDR, (uint32_t) fw_cppiGblCfgParams.cpDmaCfgs[Cppi_CpDma_QMSS_CPDMA].txChRegs);
	
		fw_cppiGblCfgParams.cpDmaCfgs[Cppi_CpDma_QMSS_CPDMA].rxChRegs =
			FW_GET_REG_VADDR(fw_qmssCfgVaddr,QMSS_CFG_BASE_ADDR, (uint32_t) fw_cppiGblCfgParams.cpDmaCfgs[Cppi_CpDma_QMSS_CPDMA].rxChRegs);
	
		fw_cppiGblCfgParams.cpDmaCfgs[Cppi_CpDma_QMSS_CPDMA].txSchedRegs =
			FW_GET_REG_VADDR(fw_qmssCfgVaddr,QMSS_CFG_BASE_ADDR, (uint32_t) fw_cppiGblCfgParams.cpDmaCfgs[Cppi_CpDma_QMSS_CPDMA].txSchedRegs);
	
		fw_cppiGblCfgParams.cpDmaCfgs[Cppi_CpDma_QMSS_CPDMA].rxFlowRegs =
			FW_GET_REG_VADDR(fw_qmssCfgVaddr,QMSS_CFG_BASE_ADDR, (uint32_t) fw_cppiGblCfgParams.cpDmaCfgs[Cppi_CpDma_QMSS_CPDMA].rxFlowRegs);

		mpm_transport_cpdma_ctl |= MPM_QM1_CPDMA_SET;
	}

	if ( (!(mpm_transport_cpdma_ctl & MPM_QM2_CPDMA_SET)) && (fw_qmssCfgVaddr != 0) ) {
		/* Second CPDMA regs */
		fw_cppiGblCfgParams.cpDmaCfgs[Cppi_CpDma_QMSS_QM2_CPDMA].gblCfgRegs =
			FW_GET_REG_VADDR(fw_qmssCfgVaddr,QMSS_CFG_BASE_ADDR, (uint32_t) fw_cppiGblCfgParams.cpDmaCfgs[Cppi_CpDma_QMSS_QM2_CPDMA].gblCfgRegs);
	
		fw_cppiGblCfgParams.cpDmaCfgs[Cppi_CpDma_QMSS_QM2_CPDMA].txChRegs =
			FW_GET_REG_VADDR(fw_qmssCfgVaddr,QMSS_CFG_BASE_ADDR, (uint32_t) fw_cppiGblCfgParams.cpDmaCfgs[Cppi_CpDma_QMSS_QM2_CPDMA].txChRegs);
	
		fw_cppiGblCfgParams.cpDmaCfgs[Cppi_CpDma_QMSS_QM2_CPDMA].rxChRegs =
			FW_GET_REG_VADDR(fw_qmssCfgVaddr,QMSS_CFG_BASE_ADDR, (uint32_t) fw_cppiGblCfgParams.cpDmaCfgs[Cppi_CpDma_QMSS_QM2_CPDMA].rxChRegs);
	
		fw_cppiGblCfgParams.cpDmaCfgs[Cppi_CpDma_QMSS_QM2_CPDMA].txSchedRegs =
			FW_GET_REG_VADDR(fw_qmssCfgVaddr,QMSS_CFG_BASE_ADDR, (uint32_t) fw_cppiGblCfgParams.cpDmaCfgs[Cppi_CpDma_QMSS_QM2_CPDMA].txSchedRegs);
	
		fw_cppiGblCfgParams.cpDmaCfgs[Cppi_CpDma_QMSS_QM2_CPDMA].rxFlowRegs =
			FW_GET_REG_VADDR(fw_qmssCfgVaddr,QMSS_CFG_BASE_ADDR, (uint32_t) fw_cppiGblCfgParams.cpDmaCfgs[Cppi_CpDma_QMSS_QM2_CPDMA].rxFlowRegs);

		mpm_transport_cpdma_ctl |= MPM_QM2_CPDMA_SET;
	}

	if ((!(mpm_transport_cpdma_ctl & MPM_SRIO_CPDMA_SET)) && (fw_srioCfgVaddr != 0) ) {
		/* SRIO regs */
		fw_cppiGblCfgParams.cpDmaCfgs[Cppi_CpDma_SRIO_CPDMA].gblCfgRegs =
			FW_GET_REG_VADDR(fw_srioCfgVaddr,SRIO_CFG_BASE_ADDR, (uint32_t) fw_cppiGblCfgParams.cpDmaCfgs[Cppi_CpDma_SRIO_CPDMA].gblCfgRegs);
	
		fw_cppiGblCfgParams.cpDmaCfgs[Cppi_CpDma_SRIO_CPDMA].txChRegs =
			FW_GET_REG_VADDR(fw_srioCfgVaddr,SRIO_CFG_BASE_ADDR, (uint32_t) fw_cppiGblCfgParams.cpDmaCfgs[Cppi_CpDma_SRIO_CPDMA].txChRegs);
	
		fw_cppiGblCfgParams.cpDmaCfgs[Cppi_CpDma_SRIO_CPDMA].rxChRegs =
			FW_GET_REG_VADDR(fw_srioCfgVaddr,SRIO_CFG_BASE_ADDR, (uint32_t) fw_cppiGblCfgParams.cpDmaCfgs[Cppi_CpDma_SRIO_CPDMA].rxChRegs);
	
		fw_cppiGblCfgParams.cpDmaCfgs[Cppi_CpDma_SRIO_CPDMA].txSchedRegs =
			FW_GET_REG_VADDR(fw_srioCfgVaddr,SRIO_CFG_BASE_ADDR, (uint32_t) fw_cppiGblCfgParams.cpDmaCfgs[Cppi_CpDma_SRIO_CPDMA].txSchedRegs);
	
		fw_cppiGblCfgParams.cpDmaCfgs[Cppi_CpDma_SRIO_CPDMA].rxFlowRegs =
			FW_GET_REG_VADDR(fw_srioCfgVaddr,SRIO_CFG_BASE_ADDR, (uint32_t) fw_cppiGblCfgParams.cpDmaCfgs[Cppi_CpDma_SRIO_CPDMA].rxFlowRegs);

		mpm_transport_cpdma_ctl |= MPM_SRIO_CPDMA_SET;
	}

	/* Initialize CPPI LLD */
	result = Cppi_init (&fw_cppiGblCfgParams);
	if (result != CPPI_SOK)
	{
		mpm_printf (1, "initCppi: Error initializing CPPI LLD, Error code : %d\n", result);
		return -1;
	}

	if (sp->rmClientServiceHandle) {
		cppiStartCfg.rmServiceHandle = sp->rmClientServiceHandle;
		Cppi_startCfg(&cppiStartCfg);
	}

	return 0;
}

int mpm_transport_open_cppi_qmss_cpdma(Cppi_Handle *cHnd, uint8_t cppiWriteFifoDepth, uint16_t cppiTimeoutCount)
{
	Cppi_CpDmaInitCfg           cpdmaCfg;
	/* Initialize QMSS CPDMA */
	memset (&cpdmaCfg, 0, sizeof (Cppi_CpDmaInitCfg));
	cpdmaCfg.dmaNum     = Cppi_CpDma_QMSS_CPDMA;
	cpdmaCfg.writeFifoDepth  = cppiWriteFifoDepth;
	cpdmaCfg.timeoutCount  = cppiTimeoutCount;

	*cHnd = Cppi_open (&cpdmaCfg);
	if (*cHnd == NULL) {
		mpm_printf (1, "initCppi: Error initializing CPPI for QMSS CPDMA %d \n", cpdmaCfg.dmaNum);
		return -1;
	}
	return 0;	
}

int mpm_transport_qmss_open_qpend_and_fd(const char *basename, Qmss_QueueHnd *rxQueueHnd)
{
	struct dirent *entry = 0;
	char          filename[MAX_FILE_NAME_LEN];
	char          devname[MAX_FILE_NAME_LEN];
	char          name[MPM_MAX_NAME_LENGTH];
	int           ret_val = -1, fd_uio;
	DIR          *dir = NULL;
	char         *name_ptr;
	int           queueNum;
	uint8_t       isAllocated;

	/* dig through /sys/class/uio to find devices with name
	 * starting with basename */
	dir = opendir("/sys/class/uio");
	if (!dir) {
		goto close_n_exit;
	}
	while ((entry = readdir(dir)) != NULL) {
		if (strstr (entry->d_name, "uio") == NULL) {
			continue;
		}

		snprintf(filename, MAX_FILE_NAME_LEN, "/sys/class/uio/%s/name", entry->d_name);

		if (uioutil_get_string(filename, name, MPM_MAX_NAME_LENGTH) < 0)
			goto close_n_exit;

		name_ptr = remove_postfix_from_device_name(name, MPM_MAX_NAME_LENGTH);

		if (!strncmp (basename, name_ptr, strlen(basename))) {

			/* Now find associated queue number in
			 * /proc/device-tree/soc/<classname>/queue
			 */
			uioutil_get_param_int(name_ptr, "ti,qm-queue", &queueNum);

			/* Try opening the queue */
			if (((*rxQueueHnd) = Qmss_queueOpen (QMSS_PARAM_NOT_SPECIFIED, queueNum, &isAllocated)) >= 0)
			{
				if (isAllocated == 1) /* I'm first to get the queue */
				{
					/* Now open the /dev/uio## associated with this qpend
					 * in order to block on interrupt */
					snprintf (devname, MAX_FILE_NAME_LEN, "/dev/%s",
								entry->d_name);

					if ( (fd_uio = open (devname, (O_RDWR | O_SYNC))) < 0)
					{
						printf ("Error open %s (%s)\n", devname, strerror(errno));
						goto close_n_exit;
					}
					ret_val = fd_uio;
					goto close_n_exit;
				}
				else
				{
					/* Somebody got this queue already */
					if (Qmss_queueClose (*rxQueueHnd) < QMSS_SOK)
					{
						goto close_n_exit;
					}
					/* else fall through and try another device */
				}
			}
			/* Else, fall through and try another uio device to see if its
			 * underlying queue is available */
		}
	}

close_n_exit:
	if (dir)
	{
		closedir(dir);
	}

	return ret_val;
}

/** ============================================================================
 *	 @n@b mpm_transport_qmss_open_infra_triplet
 *
 *	 @b Description
 *	 @n This function allocates the CPPI Tx channel, Rx channel, and Tx
 *		queue triplet.  The triplet must be allocated together since the
 *		all their offsets from the base resource value must be the same.
 *		For example, a valid triplet allocation looks like:
 *			Tx channel - 4
 *			Rx channel - 4
 *			Tx queue   - Infrastructure queue base + 4
 *
 *	 @param[in]  cppiHnd
 *		cppi handle
 *	 @param[out] pTxChHnd
 *		Point to infrastructure DMA tx channel
 *	 @param[out] pRxChHnd
 *		Pointer to infrastructure DMA rx channel
 *	 @param[out] pTxQHnd
 *		Pointer to infrastructure DMA tx queue
 *
 *	 @return	int
 *		-1 - Error : Could not malloc temporary buffer for finding triplet
 *		-2 - Error : Could not open a valid QMSS Tx infrastructure queue
 *		0  - Success
 * =============================================================================
 */
int mpm_transport_qmss_open_infra_triplet(Cppi_Handle cppiHnd,
										  Cppi_ChHnd *pTxChHnd,
										  Cppi_ChHnd *pRxChHnd,
										  Qmss_QueueHnd *pTxQHnd)
{
	Qmss_QueueHnd     tmpTxQ;
	Cppi_ChHnd        tmpTxCh = NULL;
	Cppi_ChHnd        tmpRxCh = NULL;
	Cppi_TxChInitCfg  txChCfg;
	Cppi_RxChInitCfg  rxChCfg;
	Qmss_QueueHnd    *invTrip = NULL;
	int               invIdx = 0;
	uint32_t          isAllocated;
	int               foundTriplet;
	int               i;
	int               ret_val = 0;

	/* Init array where invalid triplets are stored.  Invalid triplets will be
	 * freed once a valid triplet is found */
	invTrip = (Qmss_QueueHnd *)malloc(QMSS_MAX_INFRASTRUCTURE_QUEUE *
									  sizeof(Qmss_QueueHnd));
	if (invTrip == NULL) {
		ret_val = -1;
		goto triplet_error;
	}
	
	memset(invTrip, 0, QMSS_MAX_INFRASTRUCTURE_QUEUE * sizeof(Qmss_QueueHnd));

	foundTriplet = 0;
	while((!foundTriplet) && (invIdx < QMSS_MAX_INFRASTRUCTURE_QUEUE)) {
		/* Open the Tx infrastructure queue.  Should be offset from
		 * infrastructure queue base using the CPPI tx channel number */
		tmpTxQ = Qmss_queueOpen(Qmss_QueueType_INFRASTRUCTURE_QUEUE,
								QMSS_PARAM_NOT_SPECIFIED,
								(uint8_t *)&isAllocated);
		if (tmpTxQ < 0) {
			ret_val = -2;
			goto triplet_error;
		}

		memset((void *)&txChCfg, 0, sizeof(txChCfg));
		txChCfg.channelNum  = Qmss_getQIDFromHandle(tmpTxQ) -
							  QMSS_INFRASTRUCTURE_QUEUE_BASE;
		txChCfg.priority    = 0;
		txChCfg.filterEPIB  = 0;
		txChCfg.filterPS    = 0;
		txChCfg.aifMonoMode = 0;
		txChCfg.txEnable    = Cppi_ChState_CHANNEL_DISABLE;
		tmpTxCh = (uint32_t *)Cppi_txChannelOpen(cppiHnd, &txChCfg,
												 (uint8_t *)&isAllocated);
		if (tmpTxCh == NULL) {
			invTrip[invIdx++] = tmpTxQ;
			/* Try to get next triplet */
			continue;
		}

		memset((void *)&rxChCfg, 0, sizeof(rxChCfg));
		rxChCfg.channelNum = Qmss_getQIDFromHandle(tmpTxQ) -
								QMSS_INFRASTRUCTURE_QUEUE_BASE;
		rxChCfg.rxEnable   = Cppi_ChState_CHANNEL_DISABLE;
		tmpRxCh = (uint32_t *)Cppi_rxChannelOpen(cppiHnd, &rxChCfg,
												 (uint8_t *)&isAllocated);
		if (tmpRxCh == NULL) {
			Cppi_channelClose(tmpTxCh);
			invTrip[invIdx++] = tmpTxQ;
			/* Try to get next triplet */
			continue;
		}
		/* Found a triplet if here */
		foundTriplet = 1;
	}

	if (tmpTxCh && tmpRxCh) {
		/* Save triplet */
		*pTxQHnd  = tmpTxQ;
		*pTxChHnd = tmpTxCh;
		*pRxChHnd = tmpRxCh;
	} else {
		ret_val = -3;
	}

triplet_error:
	/* Free unused triplets */
	for (i = 0; i < invIdx; i++) {
		Qmss_queueClose(invTrip[i]);
	}
	if (invTrip) {
		free(invTrip);
	}

	return(ret_val);
}
