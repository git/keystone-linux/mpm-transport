/*
 * Copyright (C) 2013-2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include "mpm_transport_hyplnk_interface.h"
#include "uioutils.h"
#include <semaphore.h>
static sem_t hyperlink_sem[NUM_INTERFACES];

extern hyplnk_peripheral hyplnkActiveIF[NUM_INTERFACES];

#define SERDES_DIRECTION_TX 0x1
#define SERDES_DIRECTION_RX 0x2
#define SERDES_DIRECTION_RX_RESET 0x4

void * internal_hyplnk_mmap(uint32_t index, uint32_t addr, uint32_t size, int device_fd)
{
	void * virt_addr;
	uint32_t page_size, mapaddr, offset;
	page_size = getpagesize();
	mapaddr = addr & (~(page_size - 1));
	offset = addr - mapaddr;
	virt_addr = mmap(0, size + offset, (PROT_READ|PROT_WRITE), MAP_SHARED, device_fd,
		(off_t)(index * page_size));
	if ((uint32_t)virt_addr == -1)
	{
			mpm_printf(1, "mmap failed!\n");
			return NULL;
	}
	return (virt_addr+offset);
}

int internal_hyplnk_munmap( void *virt_addr, uint32_t size) {
	uint32_t page_size, offset;
	page_size = getpagesize();
	void * virt_addr_aligned;

	virt_addr_aligned = (void *)((uint32_t)virt_addr & (~(page_size - 1)));
	return(munmap(virt_addr_aligned, size+(virt_addr- virt_addr_aligned)));
	
}

int hyplnkSerdesSetup(mpm_transport_hyplnk_t *hypCfg, uint32_t direction_bitmap,
	int timeout)
{
	CSL_SERDES_REF_CLOCK          refClock = CSL_SERDES_REF_CLOCK_312p5M;
	CSL_SERDES_LINK_RATE          linkRate = CSL_SERDES_LINK_RATE_6p25G;
	uint32_t                      baseAddr;
	CSL_SERDES_RESULT             csl_retval;
	uint32_t                      i;
	CSL_SERDES_LANE_CTRL_RATE     lane_rate = CSL_SERDES_LANE_FULL_RATE;
	CSL_SERDES_STATUS             pllstat;
	uint32_t serdes_regs;
	CSL_SERDES_LOOPBACK           loopback = CSL_SERDES_LOOPBACK_DISABLED;
	int cm[CSL_SERDES_MAX_LANES], c1[CSL_SERDES_MAX_LANES], c2[CSL_SERDES_MAX_LANES], refclk, serrate, ret, index;
	int num_lanes, tx_att[CSL_SERDES_MAX_LANES], tx_vreg[CSL_SERDES_MAX_LANES], rx_att[CSL_SERDES_MAX_LANES], rx_boost[CSL_SERDES_MAX_LANES];
	char lanerate[MAX_FILE_NAME_LEN];
	int conf_coeff = 1;
	csl_serdes_phy_type phy = SERDES_HYPERLINK;
	CSL_SERDES_LANE_ENABLE_PARAMS_T serdes_init_params;
	CSL_SERDES_LANE_ENABLE_STATUS lane_retval = CSL_SERDES_LANE_ENABLE_NO_ERR;

	memset(&serdes_init_params, 0, sizeof(serdes_init_params));

	/* set to default values */
	for (i=0; i<CSL_SERDES_MAX_LANES; i++)
	{
		cm[i] = 0;
		c1[i] = 4;
		c2[i] = 0;
		tx_att[i] = 12;
		tx_vreg[i] = 4;
		rx_att[i] = 11;
		rx_boost[i] = 3;
	}

	if (hypCfg->fd[hypCfg->hyplnkIF] == -1) 
		return -1;
	if (hypCfg->hyplnkIF == hyplnk_PORT_0) {
		/* read number of lanes */
		ret = uioutil_get_param_int("hyperlink0", "ti,serdes_numlanes", &num_lanes);
		if (ret == -1)
		{
			/* if it is the legacy DTS format (one parameter for all lanes) */
			num_lanes = CSL_SERDES_MAX_LANES;
			ret = uioutil_get_param_int("hyperlink0", "ti,serdes_cm", &cm[0]);
			mpm_printf(2, "input_cm: %d\n", cm[0]);
			ret = uioutil_get_param_int("hyperlink0", "ti,serdes_c1", &c1[0]);
			mpm_printf(2, "input_c1: %d\n", c1[0]);
			ret = uioutil_get_param_int("hyperlink0", "ti,serdes_c2", &c2[0]);
			mpm_printf(2, "input_c2: %d\n", c2[0]);
			ret = uioutil_get_param_int("hyperlink0", "ti,serdes_tx_att", &tx_att[0]);
			mpm_printf(2, "input_tx_att: %d\n", tx_att[0]);
			ret = uioutil_get_param_int("hyperlink0", "ti,serdes_tx_vreg_lev", &tx_vreg[0]);
			mpm_printf(2, "input_tx_vreg: %d\n", tx_vreg[0]);
			mpm_printf(2, "input_rx_att: %d\n", rx_att[0]);
			mpm_printf(2, "input_rx_boost: %d\n", rx_boost[0]);
			for (i=1; i < num_lanes; i++)
			{
				cm[i] = cm[0];
				c1[i] = c1[0];
				c2[i] = c2[0];
				tx_att[i] = tx_att[0];
				tx_vreg[i] = tx_vreg[0];
			}
		} else
		{
			ret = uioutil_get_param_int_array("hyperlink0", "ti,serdes_cm", num_lanes, cm);
			mpm_printf(2, "input_cm: %d, %d, %d, %d\n", cm[0], cm[1], cm[2], cm[3]);
			ret = uioutil_get_param_int_array("hyperlink0", "ti,serdes_c1", num_lanes, c1);
			mpm_printf(2, "input_c1: %d, %d, %d, %d\n", c1[0], c1[1], c1[2], c1[3]);
			ret = uioutil_get_param_int_array("hyperlink0", "ti,serdes_c2", num_lanes, c2);
			mpm_printf(2, "input_c2: %d, %d, %d, %d\n", c2[0], c2[1], c2[2], c2[3]);
			ret = uioutil_get_param_int_array("hyperlink0", "ti,serdes_tx_att", num_lanes, tx_att);
			mpm_printf(2, "input_tx_att: %d, %d, %d, %d\n", tx_att[0], tx_att[1], tx_att[2], tx_att[3]);
			ret = uioutil_get_param_int_array("hyperlink0", "ti,serdes_tx_vreg", num_lanes, tx_vreg);
			mpm_printf(2, "input_tx_vreg: %d, %d, %d, %d\n", tx_vreg[0], tx_vreg[1], tx_vreg[2], tx_vreg[3]);
			ret = uioutil_get_param_int_array("hyperlink0", "ti,serdes_rx_att", num_lanes, rx_att);
			mpm_printf(2, "input_rx_att: %d, %d, %d, %d\n", rx_att[0], rx_att[1], rx_att[2], rx_att[3]);
			ret = uioutil_get_param_int_array("hyperlink0", "ti,serdes_rx_boost", num_lanes, rx_boost);
			mpm_printf(2, "input_rx_boost: %d, %d, %d, %d\n", rx_boost[0], rx_boost[1], rx_boost[2], rx_boost[3]);
		}
		ret = uioutil_get_param_int("hyperlink0", "ti,serdes_refclk_khz", &refclk);
		if (ret == -1) {
			mpm_printf(1, "Error: can't find refclk rate\n");
			return -1;
		}
		ret = uioutil_get_param_int("hyperlink0", "ti,serdes_maxserrate_khz", &serrate);
		if (ret == -1) {
			mpm_printf(1, "Error: can't find serial rate\n");
			return -1;
		}
		ret = uioutil_get_param_str("hyperlink0", "ti,serdes_lanerate", lanerate);
		if (ret == -1) {
			mpm_printf(1, "Error: can't find lane rate\n");
			return -1;
		}
		serdes_regs = CSL_HYPERLINK_0_SERDES_CFG_REGS;
	} else if (hypCfg->hyplnkIF == hyplnk_PORT_1) {
		/* read number of lanes */
		ret = uioutil_get_param_int("hyperlink1", "ti,serdes_numlanes", &num_lanes);
		if (ret == -1) {
			/* if it is the legacy DTS format (one parameter for all lanes) */
			num_lanes = CSL_SERDES_MAX_LANES;
			ret = uioutil_get_param_int("hyperlink1", "ti,serdes_cm", &cm[0]);
			mpm_printf(2, "input_cm: %d\n", cm[0]);
			ret = uioutil_get_param_int("hyperlink1", "ti,serdes_c1", &c1[0]);
			mpm_printf(2, "input_c1: %d\n", c1[0]);
			ret = uioutil_get_param_int("hyperlink1", "ti,serdes_c2", &c2[0]);
			mpm_printf(2, "input_c2: %d\n", c2[0]);
			ret = uioutil_get_param_int("hyperlink1", "ti,serdes_tx_att", &tx_att[0]);
			mpm_printf(2, "input_tx_att: %d\n", tx_att[0]);
			ret = uioutil_get_param_int("hyperlink1", "ti,serdes_tx_vreg_lev", &tx_vreg[0]);
			mpm_printf(2, "input_tx_vreg: %d\n", tx_vreg[0]);
			mpm_printf(2, "input_rx_att: %d\n", rx_att[0]);
			mpm_printf(2, "input_rx_boost: %d\n", rx_boost[0]);
			for (i=1; i < num_lanes; i++)
			{
				cm[i] = cm[0];
				c1[i] = c1[0];
				c2[i] = c2[0];
				tx_att[i] = tx_att[0];
				tx_vreg[i] = tx_vreg[0];
			}

		} else {
			ret = uioutil_get_param_int_array("hyperlink1", "ti,serdes_cm", num_lanes, cm);
			mpm_printf(2, "input_cm: %d, %d, %d, %d\n", cm[0], cm[1], cm[2], cm[3]);
			ret = uioutil_get_param_int_array("hyperlink1", "ti,serdes_c1", num_lanes, c1);
			mpm_printf(2, "input_c1: %d, %d, %d, %d\n", c1[0], c1[1], c1[2], c1[3]);
			ret = uioutil_get_param_int_array("hyperlink1", "ti,serdes_c2", num_lanes, c2);
			mpm_printf(2, "input_c2: %d, %d, %d, %d\n", c2[0], c2[1], c2[2], c2[3]);
			ret = uioutil_get_param_int_array("hyperlink1", "ti,serdes_tx_att", num_lanes, tx_att);
			mpm_printf(2, "input_tx_att: %d, %d, %d, %d\n", tx_att[0], tx_att[1], tx_att[2], tx_att[3]);
			ret = uioutil_get_param_int_array("hyperlink1", "ti,serdes_tx_vreg", num_lanes, tx_vreg);
			mpm_printf(2, "input_tx_vreg: %d, %d, %d, %d\n", tx_vreg[0], tx_vreg[1], tx_vreg[2], tx_vreg[3]);
			ret = uioutil_get_param_int_array("hyperlink1", "ti,serdes_rx_att", num_lanes, rx_att);
			mpm_printf(2, "input_rx_att: %d, %d, %d, %d\n", rx_att[0], rx_att[1], rx_att[2], rx_att[3]);
			ret = uioutil_get_param_int_array("hyperlink1", "ti,serdes_rx_boost", num_lanes, rx_boost);
			mpm_printf(2, "input_rx_boost: %d, %d, %d, %d\n", rx_boost[0], rx_boost[1], rx_boost[2], rx_boost[3]);
		}
		ret = uioutil_get_param_int("hyperlink1", "ti,serdes_refclk_khz", &refclk);
		if (ret == -1) {
			mpm_printf(1, "Error: can't find refclk rate\n");
			return -1;
		}
		ret = uioutil_get_param_int("hyperlink1", "ti,serdes_maxserrate_khz", &serrate);
		if (ret == -1) {
			mpm_printf(1, "Error: can't find serial rate\n");
			return -1;
		}
		ret = uioutil_get_param_str("hyperlink1", "ti,serdes_lanerate", lanerate);
		if (ret == -1) {
			mpm_printf(1, "Error: can't find lane rate\n");
			return -1;
		}
		serdes_regs = CSL_HYPERLINK_1_SERDES_CFG_REGS;
	}

	if (refclk == 312500)
		refClock = CSL_SERDES_REF_CLOCK_312p5M;
	else if (refclk == 156250)
		refClock = CSL_SERDES_REF_CLOCK_156p25M;
	else
		return -1;
	if (serrate == 6250000)
		linkRate = CSL_SERDES_LINK_RATE_6p25G;
	else if (serrate == 5000000)
		linkRate = CSL_SERDES_LINK_RATE_5G;
	else if (serrate == 10000000)
		linkRate = CSL_SERDES_LINK_RATE_10G;
	else if (serrate == 12500000)
		linkRate = CSL_SERDES_LINK_RATE_12p5G;
	else
		return -1;

	if (strcmp(lanerate,"full") == 0)
		lane_rate = CSL_SERDES_LANE_FULL_RATE;
	else if (strcmp(lanerate,"half") == 0)
		lane_rate = CSL_SERDES_LANE_HALF_RATE;
	else if (strcmp(lanerate,"quarter") == 0)
		lane_rate = CSL_SERDES_LANE_QUARTER_RATE;
	else
		return -1;

	if (hypCfg->hyplnkDir == hyplnk_LOOPBACK)
		loopback = CSL_SERDES_LOOPBACK_ENABLED;
	else if (hypCfg->hyplnkDir == hyplnk_REMOTE)
		loopback = CSL_SERDES_LOOPBACK_DISABLED;
	else
		return -1;

	baseAddr = (uint32_t)internal_hyplnk_mmap(2, serdes_regs, 0x2000, hypCfg->fd[hypCfg->hyplnkIF]);
	if ( baseAddr == 0) {
		mpm_printf(1, "Serdes Mmap failed\n");
		return -1;
	}

	serdes_init_params.base_addr = baseAddr;
	serdes_init_params.ref_clock = refClock;
	serdes_init_params.linkrate = linkRate;
	serdes_init_params.num_lanes = num_lanes;
	serdes_init_params.lane_mask = 0x0;
	serdes_init_params.phy_type = SERDES_HYPERLINK;

	/* Per Keystone II silicon errata, SPRZ402D, usage note.28: SerDes
	   Fails to Adapt RX BOOST Equalization. It is advised for higher
	   speeds PHY-A, force attenuation and boost values. Although Errata
	   specifies data greater than 6.25Gbps for Hyperlink, it is observed
	   very small chances of Hyperlink failure due to incorrect Rx
	   adaptation at 6.25Gbps. For lower data rates 5.0 Gbps or 3.125 Gbps,
	   the Rx adaptive may be used. The code uses Rx force for all rates,
	   the correct rx_att and rx_boost values need to be provided from
	   user by the approaches explained in Errata.
	   The Errata applies to silicon PG 1.0, 1.1 and 2.0. The code needs to
	   be revisited, when a new PG is released or the Rx adaptive is used.
	*/
	serdes_init_params.forceattboost = CSL_SERDES_FORCE_ATT_BOOST_ENABLED;

	for (i=0; i<num_lanes; i++)
	{
		serdes_init_params.lane_mask |= (1 << i) ;
		serdes_init_params.lane_ctrl_rate[i] = lane_rate;
		serdes_init_params.rx_coeff.att_start[i] = rx_att[i];
		serdes_init_params.rx_coeff.boost_start[i] = rx_boost[i];

		/* For higher speeds PHY-A, force attenuation and boost values  */
		serdes_init_params.rx_coeff.force_att_val[i] = rx_att[i];
		serdes_init_params.rx_coeff.force_boost_val[i] = rx_boost[i];

		/* Att and Boost values are obtained through Serdes Diagnostic PRBS calibration test */
		serdes_init_params.tx_coeff.cm_coeff[i] = cm[i];
		serdes_init_params.tx_coeff.c1_coeff[i] = c1[i];
		serdes_init_params.tx_coeff.c2_coeff[i] = c2[i];
		serdes_init_params.tx_coeff.tx_att[i] = tx_att[i];
		serdes_init_params.tx_coeff.tx_vreg[i] = tx_vreg[i];
	}

	if (loopback == CSL_SERDES_LOOPBACK_ENABLED)
		for (i=0; i<num_lanes; i++) serdes_init_params.loopback_mode[i] = CSL_SERDES_LOOPBACK_ENABLED;
	else
		for (i=0; i<num_lanes; i++) serdes_init_params.loopback_mode[i] = CSL_SERDES_LOOPBACK_DISABLED;

	if(direction_bitmap & SERDES_DIRECTION_TX) {
		/* set the operation mode to functional */
		serdes_init_params.operating_mode = CSL_SERDES_FUNCTIONAL_MODE;
		csl_retval = CSL_HyperlinkSerdesInit(serdes_init_params.base_addr,
				serdes_init_params.ref_clock,
				serdes_init_params.linkrate);
		if (csl_retval != 0)
		{
			mpm_printf(1, "Invalid Serdes Init Params: %d\n", csl_retval);
			goto error_return;
		}
		serdes_init_params.iteration_mode = CSL_SERDES_LANE_ENABLE_COMMON_INIT;
		lane_retval = CSL_SerdesLaneEnable(&serdes_init_params);
		if (lane_retval != 0)
		{
			mpm_printf (1, "Invalid Serdes Common Init\n");
			goto error_return;
		}
	}
	if(direction_bitmap & SERDES_DIRECTION_RX ) {

		/* Check lane status */
		csl_retval = CSL_SerdesGetSigDetStatus(serdes_init_params.base_addr,
			CSL_SERDES_MAX_LANES, serdes_init_params.lane_mask,
			serdes_init_params.phy_type);

		/* NOTE: In case of loopback the SigDetStatus is not a reliable indicator
		 *	for rx side already enabled. This will result in force signal det low
		 *	every time 
		 */
		if ( csl_retval && !(direction_bitmap & SERDES_DIRECTION_RX_RESET) 
		     && !(loopback == CSL_SERDES_LOOPBACK_ENABLED)) {
			/* exit with success : Serdes rx already operational */
			return 0;
		}

		/* Force Signal detect low for a fresh start */
		CSL_Serdes_Force_Signal_Detect_Low(serdes_init_params.base_addr,
		  serdes_init_params.num_lanes, serdes_init_params.lane_mask);
		i = 0;

		serdes_init_params.iteration_mode = CSL_SERDES_LANE_ENABLE_LANE_INIT_NO_WAIT;
		do {
			/* Enable serdes lanes */
			lane_retval = CSL_SerdesLaneEnable(&serdes_init_params);
			if (timeout) {
				if (i >= timeout) {
					break;
				}
				i++;
				usleep(1000);
			}
		} while ((lane_retval != CSL_SERDES_LANE_ENABLE_NO_ERR));
		if (lane_retval != CSL_SERDES_LANE_ENABLE_NO_ERR)
		{
			mpm_printf (1, "Invalid Serdes Lane Enable Init. lane_retval=%d\n",
			  lane_retval);
			goto error_return;
		}
	}
	internal_hyplnk_munmap((void *)baseAddr, 0x2000);
	return 0;
error_return:
	internal_hyplnk_munmap((void *)baseAddr, 0x2000);
	return -1; 
}

void hyplnkSerdesDisable(mpm_transport_hyplnk_t *hypCfg)
{
	uint32_t baseAddr, serdes_regs = 0;
	int index = hypCfg->hyplnkIF;
	if (hypCfg->fd[index] == -1) { 
		mpm_printf (1, "Error: Invalid fd \n");
		return;
	}
	if (index == 0)
		serdes_regs = CSL_HYPERLINK_0_SERDES_CFG_REGS;
	else if (index == 1)
		serdes_regs = CSL_HYPERLINK_1_SERDES_CFG_REGS;
	baseAddr = (uint32_t) internal_hyplnk_mmap(2, serdes_regs, 0x2000, hypCfg->fd[index]);
	CSL_SERDES_SHUTDOWN(baseAddr, hypCfg->hyplnkNumLanes);
	internal_hyplnk_munmap( (void *)baseAddr, 0x2000);
}

hyplnkRet_e hyplnkPeripheralSetup (mpm_transport_hyplnk_t *hypCfg, int index,
	uint32_t timeout, int reset, int interface_mode)
{
	hyplnkRevReg_t            rev;
	hyplnkControlReg_t        control;
	hyplnkStatusReg_t         status;
	hyplnkLinkStatusReg_t     linkStatus;
	hyplnkECCErrorsReg_t      ECCErrors;
	hyplnkLanePwrMgmtReg_t    lanePwrMgmt;
	hyplnkSERDESControl1Reg_t serdesControl1;
	hyplnkTXAddrOvlyReg_t TXAddrOvly;
	hyplnkRegisters_t setCtrlRegs;
	hyplnkRegisters_t setErrorRegs;
	hyplnkRegisters_t setRegs;
	hyplnkRegisters_t getRegs;
	hyplnkRet_e retVal;
	Hyplnk_Handle handle = NULL;
	uint32_t i = 0;
	int ecc_error_occurred = 0;

	sem_init(&hyperlink_sem[index], 0, 1);

	/* To prove the APIs work -- expose uninitialized variables */
	memset (&rev,            0xff, sizeof(rev));
	memset (&control,        0xff, sizeof(control));
	memset (&status,         0xff, sizeof(status));
	memset (&linkStatus,     0xff, sizeof(linkStatus));
	memset (&ECCErrors,      0xff, sizeof(ECCErrors));
	memset (&lanePwrMgmt,    0xff, sizeof(lanePwrMgmt));
	memset (&serdesControl1, 0xff, sizeof(serdesControl1));
	memset (&TXAddrOvly, 0, sizeof(TXAddrOvly));

	/* Will always write these regs when writing */
	memset (&setRegs, 0, sizeof(setRegs));

	setRegs.control        = &control;
	setRegs.status         = &status;
	setRegs.ECCErrors      = &ECCErrors;
	setRegs.lanePwrMgmt    = &lanePwrMgmt;
	setRegs.serdesControl1 = &serdesControl1;
	setRegs.TXAddrOvly     = &TXAddrOvly;

	/* Prepare structure for setCtrlRegs */
	memset (&setCtrlRegs, 0, sizeof(setCtrlRegs));
	setCtrlRegs.control    = &control;

	/* Will always read these regs when reading */
	memset (&getRegs, 0, sizeof(getRegs));

	getRegs.status         = &status;
	getRegs.linkStatus     = &linkStatus;
	getRegs.lanePwrMgmt    = &lanePwrMgmt;
	getRegs.ECCErrors      = &ECCErrors;
	getRegs.serdesControl1 = &serdesControl1;

	getRegs.control        = &control;
	getRegs.rev            = &rev;
	getRegs.status         = &status;
	getRegs.linkStatus     = &linkStatus;
	getRegs.lanePwrMgmt    = &lanePwrMgmt;
	getRegs.ECCErrors      = &ECCErrors;
	getRegs.serdesControl1 = &serdesControl1;

	if ((retVal = Hyplnk_open((int)hypCfg->hyplnkIF, &handle)) != hyplnk_RET_OK) {
		mpm_printf(1, "Open failed\n");
		return retVal;
	}

	/* Read all regs  */
	if ((retVal = Hyplnk_readRegs (handle, hyplnk_LOCATION_LOCAL, &getRegs)) != hyplnk_RET_OK) {
		mpm_printf(1, "Read revision register failed!\n");
		return retVal;
	}

	if (reset)
	{
		memset (&setRegs, 0, sizeof(setRegs));
		setRegs.control        = &control;

		/* if we need to stop serial traffic */
		/* Stop traffic before reset */
		control.serialStop  = 1;
		if ((retVal = Hyplnk_writeRegs (handle, hyplnk_LOCATION_LOCAL, &setRegs)) != hyplnk_RET_OK) {
			mpm_printf(1, "Control register write failed!\n");
			return retVal;
		}

		memset (&getRegs, 0, sizeof(getRegs));
		getRegs.status         = &status;

		/* Wait for traffic to stop */
		for (i = 0; i < timeout; i++) {
			if ((retVal = Hyplnk_readRegs (handle, hyplnk_LOCATION_LOCAL, &getRegs)) != hyplnk_RET_OK) {
				mpm_printf(1, "Read registers failed!\n");
				return retVal;
			}
			if (status.rPend == 0) {
				break; /* done waiting */
			}
			usleep(1000);
		}

		memset (&setRegs, 0, sizeof(setRegs));
		setRegs.control        = &control;
		setRegs.status         = &status;
		setRegs.ECCErrors      = &ECCErrors;
		setRegs.lanePwrMgmt    = &lanePwrMgmt;
		setRegs.serdesControl1 = &serdesControl1;

		/* Reset the peripheral */
		control.reset       = 1;
		status.lError       = 1; /* Clear any error */
		status.rError       = 1; /* Clear any error */
		ECCErrors.sglErrCor = 0;
		ECCErrors.dblErrDet = 0;
		lanePwrMgmt.singleLane = 0;
		lanePwrMgmt.zeroLane   = 0;
		lanePwrMgmt.quadLane   = 1;
		serdesControl1.sleepCnt   = 0xff;
		serdesControl1.disableCnt = 0xff;
		if ((retVal = Hyplnk_writeRegs (handle, hyplnk_LOCATION_LOCAL, &setRegs)) != hyplnk_RET_OK) {
			mpm_printf(1, "[1] Control register write failed!\n");
			return retVal;
		}
		if ((retVal = Hyplnk_close (&handle)) != hyplnk_RET_OK) {
			 mpm_printf(1, "close failed!\n");
			return retVal;
		}
		return hyplnk_RET_OK;
	}

	/* do serdes hyperlink setup */
	if (hyplnkActiveIF[index].slavesActive == 1) {
		if (hypCfg->serdesInit) {
			if (interface_mode) {
				if (hyplnkSerdesSetup(hypCfg, SERDES_DIRECTION_TX,
					timeout) == -1) {
					mpm_printf(1, "Failed to setup serdes for hyperlink 0\n");
					return -1;
				}
			} else {
				if (hyplnkSerdesSetup(hypCfg, 
					 (SERDES_DIRECTION_TX + SERDES_DIRECTION_RX),
					 timeout) == -1) {
					mpm_printf(1, "Failed to setup serdes for hyperlink 0\n");
					return -1;
				}
			}
		} else {
			if (hyplnkSerdesSetup(hypCfg, SERDES_DIRECTION_RX, timeout) == -1) {
				mpm_printf(1, "Failed to setup serdes for hyperlink 0\n");
				return -1;
			}
		}
	}

	if (!interface_mode) {
		/* enable serial traffic */
			memset (&setRegs, 0, sizeof(setRegs));

			setRegs.control        = &control;
			setRegs.TXAddrOvly     = &TXAddrOvly;

			TXAddrOvly.txSecOvl    = 0;
			TXAddrOvly.txPrivIDOvl = hypCfg->hyplnkTxPrivID;
			TXAddrOvly.txIgnMask   = 10;
			/* Take out of reset loopback */
			control.serialStop  = 0;

			control.reset           = 0;
			/* enable interrupt on lerror or rerror */
			control.statusIntEnable = 1;
			control.intLocal        = 1;
			control.iLoop           = (int) hypCfg->hyplnkDir;
			retVal = Hyplnk_writeRegs(handle, hyplnk_LOCATION_LOCAL,
				&setRegs);
			if (retVal != hyplnk_RET_OK) {
				mpm_printf(1, "Control register write failed!\n");
				return retVal;
			}

		memset (&getRegs, 0, sizeof(getRegs));

		getRegs.status         = &status;
		getRegs.linkStatus     = &linkStatus;
		getRegs.ECCErrors      = &ECCErrors;

		/* Prepare structure for setErrorRegs */
		memset (&setErrorRegs, 0, sizeof(setErrorRegs));
		setErrorRegs.ECCErrors = &ECCErrors;
                setErrorRegs.status    = &status;
                setErrorRegs.control   = &control;

		i=0;
		do
		{
			if ((retVal = Hyplnk_readRegs (handle, hyplnk_LOCATION_LOCAL, &getRegs)) != hyplnk_RET_OK) {
				mpm_printf(1, "Read status failed!\n");
				return retVal;
			}
			if ((status.link) && !(status.serialHalt) && !(status.pllUnlock) 
			  && !(linkStatus.txRSync) && (linkStatus.txPlsOK) ) {
				if (ECCErrors.sglErrCor || ECCErrors.dblErrDet) {
					ecc_error_occurred++;

					/* If error found reset error and try again */
				
					control.reset       = 1;
					control.serialStop  = 1;
					status.lError       = 1; /* Clear any error */
					status.rError       = 1; /* Clear any error */
					ECCErrors.sglErrCor = 0;
					ECCErrors.dblErrDet = 0;

					/* Set Reset, serial stop hyplnk and reset errors */
					if ((retVal = Hyplnk_writeRegs (handle, hyplnk_LOCATION_LOCAL,
					  &setErrorRegs)) != hyplnk_RET_OK) {
						mpm_printf(1, "Error register write failed!\n");
						return retVal;
					}

					/* Bring hyperlink out of reset */
					control.reset       = 0;
					if ((retVal = Hyplnk_writeRegs (handle, hyplnk_LOCATION_LOCAL,
					  &setErrorRegs)) != hyplnk_RET_OK) {
						mpm_printf(1, "Error register write failed!\n");
						return retVal;
					}

					/* Reinitialize serdes Rx */
					if (hyplnkSerdesSetup(hypCfg,
					  SERDES_DIRECTION_RX | SERDES_DIRECTION_RX_RESET,
					  timeout) == -1) {
						mpm_printf(1, "Failed to setup serdes for hyperlink 0\n");
						return -1;
					}

					control.serialStop  = 0;
					if ((retVal = Hyplnk_writeRegs (handle, hyplnk_LOCATION_LOCAL,
					  &setErrorRegs)) != hyplnk_RET_OK) {
						mpm_printf(1, "Error register write failed!\n");
						return retVal;
					}
					usleep(1000);
					i++;
					continue;
				}
				break;
			}
			usleep(1000);
			i++;

		} while ((timeout == 0) || (i < timeout));
	}

	if ((retVal = Hyplnk_close (&handle)) != hyplnk_RET_OK) {
		mpm_printf(1, "close failed!\n");
		return retVal;
	}

	if (!(interface_mode) && (i >= timeout) && (timeout != 0))
	{
		mpm_printf(1, "Timeout exceeded for connection, exiting...\n");
		mpm_printf(1,
			   "Timeout: (status.link=%d) (status.serialHalt=%d) (status.pllUnlock=%d) (linkStatus.txRSync=%d) (linkStatus.txPlsOK=%d) (ecc_error_occurred=%d)...\n",
			   status.link, status.serialHalt, status.pllUnlock, linkStatus.txRSync, 
			   linkStatus.txPlsOK, ecc_error_occurred);
		return hyplnk_RET_INV_INITCFG;
	}

	return hyplnk_RET_OK;
}

int mpm_transport_hyplnk_init_peripheral(mpm_transport_hyplnk_t *hypCfg,
	int index, interface_mode_t interface_mode, uint32_t timeout)
{
	hyplnkRet_e retVal;

	uint32_t dataBase_size = ((hypCfg->hyplnkIF == hyplnk_PORT_0)? 0x10000000 : 0x08000000);
	hyplnkActiveIF[index].physWindowBase = (uint32_t) hyplnkInitCfg.dev.bases[index].dataBase;
	hyplnkInitCfg.dev.bases[index].cfgBase = internal_hyplnk_mmap(0, (uint32_t)(hyplnkInitCfg.dev.bases[index].cfgBase), 0x100, hypCfg->fd[index]);
	hyplnkInitCfg.dev.bases[index].dataBase = internal_hyplnk_mmap(1, (uint32_t)(hyplnkInitCfg.dev.bases[index].dataBase), dataBase_size, hypCfg->fd[index]);
	hyplnkActiveIF[index].windowBase = (uint32_t) hyplnkInitCfg.dev.bases[index].dataBase;
	hyplnkActiveIF[index].configBase = (uint32_t) hyplnkInitCfg.dev.bases[index].cfgBase;

	if ((retVal = Hyplnk_init (&hyplnkInitCfg)) != hyplnk_RET_OK)
	{
			mpm_printf(1, "Hyperlink LLD device configuration failed\n");
			return (-1);
	}

	if(hypCfg->serdesInit) {
		if ((retVal = hyplnkPeripheralSetup(hypCfg, index, timeout, 1, interface_mode)) != hyplnk_RET_OK)
		{
			mpm_printf(1, "Peripheral init reset failed, retVal = %d\n",retVal);
			return -1;
		}
	}

	if ((retVal = hyplnkPeripheralSetup(hypCfg, index, timeout, 0, interface_mode)) != hyplnk_RET_OK)
	{
		mpm_printf(1, "Peripheral setup failed, retVal = %d\n",retVal);
		return -1;
	}
	return 0;
}

int mpm_transport_hyplnk_configure_interrupt_events(mpm_transport_hyplnk_t *hypCfg)
{
	Hyplnk_Handle handle = (Hyplnk_Handle) hyplnkActiveIF[(int)hypCfg->hyplnkIF].configBase;
	hyplnkRet_e retVal;
	hyplnkControlReg_t        control;
	hyplnkStatusReg_t         status;
	hyplnkIntCtrlIdxReg_t     intCtrlIdx;     /**< @brief Interrupt Control Index.  See also @ref intCtrlTbl */
	hyplnkIntCtrlValReg_t     intCtrlVal;
	hyplnkRegisters_t setRegs;
	memset (&control, 0, sizeof(control));
	memset (&status, 0, sizeof(status));
	memset (&setRegs, 0, sizeof(setRegs));
	setRegs.control        = &control;
	setRegs.status         = &status;
	int i;

	/* Note that this is configuring the hyperlink module to recieve interrupt */
	if ((retVal = Hyplnk_readRegs (handle, hyplnk_LOCATION_LOCAL, &setRegs)) != hyplnk_RET_OK) {
		mpm_printf(1, "Control register read failed! %x\n", retVal);
		return retVal;
	}

	memset (&status, 0, sizeof(status));
	control.intLocal = hypCfg->intLocal;
	control.statusIntEnable = 1;
	control.int2cfg = 1;

	if ((retVal = Hyplnk_writeRegs (handle, hyplnk_LOCATION_LOCAL, &setRegs)) != hyplnk_RET_OK) {
		mpm_printf(1, "Control register write failed! %x\n", retVal);
		return retVal;
	}

	/* Note the following statements configure hyperlink interrupts */
	for(i=0; i<32; i++) {
		if(hypCfg->eventBitmap & (1u << i)) {
			memset (&control, 0, sizeof(control));
			memset (&setRegs, 0, sizeof(setRegs));
			setRegs.status         = &status;
			setRegs.intCtrlIdx = &intCtrlIdx;
			setRegs.intCtrlVal = &intCtrlVal;

			if ((retVal = Hyplnk_readRegs (handle, hyplnk_LOCATION_LOCAL, &setRegs)) != hyplnk_RET_OK) {
				mpm_printf(1, "Control register write %d: intctrl failed! %x\n",i, retVal);
				return retVal;
			}
			memset (&status, 0, sizeof(status));
			intCtrlIdx.intCtrlIdx = i;
			intCtrlVal.intEn = 1;
			intCtrlVal.SIEN = 1;
			intCtrlVal.vector = i;

			if ((retVal = Hyplnk_writeRegs (handle, hyplnk_LOCATION_LOCAL, &setRegs)) != hyplnk_RET_OK) {
				mpm_printf(1, "Control register write %d: intctrl failed! %x\n",i, retVal);
				return retVal;
			}
		}
	}
	return(0);
}

int mpm_transport_hyplnk_reset(mpm_transport_hyplnk_t *hypCfg, int index)
{
	hyplnkControlReg_t        control;
	hyplnkStatusReg_t         status;
	hyplnkRegisters_t setRegs;
	memset (&control, 0, sizeof(control));
	memset (&status, 0, sizeof(status));
	memset (&setRegs, 0, sizeof(setRegs));
	setRegs.control        = &control;
	setRegs.status         = &status;
	hyplnkRet_e retVal;
	Hyplnk_Handle handle = NULL;
	uint32_t dataBase_size;


	if (hypCfg->serdesInit) {
		if ((retVal = Hyplnk_open((int)hypCfg->hyplnkIF, &handle)) != hyplnk_RET_OK) {
			mpm_printf(1, "Open failed\n");
			return retVal;
		}
		memset (&control, 0, sizeof(control));
		do{
				/* Read status and control registers */
				if ((retVal = Hyplnk_readRegs (handle, hyplnk_LOCATION_LOCAL, &setRegs)) != hyplnk_RET_OK) {
				mpm_printf(1, "Read status and control register failed!\n");
				}
		} while(setRegs.status->rPend);

		control.serialStop = 1;
		if ((retVal = Hyplnk_writeRegs (handle, hyplnk_LOCATION_LOCAL, &setRegs)) != hyplnk_RET_OK) {
			mpm_printf(1, "Control register write failed!\n");
			return retVal;
		}

		memset (&control, 0, sizeof(control));
		do{
				/* Read rev and control */
				if ((retVal = Hyplnk_readRegs (handle, hyplnk_LOCATION_LOCAL, &setRegs)) != hyplnk_RET_OK) {
				mpm_printf(1, "Read status and control  register failed!\n");
				}
		} while(setRegs.status->rPend);

		control.reset = 1;
		if ((retVal = Hyplnk_writeRegs (handle, hyplnk_LOCATION_LOCAL, &setRegs)) != hyplnk_RET_OK) {
			mpm_printf(1, "Control register write failed!\n");
			return retVal;
		}
		Hyplnk_close(&handle);

		if (hyplnkActiveIF[hypCfg->hyplnkIF].slavesActive == 0)
		{
			hyplnkSerdesDisable(hypCfg);
		}
	}
	dataBase_size = ((hypCfg->hyplnkIF == hyplnk_PORT_0)? 0x10000000 : 0x08000000);
	if (hypCfg->hyplnkIF == hyplnk_PORT_0)
	{
		if ( hyplnkInitCfg.dev.bases[hypCfg->hyplnkIF].cfgBase != (void *)CSL_HYPERLINK_0_SLV_CFG_REGS)
		{
			internal_hyplnk_munmap(hyplnkInitCfg.dev.bases[hypCfg->hyplnkIF].cfgBase,
				0x100);
			hyplnkInitCfg.dev.bases[hypCfg->hyplnkIF].cfgBase = (void *)CSL_HYPERLINK_0_SLV_CFG_REGS;
		} else {
			mpm_printf(1, "Double reset! 1\n");
		}
		if ( hyplnkInitCfg.dev.bases[hypCfg->hyplnkIF].dataBase != (void *)CSL_HYPERLINK_0_SLV_DATA)
		{
			internal_hyplnk_munmap(hyplnkInitCfg.dev.bases[hypCfg->hyplnkIF].cfgBase,
				dataBase_size);
			hyplnkInitCfg.dev.bases[hypCfg->hyplnkIF].dataBase = (void *)CSL_HYPERLINK_0_SLV_DATA;
		} else {
			mpm_printf(1, "Double reset! 2\n");
		}
	}
	else if (hypCfg->hyplnkIF == hyplnk_PORT_1)
	{
		if ( hyplnkInitCfg.dev.bases[hypCfg->hyplnkIF].cfgBase != (void *)CSL_HYPERLINK_1_SLV_CFG_REGS)
		{
			internal_hyplnk_munmap(hyplnkInitCfg.dev.bases[hypCfg->hyplnkIF].cfgBase,
				0x100);
			hyplnkInitCfg.dev.bases[hypCfg->hyplnkIF].cfgBase = (void *)CSL_HYPERLINK_1_SLV_CFG_REGS;
		} else {
			mpm_printf(1, "Double reset! 3\n");
		}
		if ( hyplnkInitCfg.dev.bases[hypCfg->hyplnkIF].dataBase != (void *)CSL_HYPERLINK_1_SLV_DATA)
		{
			internal_hyplnk_munmap(hyplnkInitCfg.dev.bases[hypCfg->hyplnkIF].cfgBase,
				dataBase_size);
			hyplnkInitCfg.dev.bases[hypCfg->hyplnkIF].dataBase = (void *)CSL_HYPERLINK_1_SLV_DATA;
		} else {
			mpm_printf(1, "Double reset! 4\n");
		}
	}
	return hyplnk_RET_OK;
}

void * mpm_transport_hyplnk_get_window(mpm_transport_hyplnk_t *hypCfg, uint32_t remote_addr, int index, uint32_t *offset)
{
#if TIME_PROFILE_HYPLNK_GET_WINDOW
	struct timespec tp_start, tp_end;
#endif
	hyplnkRet_e retVal;
	Hyplnk_Handle handle = (Hyplnk_Handle) hyplnkActiveIF[(int)hypCfg->hyplnkIF].configBase;
	hyplnkRXAddrSelReg_t  RXAddrSel;
	hyplnkRXPrivIDIdxReg_t RXPrivIDIdx;
	hyplnkRXPrivIDValReg_t RXPrivIDVal;
	hyplnkRXSegIdxReg_t RXSegIdx;
	hyplnkRXSegValReg_t RXSegVal;
	hyplnkRegisters_t     remoteRegs;
	uint32_t globalAddr = (uint32_t)remote_addr;
	uint32_t globalAddrBase, globalAddrOffset;
	uint32_t offsetMask = 0;
	uint32_t reg_sel_shift = 0;


	memset (&remoteRegs, 0, sizeof(remoteRegs));
	memset (&RXAddrSel,  0, sizeof(RXAddrSel));
	memset (&RXPrivIDIdx,  0, sizeof(hyplnkRXPrivIDIdxReg_t));
	memset (&RXPrivIDVal,  0, sizeof(hyplnkRXPrivIDValReg_t));
	memset (&RXSegIdx,  0, sizeof(hyplnkRXSegIdxReg_t));
	memset (&RXSegVal,  0, sizeof(hyplnkRXSegValReg_t));

	/* localRegs.TXAddrOvly   = &TXAddrOvly; */
	remoteRegs.RXAddrSel   = &RXAddrSel;
	remoteRegs.RXPrivIDIdx = &RXPrivIDIdx;
	remoteRegs.RXPrivIDVal = &RXPrivIDVal;
	remoteRegs.RXSegIdx    = &RXSegIdx;
	remoteRegs.RXSegVal    = &RXSegVal;

	RXAddrSel.rxSecHi     = 0;
	RXAddrSel.rxSecLo     = 0;
	RXAddrSel.rxSecSel    = 0;
	RXAddrSel.rxPrivIDSel = hypCfg->hyplnkRxPrivID; /* Symmetric with TXAddrOvly.txPrivIDOvl */
	RXAddrSel.rxSegSel    = hypCfg->hyplnkSegSel;

	if (hypCfg->hyplnkSegSel > 0)
		offsetMask = (1<< (hypCfg->hyplnkSegSel+16)) - 1;
	else
		offsetMask = 0xFFFFFFFF;

	globalAddrBase   = globalAddr & ~offsetMask;
	if (globalAddrBase == 0)
	{
		globalAddrBase = globalAddr;
		globalAddrOffset = globalAddr & 0xFFFF;
	}
	else
		globalAddrOffset = globalAddr &  offsetMask;

	/* Setting RX PrivID index 8 to value 14.
		Allows for 36-bit transfers to Keystone1 boards
		from ARM (priv 8) to use hyperlink priv id 14 */
	RXPrivIDIdx.rxPrivIDIdx = 8;
	RXPrivIDVal.rxPrivIDVal = 14;

	RXSegIdx.rxSegIdx = index;
	RXSegVal.rxLenVal = hypCfg->hyplnkRxLen;
	RXSegVal.rxSegVal = globalAddrBase >> 16;
#if TIME_PROFILE_HYPLNK_GET_WINDOW
	clock_gettime(CLOCK_MONOTONIC, &tp_start);
#endif
	if ((retVal = Hyplnk_writeRegs (handle, hyplnk_LOCATION_REMOTE, &remoteRegs)) != hyplnk_RET_OK) {
		mpm_printf(1, "mpm_transport_hyplnk_get_window: Remote register write failed!\n");
		return NULL;
	}
#if TIME_PROFILE_HYPLNK_GET_WINDOW
	clock_gettime(CLOCK_MONOTONIC, &tp_end);
	mpm_printf(1, "[TIME PROFILE: mpm_transport_hyplnk_get_window] Time taken for Hyplnk_writeRegs() to Remote: %lld\n",clock_diff (&tp_start, &tp_end));
#endif

	if (hypCfg->hyplnkSegSel)
	{
		reg_sel_shift = index << (hypCfg->hyplnkSegSel + 16);
	}
	if (reg_sel_shift >= 0x10000000)
	{
		mpm_printf(1, "Index out of bounds, please use smaller segment sizes and a lower rxseg value\n");
		return NULL;
	}
	*offset = globalAddrOffset + reg_sel_shift;

	mpm_printf(2, "index is 0x%08x\n",index);
	mpm_printf(2, "globalAddr is 0x%08x\n", globalAddr);
	mpm_printf(2, "globalAddrBase is 0x%08x\n", globalAddrBase);
	mpm_printf(2, "globalAddrOffset is 0x%08x\n", globalAddrOffset);
	mpm_printf(2, "reg_sel_shift is 0x%08x\n", reg_sel_shift);

	return (void *)(hyplnkActiveIF[(int)hypCfg->hyplnkIF].windowBase);
}

int mpm_transport_hyplnk_interface_read_and_ack_interrupt_event(mpm_transport_hyplnk_t *hypCfg,
	uint32_t event_bitmap, uint32_t *reported_event_bitmap )
{
	Hyplnk_Handle handle = (Hyplnk_Handle) hyplnkActiveIF[(int)hypCfg->hyplnkIF].configBase;
	hyplnkRet_e retVal;
	hyplnkIntStatusClrReg_t   intStatusClr;
	hyplnkStatusReg_t         status;
	hyplnkIntCtrlIdxReg_t     intCtrlIdx;     /**< @brief Interrupt Control Index.  See also @ref intCtrlTbl */
	hyplnkIntCtrlValReg_t     intCtrlVal;
	hyplnkRegisters_t setRegs;
	memset (&intStatusClr, 0, sizeof(intStatusClr));
	memset (&status, 0, sizeof(status));
	memset (&setRegs, 0, sizeof(setRegs));
	setRegs.intStatusClr = &intStatusClr;
	setRegs.status = &status;

	sem_wait(&hyperlink_sem[(int)hypCfg->hyplnkIF]);
	/* Note that this is configuring the hyperlink module to recieve interrupt */
	if ((retVal = Hyplnk_readRegs (handle, hyplnk_LOCATION_LOCAL, &setRegs)) != hyplnk_RET_OK) {
		mpm_printf(1, "Control register read failed! %x\n", retVal);
		sem_post(&hyperlink_sem[(int)hypCfg->hyplnkIF]);
		return retVal;
	}

	*reported_event_bitmap = intStatusClr.intClr;
	if (*reported_event_bitmap & event_bitmap) {
		intStatusClr.intClr &= event_bitmap;

		memset (&status, 0, sizeof(status));

		/* The write will clear the interrupt */

		if ((retVal = Hyplnk_writeRegs (handle, hyplnk_LOCATION_LOCAL, &setRegs)) != hyplnk_RET_OK) {
			mpm_printf(1, "Control register write failed! %x\n", retVal);
			sem_post(&hyperlink_sem[(int)hypCfg->hyplnkIF]);
			return retVal;
		}
	}
	sem_post(&hyperlink_sem[(int)hypCfg->hyplnkIF]);
	return(0);
}

int mpm_transport_hyplnk_interface_generate_interrupt_event(mpm_transport_hyplnk_t *hypCfg,
	uint32_t event_bitmap)
{
	Hyplnk_Handle handle = (Hyplnk_Handle) hyplnkActiveIF[(int)hypCfg->hyplnkIF].configBase;
	hyplnkRet_e retVal;
	hyplnkGenSoftIntReg_t     genSoftInt;
	hyplnkStatusReg_t         status;
	hyplnkRegisters_t setRegs;
	int i;

	memset (&genSoftInt, 0, sizeof(genSoftInt));
	memset (&setRegs, 0, sizeof(setRegs));
	setRegs.genSoftInt = &genSoftInt;
	setRegs.status = &status;

	for(i=0; i < 32; i++) {
		if( event_bitmap & ( 1<<i)) {
			memset (&status, 0, sizeof(status));
			genSoftInt.iVector = i;

			/* The write will generate interrupt */
			sem_wait(&hyperlink_sem[(int)hypCfg->hyplnkIF]);
			if ((retVal = Hyplnk_writeRegs (handle, hyplnk_LOCATION_LOCAL, &setRegs)) != hyplnk_RET_OK) {
				mpm_printf(1, "Control register write failed! %x\n", retVal);
				sem_post(&hyperlink_sem[(int)hypCfg->hyplnkIF]);
				return retVal;
			}
			sem_post(&hyperlink_sem[(int)hypCfg->hyplnkIF]);
		}

	}
	return(0);
}
