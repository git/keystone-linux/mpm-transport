/*
 * Copyright (C) 2013-2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <malloc.h>
#include <errno.h>
#include <semaphore.h>

#include "mpm_transport_hyplnk.h"
#include "mpm_transport_hyplnk_interface.h"
#include "uioutils.h"

#define MAX_DEVICE_NAME_LEN 32
#define MAX_FDS 32

/* Semaphores for multi thread protection */
static sem_t context_sem;
static sem_t mmap_sem;
static sem_t mpax_sem;

/* Only one set of config can be active at one time. Keep track of it globally */
hyplnk_peripheral hyplnkActiveIF[NUM_INTERFACES] = 
{
	{
		.name = "",
		.slavesActive = 0,
		.hyp_fd  = -1,
	},
	{
		.name = "",
		.slavesActive = 0,
		.hyp_fd  = -1,
	},
};
typedef struct uio_fd_list_s {
	int fd_index;
	int fd_list[MAX_FDS];
	uint32_t event_bitmap[MAX_FDS];
} uio_fd_list_t;
uio_fd_list_t mpm_transport_uio_fd_list[NUM_INTERFACES];

int edma3InstInit[NUM_EDMA3_INSTANCES];

/* Prototype definition of functions */
int mpm_transport_hyplnk_window_map64(mpm_transport_cfg_t *sp, uint64_t addr, uint32_t length, mpm_transport_mmap_t *mcfg);

static int mpm_transport_get_global_addr (mpm_transport_cfg_t *sp, uint32_t laddr, uint32_t size, uint32_t *gaddr, int *index)
{
	int i;

	if ((CSL_MSMC_CFG_REGS <= laddr) && (laddr + size <= CSL_MSMC_CFG_REGS + 0x100000) ) {
		*gaddr = laddr;
		return 0;
	}

	for (i = 0; i < sp->num_mmap; i++) {
		if (sp->mmap[i].local_addr <= laddr &&
			sp->mmap[i].local_addr + sp->mmap[i].length >= laddr + size) {
			*gaddr = sp->mmap[i].global_addr + laddr - sp->mmap[i].local_addr;
			*index = i;
			return 0;
		}
	}

	for (i = 0; i < sp->num_mmap; i++) {
		if (sp->mmap[i].global_addr <= laddr &&
			sp->mmap[i].global_addr + sp->mmap[i].length >= laddr + size) {
			*gaddr = laddr;
			*index = i;
			return 0;
		}
	}

	mpm_printf(1, "can't find global address from local address 0x%x size %d in cfg for %s\n",
		laddr, size, sp->name);
	return -1;
}

int mpm_transport_hyplnk_unregister_slave(mpm_transport_cfg_t *sp)
{
	mpm_transport_hyplnk_t *td = (mpm_transport_hyplnk_t *) sp->td;
	if (hyplnkActiveIF[td->hyplnkIF].slavesActive == 1)
	{
		strncpy(hyplnkActiveIF[td->hyplnkIF].name, "", MPM_MAX_NAME_LENGTH);
		hyplnkActiveIF[td->hyplnkIF].slavesActive = 0;
		hyplnkActiveIF[td->hyplnkIF].hyplnkSegments = 0;
		return 1;
	}
	else
	{
		hyplnkActiveIF[td->hyplnkIF].slavesActive -= 1;
		return 0;
	}
	return 0;
}

int mpm_transport_hyplnk_register_slave(mpm_transport_cfg_t *sp)
{
	mpm_transport_hyplnk_t *td = (mpm_transport_hyplnk_t *) sp->td;
	if (strlen(hyplnkActiveIF[td->hyplnkIF].name) < 2)
	{
		/* First time init for this slave. Populate hyplnkActiveIF */
		hyplnkActiveIF[td->hyplnkIF].slavesActive = 1;
		hyplnkActiveIF[td->hyplnkIF].hyplnkSegments = 0;
		if (strcmp(td->hyplnkSettings,"")==0)
			strncpy(hyplnkActiveIF[td->hyplnkIF].name, sp->name, sizeof(hyplnkActiveIF[td->hyplnkIF].name));
		else
			strncpy(hyplnkActiveIF[td->hyplnkIF].name, td->hyplnkSettings, sizeof(hyplnkActiveIF[td->hyplnkIF].name));
		mpm_printf(2, "Slave %s registered setting name %s\n",sp->name, hyplnkActiveIF[td->hyplnkIF].name);
		return 1;
	}
	else
	{
		if (strcmp(hyplnkActiveIF[td->hyplnkIF].name, sp->name) == 0)
		{
			/* Another slave already used you as the master */
			hyplnkActiveIF[td->hyplnkIF].slavesActive += 1;
			return 0;
		}
		else
		{
			if (strcmp(hyplnkActiveIF[td->hyplnkIF].name, td->hyplnkSettings) == 0)
			{
				/* hyplnkmaster already opened */
				hyplnkActiveIF[td->hyplnkIF].slavesActive += 1;
				return 0;
			}
			else
			{
				/* This hyplnk port is already opened with another set of settings */
				mpm_printf(1, "Error! Close slave: %s!\n",hyplnkActiveIF[td->hyplnkIF].name);
				return -1;
			}
		}
	}
	return 0;
}

int hyplnk_open_uio_device(int port)
{
	int ret;
	char class_name[MAX_FILE_NAME_LENGTH];
	char dev_name[MAX_NAME_LENGTH];

	if (port == 0)
		ret = uioutil_get_peripheral_device("hyperlink0", class_name, 32);
	else
		ret = uioutil_get_peripheral_device("hyperlink1", class_name, 32);
	if ( ret < 0 ) {
		mpm_printf(1, "\n Error getting device ");
		return(-1);
	}
	snprintf(dev_name, MAX_NAME_LENGTH, "/dev/%s", class_name);

	ret = open (dev_name, (O_RDWR | O_SYNC));
	if (ret < 0) {
		mpm_printf(1, "\n Error opening device ");
		return(-1);
	}

	return(ret);

} /* hyplnk_open_uio_device */

int mpm_transport_hyplnk_open(mpm_transport_cfg_t *sp, mpm_transport_open_t *ocfg)
{
	int index = 0, reg, ret = 0;
	mpm_transport_hyplnk_t *td = (mpm_transport_hyplnk_t *) sp->td;

	if (!Hyplnk_init) {
		mpm_printf(1, "mpm_transport_hyplnk_open(): Error!! Hyperlink init function null, please check if library was linked in correctly\n");
		return -1;
	}

	/* Initialise multi thread semaphores */
	sem_init(&context_sem, 0, 1);
	sem_init(&mmap_sem, 0, 1);
	sem_init(&mpax_sem, 0, 1);

	reg = mpm_transport_hyplnk_register_slave(sp);
	if (reg < 0) return -1;

	index = td->hyplnkIF;
	if (index == 0) {
		if (hyplnkActiveIF[index].hyp_fd == -1) {
			if (uioutil_check_device_exist("hyperlink0") == 1) {
				hyplnkActiveIF[index].hyp_fd = open("/dev/hyperlink0", ocfg->open_mode);
				if (hyplnkActiveIF[index].hyp_fd <= 0) return -1;
			}
			else hyplnkActiveIF[index].hyp_fd = -1;
		}
	}

	if (index == 1) {
		if (hyplnkActiveIF[index].hyp_fd == -1) {
			if (uioutil_check_device_exist("hyperlink1") == 1) {
				hyplnkActiveIF[index].hyp_fd = open("/dev/hyperlink1", ocfg->open_mode);
				if (hyplnkActiveIF[index].hyp_fd <= 0) return -1;
			}
			else hyplnkActiveIF[index].hyp_fd = -1;
		}
		else hyplnkActiveIF[1].hyp_fd = -1;
	}
	if (hyplnkActiveIF[index].hyp_fd == -1) return -1;

	td->fd[index] = hyplnkActiveIF[index].hyp_fd;
	td->serdesInit = ocfg->serdes_init;

	if (reg) {
		hyplnkActiveIF[index].uio_fd = hyplnk_open_uio_device(index);
		if (mpm_transport_hyplnk_init_peripheral(td, index, 
			MPM_TRANSPORT_INTERFACE_MODE_TX_AND_RX,
			ocfg->msec_timeout) != 0)
		{
			mpm_printf(1, "mpm_transport_hyplnk_init_peripheral failed!\n");
			ret = -1;
		}

		if (strlen(sp->edma3_name) > 1)
		{
			if (edma3InstInit[sp->edma3_inst] == 0)
			{
				mpm_printf(2, "Initialzing edma3 for instance %d\n",sp->edma3_inst);
				edma3InstInit[sp->edma3_inst] = 1;
				mpm_transport_edma3_init_cfg(sp->edma3_inst);
			}

			sp->hEdma[sp->edma3_inst] = mpm_transport_edma3_setup(sp->edma3_inst, sp->shadow_reg);
			mpm_printf(2, "Got hEdma = 0x%08x\n",(uint32_t)sp->hEdma[sp->edma3_inst]);
			if (sp->hEdma[sp->edma3_inst] == 0)
			{
				mpm_printf(1, "mpm_transport_edma3_setup failed!\n");
				ret = -1;
			}
		}
		if (ret)
		{
			mpm_printf(1, "Peripheral init failed for: %s\n", sp->name);
			mpm_transport_hyplnk_reset(td, index);
			close(hyplnkActiveIF[index].uio_fd);
			mpm_transport_hyplnk_unregister_slave(sp);
			return -1;
		}
	}

	if(td->intEnable) {
		mpm_transport_hyplnk_configure_interrupt_events(td);
	}

	if (strlen(sp->edma3_name) > 1)
	{
		if (sp->edma3_prealloc > 0) {
			for (index=0; index<sp->edma3_prealloc; index++) {
				mpm_transport_edma3_prealloc_ch(sp->hEdma[sp->edma3_inst],index,&(sp->edma3_ch[index]),&(sp->edma3_tcc[index]));
			}
		}
	}
	sp->msmc_cfg_index = -1;
	sp->rhMpax = NULL;

	return 0;

}

int mpm_transport_hyplnk_window_map(mpm_transport_cfg_t *sp, uint32_t addr, uint32_t length, mpm_transport_mmap_t *mcfg)
{
#if TIME_PROFILE_HYPLNK_WINDOW_MAP
	struct timespec tp_start, tp_end;
#endif
	int index;
	int user_index;
	uint32_t gaddr;
	uint32_t hlink_offset;
	uint8_t seg_idx = 0;
	mpm_transport_hyplnk_t *td = (mpm_transport_hyplnk_t *) sp->td;
	uint32_t seg_len, seg_offset;

#if TIME_PROFILE_HYPLNK_WINDOW_MAP
	clock_gettime(CLOCK_MONOTONIC, &tp_start);
#endif
	sem_wait(&mmap_sem);
	/* Find a free mmap index to use */
	for (user_index = 0; user_index < MPM_MAX_USER_MMAPS; user_index++) {
		if (!td->mmap_user[user_index].addr) {
			break;
		}
	}

	if (user_index >= MPM_MAX_USER_MMAPS) {
		mpm_printf(1, "exceeded maximum user mmap limits (%d)\n",
			MPM_MAX_USER_MMAPS);
		sem_post(&mmap_sem);
		return -1;
	}

	if(mpm_transport_get_global_addr(sp, addr, length, &gaddr, &index)) {
		mpm_printf(1, "can't find global address for local address 0x%x\n", addr);
		sem_post(&mmap_sem);
		return -1;
	}

	gaddr = addr;
	for (index=0; index<32; index++) {
		if ((hyplnkActiveIF[td->hyplnkIF].hyplnkSegments & (1<<index)) == 0) {
			seg_idx = index;
			break;
		}
	}

#if TIME_PROFILE_HYPLNK_WINDOW_MAP
	clock_gettime(CLOCK_MONOTONIC, &tp_end);
	mpm_printf(1, "[TIME PROFILE: mpm_transport_hyplnk_window_map] Time taken for setup: %lld\n",clock_diff (&tp_start, &tp_end));
#endif

#if TIME_PROFILE_HYPLNK_WINDOW_MAP
	clock_gettime(CLOCK_MONOTONIC, &tp_start);
#endif
	td->mmap_user[user_index].addr = mpm_transport_hyplnk_get_window(td, gaddr, seg_idx, &hlink_offset);
	if (td->mmap_user[user_index].addr == NULL) {
		mpm_printf(1, "can't mmap for the address 0x%x with length 0x%x (err: %s)\n",
			addr, length, strerror(errno));
		sem_post(&mmap_sem);
		return -1;
	}
	else {
		if (td->hyplnkRxLen < 8)
			seg_len = 0;
		else
			seg_len = 1<<(td->hyplnkRxLen+1);
		/* get the segment offset from the hlink_offset using (seg_len-1) as the mask */
		seg_offset = hlink_offset & (seg_len-1);
		mpm_printf(2, "length=0x%x, hyplnkRxLen=%d\n", length, td->hyplnkRxLen);
		mpm_printf(2, "seg_len=0x%x, hlink_offset=0x%x, seg_offset=0x%x\n", seg_len, hlink_offset, seg_offset);
		if ((seg_len-seg_offset) < length)
		{
			td->mmap_user[user_index].addr = NULL;
			mpm_printf(1, "write segment size (0x%x) larger than the available receive segement size (0x%x)\n", length, (seg_len-seg_offset));
   		        sem_post(&mmap_sem);
			return -1;
		} else
		{
		    hyplnkActiveIF[td->hyplnkIF].hyplnkSegments |= (1<<index);
	        }
	}
#if TIME_PROFILE_HYPLNK_WINDOW_MAP
	clock_gettime(CLOCK_MONOTONIC, &tp_end);
	mpm_printf(1, "[TIME PROFILE: mpm_transport_hyplnk_window_map] Time taken for get_window(): %lld\n",clock_diff (&tp_start, &tp_end));
#endif
	td->mmap_user[user_index].hyplnk_idx = seg_idx;
	td->mmap_user[user_index].size = length;
	td->mmap_user[user_index].addr_usr = td->mmap_user[user_index].addr + hlink_offset;
	td->mmap_user[user_index].phy_addr = hyplnkActiveIF[td->hyplnkIF].physWindowBase + hlink_offset;
	sem_post(&mmap_sem);

	return user_index;
}

void *mpm_transport_hyplnk_mmap(mpm_transport_cfg_t *sp, uint32_t addr, uint32_t length, mpm_transport_mmap_t *mcfg)
{
	int index;
	mpm_transport_hyplnk_t *td = (mpm_transport_hyplnk_t *) sp->td;
	index = mpm_transport_hyplnk_window_map(sp, addr, length, mcfg);
	if (index < 0)
	{
		mpm_printf(1, "hyperlink window mapping error!\n");
		return MAP_FAILED;
	}
	mpm_printf(2, "[debug] index is %d\n",index);
	mpm_printf(2, "[debug] addr is 0x%08x\n", (uint32_t) td->mmap_user[index].addr);
	mpm_printf(2, "[debug] addr_usr is 0x%08x\n",(uint32_t) td->mmap_user[index].addr_usr);
	mpm_printf(2, "[debug] phy_addr is 0x%08x\n",td->mmap_user[index].phy_addr);
	return (td->mmap_user[index].addr_usr);
}


void *mpm_transport_hyplnk_mmap64(mpm_transport_cfg_t *sp, uint64_t addr, uint32_t length, mpm_transport_mmap_t *mcfg)
{
	int index;
	mpm_transport_hyplnk_t *td = (mpm_transport_hyplnk_t *) sp->td;
	index = mpm_transport_hyplnk_window_map64(sp, addr, length, mcfg);
	if (index < 0)
	{
		mpm_printf(1, "hyperlink window map64 mapping error!\n");
		return MAP_FAILED;
	}
	mpm_printf(2, "[debug] index is %d\n",index);
	mpm_printf(2, "[debug] addr is 0x%08x\n", (uint32_t) td->mmap_user[index].addr);
	mpm_printf(2, "[debug] addr_usr is 0x%08x\n",(uint32_t) td->mmap_user[index].addr_usr);
	mpm_printf(2, "[debug] phy_addr is 0x%08x\n",td->mmap_user[index].phy_addr);
	return (td->mmap_user[index].addr_usr);
}

uint32_t mpm_transport_hyplnk_phys_map(mpm_transport_cfg_t *sp, uint32_t addr, uint32_t length, mpm_transport_mmap_t *mcfg)
{
	int index;
	mpm_transport_hyplnk_t *td = (mpm_transport_hyplnk_t *) sp->td;
	index = mpm_transport_hyplnk_window_map(sp, addr, length, mcfg);
	if (index < 0)
	{
		mpm_printf(1, "hyperlink window mapping error!\n");
		return 0;
	}
	mpm_printf(2, "[debug] index is %d\n",index);
	mpm_printf(2, "[debug] addr is 0x%08x\n", (uint32_t) td->mmap_user[index].addr);
	mpm_printf(2, "[debug] addr_usr is 0x%08x\n",(uint32_t) td->mmap_user[index].addr_usr);
	mpm_printf(2, "[debug] phy_addr is 0x%08x\n",td->mmap_user[index].phy_addr);
	return (td->mmap_user[index].phy_addr);
}

static void clear_mmap_entry(mpm_transport_hyplnk_t *td, int user_index)
{
	hyplnkActiveIF[td->hyplnkIF].hyplnkSegments &= ~(1<<td->mmap_user[user_index].hyplnk_idx);
	td->mmap_user[user_index].addr = 0;
	td->mmap_user[user_index].size = 0;
	td->mmap_user[user_index].addr_usr = 0;
	td->mmap_user[user_index].phy_addr = 0;
	td->mmap_user[user_index].logical_addr = 0;
	td->mmap_user[user_index].hyplnk_idx = 0;
}

int mpm_transport_hyplnk_munmap(mpm_transport_cfg_t *sp, void *va, uint32_t length)
{
	int user_index;
	mpm_transport_hyplnk_t *td = (mpm_transport_hyplnk_t *) sp->td;

	sem_wait(&mmap_sem);
	for (user_index = 0; user_index < MPM_MAX_USER_MMAPS; user_index++) {
		if (td->mmap_user[user_index].addr_usr == va) {
			break;
		}
	}

	if (user_index >= MPM_MAX_USER_MMAPS) {
		mpm_printf(1, "can't find mapping to unmap\n");
		sem_post(&mmap_sem);
		return -1;
	}
	clear_mmap_entry(td, user_index);
	sem_post(&mmap_sem);

	return 0;
}

static void mpm_transport_hyplnk_rw_unmap(mpm_transport_cfg_t *sp, int user_index, uint32_t length) 
{
	int logical_addr;
	mpm_transport_hyplnk_t *td = (mpm_transport_hyplnk_t *) sp->td;

	/* Record logical address */
	logical_addr = td->mmap_user[user_index].logical_addr;
	if (logical_addr)
		mpm_transport_keystone_unmap(sp->rhMpax, logical_addr, length);

	clear_mmap_entry(td, user_index);
}

int mpm_transport_hyplnk_munmap64(mpm_transport_cfg_t *sp, void *va, uint32_t length)
{
	int user_index;
	mpm_transport_hyplnk_t *td = (mpm_transport_hyplnk_t *) sp->td;

	sem_wait(&mmap_sem);

	/* Find entry of virtual address in list */
	for (user_index = 0; user_index < MPM_MAX_USER_MMAPS; user_index++) {
		if (td->mmap_user[user_index].addr_usr == va) {
			break;
		}
	}

	if (user_index >= MPM_MAX_USER_MMAPS) {
		mpm_printf(1, "can't find mapping to unmap\n");
		sem_post(&mmap_sem);
		return -1;
	}
	mpm_transport_hyplnk_rw_unmap(sp, user_index, length);
	sem_post(&mmap_sem);

	return 0;
}

int mpm_transport_hyplnk_phys_unmap(mpm_transport_cfg_t *sp, void *va, uint32_t length)
{
	int user_index;
	mpm_transport_hyplnk_t *td = (mpm_transport_hyplnk_t *) sp->td;

	sem_wait(&mmap_sem);
	for (user_index = 0; user_index < MPM_MAX_USER_MMAPS; user_index++) {
		if (td->mmap_user[user_index].phy_addr == (uint32_t) va) {
			break;
		}
	}

	if (user_index >= MPM_MAX_USER_MMAPS) {
		mpm_printf(1, "can't find mapping to unmap\n");
		sem_post(&mmap_sem);
		return -1;
	}
	mpm_transport_hyplnk_rw_unmap(sp, user_index, length);
	sem_post(&mmap_sem);
	return 0;
}

void *mpm_transport_hyplnk_rw_map (mpm_transport_cfg_t *sp, uint32_t addr, uint32_t length, int *index)
{
	mpm_transport_hyplnk_t *td = (mpm_transport_hyplnk_t *) sp->td;
	mpm_transport_mmap_t mcfg = {
		.mmap_prot	= (PROT_READ|PROT_WRITE),
		.mmap_flags	= MAP_SHARED,
	};

	*index = mpm_transport_hyplnk_window_map(sp, addr, length, &mcfg);
	if (*index < 0)
	{
		mpm_printf(1, "hyperlink window mapping error!\n");
		return 0;
	}
	return (td->mmap_user[*index].addr_usr);
}

void *mpm_transport_hyplnk_rw_map64 (mpm_transport_cfg_t *sp, uint64_t addr, uint32_t length, int *index)
{
	mpm_transport_hyplnk_t *td = (mpm_transport_hyplnk_t *) sp->td;
	mpm_transport_mmap_t mcfg = {
		.mmap_prot	= (PROT_READ|PROT_WRITE),
		.mmap_flags	= MAP_SHARED,
	};

	*index = mpm_transport_hyplnk_window_map64(sp, addr, length, &mcfg);
	if (*index < 0)
	{
		mpm_printf(1, "hyperlink window mapping error!\n");
		return 0;
	}
	return (td->mmap_user[*index].addr_usr);
}

int mpm_transport_hyplnk_read(mpm_transport_cfg_t *sp, uint32_t addr, uint32_t length, char *buf, mpm_transport_read_t *rcfg)
{
	int index = 0;
	void *mmap_addr;

	mmap_addr = mpm_transport_hyplnk_rw_map (sp, addr, length, &index);

	if (!mmap_addr) {
		mpm_printf(1, "can't map for address 0x%x\n", addr);
		return -1;
	}
	memcpy(buf, mmap_addr, length);
	sem_wait(&mmap_sem);
	mpm_transport_hyplnk_rw_unmap(sp, index, length);
	sem_post(&mmap_sem);
	return 0;
}

int mpm_transport_hyplnk_write(mpm_transport_cfg_t *sp, uint32_t addr, uint32_t length, char *buf, mpm_transport_write_t *cfg)
{
	int index = 0;
	void *mmap_addr;

	mmap_addr = mpm_transport_hyplnk_rw_map (sp, addr, length, &index);

	if (!mmap_addr) {
		mpm_printf(1, "can't map for address 0x%x\n", addr);
		return -1;
	}
	memcpy(mmap_addr, buf, length);
	sem_wait(&mmap_sem);
	mpm_transport_hyplnk_rw_unmap(sp, index, length);
	sem_post(&mmap_sem);
	return 0;
}

int mpm_transport_hyplnk_read64(mpm_transport_cfg_t *sp, uint64_t addr, uint32_t length, char *buf, mpm_transport_read_t *rcfg)
{
	int index = 0;
	void *mmap_addr;

	mmap_addr = mpm_transport_hyplnk_rw_map64 (sp, addr, length, &index);
	if (!mmap_addr) {
		mpm_printf(1, "can't map for address 0x%x\n", addr);
		return -1;
	}

	memcpy(buf, mmap_addr, length);
	sem_wait(&mmap_sem);
	mpm_transport_hyplnk_rw_unmap(sp, index, length);
	sem_post(&mmap_sem);
	return 0;
}

int mpm_transport_hyplnk_write64(mpm_transport_cfg_t *sp, uint64_t addr, uint32_t length, char *buf, mpm_transport_write_t *cfg)
{
	int index = 0;
	void *mmap_addr;

	mmap_addr = mpm_transport_hyplnk_rw_map64 (sp, addr, length, &index);

	if (!mmap_addr) {
		mpm_printf(1, "can't map for address 0x%x\n", addr);
		return -1;
	}
	memcpy(mmap_addr, buf, length);
	sem_wait(&mmap_sem);
	mpm_transport_hyplnk_rw_unmap(sp, index, length);
	sem_post(&mmap_sem);
	return 0;
}


mpm_transport_trans_h mpm_transport_hyplnk_get_initiate(mpm_transport_cfg_t *sp, uint32_t from_addr, uint32_t to_addr, uint32_t length, bool is_blocking, mpm_transport_read_t *cfg)
{
#if TIME_PROFILE_HYPLNK_GET_INITIATE
	struct timespec tp_start, tp_end;
#endif
	mpm_transport_trans_t *tp = NULL;
	int i;
	void *mmap_addr;
	mpm_transport_mmap_t mcfg = {
		.mmap_prot	= (PROT_READ|PROT_WRITE),
		.mmap_flags	= MAP_SHARED,
	};
	for (i = 0; i < MPM_MAX_DMA_TRANS; i++)
	{
		if (sp->ta[i].is_active == 0)
			break;
	}
	if (i == MPM_MAX_DMA_TRANS)
	{
		mpm_printf(1, "MAX DMA Transfers! Call mpm_transport_transfer_check to wait/ccomplete pending transfers\n");
		return MPM_TRANSPORT_TRANS_FAIL;
	}
	tp = (mpm_transport_trans_t *) &(sp->ta[i]);
	tp->key =  (i+3) * (i+3) * 2;
	tp->is_active = 1;

#if TIME_PROFILE_HYPLNK_GET_INITIATE
	clock_gettime(CLOCK_MONOTONIC, &tp_start);
#endif
	mmap_addr = (void *) mpm_transport_hyplnk_phys_map (sp, from_addr, length, &mcfg);
	if (!mmap_addr) {
		mpm_printf(1, "can't map for address 0x%x\n", to_addr);
		tp->is_active = 0;
		return MPM_TRANSPORT_TRANS_FAIL;
	}
#if TIME_PROFILE_HYPLNK_GET_INITIATE
	clock_gettime(CLOCK_MONOTONIC, &tp_end);
	mpm_printf(1, "[TIME PROFILE: mpm_transport_hyplnk_get_initiate] Time taken for phys_map: %lld\n",clock_diff (&tp_start, &tp_end));
#endif

	tp->trans_addr = (uint32_t) mmap_addr;

#if TIME_PROFILE_HYPLNK_GET_INITIATE
	clock_gettime(CLOCK_MONOTONIC, &tp_start);
#endif
	mpm_printf(2, "mpm_transport_get_initiate: getting address 0x%08x, to 0x%08x\n", from_addr, to_addr);
	if (mpm_transport_edma3_transfer(sp->hEdma[sp->edma3_inst], 
		(uint32_t) mmap_addr, to_addr, length, is_blocking, tp) < 0){
		mpm_printf(1, "Edma transfer failed \n");
		mpm_transport_hyplnk_phys_unmap(sp, mmap_addr, 0);
		tp->is_active = 0;
		return MPM_TRANSPORT_TRANS_FAIL;
	}
	mpm_printf(2, "hEdma = 0x%08x, Channel id: 0x%08x\n", (uint32_t) sp->hEdma[sp->edma3_inst], tp->chan_id);
#if TIME_PROFILE_HYPLNK_GET_INITIATE
	clock_gettime(CLOCK_MONOTONIC, &tp_end);
	mpm_printf(1, "[TIME PROFILE: mpm_transport_hyplnk_get_initiate] Time taken for edma3_transfer(): %lld\n",clock_diff (&tp_start, &tp_end));
#endif
	if (is_blocking == true) mpm_transport_hyplnk_phys_unmap(sp, mmap_addr, 0);
	return tp;
}

mpm_transport_trans_h mpm_transport_hyplnk_put_initiate(mpm_transport_cfg_t *sp, uint32_t to_addr, uint32_t from_addr, uint32_t length, bool is_blocking, mpm_transport_write_t *cfg)
{
#if TIME_PROFILE_HYPLNK_PUT_INITIATE
	struct timespec tp_start, tp_end;
#endif
	mpm_transport_trans_t *tp = NULL;
	int i;
	void *mmap_addr;
	mpm_transport_mmap_t mcfg = {
		.mmap_prot	= (PROT_READ|PROT_WRITE),
		.mmap_flags	= MAP_SHARED,
	};
	for (i = 0; i < MPM_MAX_DMA_TRANS; i++)
	{
		if (sp->ta[i].is_active == 0)
			break;
	}
	if (i == MPM_MAX_DMA_TRANS)
	{
		mpm_printf(1, "MAX DMA Transfers! Call mpm_transport_transfer_check to wait/ccomplete pending transfers\n");
		return MPM_TRANSPORT_TRANS_FAIL;
	}
	tp = (mpm_transport_trans_t *) &(sp->ta[i]);
	tp->key = (i+1) * (i+1) * 2;
	tp->is_active = 1;

#if TIME_PROFILE_HYPLNK_PUT_INITIATE
	clock_gettime(CLOCK_MONOTONIC, &tp_start);
#endif
	mmap_addr = (void *) mpm_transport_hyplnk_phys_map (sp, to_addr, length, &mcfg);
	if (!mmap_addr) {
		mpm_printf(1, "can't map for address 0x%x\n", to_addr);
		tp->is_active = 0;
		return MPM_TRANSPORT_TRANS_FAIL;
	}
	tp->trans_addr = (uint32_t) mmap_addr;
#if TIME_PROFILE_HYPLNK_PUT_INITIATE
	clock_gettime(CLOCK_MONOTONIC, &tp_end);
	mpm_printf(1, "[TIME PROFILE: mpm_transport_hyplnk_put_initiate] Time taken for phys_map: %lld\n",clock_diff (&tp_start, &tp_end));
#endif

#if TIME_PROFILE_HYPLNK_PUT_INITIATE
	clock_gettime(CLOCK_MONOTONIC, &tp_start);
#endif
	mpm_printf(2, "mpm_transport_put_initiate:"
		" writing to address 0x%08x, from 0x%08x\n", to_addr, from_addr);
	if (mpm_transport_edma3_transfer(sp->hEdma[sp->edma3_inst], from_addr,
		 (uint32_t) mmap_addr, length, is_blocking, tp) < 0) {
		mpm_printf(1, "Edma transfer failed \n");
		mpm_transport_hyplnk_phys_unmap(sp, mmap_addr, 0);
		tp->is_active = 0;
		return MPM_TRANSPORT_TRANS_FAIL;
	}
	mpm_printf(2, "hEdma = 0x%08x, Channel id: 0x%08x\n", (uint32_t) sp->hEdma[sp->edma3_inst], tp->chan_id);
#if TIME_PROFILE_HYPLNK_PUT_INITIATE
	clock_gettime(CLOCK_MONOTONIC, &tp_end);
	mpm_printf(1, "[TIME PROFILE: mpm_transport_hyplnk_put_initiate] Time taken for edma3_transfer(): %lld\n",clock_diff (&tp_start, &tp_end));
#endif

	if (is_blocking == true) {
		mpm_transport_hyplnk_phys_unmap(sp, mmap_addr, 0);
		tp->is_active = 0;
		return NULL;
	}
	return tp;
}

mpm_transport_trans_h mpm_transport_hyplnk_get_initiate_linked(mpm_transport_cfg_t *sp, uint32_t num_links, uint32_t from_addr[], uint32_t to_addr[], uint32_t length[], bool is_blocking, mpm_transport_read_t *cfg)
{
#if TIME_PROFILE_HYPLNK_GET_INITIATE_LINKED
	struct timespec tp_start, tp_end;
#endif
	mpm_transport_trans_t *tp = NULL;
	mpm_transport_trans_t *trav = NULL;
	int i;
	uint32_t mmap_addr[num_links];
	mpm_transport_mmap_t mcfg = {
		.mmap_prot	= (PROT_READ|PROT_WRITE),
		.mmap_flags	= MAP_SHARED,
	};
	for (i = 0; i < MPM_MAX_DMA_TRANS; i++)
	{
		if (sp->ta[i].is_active == 0)
			break;
	}
	if (i == MPM_MAX_DMA_TRANS)
	{
		mpm_printf(1, "MAX DMA Transfers! Call mpm_transport_transfer_check to wait/ccomplete pending transfers\n");
		return MPM_TRANSPORT_TRANS_FAIL;
	}
	tp = (mpm_transport_trans_t *) &(sp->ta[i]);
	trav = tp;
	tp->key =  (i+3) * (i+3) * 2;
	tp->is_active = 1;

	for (i = 0; i<num_links; i++)
	{
#if TIME_PROFILE_HYPLNK_GET_INITIATE_LINKED
		clock_gettime(CLOCK_MONOTONIC, &tp_start);
#endif
		mmap_addr[i] = (uint32_t) mpm_transport_hyplnk_phys_map (sp, from_addr[i],
			length[i], &mcfg);
		if (!mmap_addr[i]) {
			mpm_printf(1, "can't map for address 0x%x\n", from_addr[i]);
			goto error;
		}
#if TIME_PROFILE_HYPLNK_GET_INITIATE_LINKED
		clock_gettime(CLOCK_MONOTONIC, &tp_end);
		mpm_printf(1, "[TIME PROFILE: mpm_transport_hyplnk_get_initiate_linked] Time taken for phys_map: %lld, iteration: %d\n",clock_diff (&tp_start, &tp_end),i);
#endif
	}

#if TIME_PROFILE_HYPLNK_GET_INITIATE_LINKED
	clock_gettime(CLOCK_MONOTONIC, &tp_start);
#endif

	if (mpm_transport_edma3_transfer_linked(sp->hEdma[sp->edma3_inst],
		sp->edma3_ch, sp->edma3_prealloc, sp->edma3_tcc[0], num_links, mmap_addr,
		to_addr, length, is_blocking, tp) < 0){
		mpm_printf(1, "Edma transfer linked failed \n");
		goto error;
	}
#if TIME_PROFILE_HYPLNK_GET_INITIATE_LINKED
	clock_gettime(CLOCK_MONOTONIC, &tp_end);
	mpm_printf(1, "[TIME PROFILE: mpm_transport_hyplnk_get_initiate_linked] Time taken for edma3_transfer_linked(): %lld\n",clock_diff (&tp_start, &tp_end));
#endif

	/* Record the addresses used so we can unmap later */
	i = 0;
	do {
		if (i<num_links) trav->trans_addr = (uint32_t) mmap_addr[i];
		else trav->trans_addr = 0;
		trav->local_logical_addr = 0;
		trav = (mpm_transport_trans_t *) trav->chain;
		i++;
	} while (trav != NULL);

	if (is_blocking == true) while(mpm_transport_hyplnk_transfer_check(sp, tp) != 1);
	return tp;
error:
	for (i--; i>=0; i--) {
		mpm_transport_hyplnk_phys_unmap(sp, (void *) mmap_addr[i], 0);	
	}
	tp->is_active = 0;		
	return MPM_TRANSPORT_TRANS_FAIL;
}

mpm_transport_trans_h mpm_transport_hyplnk_put_initiate_linked(mpm_transport_cfg_t *sp, uint32_t num_links, uint32_t to_addr[], uint32_t from_addr[], uint32_t length[], bool is_blocking, mpm_transport_write_t *cfg)
{
#if TIME_PROFILE_HYPLNK_PUT_INITIATE_LINKED
	struct timespec tp_start, tp_end;
#endif
	mpm_transport_trans_t *tp = NULL;
	mpm_transport_trans_t *trav = NULL;
	int i;
	uint32_t mmap_addr[num_links];
	mpm_transport_mmap_t mcfg = {
		.mmap_prot	= (PROT_READ|PROT_WRITE),
		.mmap_flags	= MAP_SHARED,
	};
	for (i = 0; i < MPM_MAX_DMA_TRANS; i++)
	{
		if (sp->ta[i].is_active == 0)
			break;
	}
	if (i == MPM_MAX_DMA_TRANS)
	{
		mpm_printf(1, "MAX DMA Transfers! Call mpm_transport_transfer_check to wait/ccomplete pending transfers\n");
		return MPM_TRANSPORT_TRANS_FAIL;
	}
	tp = (mpm_transport_trans_t *) &(sp->ta[i]);
	trav = tp;
	tp->key = (i+1) * (i+1) * 2;
	tp->is_active = 1;

	for (i = 0; i<num_links; i++)
	{
#if TIME_PROFILE_HYPLNK_PUT_INITIATE_LINKED
	clock_gettime(CLOCK_MONOTONIC, &tp_start);
#endif
	mmap_addr[i] = (uint32_t) mpm_transport_hyplnk_phys_map (sp, to_addr[i], length[i], &mcfg);
	if (!mmap_addr[i]) {
		mpm_printf(1, "can't map for address 0x%x\n", to_addr[i]);
		goto error;

	}
#if TIME_PROFILE_HYPLNK_PUT_INITIATE_LINKED
	clock_gettime(CLOCK_MONOTONIC, &tp_end);
	mpm_printf(1, "[TIME PROFILE: mpm_transport_hyplnk_put_initiate_linked] Time taken for phys_map: %lld, iteration: %d\n",clock_diff (&tp_start, &tp_end),i);
#endif
	}

#if TIME_PROFILE_HYPLNK_PUT_INITIATE_LINKED
	clock_gettime(CLOCK_MONOTONIC, &tp_start);
#endif
	/* mpm_printf(1, "EDMA3 DBG: sp->edma3_ch %d, sp->edma3_prealloc %d, sp->edma3_tcc[0] %d\n",sp->edma3_ch, sp->edma3_prealloc, sp->edma3_tcc[0]); */
	if (mpm_transport_edma3_transfer_linked(sp->hEdma[sp->edma3_inst],sp->edma3_ch, sp->edma3_prealloc, sp->edma3_tcc[0], num_links, from_addr, mmap_addr, length, is_blocking, tp) < 0){
		mpm_printf(1, "EDMA transfer failed\n");
		goto error;

	}
#if TIME_PROFILE_HYPLNK_PUT_INITIATE_LINKED
	clock_gettime(CLOCK_MONOTONIC, &tp_end);
	mpm_printf(1, "[TIME PROFILE: mpm_transport_hyplnk_put_initiate_linked] Time taken for edma3_transfer_linked(): %lld\n",clock_diff (&tp_start, &tp_end));
#endif

	/* Record the addresses used so we can unmap later */
	i = 0;
	do {
		if (i<num_links) trav->trans_addr = (uint32_t) mmap_addr[i];
		else trav->trans_addr = 0;
		trav->local_logical_addr = 0;
		trav = (mpm_transport_trans_t *) trav->chain;
		i++;
	} while (trav != NULL);

	if (is_blocking == true) while(mpm_transport_hyplnk_transfer_check(sp, tp) != 1);
	return tp;

error:
	for (i--; i>=0; i--) {
		mpm_transport_hyplnk_phys_unmap(sp, (void *) mmap_addr[i], 0);	
	}
	tp->is_active = 0;		
	return MPM_TRANSPORT_TRANS_FAIL;
}

int mpm_transport_hyplnk_transfer_check(mpm_transport_cfg_t *sp, mpm_transport_trans_h th)
{
#if TIME_PROFILE_HYPLNK_TRANSFER_CHECK
	struct timespec tp_start, tp_end;
#endif
	mpm_transport_trans_t *tp = (mpm_transport_trans_t *) th;
	mpm_transport_trans_t *trav = tp;
	mpm_transport_trans_t *travBack = trav;
	int ret = 0;
	int i;

#if TIME_PROFILE_HYPLNK_TRANSFER_CHECK
	clock_gettime(CLOCK_MONOTONIC, &tp_start);
#endif
	for (i = 0; i<MPM_MAX_DMA_TRANS; i++)
	{
		if ((sp->ta[i].is_active == 1) && (sp->ta[i].key == tp->key))
			break;
	}
	if (i == MPM_MAX_DMA_TRANS)
	{
		mpm_printf(1, "Transaction handle with key 0x%08x not found!\n",tp->key);
		return -1;
	}
	mpm_printf(2, "Checking...: hEdma = 0x%08x, chan_id = 0x%08x, tcc_no = 0x%08x\n", (uint32_t) sp->hEdma[sp->edma3_inst], tp->chan_id, tp->tcc_no);
	ret = mpm_transport_edma3_checkAndClearTcc(sp->hEdma[sp->edma3_inst],trav->tcc_no);
#if TIME_PROFILE_HYPLNK_TRANSFER_CHECK
	clock_gettime(CLOCK_MONOTONIC, &tp_end);
	mpm_printf(1, "[TIME PROFILE: mpm_transport_hyplnk_transfer_check] Time taken for edma3_checkAndClearTcc(): %lld\n",clock_diff (&tp_start, &tp_end));
#endif
	if (ret == 1)
	{

#if TIME_PROFILE_HYPLNK_TRANSFER_CHECK
	clock_gettime(CLOCK_MONOTONIC, &tp_start);
#endif
		do {
			if (trav->local_logical_addr) {
				mpm_transport_keystone_unmap(sp->hMpax,trav->local_logical_addr,sp->mpax_size);
			}
			if (trav->trans_addr) mpm_transport_hyplnk_phys_unmap(sp, (void *) trav->trans_addr, 0);
			/* if (trav->key != 0x00aa00bb) mpm_transport_edma3_free_channel(sp->hEdma[sp->edma3_inst], trav->chan_id); */
			trav = (mpm_transport_trans_t *) trav->chain;
		} while (trav != NULL);

		/* Destroying linked transfer chain */
		trav = tp->chain;
		while (trav != NULL) {
			travBack = trav;
			trav = trav->chain;
			free(travBack);
		}

		mpm_printf(2, "Got index %d with key 0x%08x! clearing...\n",i,sp->ta[i].key);
		sp->ta[i].key = 0;
		sp->ta[i].is_active = 0;
		sp->ta[i].trans_addr = 0;
		sp->ta[i].local_logical_addr = 0;
		sp->ta[i].tcc_no = 0;
		sp->ta[i].chain = NULL;
#if TIME_PROFILE_HYPLNK_TRANSFER_CHECK
	clock_gettime(CLOCK_MONOTONIC, &tp_end);
	mpm_printf(1, "[TIME PROFILE: mpm_transport_hyplnk_transfer_check] Time taken for clearing resources: %lld\n",clock_diff (&tp_start, &tp_end));
#endif
	}

	return ret;

}

void mpm_transport_hyplnk_close(mpm_transport_cfg_t *sp)
{
	int i;
	int index, reg;
	mpm_transport_hyplnk_t *td = (mpm_transport_hyplnk_t *) sp->td;

	if (!td) {
		return;
	}

	if (sp->msmc_cfg_index >= 0) {
		clear_mmap_entry(td, sp->msmc_cfg_index);
		sp->msmc_cfg_index = -1;
	}

	index = td->hyplnkIF;
	reg = mpm_transport_hyplnk_unregister_slave(sp);
	if (reg) {
		mpm_transport_hyplnk_reset(td, index);
		close(hyplnkActiveIF[index].uio_fd);
	}

	if (strlen(sp->edma3_name) > 1) {
		if (sp->edma3_prealloc > 0)
			for (i=0; i<sp->edma3_prealloc;i++)
				mpm_transport_edma3_free_channel(sp->hEdma[sp->edma3_inst], sp->edma3_ch[i]);
		mpm_transport_edma3_close(sp->hEdma[sp->edma3_inst], sp->edma3_inst);
	}

	if (sp->hMpax) {
		mpm_transport_keystone_free(sp->hMpax);
	}

	if (sp->rhMpax) {
		mpm_transport_keystone_free(sp->rhMpax);
		sp->rhMpax = NULL;
	}

	for (i = 0; i < MPM_MAX_USER_MMAPS; i++) {
		if(td->mmap_user[i].addr) {
			memset(&(td->mmap_user[i]), 0, sizeof(mmap_data_t));
		}
	}
	for (i = 0; i < MPM_MAX_MMAPS; i++) {
		if(td->mmap_rw[i].addr) {
			memset(&(td->mmap_rw[i]), 0, sizeof(mmap_data_t));
		}
		pthread_mutex_destroy(&td->mutex_rw[i]);
	}

	td->fd[index] = hyplnkActiveIF[index].hyp_fd;
	if (hyplnkActiveIF[index].slavesActive == 0)
	{
		if (hyplnkActiveIF[index].hyp_fd >= 0) close(hyplnkActiveIF[index].hyp_fd);
		hyplnkActiveIF[index].hyp_fd = -1;
		for(i=0; i< MAX_FDS && mpm_transport_uio_fd_list[index].event_bitmap[i] != 0; i++) {
			close( mpm_transport_uio_fd_list[index].fd_list[i] );
			mpm_transport_uio_fd_list[index].fd_list[i] = 0;
			mpm_transport_uio_fd_list[index].event_bitmap[i] = 0;
		}
		///memset(&mpm_transport_uio_fd_list[index], 0, sizeof(uio_fd_list_t));
		///memset(&hyplnkActiveIF[index], 0, sizeof(hyplnk_peripheral));
	}

	sem_destroy(&context_sem);
	sem_destroy(&mmap_sem);
	sem_destroy(&mpax_sem);
	if (td) free(td);
}

int mpm_transport_hyplnk_window_map64(mpm_transport_cfg_t *sp, uint64_t addr, uint32_t length, mpm_transport_mmap_t *mcfg)
{
	int index, logical_addr;
	mpm_transport_hyplnk_t *td = (mpm_transport_hyplnk_t *) sp->td;
	if (((addr>>32)&(0xFFFFFFFF)) == 0)
		index = mpm_transport_hyplnk_window_map(sp, (uint32_t)addr, length, mcfg);
	else
	{
		sem_wait(&mpax_sem);
		if(sp->msmc_cfg_index < 0) {
			index = mpm_transport_hyplnk_window_map(sp, CSL_MSMC_CFG_REGS, 0x100000, mcfg);	
			if (index < 0) {
				mpm_printf(1, "mpm_transport_hyplnk_window_map64: Error mapping to remote MSMC!\n");
				return -1;
			}
			sp->msmc_cfg_index = index;
		}
		if (!(sp->rhMpax)) {
			sp->rhMpax = mpm_transport_keystone_mmap_open(sp->mpax_base, sp->mpax_size,
				sp->mpax_index, sp->mpax_count, td->mmap_user[sp->msmc_cfg_index].phy_addr, 
				td->fd[td->hyplnkIF], 14);
			if(sp->rhMpax == NULL)
				return -1;
		}
		sem_post(&mpax_sem);
		logical_addr = mpm_transport_keystone_mmap(sp->rhMpax, addr, length);
		mpm_printf(2, "Got hyplnk-mapped msmc config logical_addr: 0x%08x\n",logical_addr);

		index = mpm_transport_hyplnk_window_map(sp, logical_addr, length, mcfg);
		if (index < 0) {
			mpm_printf(1, "mpm_transport_hyplnk_window_map64:"
				" Error mapping to remote logical address!\n");
			return -1;
		}
		td->mmap_user[index].logical_addr = logical_addr;
	}
	return(index);
}

uint32_t mpm_transport_hyplnk_phys_map64(mpm_transport_cfg_t *sp, uint64_t addr, uint32_t length, mpm_transport_mmap_t *mcfg)
{
	int index;
	mpm_transport_hyplnk_t *td = (mpm_transport_hyplnk_t *) sp->td;

	index = mpm_transport_hyplnk_window_map64(sp, addr,length, mcfg);
	if (index < 0)
	{
		mpm_printf(1, "hyperlink window mapping error!\n");
		return 0;
	}
	mpm_printf(2, "[debug] index is %d\n",index);
	mpm_printf(2, "[debug] addr is 0x%08x\n", (uint32_t) td->mmap_user[index].addr);
	mpm_printf(2, "[debug] addr_usr is 0x%08x\n",(uint32_t) td->mmap_user[index].addr_usr);
	mpm_printf(2, "[debug] phy_addr is 0x%08x\n",td->mmap_user[index].phy_addr);
	return (td->mmap_user[index].phy_addr);
}

mpm_transport_trans_h mpm_transport_hyplnk_get_initiate64(mpm_transport_cfg_t *sp, uint64_t from_addr, uint64_t to_addr, uint32_t length, bool is_blocking, mpm_transport_read_t *cfg)
{
	mpm_transport_trans_t *tp = NULL;
	int i;
	void *mmap_addr;
	uint32_t logical_addr;

	mpm_printf(2, "Base = 0x%08x\n",(sp->mpax_base));
	mpm_printf(2, "Size = 0x%08x\n",(sp->mpax_size));

	mpm_transport_mmap_t mcfg = {
		.mmap_prot	= (PROT_READ|PROT_WRITE),
		.mmap_flags	= MAP_SHARED,
	};
	for (i = 0; i < MPM_MAX_DMA_TRANS; i++)
	{
		if (sp->ta[i].is_active == 0)
			break;
	}
	if (i == MPM_MAX_DMA_TRANS)
	{
		mpm_printf(1, "MAX DMA Transfers! Call mpm_transport_transfer_check to wait/ccomplete pending transfers\n");
		return MPM_TRANSPORT_TRANS_FAIL;
	}
	tp = (mpm_transport_trans_t *) &(sp->ta[i]);
	tp->key = (i+1) * (i+1) * 2;
	tp->is_active = 1;

	mmap_addr = (void *) mpm_transport_hyplnk_phys_map64 (sp, from_addr, length, &mcfg);
	if (!mmap_addr) {
		mpm_printf(1, "can't map for address to_addr 0x%llx\n", to_addr);
		tp->is_active = 0;
		return MPM_TRANSPORT_TRANS_FAIL;
	}
	mpm_printf(2, "get_initiate64: Got mmap_addr = 0x%08x\n",(uint32_t)mmap_addr);
	tp->trans_addr = (uint32_t) mmap_addr;

	if (!(sp->hMpax)) {
		sp->hMpax = mpm_transport_keystone_mmap_open(sp->mpax_base, sp->mpax_size, 
			sp->mpax_index, sp->mpax_count, 0, 0, 8);
	}
	logical_addr = mpm_transport_keystone_mmap(sp->hMpax, to_addr, length);
	if (logical_addr == 0) {
		mpm_printf(1, "can't map for address from_addr 0x%llx\n", to_addr);
		tp->is_active = 0;
		return MPM_TRANSPORT_TRANS_FAIL;
	}
	mpm_printf(2, "get_initiate64: Got logical = 0x%08x\n",logical_addr);
	tp->local_logical_addr = logical_addr;

	mpm_printf(2, "mpm_transport_get_initiate64: getting address 0x%llx, to 0x%llx\n", from_addr, to_addr);
	if(mpm_transport_edma3_transfer(sp->hEdma[sp->edma3_inst],
		(uint32_t) mmap_addr, logical_addr, length, is_blocking, tp) < 0) {
		mpm_printf(1, "EDMA transfer failed\n");
		mpm_transport_keystone_unmap(sp->hMpax,logical_addr,sp->mpax_size);
		tp->is_active = 0;
		return MPM_TRANSPORT_TRANS_FAIL;
	}

	mpm_printf(2, "hEdma = 0x%08x, Channel id: 0x%08x\n", (uint32_t) sp->hEdma[sp->edma3_inst], tp->chan_id);
	if (is_blocking == true) {
		mpm_transport_hyplnk_phys_unmap(sp, mmap_addr, 0);
		mpm_transport_keystone_unmap(sp->hMpax,logical_addr,sp->mpax_size);
		tp->is_active = 0;
		return NULL;
	}
	return tp;
}

mpm_transport_trans_h mpm_transport_hyplnk_put_initiate64(mpm_transport_cfg_t *sp, uint64_t to_addr, uint64_t from_addr, uint32_t length, bool is_blocking, mpm_transport_write_t *cfg)
{
	mpm_transport_trans_t *tp = NULL;
	int i;
	void *mmap_addr;
	uint32_t logical_addr;

	mpm_printf(2, "Base = 0x%08x\n",(sp->mpax_base));
	mpm_printf(2, "Size = 0x%08x\n",(sp->mpax_size));

	mpm_transport_mmap_t mcfg = {
		.mmap_prot	= (PROT_READ|PROT_WRITE),
		.mmap_flags	= MAP_SHARED,
	};
	for (i = 0; i < MPM_MAX_DMA_TRANS; i++)
	{
		if (sp->ta[i].is_active == 0)
			break;
	}
	if (i == MPM_MAX_DMA_TRANS)
	{
		mpm_printf(1, "MAX DMA Transfers! Call mpm_transport_transfer_check to wait/ccomplete pending transfers\n");
		return MPM_TRANSPORT_TRANS_FAIL;
	}
	tp = (mpm_transport_trans_t *) &(sp->ta[i]);
	tp->key = (i+1) * (i+1) * 2;
	tp->is_active = 1;

	mmap_addr = (void *) mpm_transport_hyplnk_phys_map64 (sp, to_addr, length, &mcfg);
	if (!mmap_addr) {
		mpm_printf(1, "can't map for address to_addr 0x%llx\n", to_addr);
		tp->is_active = 0;
		return MPM_TRANSPORT_TRANS_FAIL;
	}
	mpm_printf(2, "put_initiate64: Got mmap_addr = 0x%08x\n",(uint32_t)mmap_addr);
	tp->trans_addr = (uint32_t) mmap_addr;

	if (!(sp->hMpax)) {
		sp->hMpax = mpm_transport_keystone_mmap_open(sp->mpax_base, sp->mpax_size,
			sp->mpax_index, sp->mpax_count, 0, 0, 8);
	}
	logical_addr = mpm_transport_keystone_mmap(sp->hMpax, from_addr, length);
	if (logical_addr == 0) {
		mpm_printf(1, "can't map for address from_addr 0x%llx\n", from_addr);
		tp->is_active = 0;
		return MPM_TRANSPORT_TRANS_FAIL;
	}
	mpm_printf(2, "put_initiate64: Got logical = 0x%08x\n",logical_addr);
	tp->local_logical_addr = logical_addr;

	mpm_printf(2, "mpm_transport_put_initiate64: writing to address 0x%llx, from 0x%llx\n", to_addr, from_addr);
	if ( mpm_transport_edma3_transfer(sp->hEdma[sp->edma3_inst], logical_addr,
		 (uint32_t) mmap_addr, length, is_blocking, tp) < 0 ) {
		mpm_transport_keystone_unmap(sp->hMpax,logical_addr,sp->mpax_size);
		tp->is_active = 0;
		return MPM_TRANSPORT_TRANS_FAIL;
	}
	mpm_printf(2, "hEdma = 0x%08x, Channel id: 0x%08x\n", (uint32_t) sp->hEdma[sp->edma3_inst], tp->chan_id);
	if (is_blocking == true) {
		mpm_transport_keystone_unmap(sp->hMpax,logical_addr,sp->mpax_size);
		mpm_transport_hyplnk_phys_unmap(sp, mmap_addr, 0);
		tp->is_active = 0;
		return NULL;
	}
	return tp;
}

int mpm_transport_hyplnk_enable_peripheral(mpm_transport_cfg_t *sp,
	mpm_transport_open_t *cfg)
{
	int index = 0, reg, ret = 0;
	mpm_transport_hyplnk_t *td = (mpm_transport_hyplnk_t *) sp->td;

	reg = mpm_transport_hyplnk_register_slave(sp);
	if (reg < 0) return -1;

	td->serdesInit = 1;
	index = td->hyplnkIF;
	if ((hyplnkActiveIF[index].hyp_fd == -1)&&(index==0)) {
		if (uioutil_check_device_exist("hyperlink0") == 1) {
			hyplnkActiveIF[index].hyp_fd = open("/dev/hyperlink0", cfg->open_mode);
			if (hyplnkActiveIF[index].hyp_fd < 0) return -1;
		} else {
			hyplnkActiveIF[index].hyp_fd = -1;
		}
	}
	if ((hyplnkActiveIF[index].hyp_fd == -1)&&(index==1)) {
		if (uioutil_check_device_exist("hyperlink1") == 1) {
			hyplnkActiveIF[index].hyp_fd = open("/dev/hyperlink1", cfg->open_mode);
			if (hyplnkActiveIF[index].hyp_fd < 0) return -1;
		} else {
			hyplnkActiveIF[index].hyp_fd = -1;
		}
	}
	if (hyplnkActiveIF[index].hyp_fd == -1) return -1;
	td->fd[index] = hyplnkActiveIF[index].hyp_fd;

	if (reg)
	{
		hyplnkActiveIF[index].uio_fd = hyplnk_open_uio_device(index);

		if (mpm_transport_hyplnk_init_peripheral(td, index, cfg->interface_mode,
			cfg->msec_timeout) != 0) {
			mpm_printf(1, "mpm_transport_hyplnk_init_peripheral failed!\n");
			mpm_transport_hyplnk_reset(td, index);
			close(hyplnkActiveIF[index].uio_fd);
			mpm_transport_hyplnk_unregister_slave(sp);
			return -1;
		}
	}

	return 0;
}

int mpm_transport_hyplnk_disable_peripheral(mpm_transport_cfg_t *sp)
{
	int index;
	mpm_transport_hyplnk_t *td = (mpm_transport_hyplnk_t *) sp->td;
	int i;

	if (!td) {
		return -1;
	}

	td->serdesInit = 1;
	index = td->hyplnkIF;
	td->fd[index] = hyplnkActiveIF[index].hyp_fd;
	mpm_transport_hyplnk_unregister_slave(sp);
	if (hyplnkActiveIF[index].slavesActive == 0) {
		mpm_transport_hyplnk_reset(td, index);
		close(hyplnkActiveIF[index].uio_fd);
	}

	if (hyplnkActiveIF[index].hyp_fd > 0)
		close(hyplnkActiveIF[index].hyp_fd);

	hyplnkActiveIF[index].hyp_fd = -1;
	for(i=0; i< MAX_FDS && mpm_transport_uio_fd_list[index].event_bitmap[i] != 0; i++) {
		close( mpm_transport_uio_fd_list[index].fd_list[i] );
		mpm_transport_uio_fd_list[index].fd_list[i] = 0;
		mpm_transport_uio_fd_list[index].event_bitmap[i] = 0;
	}
	///memset(&mpm_transport_uio_fd_list[index], 0, sizeof(uio_fd_list_t));
	///memset(&hyplnkActiveIF[index], 0, sizeof(hyplnk_peripheral));

	if (td) free(td);
	return 0;
}

int mpm_transport_hyplnk_get_event_fd(mpm_transport_cfg_t *sp, uint32_t event_bitmap)
{
	mpm_transport_hyplnk_t *td = (mpm_transport_hyplnk_t *) sp->td;
	int i;

	sem_wait(&context_sem);
	for(i=0; i< MAX_FDS; i++) {
		if(mpm_transport_uio_fd_list[td->hyplnkIF].event_bitmap[i] == 0) {
			mpm_transport_uio_fd_list[td->hyplnkIF].event_bitmap[i]= event_bitmap;
			mpm_transport_uio_fd_list[td->hyplnkIF].fd_list[i] = hyplnk_open_uio_device(td->hyplnkIF);
			if (i > mpm_transport_uio_fd_list[td->hyplnkIF].fd_index)
				mpm_transport_uio_fd_list[td->hyplnkIF].fd_index=i;
			sem_post(&context_sem);
			return(mpm_transport_uio_fd_list[td->hyplnkIF].fd_list[i]);
		}
		if(mpm_transport_uio_fd_list[td->hyplnkIF].event_bitmap[i] == event_bitmap) {
			sem_post(&context_sem);
			/* Already fd exists for this: return fd */
			return(mpm_transport_uio_fd_list[td->hyplnkIF].fd_list[i]);
		}
	}

	sem_post(&context_sem);
	mpm_printf(1, "Event fd exceeded Maximum fds: %s\n", sp->name);
	return(-1);
}

int mpm_transport_hyplnk_read_and_ack_interrupt_event(mpm_transport_cfg_t *sp,
	uint32_t event_bitmap, uint32_t *reported_event_bitmap )
{
	mpm_transport_hyplnk_t *td = (mpm_transport_hyplnk_t *) sp->td;

	return(mpm_transport_hyplnk_interface_read_and_ack_interrupt_event
		(td, event_bitmap, reported_event_bitmap));
}
int mpm_transport_hyplnk_generate_interrupt_event(mpm_transport_cfg_t *sp,
	uint32_t event_bitmap)
{
	mpm_transport_hyplnk_t *td = (mpm_transport_hyplnk_t *) sp->td;

	return(mpm_transport_hyplnk_interface_generate_interrupt_event
		(td, event_bitmap));
}
