/*
 * Copyright (C) 2013-2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include <stdint.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <dirent.h>

#include <ti/drv/hyplnk/device/hyplnk_device.h>
#include <ti/csl/cslr_device.h>
#include <ti/csl/csl_pscAux.h>
#include <ti/csl/csl_serdes_hyperlink.h>
#include <ti/csl/csl_serdes_restore_default.h>
#include "mpm_transport_hyplnk.h"

int mpm_transport_hyplnk_init_peripheral(mpm_transport_hyplnk_t *hypCfg,
	int index, interface_mode_t interface_mode, uint32_t timeout);
int mpm_transport_hyplnk_reset(mpm_transport_hyplnk_t *hypCfg, int index);
void * mpm_transport_hyplnk_get_window(mpm_transport_hyplnk_t *hypCfg, uint32_t remote_addr, int index, uint32_t *offset);
int mpm_transport_hyplnk_configure_interrupt_events(mpm_transport_hyplnk_t *hypCfg);
int mpm_transport_hyplnk_interface_read_and_ack_interrupt_event(mpm_transport_hyplnk_t *hypCfg,
	uint32_t event_bitmap, uint32_t *reported_event_bitmap );
int mpm_transport_hyplnk_interface_generate_interrupt_event(mpm_transport_hyplnk_t *hypCfg,
	uint32_t event_bitmap);

int hyplnkSerdesSetup(mpm_transport_hyplnk_t *hypCfg, uint32_t direction_bitmap,
	int timeout);
void hyplnkSerdesDisable(mpm_transport_hyplnk_t *hypCfg);

#ifdef STATIC_BUILD
hyplnkRet_e Hyplnk_open(int portNum, Hyplnk_Handle *pHandle) __attribute__((weak));
hyplnkRet_e Hyplnk_init(Hyplnk_InitCfg *cfg) __attribute__((weak));
hyplnkRet_e Hyplnk_close(Hyplnk_Handle *pHandle) __attribute__((weak));
hyplnkRet_e Hyplnk_readRegs(Hyplnk_Handle handle, hyplnkLocation_e location, hyplnkRegisters_t *readRegs) __attribute__((weak));
hyplnkRet_e Hyplnk_writeRegs(Hyplnk_Handle handle, hyplnkLocation_e location, hyplnkRegisters_t *readRegs) __attribute__((weak));
hyplnkRet_e Hyplnk_getWindow(Hyplnk_Handle  handle, void **base, uint32_t *size) __attribute__((weak));
Hyplnk_InitCfg hyplnkInitCfg __attribute__((weak));
#endif
