/*
 * Copyright (C) 2013-2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef __MPM_TRANSPORT_HYPLNK_H__
#define __MPM_TRANSPORT_HYPLNK_H__

#include <pthread.h>
#include "mpm_transport_cfg.h"
#define MPM_MAX_USER_MMAPS	(16)
#define NUM_INTERFACES (2)
#define MAX_FILE_NAME_LENGTH	128
#define MAX_NAME_LENGTH		64
#define MAX_LINE_LENGTH         16

typedef enum
{
	hyplnk_LOOPBACK = 1,
	hyplnk_REMOTE = 0
}hyplnk_direction;

typedef enum
{
	hyplnk_PORT_0 = 0,
	hyplnk_PORT_1 = 1
}hyplnk_interfaces;

typedef struct mpm_transport_hyplnk_tag {
	int	fd[MPM_MAX_MMAPS];
	mmap_data_t mmap_rw[MPM_MAX_MMAPS];
	pthread_mutex_t mutex_rw[MPM_MAX_MMAPS];
	mmap_data_t mmap_user[MPM_MAX_USER_MMAPS];
	char name[MPM_MAX_NAME_LENGTH];
	uint32_t edma3_inst;
	char hyplnkSettings[MPM_MAX_NAME_LENGTH];
	hyplnk_direction hyplnkDir;
	hyplnk_interfaces hyplnkIF;
	uint32_t hyplnkWindowSize;
	uint32_t hyplnkTxPrivID;
	uint32_t hyplnkRxPrivID;
	uint32_t hyplnkSegSel;
	uint32_t hyplnkRxLen;
	int hyplnkNumLanes;
	int serdesInit;
	uint32_t intEnable;
	uint32_t intLocal;
	uint32_t eventBitmap;
} mpm_transport_hyplnk_t;

typedef struct hyplnk_peripheral {
	char name[MPM_MAX_NAME_LENGTH];
	int hyp_fd;
	uint32_t hyplnkSegments;
	uint32_t slavesActive;
	uint32_t physWindowBase;
	uint32_t windowBase;
	uint32_t configBase;
	uint32_t serdesConfigBase;
	char hypDevice[MPM_MAX_NAME_LENGTH];
	int uio_fd;
} hyplnk_peripheral;

int mpm_transport_hyplnk_open(mpm_transport_cfg_t *sp, mpm_transport_open_t *ocfg);

int mpm_transport_hyplnk_read(mpm_transport_cfg_t *sp, uint32_t addr, uint32_t length, char *buf, mpm_transport_read_t *rcfg);

int mpm_transport_hyplnk_write(mpm_transport_cfg_t *sp, uint32_t addr, uint32_t length, char *buf, mpm_transport_write_t *wcfg);

int mpm_transport_hyplnk_read64(mpm_transport_cfg_t *sp, uint64_t addr, uint32_t length, char *buf, mpm_transport_read_t *rcfg);

int mpm_transport_hyplnk_write64(mpm_transport_cfg_t *sp, uint64_t addr, uint32_t length, char *buf, mpm_transport_write_t *cfg);

void *mpm_transport_hyplnk_mmap(mpm_transport_cfg_t *sp, uint32_t addr, uint32_t length, mpm_transport_mmap_t *mcfg);

int mpm_transport_hyplnk_munmap(mpm_transport_cfg_t *sp, void *va, uint32_t length);

void *mpm_transport_hyplnk_mmap64(mpm_transport_cfg_t *sp, uint64_t addr, uint32_t length, mpm_transport_mmap_t *mcfg);

int mpm_transport_hyplnk_munmap64(mpm_transport_cfg_t *sp, void *va, uint32_t length);

void mpm_transport_hyplnk_close(mpm_transport_cfg_t *sp);

mpm_transport_trans_h mpm_transport_hyplnk_get_initiate(mpm_transport_cfg_t *sp, uint32_t from_addr, uint32_t to_addr, uint32_t length, bool is_blocking, mpm_transport_read_t *cfg);

mpm_transport_trans_h mpm_transport_hyplnk_put_initiate(mpm_transport_cfg_t *sp, uint32_t to_addr, uint32_t from_addr, uint32_t length, bool is_blocking, mpm_transport_write_t *cfg);

mpm_transport_trans_h mpm_transport_hyplnk_get_initiate_linked(mpm_transport_cfg_t *sp, uint32_t num_links, uint32_t from_addr[], uint32_t to_addr[], uint32_t length[], bool is_blocking, mpm_transport_read_t *cfg);

mpm_transport_trans_h mpm_transport_hyplnk_put_initiate_linked(mpm_transport_cfg_t *sp, uint32_t num_links, uint32_t to_addr[], uint32_t from_addr[], uint32_t length[], bool is_blocking, mpm_transport_write_t *cfg);

int mpm_transport_hyplnk_transfer_check(mpm_transport_cfg_t *sp, mpm_transport_trans_h th);

uint32_t mpm_transport_hyplnk_phys_map(mpm_transport_cfg_t *sp, uint32_t addr, uint32_t length, mpm_transport_mmap_t *mcfg);

uint32_t mpm_transport_hyplnk_phys_map64(mpm_transport_cfg_t *sp, uint64_t addr, uint32_t length, mpm_transport_mmap_t *mcfg);

mpm_transport_trans_h mpm_transport_hyplnk_get_initiate64(mpm_transport_cfg_t *sp, uint64_t from_addr, uint64_t to_addr, uint32_t length, bool is_blocking, mpm_transport_read_t *cfg);

mpm_transport_trans_h mpm_transport_hyplnk_put_initiate64(mpm_transport_cfg_t *sp, uint64_t to_addr, uint64_t from_addr, uint32_t length, bool is_blocking, mpm_transport_write_t *cfg);

int mpm_transport_hyplnk_enable_peripheral(mpm_transport_cfg_t *sp, mpm_transport_open_t *cfg);

int mpm_transport_hyplnk_disable_peripheral(mpm_transport_cfg_t *sp);

int mpm_transport_hyplnk_get_event_fd(mpm_transport_cfg_t *sp, uint32_t event_bitmap);

int mpm_transport_hyplnk_read_and_ack_interrupt_event(mpm_transport_cfg_t *sp,
	uint32_t event_bitmap, uint32_t *reported_event_bitmap );

int mpm_transport_hyplnk_generate_interrupt_event(mpm_transport_cfg_t *sp,
	uint32_t event_bitmap);
#endif
