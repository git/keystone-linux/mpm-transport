/*
 * Copyright (C) 2013-2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include <stdio.h>
#include <string.h>
#include <malloc.h>
#include <errno.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <unistd.h>

#include "cJSON/cJSON.h"
#include "mpm_transport_cfg.h"
#include "mpm_transport_sharedmem.h"
#include "mpm_transport_qmss.h"
#ifdef HYPLNK_TRANSPORT
#include "mpm_transport_hyplnk.h"
#endif
#ifdef SRIO_TRANSPORT
#include "mpm_transport_srio.h"
#endif

#ifdef LOGFILE_DEBUG
FILE *debug_fp=NULL;
#endif

void mpm_printf(int val, const char* str, ...) {
	if (VERBOSITY_LEVEL == 0) return;
	if (val <= VERBOSITY_LEVEL) {
		va_list args;
		va_start(args, str);
#ifdef SYSLOG_DEBUG
		vsyslog(LOG_INFO, str, args);
#endif
#ifdef LOGFILE_DEBUG
		vfprintf(debug_fp, str, args);
#endif
#ifdef DEBUG
		vprintf(str, args);
#endif
		va_end(args);
#ifdef LOGFILE_DEBUG
		fflush(debug_fp);
#endif
	}
}

#define DEFAULT_CONFIG_FILE_NAME "/etc/mpm/mpm_config.json"

#define json_obj_getnum(obj, item, iteme) \
	do { \
		if (obj->type == cJSON_Number) { \
			item->iteme = obj->valueint; \
		} else if (obj->type == cJSON_String) { \
			item->iteme = strtoull(obj->valuestring, NULL, 0); \
		} else { \
			mpm_printf(1, "Invalid type (%d) for segment %s\n", \
					obj->type, item->name); \
			return (-1); \
		} \
	} while(0)

int json_get_mmap_info (cJSON *json, char *map_name, mpm_transport_cfg_t *sp, int map_index)
{
	mpm_mmap_t *mmap = &(sp->mmap[map_index]);
	cJSON * segs;
	cJSON * segp;
	cJSON * sege;
	int len, i;

	segs = cJSON_GetObjectItem(json,"segments");
	if (!segs) {
		mpm_printf(1, "No segments object array in cfg file\n");
		return (-1);
	}

	if (segs->type != cJSON_Array) {
		mpm_printf(1, "Segments must be array type\n");
		return (-1);
	}

	len = cJSON_GetArraySize(segs);
	if (len > MPM_MAX_SEGMENTS) {
		mpm_printf(1, "The number of segments (%d) must be less than %d\n",
				len, MPM_MAX_SEGMENTS);
		return (-1);
	}
	for (i = 0; i < len; i++) {
		segp = cJSON_GetArrayItem(segs, i);
		if (!segp) {
			mpm_printf(1, "Could not read segment for index %d\n", i);
			return (-1);
		}
		sege = cJSON_GetObjectItem(segp, "name");
		if (!sege) {
			mpm_printf(1, "Segment without a name, skipping\n");
			continue;
		}

		if (strlen(sege->valuestring) > MPM_MAX_NAME_LENGTH) {
			mpm_printf(1, "Segment name is too big (%d), must be less than %d\n",
					(int) strlen(sege->valuestring), MPM_MAX_NAME_LENGTH);
			return (-1);
		}

		if (strncmp(sege->valuestring, map_name, MPM_MAX_NAME_LENGTH)) {
			continue;
		}

		strncpy(mmap->name, sege->valuestring, MPM_MAX_NAME_LENGTH);

		sege = cJSON_GetObjectItem(segp, "globaladdr");
		if (!sege) {
			mpm_printf(1, "No global address for segment %s\n", mmap->name);
			return (-1);
		}
		json_obj_getnum(sege, mmap, global_addr);

		sege = cJSON_GetObjectItem(segp, "length");
		if (!sege) {
			mpm_printf(1, "No length for segment %s\n", mmap->name);
			return (-1);
		}

		json_obj_getnum(sege, mmap, length);

		sege = cJSON_GetObjectItem(segp, "localaddr");
		if (!sege) {
			mmap->local_addr = mmap->global_addr;
		} else {
			json_obj_getnum(sege, mmap, local_addr);
		}

		if (sp->transport == shared_memory) {
			/* This section to be moved to sharedmem directory */
			sege = cJSON_GetObjectItem(segp, "devicename");
			if (!sege) {
				mpm_printf(1, "Need device name for shared memory transport\n");
				return (-1);
			}

			if (strlen(sege->valuestring) > MPM_MAX_NAME_LENGTH) {
				mpm_printf(1, "Segment device name is too big (%d), must be less than %d\n",
						(int) strlen(sege->valuestring), MPM_MAX_NAME_LENGTH);
				return (-1);
			}
			strncpy(mmap->devicename, sege->valuestring, MPM_MAX_NAME_LENGTH);
		}
#ifdef HYPLNK_TRANSPORT
		if (sp->transport == hyperlink) {
			/* This section to be moved to hyplnk directory */
			sege = cJSON_GetObjectItem(segp, "devicename");
			if (!sege) {
				mpm_printf(1, "Need device name for hyperlink transport\n");
				return (-1);
			}

			if (strlen(sege->valuestring) > MPM_MAX_NAME_LENGTH) {
				mpm_printf(1, "Segment device name is too big (%d), must be less than %d\n",
						(int) strlen(sege->valuestring), MPM_MAX_NAME_LENGTH);
				return (-1);
			}
			strncpy(mmap->devicename, sege->valuestring, MPM_MAX_NAME_LENGTH);
		}
#endif

		break;
	}

	return (0);
}

int json_get_dma_info (cJSON *json, char *dma_name, mpm_transport_cfg_t *sp)
{
	cJSON * segs;
	cJSON * segp;
	cJSON * sege;
	int len, i;

	segs = cJSON_GetObjectItem(json,"dma-params");
	if (!segs) {
		mpm_printf(1, "No dma-params object array in cfg file\n");
		return (-1);
	}

	if (segs->type != cJSON_Array) {
		mpm_printf(1, "dma-params must be array type\n");
		return (-1);
	}

	len = cJSON_GetArraySize(segs);
	if (len > MPM_MAX_DMA_PARAMS) {
		mpm_printf(1, "The number of dma-params (%d) must be less than %d\n",
				len, MPM_MAX_DMA_PARAMS);
		return (-1);
	}
	for (i = 0; i < len; i++) {
		segp = cJSON_GetArrayItem(segs, i);
		if (!segp) {
			mpm_printf(1, "Could not read segment for index %d\n", i);
			return (-1);
		}
		sege = cJSON_GetObjectItem(segp, "name");
		if (!sege) {
			mpm_printf(1, "DMA profile without a name, skipping\n");
			continue;
		}

		if (strlen(sege->valuestring) > MPM_MAX_NAME_LENGTH) {
			mpm_printf(1, "DMA profile name is too big (%d), must be less than %d\n",
					(int) strlen(sege->valuestring), MPM_MAX_NAME_LENGTH);
			return (-1);
		}

		if (strncmp(sege->valuestring, dma_name, MPM_MAX_NAME_LENGTH)) {
			continue;
		}

		sege = cJSON_GetObjectItem(segp, "edma3inst");
		if (!sege) {
			mpm_printf(1, "No edma3inst specified for %s\n", dma_name);
			return (-1);
		}
		else {
			json_obj_getnum(sege, sp, edma3_inst);
		}

		sege = cJSON_GetObjectItem(segp, "shadowregion");
		if (!sege) {
			mpm_printf(1, "No shadowregion specified for %s\n", dma_name);
			return (-1);
		}
		else {
			json_obj_getnum(sege, sp, shadow_reg);
		}

		sege = cJSON_GetObjectItem(segp, "prealloc");
		if (!sege) {
			sp->edma3_prealloc = 0;
		}
		else {
			json_obj_getnum(sege, sp, edma3_prealloc);
		}
	}
	return 0;
}

int json_get_mpax_info (cJSON *json, char *mpax_name, mpm_transport_cfg_t *sp)
{
	cJSON * segs;
	cJSON * segp;
	cJSON * sege;
	int len, i;

	segs = cJSON_GetObjectItem(json,"mpax-params");
	if (!segs) {
		mpm_printf(1, "No mpax-params object array in cfg file\n");
		return (-1);
	}

	if (segs->type != cJSON_Array) {
		mpm_printf(1, "mpax-params must be array type\n");
		return (-1);
	}

	len = cJSON_GetArraySize(segs);
	if (len > MPM_MAX_MPAX_PARAMS) {
		mpm_printf(1, "The number of mpax-params (%d) must be less than %d\n",
				len, MPM_MAX_MPAX_PARAMS);
		return (-1);
	}
	for (i = 0; i < len; i++) {
		segp = cJSON_GetArrayItem(segs, i);
		if (!segp) {
			mpm_printf(1, "Could not read segment for index %d\n", i);
			return (-1);
		}
		sege = cJSON_GetObjectItem(segp, "name");
		if (!sege) {
			mpm_printf(1, "mpax profile without a name, skipping\n");
			continue;
		}

		if (strlen(sege->valuestring) > MPM_MAX_NAME_LENGTH) {
			mpm_printf(1, "mpax profile name is too big (%d), must be less than %d\n",
					(int) strlen(sege->valuestring), MPM_MAX_NAME_LENGTH);
			return (-1);
		}

		if (strncmp(sege->valuestring, mpax_name, MPM_MAX_NAME_LENGTH)) {
			continue;
		}

		sege = cJSON_GetObjectItem(segp, "base");
		if (!sege) {
			mpm_printf(1, "No base specified for %s\n", mpax_name);
			return (-1);
		}
		else {
			json_obj_getnum(sege, sp, mpax_base);
		}

		sege = cJSON_GetObjectItem(segp, "size");
		if (!sege) {
			mpm_printf(1, "No size specified for %s\n", mpax_name);
			return (-1);
		}
		else {
			json_obj_getnum(sege, sp, mpax_size);
		}

		sege = cJSON_GetObjectItem(segp, "index");
		if (!sege) {
			mpm_printf(1, "No index specified for %s\n", mpax_name);
			return (-1);
		}
		else {
			json_obj_getnum(sege, sp, mpax_index);
		}

		sege = cJSON_GetObjectItem(segp, "count");
		if (!sege) {
			mpm_printf(1, "No count specified for %s\n", mpax_name);
			return (-1);
		}
		else {
			json_obj_getnum(sege, sp, mpax_count);
		}
	}
	return 0;
}

int json_get_transport_info (cJSON *json, char *transport_name, mpm_transport_cfg_t *sp)
{
	mpm_transport_qmss_t *qmInfo;
#ifdef HYPLNK_TRANSPORT
	mpm_transport_hyplnk_t * hypInfo;
#endif
#ifdef SRIO_TRANSPORT
	mpm_transport_srio_t *srioInfo;
#endif
	cJSON * segs;
	cJSON * segp;
	cJSON * sege;
	int len, i;

	segs = cJSON_GetObjectItem(json,"transports");
	if (!segs) {
		mpm_printf(1, "No transports object array in cfg file\n");
		return (-1);
	}

	if (segs->type != cJSON_Array) {
		mpm_printf(1, "Transports must be array type\n");
		return (-1);
	}

	len = cJSON_GetArraySize(segs);
	if (len > MPM_MAX_TRANSPORTS) {
		mpm_printf(1, "The number of transports (%d) must be less than %d\n",
				len, MPM_MAX_TRANSPORTS);
		return (-1);
	}

	if (!strcmp(transport_name, "sharedmemory")) {
		sp->transport = shared_memory;
		return 0;
	}

	for (i = 0; i < len; i++) {
		segp = cJSON_GetArrayItem(segs, i);
		if (!segp) {
			mpm_printf(1, "Could not read segment for index %d\n", i);
			return (-1);
		}
		sege = cJSON_GetObjectItem(segp, "name");
		if (!sege) {
			mpm_printf(1, "Transport without a name, skipping\n");
			continue;
		}

		if (strlen(sege->valuestring) > MPM_MAX_NAME_LENGTH) {
			mpm_printf(1, "Transport name is too big (%d), must be less than %d\n",
					(int) strlen(sege->valuestring), MPM_MAX_NAME_LENGTH);
			return (-1);
		}

		if (strncmp(sege->valuestring, transport_name, MPM_MAX_NAME_LENGTH)) {
			continue;
		}

		sege = cJSON_GetObjectItem(segp, "transporttype");
		if (!sege) {
			mpm_printf(1, "No transporttype specified for %s\n", transport_name);
			return (-1);
		}
		else {
			if (!strcmp(sege->valuestring, "sharedmemory")) {
				sp->transport = shared_memory;
			} else if (!strcmp(sege->valuestring, "pcie")) {
				sp->transport = pcie;
			} else if (!strcmp(sege->valuestring, "srio")) {
				sp->transport = srio;
			} else if (!strcmp(sege->valuestring, "ethernet")) {
				sp->transport = ethernet;
			} else if (!strcmp(sege->valuestring, "hyperlink")) {
				sp->transport = hyperlink;
			} else if (!strcmp(sege->valuestring, "qmss")) {
				sp->transport = qmss;
			} else {
				mpm_printf(1, "Invalid transport %s in configuration\n",
						sege->valuestring);
				return (-1);
			}
		}

		switch (sp->transport) {
			case shared_memory:
				break;
			case qmss:
				qmInfo = malloc(sizeof(mpm_transport_qmss_t));
				if (!qmInfo) {
					mpm_printf(1, "can't allocate memory for qmss data \n");
					return -1;
				}
				sege = cJSON_GetObjectItem(segp, "initregion");
				if (sege)
					strncpy(qmInfo->regionName, sege->valuestring, MPM_MAX_NAME_LENGTH);

				sege = cJSON_GetObjectItem(segp, "txfreeq");
				if (!sege) {
					mpm_printf(1, "No txfreeq given for this endpoint!\n");
					free(qmInfo);
					return (-1);
				}
				else {
					strncpy(qmInfo->txFqName, sege->valuestring, MPM_MAX_NAME_LENGTH);
				}

				sege = cJSON_GetObjectItem(segp, "rxfreeq");
				if (!sege) {
					mpm_printf(1, "No rxfreeq given for this endpoint!\n");
					free(qmInfo);
					return (-1);
				}
				else {
					strncpy(qmInfo->rxFqName, sege->valuestring, MPM_MAX_NAME_LENGTH);
				}

				sege = cJSON_GetObjectItem(segp, "writefifodepth");
				if (!sege) {
					mpm_printf(1, "No writefifodepth given for this endpoint!\n");
					free(qmInfo);
					return (-1);
				}
				else {
					qmInfo->cppiWriteFifoDepth = sege->valueint;
				}

				sege = cJSON_GetObjectItem(segp, "cpdmatimeout");
				if (!sege) {
					mpm_printf(1, "No cpdmatimeout given for this endpoint!\n");
					free(qmInfo);
					return (-1);
				}
				else {
					qmInfo->cppiTimeoutCount = sege->valueint;
				}

				sege = cJSON_GetObjectItem(segp, "qmssmaxdesc");
				if (sege)
					qmInfo->maxDescNum = sege->valueint;

				sp->td = (void *) qmInfo;
				break;
#ifdef SRIO_TRANSPORT
			case srio:
				srioInfo = malloc(sizeof(mpm_transport_srio_t));
				if (!srioInfo) {
					mpm_printf(1, "can't allocate memory for srio data \n");
					return -1;
				}
				sege = cJSON_GetObjectItem(segp, "initregion");
				if (sege)
					strncpy(srioInfo->regionName, sege->valuestring, MPM_MAX_NAME_LENGTH);
				else {
					sege = cJSON_GetObjectItem(segp, "memregion");
					if (!sege) {
						mpm_printf(1, "No memory region given for this srio endpoint!\n");
						free(srioInfo);
						return (-1);
					}
					else
						strncpy(srioInfo->regionName, sege->valuestring, MPM_MAX_NAME_LENGTH);
				}

				sege = cJSON_GetObjectItem(segp, "txfreeq");
				if (!sege) {
					mpm_printf(1, "No txfreeq given for this endpoint!\n");
					free(srioInfo);
					return (-1);
				}
				else {
					strncpy(srioInfo->txFqName, sege->valuestring, MPM_MAX_NAME_LENGTH);
				}

				sege = cJSON_GetObjectItem(segp, "rxfreeq");
				if (!sege) {
					mpm_printf(1, "No rxfreeq given for this endpoint!\n");
					free(srioInfo);
					return (-1);
				}
				else {
					strncpy(srioInfo->rxFqName, sege->valuestring, MPM_MAX_NAME_LENGTH);
				}

				sege = cJSON_GetObjectItem(segp, "qmssmaxdesc");
				if (sege)
					srioInfo->maxDescNum = sege->valueint;

				sp->td = (void *) srioInfo;
				break;
#endif
#ifdef HYPLNK_TRANSPORT
			case hyperlink:
				hypInfo = calloc(1, sizeof(mpm_transport_hyplnk_t));
				if (!hypInfo) {
					mpm_printf(1, "can't allocate memory for hyperlink data \n");
					return -1;
				}
				else
				{
					strncpy(hypInfo->hyplnkSettings, transport_name, MPM_MAX_NAME_LENGTH);
					strncpy(hypInfo->name, transport_name, MPM_MAX_NAME_LENGTH);
					sege = cJSON_GetObjectItem(segp, "direction");
					if (!sege)
					{
						mpm_printf(1, "No hyperlink direction given\n");
						free(hypInfo);
						return (-1);
					}
					else
					{
						if (!strcmp(sege->valuestring, "loopback")) {
							hypInfo->hyplnkDir = hyplnk_LOOPBACK;
						} else if (!strcmp(sege->valuestring, "remote")) {
							hypInfo->hyplnkDir = hyplnk_REMOTE;}
						else
						{
							mpm_printf(1, "hyperlink direction not supported\n");
						free(hypInfo);
							return (-1);
						}
					}

					sege = cJSON_GetObjectItem(segp, "hyplnkinterface");
					if (!sege)
					{
						mpm_printf(1, "No hyperlink interface given\n");
						free(hypInfo);
						return (-1);
					}
					else
					{
						if (!strcmp(sege->valuestring, "hyplnk0")) {
							hypInfo->hyplnkIF = hyplnk_PORT_0;
						} else if (!strcmp(sege->valuestring, "hyplnk1")) {
							hypInfo->hyplnkIF = hyplnk_PORT_1;}
						else
						{
							mpm_printf(1, "hyperlink interface not supported\n");
							free(hypInfo);
							return (-1);
						}
					}

					sege = cJSON_GetObjectItem(segp, "txprivid");
					if (!sege)
					{
						mpm_printf(1, "No hyperlink txprivid size given\n");
						free(hypInfo);
						return (-1);
					}
					else {
						if ( (sege->type == cJSON_Number) || (sege->type == cJSON_String) )
							json_obj_getnum(sege, hypInfo, hyplnkTxPrivID);
						else {
							mpm_printf(1, "Invalid JSON object\n");
							free(hypInfo);
							return (-1);
						}
					}

					sege = cJSON_GetObjectItem(segp, "rxprivid");
					if (!sege)
					{
						mpm_printf(1, "No hyperlink rxprivid size given\n");
						free(hypInfo);
						return (-1);
					}
					else {
						if ( (sege->type == cJSON_Number) || (sege->type == cJSON_String) )
							json_obj_getnum(sege, hypInfo, hyplnkRxPrivID);
						else {
							mpm_printf(1, "Invalid JSON object\n");
							free(hypInfo);
							return (-1);
						}
					}

					sege = cJSON_GetObjectItem(segp, "rxsegsel");
					if (!sege)
					{
						mpm_printf(1, "No hyperlink rxsegsel size given\n");
						free(hypInfo);
						return (-1);
					}
					else {
						if ( (sege->type == cJSON_Number) || (sege->type == cJSON_String) )
							json_obj_getnum(sege, hypInfo, hyplnkSegSel);
						else {
							mpm_printf(1, "Invalid JSON object\n");
							free(hypInfo);
							return (-1);
						}
					}

					sege = cJSON_GetObjectItem(segp, "rxlenval");
					if (!sege)
					{
						mpm_printf(1, "No hyperlink rxlenval size given\n");
						free(hypInfo);
						return (-1);
					}
					else {
						if ( (sege->type == cJSON_Number) || (sege->type == cJSON_String) )
							json_obj_getnum(sege, hypInfo, hyplnkRxLen);
						else {
							mpm_printf(1, "Invalid JSON object\n");
							free(hypInfo);
							return (-1);
						}
					}

					sege = cJSON_GetObjectItem(segp, "numlanes");
					if (!sege)
					{
						mpm_printf(1, "No hyperlink number of lanes given\n");
						free(hypInfo);
						return (-1);
					}
					else {
						if ( (sege->type == cJSON_Number) || (sege->type == cJSON_String) )
							json_obj_getnum(sege, hypInfo, hyplnkNumLanes);
						else {
							mpm_printf(1, "Invalid JSON object\n");
							free(hypInfo);
							return (-1);
						}
					}
					sege = cJSON_GetObjectItem(segp, "intenable");
					if (sege) {
						if ( (sege->type == cJSON_Number) || (sege->type == cJSON_String) )
							json_obj_getnum(sege, hypInfo, intEnable);
						else {
							mpm_printf(1, "Invalid JSON object\n");
							free(hypInfo);
							return (-1);
						}
					}
					sege = cJSON_GetObjectItem(segp, "intlocal");
					if (sege) {
						if ( (sege->type == cJSON_Number) || (sege->type == cJSON_String) )
							json_obj_getnum(sege, hypInfo, intLocal);
						else {
							mpm_printf(1, "Invalid JSON object\n");
							free(hypInfo);
							return (-1);
						}
					}
					sege = cJSON_GetObjectItem(segp, "eventbitmap");
					if (sege) {
						if ( (sege->type == cJSON_Number) || (sege->type == cJSON_String) )
							json_obj_getnum(sege, hypInfo, eventBitmap);
						else {
							mpm_printf(1, "Invalid JSON object\n");
							free(hypInfo);
							return (-1);
						}
					}

					if ((hypInfo->hyplnkNumLanes != 4) && (hypInfo->hyplnkNumLanes != 1))
					{
						mpm_printf(1, "Only 4 and 1 lane supported\n");
						free(hypInfo);
						return (-1);
					}
				}
				sp->td = (void *) hypInfo;
				break;
#endif
			default:
				break;
		}

		break;
	}

	return (0);
}

int json_manage_descriptors (cJSON *json, char *q_name, mpm_transport_cfg_t *sp, Qmss_QueueHnd *qHnd, int *bufSize, int *descNum)
{
	int len, i;
	cJSON * segs;
	cJSON * segp;
	cJSON * sege;
	mpm_transport_qmss_desc_info_t descParams;

	segs = cJSON_GetObjectItem(json,"qmss-queue-map");
	if (!segs) {
		mpm_printf(1, "No qmss-queue-map object array in cfg file\n");
		return (-1);
	}
	if (segs->type != cJSON_Array) {
		mpm_printf(1, "qmss-queue-map profiles must be array type\n");
		return (-1);
	}

	len = cJSON_GetArraySize(segs);

	for (i = 0; i < len; i++) {
		segp = cJSON_GetArrayItem(segs, i);
		if (!segp) {
			mpm_printf(1, "Could not read segment for index %d\n", i);
			return (-1);
		}
		sege = cJSON_GetObjectItem(segp, "name");
		if (!sege) {
			mpm_printf(1, "Queue info without a name, skipping\n");
			continue;
		}

		if (strlen(sege->valuestring) > MPM_MAX_NAME_LENGTH) {
			mpm_printf(1, "Queue info name is too big (%d), must be less than %d\n",
					(int) strlen(sege->valuestring), MPM_MAX_NAME_LENGTH);
			return (-1);
		}

		if (strncmp(sege->valuestring, q_name, MPM_MAX_NAME_LENGTH)) {
			continue;
		}

		sege = cJSON_GetObjectItem(segp, "queue");
		if (!sege) {
			mpm_printf(1, "No queue specified for %s\n", q_name);
			return (-1);
		}
		else {
			descParams.qid = sege->valueint;
		}

		sege = cJSON_GetObjectItem(segp, "numdesc");
		if (!sege) {
			mpm_printf(1, "No numdesc specified for %s\n", q_name);
			return (-1);
		}
		else {
			descParams.numDesc = sege->valueint;
		}

		/* TODO: remove hard checking when 0 copy comes in */
		sege = cJSON_GetObjectItem(segp, "sizebuf");
		if (!sege) {
			mpm_printf(1, "No sizebuf specified for %s\n", q_name);
			return (-1);
		}
		else {
			descParams.sizeBuf = sege->valueint;
		}

		/* Save params if needed */
		if (bufSize) *bufSize = descParams.sizeBuf;
		if (descNum) *descNum = descParams.numDesc;

		switch (sp->transport) {
			case qmss:
				*qHnd = mpm_transport_qmss_init_desc(sp, q_name, &descParams);
				break;
#ifdef SRIO_TRANSPORT
			case srio:
				*qHnd = mpm_transport_srio_init_desc(sp, q_name, &descParams);
				break;
#endif
			default:
				break;
		}
	}
	return 0;
}

int json_get_and_init_qmss (cJSON *json, mpm_transport_cfg_t *sp)
{
	int len, i;
	cJSON * segs;
	cJSON * segp;
	cJSON * sege;
	mpm_transport_qmss_mem_region_info_t regionParams;
	char regionName[MPM_MAX_NAME_LENGTH];

	switch (sp->transport) {
		case qmss:
			strncpy(regionName, ((mpm_transport_qmss_t *) sp->td)->regionName, MPM_MAX_NAME_LENGTH);
			break;
#ifdef SRIO_TRANSPORT
		case srio:
			strncpy(regionName, ((mpm_transport_srio_t *) sp->td)->regionName, MPM_MAX_NAME_LENGTH);
			break;
#endif
		default:
			break;
	}

	segs = cJSON_GetObjectItem(json,"qmss-mem-region");
	if (!segs) {
		mpm_printf(1, "No qmss-mem-region object array in cfg file\n");
		return (-1);
	}
	if (segs->type != cJSON_Array) {
		mpm_printf(1, "QMSS mem region profiles must be array type\n");
		return (-1);
	}

	len = cJSON_GetArraySize(segs);
	if (len > MPM_MAX_TRANSPORTS) {
		mpm_printf(1, "The number of memory region profiles (%d) must be less than %d\n",
				len, MPM_MAX_TRANSPORTS);
		return (-1);
	}

	for (i = 0; i < len; i++) {
		segp = cJSON_GetArrayItem(segs, i);
		if (!segp) {
			mpm_printf(1, "Could not read segment for index %d\n", i);
			return (-1);
		}
		sege = cJSON_GetObjectItem(segp, "name");
		if (!sege) {
			mpm_printf(1, "QMSS mem region without a name, skipping\n");
			continue;
		}

		if (strlen(sege->valuestring) > MPM_MAX_NAME_LENGTH) {
			mpm_printf(1, "QMSS mem region name is too big (%d), must be less than %d\n",
					(int) strlen(sege->valuestring), MPM_MAX_NAME_LENGTH);
			return (-1);
		}

		if (strncmp(sege->valuestring, regionName, MPM_MAX_NAME_LENGTH)) {
			continue;
		}

		sege = cJSON_GetObjectItem(segp, "regiondescnum");
		if (!sege) {
			mpm_printf(1, "No regiondescnum specified for %s\n", regionName);
			return (-1);
		}
		else {
			regionParams.regionDescNum = sege->valueint;
		}

		sege = cJSON_GetObjectItem(segp, "regiondescsize");
		if (!sege) {
			mpm_printf(1, "No regiondescsize specified for %s\n", regionName);
			return (-1);
		}
		else {
			regionParams.regionDescSize = sege->valueint;
		}

		sege = cJSON_GetObjectItem(segp, "regionnum");
		if (!sege) {
			mpm_printf(1, "No regionnum specified for %s\n", regionName);
			return (-1);
		}
		else {
			regionParams.regionNum = sege->valueint;
		}

		sege = cJSON_GetObjectItem(segp, "regionstartidx");
		if (!sege) {
			mpm_printf(1, "No regionstartidx specified for %s\n", regionName);
			return (-1);
		}
		else {
			regionParams.regionStartIdx = sege->valueint;
		}

		switch (sp->transport) {
			case qmss:
				if (mpm_transport_qmss_insert_mem_region(sp, &regionParams)) {
					mpm_printf(1, "Error inserting memory region!\n");
					return (-1);
				}
				break;
#ifdef SRIO_TRANSPORT
			case srio:
				if (mpm_transport_srio_insert_mem_region(sp, &regionParams)) {
					mpm_printf(1, "Error inserting memory region!\n");
					return (-1);
				}
				break;
#endif
			default:
				break;
		}

		break;
	}

	return (0);
}

int json_get_slave_info (cJSON *json, char *slave_name, mpm_transport_cfg_t *sp)
{
	char *namep;
	int len, i;
	int lenm, m;
	cJSON *slvs, *slvp, *slve, *slvm;
	int ret = -1;

	slvs = cJSON_GetObjectItem(json,"slaves");
	if (!slvs) {
		mpm_printf(1, "No slaves object array in cfg file\n");
		return (-1);
	}

	if (slvs->type != cJSON_Array) {
		mpm_printf(1, "Slaves must be array type\n");
		return (-1);
	}

	len = cJSON_GetArraySize(slvs);
	for (i = 0; i < len; i++) {
		slvp = cJSON_GetArrayItem(slvs, i);
		if (!slvp) {
			mpm_printf(1, "Could not read slave info for index %d\n", i);
			return (-1);
		}

		slve = cJSON_GetObjectItem(slvp, "name");
		if (!slve) {
			mpm_printf(1, "Slave processor without a name, skipping\n");
			continue;
		}

		if (strlen(slve->valuestring) > MPM_MAX_NAME_LENGTH) {
			mpm_printf(1, "Slave core name is too big (%d), must be less than %d\n",
					(int) strlen(slve->valuestring), MPM_MAX_NAME_LENGTH);
			continue;
		}

		if (strncmp(slve->valuestring, slave_name, MPM_MAX_NAME_LENGTH)) {
			continue;
		}

		strncpy(sp->name, slve->valuestring, MPM_MAX_NAME_LENGTH);

		slve = cJSON_GetObjectItem(slvp, "coreid");
		if (slve) {
			json_obj_getnum(slve, sp, core_id);
		}

		slve = cJSON_GetObjectItem(slvp, "clusterid");
		if (slve) {
			json_obj_getnum(slve, sp, cluster_id);
		}

		slve = cJSON_GetObjectItem(slvp, "transport");
		if (!slve) {
			mpm_printf(1, "No transport provided for slave %s\n",sp->name);
			return (-1);
		}
		if (json_get_transport_info(json, slve->valuestring, sp)) {
			mpm_printf(1, "Could not read transport info for slave %s\n", sp->name);
			return (-1);
		}

		slve = cJSON_GetObjectItem(slvp, "dma");
		if (slve) {
			strncpy(sp->edma3_name, slve->valuestring, MPM_MAX_NAME_LENGTH);
			if (json_get_dma_info(json, slve->valuestring, sp)) {
				mpm_printf(1, "Could not read dma info for slave %s\n", sp->name);
				return (-1);
			}
		}

		slve = cJSON_GetObjectItem(slvp, "mpax");
		if (slve) {
			strncpy(sp->mpax_name, slve->valuestring, MPM_MAX_NAME_LENGTH);
			if (json_get_mpax_info(json, slve->valuestring, sp)) {
				mpm_printf(1, "Could not read mpax info for slave %s\n", sp->name);
				return (-1);
			}
		}

		slvm = cJSON_GetObjectItem(slvp, "memorymap");
		if (!slvm) {
			if (sp->transport != qmss) {
				mpm_printf(1, "No slave memory map in cfg file\n");
				return (-1);
			}
		}
		else {
			if (slvm->type != cJSON_Array) {
				mpm_printf(1, "Slave memory map must be array type\n");
				return (-1);
			}

			lenm = cJSON_GetArraySize(slvm);
			if (lenm > MPM_MAX_MMAPS) {
				mpm_printf(1, "The number of memory map sections (%d) must be less than %d\n",
						lenm, MPM_MAX_MMAPS);
				return (-1);
			}

			for (m = 0; m < lenm; m++) {
				slvp = cJSON_GetArrayItem(slvm, m);
				if (!slvp) {
					mpm_printf(1, "Could not read map name for the index %d\n", m);
					return (-1);
				}

				namep = slvp->valuestring;
				if (json_get_mmap_info(json, namep, sp, m)) {
					mpm_printf(1, "Could not read slave info for index %d\n", m);
					return (-1);
				}
			}
			sp->num_mmap = lenm;
		}
		ret = 0;
		break;
	}

	return ret;
}

/*
	slave_name:  should match the name in config file.
	cfg: runtime configuration needed for this device.
	In case of shared memory it might not have anything.
	Returns mpm_transport_h or NULL on error.
*/
mpm_transport_h mpm_transport_open(char *slave_name, mpm_transport_open_t *cfg)
{
	mpm_transport_cfg_t *sp = NULL;
	char *config_buf = NULL;
	cJSON *json = NULL;
	int retval = -1;
	FILE *fpr = NULL;
	struct stat buf;
	int flen;
	int res;
	int ret;

#ifdef LOGFILE_DEBUG
	if (!debug_fp) {
		debug_fp = fopen ("/var/log/mpmfilelog", "w+");
		mpm_printf(1,"\nLog file opened ..\n");fflush(debug_fp);
	}
#endif
	if (!slave_name || !cfg) {
		mpm_printf(1, "Null parameter\n");
		goto close_n_exit;
	}
	if (strlen(slave_name) > MPM_MAX_NAME_LENGTH) {
		mpm_printf(1, "Name should be less than %d\n", MPM_MAX_NAME_LENGTH);
		goto close_n_exit;
	}

	sp = calloc(1, sizeof(mpm_transport_cfg_t));
	if (!sp) {
		mpm_printf(1, "can't allocate memory for handle\n");
		goto close_n_exit;
	}

	fpr = fopen(DEFAULT_CONFIG_FILE_NAME, "r");
	if (!fpr){
		mpm_printf(1, "Error opening file %s (error: %s)\n",
				DEFAULT_CONFIG_FILE_NAME, strerror(errno));
		goto close_n_exit;
	}

	if (fstat(fileno(fpr), &buf) != 0) {
		mpm_printf(1, "Error reading file %s (error: %s)\n",
				DEFAULT_CONFIG_FILE_NAME, strerror(errno));
		goto close_n_exit;
	}
	flen = buf.st_size;

	config_buf = (char *) malloc(flen + 1);
	if (!config_buf) {
		mpm_printf(1, "Error in allocating cfg buffer (error: %s)\n",
				strerror(errno));
		goto close_n_exit;
	}
	config_buf[flen] = 0;

	res = fread (config_buf, 1, flen, fpr);
	if (res != flen) {
		mpm_printf(1, "Error in reading cfg file (error: %s)\n",
				strerror(errno));
		goto close_n_exit;
	}

	json = cJSON_Parse(config_buf);
	if (!json) {
		mpm_printf(1, "JSON parser error before: [%s]\n",cJSON_GetErrorPtr());
		goto close_n_exit;
	}

	if (json_get_slave_info(json, slave_name, sp)) {
		mpm_printf(1, "Error in JSON parsing\n");
		goto close_n_exit;
	}

	switch (sp->transport) {
		case shared_memory:
			ret = mpm_transport_sharedmem_open(sp, cfg);
			if (ret) {
				mpm_printf(1, "shm open failed ret %d\n", ret);
				goto close_n_exit;
			}
			break;
#ifdef HYPLNK_TRANSPORT
		case hyperlink:
			if (mpm_transport_hyplnk_open(sp, cfg)) {
				mpm_printf(1, "hyplnk open failed\n");
				goto close_n_exit;
			}
			break;
#endif
		case qmss:
			if (mpm_transport_qmss_init(sp, cfg)) {
				mpm_printf(1, "error initializing for qmss\n");
				goto close_n_exit;
			}
			if (json_get_and_init_qmss(json, sp)) {
				mpm_printf(1, "error parsing/initializing memory region and free descriptors\n");
				goto close_n_exit;
			}
			if (json_manage_descriptors(json,
				((mpm_transport_qmss_t *) sp->td)->txFqName,
				sp,
				&(((mpm_transport_qmss_t *) sp->td)->txFqHnd),
				&(((mpm_transport_qmss_t *) sp->td)->txBufSize),
				NULL)) {
				mpm_printf(1, "error initializing tx free queue\n");
				goto close_n_exit;
			}
			if (json_manage_descriptors(json,
				((mpm_transport_qmss_t *) sp->td)->rxFqName,
				sp,
				&(((mpm_transport_qmss_t *) sp->td)->rxFqHnd),
				NULL,
				NULL)) {
				mpm_printf(1, "error initializing tx free queue\n");
				goto close_n_exit;
			}
			if (mpm_transport_qmss_open(sp, cfg)) {
				mpm_printf(1, "qmss open failed\n");
				goto close_n_exit;
			}
			break;
#ifdef SRIO_TRANSPORT
		case srio:
			if (mpm_transport_srio_init(sp, cfg)) {
				mpm_printf(1, "error initializing for srio\n");
				goto close_n_exit;
			}
			if (json_get_and_init_qmss(json, sp)) {
				mpm_printf(1, "error parsing and inserting memory region\n");
				goto close_n_exit;
			}
			if (json_manage_descriptors(json,
				((mpm_transport_srio_t *) sp->td)->txFqName,
				sp,
				&(((mpm_transport_srio_t *) sp->td)->txFqHnd),
				&(((mpm_transport_srio_t *) sp->td)->txBufSize),
				NULL)) {
				mpm_printf(1, "error initializing tx free queue\n");
				goto close_n_exit;
			}
			if (json_manage_descriptors(json,
				((mpm_transport_srio_t *) sp->td)->rxFqName,
				sp,
				&(((mpm_transport_srio_t *) sp->td)->rxFqHnd),
				NULL,
				&(((mpm_transport_srio_t *) sp->td)->rxDescNum))) {
				mpm_printf(1, "error initializing tx free queue\n");
				goto close_n_exit;
			}
			if (mpm_transport_srio_open(sp, cfg)) {
				mpm_printf(1, "srio open failed\n");
				goto close_n_exit;
			}
			break;
#endif
		default:
			mpm_printf(1, "unsupported transport %d\n", sp->transport);
			goto close_n_exit;
	}

	retval = 0;

close_n_exit:
	if(json) cJSON_Delete(json);
	if (config_buf) free(config_buf);
	if (fpr) fclose(fpr);

	if (retval < 0){
		if (sp->td) free(sp->td);
		free(sp);
	}
	return ((retval < 0) ?  (mpm_transport_h) 0 : sp);
}

/*
	Read from slave[DSP] local address and length into the buffer.
	Returns 0 on success, error code on error.
*/
int mpm_transport_read(mpm_transport_h h, uint32_t addr, uint32_t length, char *buf, mpm_transport_read_t *cfg)
{
	mpm_transport_cfg_t *sp = (mpm_transport_cfg_t *)h;
	if (!h) {
		mpm_printf(1, "invalid handle \n");
		return -1;
	}
	switch (sp->transport) {
		case shared_memory:
			if (mpm_transport_sharedmem_read(sp, addr, length, buf, cfg)) {
				mpm_printf(1, "shm read failed\n");
				return -1;
			}
			break;
#ifdef HYPLNK_TRANSPORT
		case hyperlink:
			if (mpm_transport_hyplnk_read(sp, addr, length, buf, cfg)) {
				mpm_printf(1, "hyplnk read failed\n");
				return -1;
			}
			break;
#endif
		default:
			mpm_printf(1, "unsupported transport %d\n", sp->transport);
			return -1;
	}
	return 0;
}

/*
	Read from slave[DSP] local address and length into the buffer.
	Returns 0 on success, error code on error.
*/
int mpm_transport_read64(mpm_transport_h h, uint64_t addr, uint32_t length, char *buf, mpm_transport_read_t *cfg)
{
	mpm_transport_cfg_t *sp = (mpm_transport_cfg_t *)h;
	if (!h) {
		mpm_printf(1, "invalid handle \n");
		return -1;
	}
	switch (sp->transport) {
#ifdef HYPLNK_TRANSPORT
		case hyperlink:
			if (mpm_transport_hyplnk_read64(sp, addr, length, buf, cfg)) {
				mpm_printf(1, "hyplnk read failed\n");
				return -1;
			}
			break;
#endif
		case shared_memory:
		default:
			mpm_printf(1, "unsupported transport %d\n", sp->transport);
			return -1;
	}
	return 0;
}

/*
	Similar to mpm_transport_read, except memcpy is replaced with EDMA3 operation.
	Returns 0 on success, error code on error.
*/
mpm_transport_trans_h mpm_transport_get_initiate(mpm_transport_h h, uint32_t from_addr, uint32_t to_addr, uint32_t length, bool is_blocking, mpm_transport_read_t *cfg)
{
	mpm_transport_cfg_t *sp = (mpm_transport_cfg_t *)h;
	mpm_transport_trans_t *tp = (mpm_transport_trans_t *) NULL;
	if (!h) {
		mpm_printf(1, "invalid handle \n");
		return tp;
	}
	switch (sp->transport) {
		case shared_memory:
			mpm_printf(1, "shared_mem dma functions not supported\n");
			break;
#ifdef HYPLNK_TRANSPORT
		case hyperlink:
			tp = mpm_transport_hyplnk_get_initiate(sp, from_addr, to_addr, length, is_blocking, cfg);
			if (tp == NULL)
				break;
			if (tp == MPM_TRANSPORT_TRANS_FAIL)
				mpm_printf(1, "hyplnk dma read failed\n");
			else
				mpm_printf(2, "got key: 0x%08x\n",tp->key);
			break;
#endif
		default:
			mpm_printf(1, "unsupported transport %d\n", sp->transport);
	}
	return tp;
}

/*
	Write to slave[DSP] local address "addr" with "length" from the buffer "buf".
	Returns 0 on success, error code on error.
*/
int mpm_transport_write64(mpm_transport_h h, uint64_t addr, uint32_t length, char *buf, mpm_transport_write_t *cfg)
{
	mpm_transport_cfg_t *sp = (mpm_transport_cfg_t *)h;
	if (!h) {
		mpm_printf(1, "invalid handle \n");
		return -1;
	}
	switch (sp->transport) {
#ifdef HYPLNK_TRANSPORT
		case hyperlink:
			if (mpm_transport_hyplnk_write64(sp, addr, length, buf, cfg)) {
				mpm_printf(1, "hyplnk write64 failed\n");
				return -1;
			}
			break;
#endif
		case shared_memory:
		default:
			mpm_printf(1, "unsupported transport %d\n", sp->transport);
			return -1;
	}
	return 0;
}

/*
	Write to slave[DSP] local address "addr" with "length" from the buffer "buf".
	Returns 0 on success, error code on error.
*/
int mpm_transport_write(mpm_transport_h h, uint32_t addr, uint32_t length, char *buf, mpm_transport_write_t *cfg)
{
	mpm_transport_cfg_t *sp = (mpm_transport_cfg_t *)h;
	if (!h) {
		mpm_printf(1, "invalid handle \n");
		return -1;
	}
	switch (sp->transport) {
		case shared_memory:
			if (mpm_transport_sharedmem_write(sp, addr, length, buf, cfg)) {
				mpm_printf(1, "shm write failed\n");
				return -1;
			}
			break;
#ifdef HYPLNK_TRANSPORT
		case hyperlink:
			if (mpm_transport_hyplnk_write(sp, addr, length, buf, cfg)) {
				mpm_printf(1, "hyplnk write failed\n");
				return -1;
			}
			break;
#endif
		default:
			mpm_printf(1, "unsupported transport %d\n", sp->transport);
			return -1;
	}
	return 0;
}

/*
	Similar to mpm_transport_write, except memcpy is replaced with EDMA3 operation.
	Returns 0 on success, error code on error.
*/
mpm_transport_trans_h mpm_transport_put_initiate(mpm_transport_h h, uint32_t to_addr, uint32_t from_addr, uint32_t length, bool is_blocking, mpm_transport_write_t *cfg)
{
	mpm_transport_cfg_t *sp = (mpm_transport_cfg_t *)h;
	mpm_transport_trans_t *tp = (mpm_transport_trans_t *) NULL;
	if (!h) {
		mpm_printf(1, "invalid handle \n");
		return tp;
	}
	switch (sp->transport) {
		case shared_memory:
			mpm_printf(1, "shared_mem dma functions not supported\n");
			break;
#ifdef HYPLNK_TRANSPORT
		case hyperlink:
			tp = mpm_transport_hyplnk_put_initiate(sp, to_addr, from_addr, length, is_blocking, cfg);
			if (tp == NULL)
				break;
			if (tp == MPM_TRANSPORT_TRANS_FAIL)
				mpm_printf(1, "hyplnk dma write failed\n");
			else
				mpm_printf(2, "got key: 0x%08x\n",tp->key);
			break;
#endif
		default:
			mpm_printf(1, "unsupported transport %d\n", sp->transport);
	}
	return tp;
}

/*
	Similar to mpm_transport_get_initiate(), but with multiple transfers linked over EDMA3.
	Returns 0 on success, error code on error.
*/
mpm_transport_trans_h mpm_transport_get_initiate_linked(mpm_transport_h h,
	uint32_t num_links, uint32_t from_addr[], uint32_t to_addr[],
	uint32_t length[], bool is_blocking, mpm_transport_read_t *cfg)
{
	mpm_transport_cfg_t *sp = (mpm_transport_cfg_t *)h;
	mpm_transport_trans_t *tp = (mpm_transport_trans_t *) NULL;
	if (!h) {
		mpm_printf(1, "invalid handle \n");
		return tp;
	}
	switch (sp->transport) {
		case shared_memory:
			mpm_printf(1, "shared_mem dma functions not supported\n");
			break;
#ifdef HYPLNK_TRANSPORT
		case hyperlink:
			tp = mpm_transport_hyplnk_get_initiate_linked(sp, num_links, from_addr,
				to_addr, length, is_blocking, cfg);
			if (tp == NULL)
				break;
			if (tp == MPM_TRANSPORT_TRANS_FAIL)
				mpm_printf(1, "hyplnk dma write failed\n");
			else
				mpm_printf(2, "got key: 0x%08x\n",tp->key);
			break;
#endif
		default:
			mpm_printf(1, "unsupported transport %d\n", sp->transport);
	}
	return tp;
}

/*
	Similar to mpm_transport_put_initiate(), but with multiple transfers linked over EDMA3.
	Returns 0 on success, error code on error.
*/
mpm_transport_trans_h mpm_transport_put_initiate_linked(mpm_transport_h h, uint32_t num_links, uint32_t to_addr[], uint32_t from_addr[], uint32_t length[], bool is_blocking, mpm_transport_write_t *cfg)
{
	mpm_transport_cfg_t *sp = (mpm_transport_cfg_t *)h;
	mpm_transport_trans_t *tp = (mpm_transport_trans_t *) NULL;
	if (!h) {
		mpm_printf(1, "invalid handle \n");
		return tp;
	}
	switch (sp->transport) {
		case shared_memory:
			mpm_printf(1, "shared_mem dma functions not supported\n");
			break;
#ifdef HYPLNK_TRANSPORT
		case hyperlink:
			tp = mpm_transport_hyplnk_put_initiate_linked(sp, num_links, to_addr, from_addr, length, is_blocking, cfg);
			if (tp == NULL)
				break;
			if (tp == MPM_TRANSPORT_TRANS_FAIL)
				mpm_printf(1, "hyplnk dma write failed\n");
			else
				mpm_printf(2, "got key: 0x%08x\n",tp->key);
			break;
#endif
		default:
			mpm_printf(1, "unsupported transport %d\n", sp->transport);
	}
	return tp;
}

/*
	Checks for EDMA completion. Clears the pending interrupt if complete.
	Returns:
		-1 = error
		0 = In progress
		1 = complete
*/
int mpm_transport_transfer_check(mpm_transport_h h, mpm_transport_trans_h th)
{
	mpm_transport_cfg_t *sp = (mpm_transport_cfg_t *)h;
	int ret = -1;
	if (!h) {
		mpm_printf(1, "invalid handle \n");
		return -1;
	}
	switch (sp->transport) {
		case shared_memory:
			mpm_printf(1, "shared_mem dma functions not supported\n");
			break;
#ifdef HYPLNK_TRANSPORT
		case hyperlink:
			ret = mpm_transport_hyplnk_transfer_check(sp, th);
			break;
#endif
		default:
			mpm_printf(1, "unsupported transport %d\n", sp->transport);
			return -1;
	}
	return ret;
}

/*
	Creates a mmap for the memory location and returns virtual address.
*/
void *mpm_transport_mmap(mpm_transport_h h, uint32_t addr, uint32_t length, mpm_transport_mmap_t *cfg)
{
	mpm_transport_cfg_t *sp = (mpm_transport_cfg_t *)h;
	if (!h) {
		mpm_printf(1, "invalid handle \n");
		return MAP_FAILED;
	}
	switch (sp->transport) {
		case shared_memory:
			return (mpm_transport_sharedmem_mmap(sp, addr, length, cfg));
			break;
#ifdef HYPLNK_TRANSPORT
		case hyperlink:
			return (mpm_transport_hyplnk_mmap(sp, addr, length, cfg));
			break;
#endif
		default:
			mpm_printf(1, "unsupported transport %d\n", sp->transport);
	}
	return MAP_FAILED;
}

/*
	Creates a mmap for the 64 bit memory location and returns virtual address.
*/
void *mpm_transport_mmap64(mpm_transport_h h, uint64_t addr, uint32_t length, mpm_transport_mmap_t *cfg)
{
	mpm_transport_cfg_t *sp = (mpm_transport_cfg_t *)h;
	if (!h) {
		mpm_printf(1, "invalid handle \n");
		return MAP_FAILED;
	}
	switch (sp->transport) {
#ifdef HYPLNK_TRANSPORT
		case hyperlink:
			return (mpm_transport_hyplnk_mmap64(sp, addr, length, cfg));
			break;
#endif
		case shared_memory:
		default:
			mpm_printf(1, "unsupported transport %d\n", sp->transport);
	}
	return MAP_FAILED;
}

/*
	Creates a mmap for the memory location and returns virtual address.
*/
uint32_t mpm_transport_phys_map(mpm_transport_h h, uint32_t addr, uint32_t length, mpm_transport_mmap_t *cfg)
{
	mpm_transport_cfg_t *sp = (mpm_transport_cfg_t *)h;
	if (!h) {
		mpm_printf(1, "invalid handle \n");
		return -1;
	}
	switch (sp->transport) {
		case shared_memory:
			mpm_printf(1, "shared_mem does not support phys map\n");
			break;
#ifdef HYPLNK_TRANSPORT
		case hyperlink:
			return (mpm_transport_hyplnk_phys_map(sp, addr, length, cfg));
			break;
#endif
		default:
			mpm_printf(1, "unsupported transport %d\n", sp->transport);
	}
	return -1;
}

/*
	Unmaps previous mmap from mapped virtual address
*/
int mpm_transport_munmap(mpm_transport_h h, void *va, uint32_t length)
{
	mpm_transport_cfg_t *sp = (mpm_transport_cfg_t *)h;
	if (!h) {
		mpm_printf(1, "invalid handle \n");
		return -1;
	}
	switch (sp->transport) {
		case shared_memory:
			if (mpm_transport_sharedmem_munmap(sp, va, length)) {
				mpm_printf(1, "shm munmap failed\n");
				return -1;
			}
			break;
#ifdef HYPLNK_TRANSPORT
		case hyperlink:
			if (mpm_transport_hyplnk_munmap(sp, va, length)) {
				mpm_printf(1, "shm munmap failed\n");
				return -1;
			}
			break;
#endif
		default:
			mpm_printf(1, "unsupported transport %d\n", sp->transport);
			return -1;
	}
	return 0;
}

/*
	Unmaps previous 64 bit mmap from mapped virtual address
*/
int mpm_transport_munmap64(mpm_transport_h h, void *va, uint32_t length)
{
	mpm_transport_cfg_t *sp = (mpm_transport_cfg_t *)h;
	if (!h) {
		mpm_printf(1, "invalid handle \n");
		return -1;
	}
	switch (sp->transport) {

#ifdef HYPLNK_TRANSPORT
		case hyperlink:
			if (mpm_transport_hyplnk_munmap64(sp, va, length)) {
				mpm_printf(1, "shm munmap failed\n");
				return -1;
			}
			break;
#endif
		case shared_memory:
		default:
			mpm_printf(1, "unsupported transport %d\n", sp->transport);
			return -1;
	}
	return 0;
}

/*
	Close mct connection.
*/
void mpm_transport_close(mpm_transport_h h)
{
	mpm_transport_cfg_t *sp = (mpm_transport_cfg_t *)h;
	if (!h) {
		mpm_printf(1, "invalid handle \n");
		return;
	}
	switch (sp->transport) {
		case shared_memory:
			mpm_transport_sharedmem_close(sp);
			break;
#ifdef HYPLNK_TRANSPORT
		case hyperlink:
			mpm_transport_hyplnk_close(sp);
			break;
#endif
		case qmss:
			mpm_transport_qmss_close(sp);
			break;
#ifdef SRIO_TRANSPORT
		case srio:
			mpm_transport_srio_close(sp);
			break;
#endif
		default:
			mpm_printf(1, "unsupported transport %d\n", sp->transport);
			return;
	}

	free(sp);
}

/*
	Creates a mmap for an extended 36-bit memory location and returns 32-bit virtual address.
*/
uint32_t mpm_transport_phys_map64(mpm_transport_h h, uint64_t addr, uint32_t length, mpm_transport_mmap_t *cfg)
{
	mpm_transport_cfg_t *sp = (mpm_transport_cfg_t *)h;
	if (!h) {
		mpm_printf(1, "invalid handle \n");
		return -1;
	}
	switch (sp->transport) {
		case shared_memory:
			mpm_printf(1, "shared_mem does not support phys map\n");
			break;
#ifdef HYPLNK_TRANSPORT
		case hyperlink:
			return (mpm_transport_hyplnk_phys_map64(sp, addr, length, cfg));
			break;
#endif
		default:
			mpm_printf(1, "unsupported transport %d\n", sp->transport);
	}
	return -1;
}

/*
	Similar to mpm_transport_read, except memcpy is replaced with EDMA3 operation.
	Returns 0 on success, error code on error.
*/
mpm_transport_trans_h mpm_transport_get_initiate64(mpm_transport_h h, uint64_t from_addr, uint64_t to_addr, uint32_t length, bool is_blocking, mpm_transport_read_t *cfg)
{
	mpm_transport_cfg_t *sp = (mpm_transport_cfg_t *)h;
	mpm_transport_trans_t *tp = (mpm_transport_trans_t *) NULL;
	if (!h) {
		mpm_printf(1, "invalid handle \n");
		return tp;
	}
	switch (sp->transport) {
		case shared_memory:
			mpm_printf(1, "shared_mem dma functions not supported\n");
			break;
#ifdef HYPLNK_TRANSPORT
		case hyperlink:
			tp = mpm_transport_hyplnk_get_initiate64(sp, from_addr, to_addr, length, is_blocking, cfg);
			if (tp == NULL)
				break;
			if (tp == MPM_TRANSPORT_TRANS_FAIL)
				mpm_printf(1, "hyplnk dma read failed\n");
			else
				mpm_printf(2, "got key: 0x%08x\n",tp->key);
			break;
#endif
		default:
			mpm_printf(1, "unsupported transport %d\n", sp->transport);
	}
	return tp;
}

/*
	Similar to mpm_transport_write, except memcpy is replaced with EDMA3 operation.
	Returns 0 on success, error code on error.
*/
mpm_transport_trans_h mpm_transport_put_initiate64(mpm_transport_h h, uint64_t to_addr, uint64_t from_addr, uint32_t length, bool is_blocking, mpm_transport_write_t *cfg)
{
	mpm_transport_cfg_t *sp = (mpm_transport_cfg_t *)h;
	mpm_transport_trans_t *tp = (mpm_transport_trans_t *) NULL;
	if (!h) {
		mpm_printf(1, "invalid handle \n");
		return tp;
	}
	switch (sp->transport) {
		case shared_memory:
			mpm_printf(1, "shared_mem dma functions not supported\n");
			break;
#ifdef HYPLNK_TRANSPORT
		case hyperlink:
			tp = mpm_transport_hyplnk_put_initiate64(sp, to_addr, from_addr, length, is_blocking, cfg);
			if (tp == NULL)
				break;
			if (tp == MPM_TRANSPORT_TRANS_FAIL)
				mpm_printf(1, "hyplnk dma write failed\n");
			else
				mpm_printf(2, "got key: 0x%08x\n",tp->key);
			break;
#endif
		default:
			mpm_printf(1, "unsupported transport %d\n", sp->transport);
	}
	return tp;
}

int mpm_transport_peripheral_enable(char *slave_name, mpm_transport_open_t *cfg)
{
	mpm_transport_cfg_t *sp = NULL;
	char *config_buf = NULL;
	cJSON *json = NULL;
	int retval = 0;
	FILE *fpr = NULL;
	struct stat buf;
	int flen;
	int res;

	if (!slave_name || !cfg) {
		mpm_printf(1, "Null parameter\n");
		goto close_n_exit;
	}
	if (strlen(slave_name) > MPM_MAX_NAME_LENGTH) {
		mpm_printf(1, "Name should be less than %d\n", MPM_MAX_NAME_LENGTH);
		goto close_n_exit;
	}

	sp = calloc(1, sizeof(mpm_transport_cfg_t));
	if (!sp) {
		mpm_printf(1, "can't allocate memory for handle\n");
		goto close_n_exit;
	}

	fpr = fopen(DEFAULT_CONFIG_FILE_NAME, "r");
	if (!fpr){
		mpm_printf(1, "Error opening file %s (error: %s)\n",
				DEFAULT_CONFIG_FILE_NAME, strerror(errno));
		goto close_n_exit;
	}

	if (fstat(fileno(fpr), &buf) != 0) {
		mpm_printf(1, "Error reading file %s (error: %s)\n",
				DEFAULT_CONFIG_FILE_NAME, strerror(errno));
		goto close_n_exit;
	}
	flen = buf.st_size;

	config_buf = (char *) malloc(flen + 1);
	if (!config_buf) {
		mpm_printf(1, "Error in allocating cfg buffer (error: %s)\n",
				strerror(errno));
		goto close_n_exit;
	}
	config_buf[flen] = 0;

	res = fread (config_buf, 1, flen, fpr);
	if (res != flen) {
		mpm_printf(1, "Error in reading cfg file (error: %s)\n",
				strerror(errno));
		goto close_n_exit;
	}

	json = cJSON_Parse(config_buf);
	if (!json) {
		mpm_printf(1, "JSON parser error before: [%s]\n",cJSON_GetErrorPtr());
		goto close_n_exit;
	}

	if (json_get_slave_info(json, slave_name, sp)) {
		mpm_printf(1, "Error in JSON parsing\n");
		goto close_n_exit;
	}

	switch (sp->transport) {
		case shared_memory:
			mpm_printf(2, "No special routine needed to enable shared_memory\n");
			goto close_n_exit;
			break;

#ifdef HYPLNK_TRANSPORT
		case hyperlink:
			if ((retval = mpm_transport_hyplnk_enable_peripheral(sp, cfg))) {
				mpm_printf(1, "hyplnk peripheral enable failed\n");
				goto close_n_exit;
			}
			break;
#endif
		default:
			mpm_printf(1, "unsupported transport %d\n", sp->transport);
			goto close_n_exit;
	}


close_n_exit:
	if(json) cJSON_Delete(json);
	if (config_buf) free(config_buf);
	if (fpr) fclose(fpr);
	free(sp);

	return retval;
}

int mpm_transport_peripheral_disable(char *slave_name)
{
	mpm_transport_cfg_t *sp = NULL;
	char *config_buf = NULL;
	cJSON *json = NULL;
	int retval = 0;
	FILE *fpr = NULL;
	struct stat buf;
	int flen;
	int res;

	if (!slave_name) {
		mpm_printf(1, "Null parameter\n");
		goto close_n_exit;
	}
	if (strlen(slave_name) > MPM_MAX_NAME_LENGTH) {
		mpm_printf(1, "Name should be less than %d\n", MPM_MAX_NAME_LENGTH);
		goto close_n_exit;
	}

	sp = calloc(1, sizeof(mpm_transport_cfg_t));
	if (!sp) {
		mpm_printf(1, "can't allocate memory for handle\n");
		goto close_n_exit;
	}

	fpr = fopen(DEFAULT_CONFIG_FILE_NAME, "r");
	if (!fpr){
		mpm_printf(1, "Error opening file %s (error: %s)\n",
				DEFAULT_CONFIG_FILE_NAME, strerror(errno));
		goto close_n_exit;
	}

	if (fstat(fileno(fpr), &buf) != 0) {
		mpm_printf(1, "Error reading file %s (error: %s)\n",
				DEFAULT_CONFIG_FILE_NAME, strerror(errno));
		goto close_n_exit;
	}
	flen = buf.st_size;

	config_buf = (char *) malloc(flen + 1);
	if (!config_buf) {
		mpm_printf(1, "Error in allocating cfg buffer (error: %s)\n",
				strerror(errno));
		goto close_n_exit;
	}
	config_buf[flen] = 0;

	res = fread (config_buf, 1, flen, fpr);
	if (res != flen) {
		mpm_printf(1, "Error in reading cfg file (error: %s)\n",
				strerror(errno));
		goto close_n_exit;
	}

	json = cJSON_Parse(config_buf);
	if (!json) {
		mpm_printf(1, "JSON parser error before: [%s]\n",cJSON_GetErrorPtr());
		goto close_n_exit;
	}

	if (json_get_slave_info(json, slave_name, sp)) {
		mpm_printf(1, "Error in JSON parsing\n");
		goto close_n_exit;
	}

	switch (sp->transport) {
		case shared_memory:
			mpm_printf(2, "No special routine needed to disable shared_memory\n");
			goto close_n_exit;
			break;
#ifdef HYPLNK_TRANSPORT
		case hyperlink:
			if ((retval = mpm_transport_hyplnk_disable_peripheral(sp))) {
				mpm_printf(1, "hyplnk peripheral disable failed\n");
				goto close_n_exit;
			}
			break;
#endif
		default:
			mpm_printf(1, "unsupported transport %d\n", sp->transport);
			goto close_n_exit;
	}


close_n_exit:
	if(json) cJSON_Delete(json);
	if (config_buf) free(config_buf);
	if (fpr) fclose(fpr);
	free(sp);

	return retval;
}

/**
 *  @b Description: Function mpm_transport_get_event_fd
 *  @n Returns the file descriptor to be used with read/select
 *     to wait on interrupt events
 *
 *  @param[in] transport_handle
 *		Transport handle
 *  @param[in] event_bitmap
 *             bit map of events. currently ignored. for future use.
 *	@retval
 *		file descriptor : positive integer
 *	@retval
 *		Failure - returns value less than 0 (negative integer)
 *  @pre  call to mpm_transport_open is needed before calling this routine
 *  @post
 *
 */
int mpm_transport_get_event_fd(mpm_transport_h transport_handle, uint32_t event_bitmap)
{
	int event_fd;

	mpm_transport_cfg_t *sp = (mpm_transport_cfg_t *)transport_handle;
	if (!transport_handle) {
		mpm_printf(1, "invalid handle \n");
		return -1;
	}
	switch (sp->transport) {
#ifdef HYPLNK_TRANSPORT
		case hyperlink:
			event_fd = mpm_transport_hyplnk_get_event_fd(sp, event_bitmap);
			break;
#endif
		default:
			mpm_printf(1, "unsupported transport %d\n", sp->transport);
			return -1;
	}
	return(event_fd);
}

/**
 *  @b Description: Function mpm_transport_read_and_ack_interrupt_event
 *  @n Routine read and ack the interrupt event occurred.
 *
 *
 *  @param[in]  transport_handle       Transport handle
 *  @param[in]  event_bitmap           Bitmap of events to ack
 *  @param[out] reported_event_bitmap  reported bitmap of events triggering interrupt
 *
 *  @retval
 *		Success - returns 0
 *  @retval
 *		Failure - returns value less than 0 (negative integer)
 *
 *  @pre  call to mpm_transport_open is needed before calling this routine
 *  @post
 */
int mpm_transport_read_and_ack_interrupt_event (mpm_transport_h transport_handle,
		uint32_t event_bitmap, uint32_t *reported_event_bitmap)
{
	mpm_transport_cfg_t *sp = (mpm_transport_cfg_t *)transport_handle;
	int retval=0;

	/* Read and Clear interrupt event with the peripheral*/
	switch (sp->transport) {
#ifdef HYPLNK_TRANSPORT
		case hyperlink:
			retval =
				mpm_transport_hyplnk_read_and_ack_interrupt_event
					(sp, event_bitmap, reported_event_bitmap);
			break;
#endif
		default:
			mpm_printf(1, "unsupported transport %d\n", sp->transport);
			return -1;
	}
	return(retval);

}

/**
 *  @b Description: Function mpm_transport_wait_for_interrupt_event
 *  @n Routine waits for the device interrupt to occur. Blocking call:
 *     sleep till either interrupt occurs or wait time expires
 *
 *
 *  @param[in] transport_handle  Transport handle
 *  @param[in] wait_time               Time out
 *  @param[in] event_bitmap
 *             bit map of events. Currently ignored, for future use.
 *  @param[out] reported_event_bitmap
 *              reported bitmap of events triggering interrupt
 *  @retval
 *		Success - returns 0 if time out or positive number
 *  @retval
 *		Failure - returns value less than 0 (negative integer)
 *
 *  @pre  call to mpm_transport_open is needed before calling this routine
 *  @post
 */
int mpm_transport_wait_for_interrupt_event(mpm_transport_h transport_handle,
		uint32_t event_bitmap, struct timeval *wait_time, uint32_t *reported_event_bitmap )
{
	int event_fd;
	int retval, retval1;
	uint32_t value;
	fd_set fdset;
	struct timeval init_time, current_time, delta_time;
	bool loop_condition;


	event_fd = mpm_transport_get_event_fd(transport_handle, event_bitmap);
	if(event_fd < 0) {
		mpm_printf(1, "\n  Error getting event fd");
		return(event_fd);
	}

	FD_ZERO(&fdset);
	FD_SET(event_fd, &fdset);

	/* Enable interrupt */
	value = 1;
	retval = write(event_fd, &value, sizeof(uint32_t));
	if( retval < 0 ) {
		mpm_printf(1, "\n Error writing to fd (error: %s)", strerror(errno));
		return(retval);
	}

	if(wait_time == NULL) {
		loop_condition = true;
	} else {
		/* Keep record of init time to hanlding timeout */
		retval = gettimeofday(&init_time, NULL);
		if( retval < 0 )
		{
			mpm_printf(1, "\n Error getting init timer");
			return(retval);
		}
	}

	do {

		/* Use select to pend on interrupt */
		retval = select(event_fd+1, &fdset, NULL, NULL, wait_time);
		if(retval < 0) {
			mpm_printf(1, "\nSelect api call failed (error: %s)", strerror(errno));
			return(retval);
		}
		if (retval == 0) {
			/* Timed out : no events reported */
			mpm_printf(1, "\n Timeout occurred  (error: %s)", strerror(errno));
			return(retval);
		}

		/* Read and ack interrupt event */
		retval1 = mpm_transport_read_and_ack_interrupt_event(transport_handle,
			event_bitmap, reported_event_bitmap);
		if (retval1 < 0 ) {
			mpm_printf(1, "\nread and ack interrupt failed");
			return(retval1);
		}
		/* Read file to ack the interrupt */
		retval1 = read(event_fd, &value, sizeof(uint32_t));
		if(retval1 == 0) {
			mpm_printf(1, "\nread  failed (error: %s)", strerror(errno));
			retval= -1;
		}

		/* Check if event map to see interrupt in bit map */
		if(event_bitmap & *reported_event_bitmap) {
			break;
		}

		if ( wait_time == NULL ) {
			continue;
		}

		/* Looks like got interrupt for event not in bit map */
		retval1 = gettimeofday(&current_time, NULL);
		if( retval1 < 0 ) {
			mpm_printf(1, "\n Error getting current timer (error: %s)", strerror(errno));
			return(retval1);
		}
		timersub(&current_time, &init_time, &delta_time);
		loop_condition = timercmp(&delta_time, wait_time, <) || (wait_time == NULL);
	}while (loop_condition);

	return(retval);
}


/**
 *  @b Description: Function mpm_transport_generate_interrupt_event
 *  @n Generate interrupt event.
 *
 *
 *  @param[in] transport_handle  Transport handle
 *  @param [in] event_bitmap      Bitmap of events triggering interrupt
 *
 *  @retval
 *		Success - returns 0
 *  @retval
 *		Failure - returns value less than 0 (negative integer)
 *
 *  @pre  call to mpm_transport_open is needed before calling this routine
 *  @post
 */
int mpm_transport_generate_interrupt_event (mpm_transport_h transport_handle,
		uint32_t event_bitmap)
{
	mpm_transport_cfg_t *sp = (mpm_transport_cfg_t *)transport_handle;
	int retval=0;

	/* Read and Clear interrupt event with the peripheral*/
	switch (sp->transport) {
#ifdef HYPLNK_TRANSPORT
		case hyperlink:
			retval =
				mpm_transport_hyplnk_generate_interrupt_event
					(sp, event_bitmap);
			break;
#endif
		default:
			mpm_printf(1, "unsupported transport %d\n", sp->transport);
			return -1;
	}
	return(retval);
}

/**
 *  @b Description
 *  @n Pushes a message of size len from pointer buf to the destination specified by addr_info
 *
 *	@retval
 *		Success - returns 0
 *	@retval
 *		Failure - returns value less than 0 (negative integer)
 *
 */
int mpm_transport_packet_send(mpm_transport_h h, char **buf, int *len, mpm_transport_packet_addr_t *addr_info, mpm_transport_send_t *cfg)
{
	mpm_transport_cfg_t *sp = (mpm_transport_cfg_t *) h;
	int ret = 0;

	switch (sp->transport) {
		case qmss:
			ret = mpm_transport_qmss_packet_send(sp, buf, len, addr_info, cfg);
			break;
#ifdef SRIO_TRANSPORT
		case srio:
			ret = mpm_transport_srio_packet_send(sp, buf, len, addr_info, cfg);
			break;
#endif
		default:
			mpm_printf(1, "mpm_transport_packet_send: unsupported transport %d\n", sp->transport);
			ret = -1;
			break;
	}
	return ret;
}

/**
 *  @b Description
 *  @n Pops information from the packet_addr_t binded to handle h. Data is
 *     copied to buf, of size len.
 *
 *     Valid for SRIO recv: The source address from which the data was
 *     received is returned in the src_addr structure.
 *
 *	@retval
 *		Success - returns 0
 *	@retval
 *		Failure - returns value less than 0 (negative integer)
 *
 */
int mpm_transport_packet_recv(mpm_transport_h h, char **buf, int *len,
							  mpm_transport_packet_addr_t *src_addr,
							  mpm_transport_recv_t *cfg)
{
	mpm_transport_cfg_t *sp = (mpm_transport_cfg_t *) h;
	int ret = 0;

	switch (sp->transport) {
		case qmss:
			ret = mpm_transport_qmss_packet_recv(sp, buf, len, cfg);
			break;
#ifdef SRIO_TRANSPORT
		case srio:
			ret = mpm_transport_srio_packet_recv(sp, buf, len, src_addr, cfg);
			break;
#endif
		default:
			mpm_printf(1, "mpm_transport_packet_recv: unsupported transport %d\n", sp->transport);
			ret = -1;
			break;
	}
	return ret;
}
