/*
 * Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include "mpm_transport_srio.h"
#include "mpm_transport_qmss_interface.h"
#include "fw_mem_allocator.h"

int srio_open_uio_device()
{
	int  ret;
	char class_name[MPM_MAX_NAME_LENGTH];
	char dev_name[MPM_MAX_NAME_LENGTH];

	ret = uioutil_get_peripheral_device("srio", class_name, MPM_MAX_NAME_LENGTH);

	if ( ret < 0 ) {
		mpm_printf(1, "\n Error getting SRIO device ");
		return(-1);
	}
	snprintf(dev_name, MPM_MAX_NAME_LENGTH, "/dev/%s", class_name);

	ret = open (dev_name, (O_RDWR | O_SYNC));
	if (ret < 0) {
		mpm_printf(1, "\n Error opening SRIO device ");
		return(-1);
	}

	return(ret);
}

void mpm_transport_srio_recycle_rx(void *transportData, Srio_DrvBuffer hDrvBuffer)
{
	mpm_transport_srio_t *td         = (mpm_transport_srio_t *) transportData;
	Cppi_HostDesc        *hostDesc   = (Cppi_HostDesc *) hDrvBuffer;

	Qmss_queuePushDescSize (td->rxFqHnd, (uint32_t*)hostDesc, td->descSize);
}

int mpm_transport_srio_init(mpm_transport_cfg_t *sp, mpm_transport_open_t *ocfg)
{
	mpm_transport_srio_t  *td             = (mpm_transport_srio_t *) sp->td;
	off_t                  pCMemBase      = 0;
	Srio_InitConfig        srioInitCfg;

	/* Store necessary information into our MPM Transport handle */
	if (ocfg->rm_info.rm_client_handle == NULL) {
		mpm_printf(1, "mpm_transport_srio_init(): RM client handle missing!\n");
		return -1;
	}
	sp->rmClientServiceHandle = (Rm_ServiceHandle *) ocfg->rm_info.rm_client_handle;
	td->deviceInit = ocfg->transport_info.srio.deviceInit;
	td->initCfg = ocfg->transport_info.srio.initCfg;
	td->deviceDeinit = ocfg->transport_info.srio.deviceDeinit;
	td->deinitCfg = ocfg->transport_info.srio.deinitCfg;
	td->qmssInit = ocfg->transport_info.qmss.qmss_init;

	/* Grab CMEM memory for DMA, descriptors, and peripheral usage */
	if (fw_memAllocInit(&pCMemBase,
						FW_QMSS_CMEM_SIZE) == fw_FALSE) {
		mpm_printf(1, "mpm_transport_srio_init(): fw_memAllocInit failed\n");
		return (-1);
	}

	/* Create virtual memory maps */
	/* QMSS CFG Regs */
	if (!fw_qmssCfgVaddr) {
		fw_qmssCfgVaddr = fw_memMap((void*)QMSS_CFG_BASE_ADDR,
												QMSS_CFG_BLK_SZ);
		if (!fw_qmssCfgVaddr)
		{
			mpm_printf(1, "mpm_transport_srio_init(): Failed to fw_memMap QMSS CFG registers\n");
			return (-1);
		}
	}

	/* QMSS DATA Regs */
	if (!fw_qmssDataVaddr) {
		fw_qmssDataVaddr = fw_memMap((void*)QMSS_DATA_BASE_ADDR,
												QMSS_DATA_BLK_SZ);
		if (!fw_qmssDataVaddr)
		{
			mpm_printf(1, "mpm_transport_srio_init(): Failed to fw_memMap QMSS DATA registers\n");
			return (-1);
		}
	}

	/* SRIO CFG Regs */
	if (!fw_srioCfgVaddr) {
		fw_srioCfgVaddr = fw_memMap((void*)SRIO_CFG_BASE_ADDR, SRIO_CFG_BLK_SZ);
		if (!fw_srioCfgVaddr)
		{
			printf("ERROR: Failed to map SRIO registers\n");
			return (-1);
		}
	}

	/* SRIO SERDES CFG Regs */
	if (!fw_srioSerdesCfgVaddr) {
		fw_srioSerdesCfgVaddr = fw_memMap((void*)SRIO_SERDES_CFG_BASE_ADDR, SRIO_SERDES_CFG_BLK_SZ);
		if (!fw_srioSerdesCfgVaddr)
		{
			printf("ERROR: Failed to map SRIO SERDES registers\n");
			return (-1);
		}
	}
	
	/* Setup QMSS and initialize CPPI */
	if (td->qmssInit){
		if (mpm_transport_init_qm(sp, td->maxDescNum))  {
			mpm_printf (1, "mpm_transport_srio_init(): mpm_transport_init_qm returned error, exiting\n");
			return (-1);
		}
	}

	if (mpm_transport_init_cppi(sp))  {
		mpm_printf (1, "initQm: initCppi failed\n");
		return (-1);
	}

	/* Turn on SRIO */
	td->srioFd = srio_open_uio_device();

	/* Only do the specified device initialization if presented. */
	if (td->deviceInit) {
		if ((td->deviceInit)(td->initCfg, fw_srioCfgVaddr, (uint32_t) fw_srioSerdesCfgVaddr) < 0) {
			mpm_printf(1, "mpm_transport_srio_init() : error trying to do srio device init\n");
		}
	}

	/* Initialize the SRIO Driver for this task/address space */
	srioInitCfg.rmServiceHandle = (Rm_ServiceHandle *) sp->rmClientServiceHandle;
	srioInitCfg.srioCfgVAddr = fw_srioCfgVaddr;
	if (Srio_initCfg(&srioInitCfg) < 0 ) {
		mpm_printf(1, "mpm_transport_srio_init() : error trying to init srio config\n");
	}
	return 0;
}

Qmss_QueueHnd mpm_transport_srio_init_desc(mpm_transport_cfg_t *sp, char *name, mpm_transport_qmss_desc_info_t *params)
{
	mpm_transport_srio_t *td = (mpm_transport_srio_t *) sp->td;
	Cppi_DescCfg descCfg;
	Qmss_QueueHnd tmpQ;
	Cppi_Desc *desc;
	uint8_t *descPtr;
	uint32_t numAllocated;
	int i;
	Qmss_Queue queueInfo;

	mpm_printf(2, "mpm_transport_srio_init_desc() got params:"
		"\n\tname = %s"
		"\n\tqid = %d"
		"\n\tnumDesc = %d"
		"\n\tsizeBuf = %d\n", name, params->qid, params->numDesc, params->sizeBuf);

	/* Setup the descriptors for transmit free queue */
	memset(&descCfg, 0, sizeof(descCfg));
	descCfg.memRegion = (Qmss_MemRegion)td->memReg;
	descCfg.descNum = params->numDesc;
	descCfg.destQueueNum = params->qid;
	descCfg.queueType = Qmss_QueueType_GENERAL_PURPOSE_QUEUE;
	descCfg.initDesc = Cppi_InitDesc_INIT_DESCRIPTOR;
	descCfg.descType = Cppi_DescType_HOST;
	descCfg.epibPresent = Cppi_EPIB_NO_EPIB_PRESENT;

	/* Descriptor should be recycled to TX completion queue */
	descCfg.returnQueue.qMgr = QMSS_PARAM_NOT_SPECIFIED;
	descCfg.returnQueue.qNum = QMSS_PARAM_NOT_SPECIFIED;

	descCfg.returnPushPolicy      = Qmss_Location_TAIL;
	descCfg.cfg.host.returnPolicy = Cppi_ReturnPolicy_RETURN_ENTIRE_PACKET;
	descCfg.cfg.host.psLocation   = Cppi_PSLoc_PS_IN_DESC;

	/* Initialize the descriptors and push to free Queue */
	if ((tmpQ = Cppi_initDescriptor (&descCfg, &numAllocated)) < 0)
	{
		mpm_printf(1, "mpm_transport_srio_init_desc(): Error initing descriptors\n");
		return (Qmss_QueueHnd) NULL;
	}
	queueInfo = Qmss_getQueueNumber(tmpQ);

	/* If buffer size specify, allocate memory... */
	if (params->sizeBuf) {
		descPtr = (uint8_t *)fw_memAlloc((params->sizeBuf * params->numDesc), CACHE_LINESZ);
		if (descPtr == NULL) {
			printf ("mpm_transport_srio_init_desc(): Failed to fw_memAlloc descPtr\n");
			return (Qmss_QueueHnd) NULL;
		}
		for (i = 0; i<numAllocated; i++) {
			if ((desc = (Cppi_Desc *) Qmss_queuePop(tmpQ)) == NULL) {
				mpm_printf(1, "mpm_transport_srio_init_desc(): error grabbing a packet from rx fq %d\n", tmpQ);
				return (Qmss_QueueHnd) NULL;
			}
			/* Alignment */
			desc = (Cppi_Desc *) QMSS_DESC_PTR(desc);
			Cppi_setData (Cppi_DescType_HOST, desc, descPtr + (i * params->sizeBuf), params->sizeBuf);
			Cppi_setOriginalBufInfo (Cppi_DescType_HOST, desc, descPtr + (i * params->sizeBuf), params->sizeBuf);
			Cppi_setReturnQueue(Cppi_DescType_HOST, desc, queueInfo);
			Qmss_queuePushDescSize(tmpQ, (void *) desc, td->descSize);
		}
	}
	mpm_printf(2, "\tFinished setting up queue: %d\n",Qmss_getQIDFromHandle(tmpQ));
	return tmpQ;
}

int mpm_transport_srio_insert_mem_region(mpm_transport_cfg_t *sp, mpm_transport_qmss_mem_region_info_t *params)
{
	mpm_transport_srio_t *td = (mpm_transport_srio_t *) sp->td;
	Qmss_MemRegInfo memInfo;
	uint8_t *descPtr;

	descPtr = (uint8_t *)fw_memAlloc((params->regionDescNum * params->regionDescSize), CACHE_LINESZ);
	if (descPtr == NULL) {
		mpm_printf (1, "mpm_transport_srio_insert_mem_region(): Failed to allocate descriptors\n");
		return (-1);
	}
	mpm_printf(2, "mpm_transport_srio_insert_mem_region() got params:"
		"\n\tdesc size = %d"
		"\n\tdesc num = %d"
		"\n\tregion num = %d"
		"\n\tstart index = %d\n", params->regionDescSize, params->regionDescNum, params->regionNum, params->regionStartIdx);

	/* Setup memory region for descriptors */
	memset(&memInfo, 0, sizeof(memInfo));
	memset ((void *) descPtr, 0, params->regionDescNum * params->regionDescSize);
	memInfo.descBase = (uint32_t *) descPtr;
	memInfo.descSize = params->regionDescSize;
	memInfo.descNum = params->regionDescNum;
	memInfo.manageDescFlag = Qmss_ManageDesc_MANAGE_DESCRIPTOR;
	memInfo.memRegion = params->regionNum;
	memInfo.startIndex = params->regionStartIdx;
	td->memReg = Qmss_insertMemoryRegion (&memInfo);
	if (td->memReg < QMSS_SOK) {
		mpm_printf(1, "mpm_transport_srio_insert_mem_region(): Error inserting region %d\n", td->memReg);
		return -1;
	}
	td->descSize = params->regionDescSize;

	return 0;
}

int mpm_transport_srio_open(mpm_transport_cfg_t *sp, mpm_transport_open_t *ocfg)
{
	mpm_transport_srio_t *td = (mpm_transport_srio_t *) sp->td;
	Srio_DrvConfig srioCfg;
	Srio_SockBindAddrInfo bindInfo;
	Qmss_Queue queueInfo;

	td->rxFd = mpm_transport_qmss_open_qpend_and_fd("qpend", &(td->rxqHnd));
	if (td->rxFd < 0) {
		mpm_printf(1, "mpm_transport_srio_open(): Error obtaining qpend queue and fd\n");
		return -1;
	}

	mpm_printf(2, "mpm_transport_srio_open(): opened qpend %d\n", td->rxqHnd);

	/* SRIO driver configuration */
	memset((void *)&srioCfg, 0, sizeof(srioCfg));
	/* App-managed: handle all aspects of SRIO config and buffer management */
	srioCfg.bAppManagedConfig = TRUE;

	srioCfg.u.appManagedCfg.bIsRxFlowCfgValid = 1;
	/* Configure the receive flow */
	srioCfg.u.appManagedCfg.rxFlowCfg.flowIdNum          = -1;
	queueInfo = Qmss_getQueueNumber(td->rxqHnd);
	srioCfg.u.appManagedCfg.rxFlowCfg.rx_dest_qnum       = queueInfo.qNum;
	srioCfg.u.appManagedCfg.rxFlowCfg.rx_dest_qmgr       = queueInfo.qMgr;
	srioCfg.u.appManagedCfg.rxFlowCfg.rx_desc_type       = 0x1; /* Host descriptor */
	srioCfg.u.appManagedCfg.rxFlowCfg.rx_psinfo_present  = 0x1; /* PS information */
	/* Use the rxFreeQ for picking up descriptors */
	queueInfo = Qmss_getQueueNumber(td->rxFqHnd);
	srioCfg.u.appManagedCfg.rxFlowCfg.rx_fdq1_qnum       = queueInfo.qNum;
	srioCfg.u.appManagedCfg.rxFlowCfg.rx_fdq1_qmgr       = queueInfo.qMgr;
	/* Use the rxFreeQ for picking the SOP packet also */
	srioCfg.u.appManagedCfg.rxFlowCfg.rx_fdq0_sz0_qnum   = queueInfo.qNum;
	srioCfg.u.appManagedCfg.rxFlowCfg.rx_fdq0_sz0_qmgr   = queueInfo.qMgr;

	/* Populate the rest of the receive configuration. */
	srioCfg.u.appManagedCfg.rawRxFreeDrvBufferArg = mpm_transport_srio_recycle_rx;
	srioCfg.u.appManagedCfg.rawRxFreeDrvBufferFuncArg = (void *) td;
	srioCfg.u.appManagedCfg.rxDescSize = td->descSize;

	/* Specify that Srio queue allocation is up to the driver */
	srioCfg.u.appManagedCfg.txQueueNum = QMSS_PARAM_NOT_SPECIFIED;

	/* Use the Receive Queue for picking all descriptors. */
	queueInfo = Qmss_getQueueNumber(td->rxFqHnd);
	srioCfg.u.appManagedCfg.rxFlowCfg.rx_fdq1_qnum       = queueInfo.qNum;
	srioCfg.u.appManagedCfg.rxFlowCfg.rx_fdq1_qmgr       = queueInfo.qMgr;
	srioCfg.u.appManagedCfg.rxFlowCfg.rx_fdq2_qnum       = queueInfo.qNum;
	srioCfg.u.appManagedCfg.rxFlowCfg.rx_fdq2_qmgr       = queueInfo.qMgr;
	srioCfg.u.appManagedCfg.rxFlowCfg.rx_fdq3_qnum       = queueInfo.qNum;
	srioCfg.u.appManagedCfg.rxFlowCfg.rx_fdq3_qmgr       = queueInfo.qMgr;

	/* Use the Receive Queue for picking the SOP packet also. */
	srioCfg.u.appManagedCfg.rxFlowCfg.rx_fdq0_sz0_qnum   = queueInfo.qNum;
	srioCfg.u.appManagedCfg.rxFlowCfg.rx_fdq0_sz0_qmgr   = queueInfo.qMgr;

	/* Provide service RM handle to SRIO.  SRIO will manage own resources
	 * if service handle is NULL */
	srioCfg.rmServiceHandle = sp->rmClientServiceHandle;

	/* Start the SRIO Driver */
	td->srioHnd = Srio_start(&srioCfg);
	if (td->srioHnd == NULL) {
		mpm_printf(1, "mpm_transport_srio_open(): failed to start srio\n");
		return -1;
	}

	if (ocfg->transport_info.srio.type == packet_addr_type_SRIO_TYPE9) {
		td->srioSockHnd = Srio_sockOpen(td->srioHnd, Srio_SocketType_RAW_TYPE9, FALSE);
	}
	else if (ocfg->transport_info.srio.type == packet_addr_type_SRIO_TYPE11) {
		td->srioSockHnd = Srio_sockOpen(td->srioHnd, Srio_SocketType_RAW_TYPE11, FALSE);
	}
	if (td->srioSockHnd == NULL) {
		mpm_printf(1, "mpm_transport_srio_open(): failed to open socket!\n");
		return -1;
	}

	if (ocfg->transport_info.srio.type == packet_addr_type_SRIO_TYPE9) {
		bindInfo.type9.tt         = ocfg->transport_info.srio.type9.tt;
		bindInfo.type9.id         = ocfg->transport_info.srio.type9.id;
		bindInfo.type9.cos        = ocfg->transport_info.srio.type9.cos;
		bindInfo.type9.streamId   = ocfg->transport_info.srio.type9.streamId;
	}
	else if (ocfg->transport_info.srio.type == packet_addr_type_SRIO_TYPE11) {
		bindInfo.type11.tt       = ocfg->transport_info.srio.type11.tt;
		bindInfo.type11.id       = ocfg->transport_info.srio.type11.id;
		bindInfo.type11.letter   = ocfg->transport_info.srio.type11.letter;
		bindInfo.type11.mbox     = ocfg->transport_info.srio.type11.mailbox;
		bindInfo.type11.segMap   = ocfg->transport_info.srio.type11.segmap;
	}
	else {
		mpm_printf (1, "mpm_transport_srio_open(): packet type not supported!!\n");
		return -1;
	}

	if (Srio_sockBind(td->srioSockHnd, &bindInfo) < 0) {
		mpm_printf(1, "mpm_transport_srio_open(): failed to bind socket info\n");
		return -1;
	}

	/* Set socket receive queue to be number of descriptors RX descriptors
	 * to avoid socket receive overflow */
	if (Srio_setSockOpt(td->srioSockHnd, Srio_Opt_PENDING_PKT_COUNT,
						(void *) &td->rxDescNum, sizeof(uint16_t))) {
		mpm_printf(1, "mpm_transport_srio_open(): failed to set sock opt\n");
		return -1;
	}

	return 0;
}

void mpm_transport_srio_close(mpm_transport_cfg_t *sp)
{
	mpm_transport_srio_t *td = (mpm_transport_srio_t *) sp->td;
	Qmss_Result result;

	/* close qpend descriptor */
	close(td->rxFd);

	mpm_printf(2,"mpm_transport_srio_close(): Exit queue count for td->txFqHnd: %d\n",Qmss_getQueueEntryCount(td->txFqHnd));
	mpm_printf(2,"mpm_transport_srio_close(): Exit queue count for td->rxFqHnd: %d\n",Qmss_getQueueEntryCount(td->rxFqHnd));
	mpm_printf(2,"mpm_transport_srio_close(): Exit queue count for td->rxqHnd: %d\n",Qmss_getQueueEntryCount(td->rxqHnd));
	/* empty and close queues used by this handle */
	Qmss_queueEmpty(td->txFqHnd);
	Qmss_queueEmpty(td->rxFqHnd);
	Qmss_queueClose(td->txFqHnd);
	Qmss_queueClose(td->rxFqHnd);
	Qmss_queueClose(td->rxqHnd);

	/* Close the SRIO-related instances used by this handle */
	Srio_sockClose(td->srioSockHnd);

	if (Srio_stop(td->srioHnd) < 0) {
			mpm_printf(1, "mpm_transport_srio_close(): error stopping srio\n");
	}

	if (Srio_close() < 0) {
			mpm_printf(1, "mpm_transport_srio_close(): error closing srio\n");
	}

	if (td->deviceDeinit) {
		if ((td->deviceDeinit)(td->deinitCfg, fw_srioCfgVaddr) < 0) {
			mpm_printf(1, "mpm_transport_srio_close(): error SrioDevice_deinit\n");
		}
	}

	/* Remove memory region used by this handle */
	if ((result = Qmss_removeMemoryRegion(td->memReg, 0)) != QMSS_SOK) {
		mpm_printf(1,"mpm_transport_srio_close(): Error removing region\n");
	}

	/* Power down the peripheral (only if this is the last opened FD in the system) */
	close(td->srioFd);

	if (td->qmssInit) {
		/* Handle is closing, exit CPPI */
		Cppi_exit();
	
		/* Handle is closing, exit QMSS*/
		if ((result = Qmss_exit()) != QMSS_SOK) {
			mpm_printf(1,"mpm_transport_srio_close(): Error qmss exit %d\n", result);
		}
	}

	/* Release the CMEM region taken by MPM Transport */
	fw_memAllocExit ();

	/* Free the transport data struct */
	free(td);
}


int mpm_transport_srio_packet_send(mpm_transport_cfg_t *sp, char **buf, int *len, mpm_transport_packet_addr_t *addr_info, mpm_transport_send_t *cfg)
{
	mpm_transport_srio_t *td = (mpm_transport_srio_t *) sp->td;
	Srio_SockAddrInfo to;
	uint32_t buffLen;
	uint8_t *tempBuf;
	Cppi_Desc *desc;
	packet_send_buffer_opt bufferOption;

	if ((desc = (Cppi_Desc *) Qmss_queuePop(td->txFqHnd)) == NULL) {
		mpm_printf(1, "mpm_transport_srio_packet_send(): error grabbing a packet from fq %d\n", td->txFqHnd);
		return -1;
	}

	/* Alignment */
	desc = (Cppi_Desc *) QMSS_DESC_PTR(desc);

	/* Grab the packet's buffer into tempBuf */
	Cppi_getOriginalBufInfo(Cppi_DescType_HOST, desc, &tempBuf, &buffLen);

	if (cfg == NULL) {
		bufferOption = packet_send_BUFFER_DEFAULT;
	}
	else {
		bufferOption = cfg->buf_opt;
	}

	if (tempBuf == NULL) {
		if (bufferOption == packet_send_MEMCPY_BUFFER) {
			mpm_printf(1, "mpm_transport_srio_packet_send(): no buffer attached to desc, can't use memcpy\n");
			return -1;
		}
	}

	if ( (bufferOption == packet_send_MEMCPY_BUFFER) || (bufferOption == packet_send_BUFFER_DEFAULT)) {
		if (*len > buffLen) {
			mpm_printf(1, "mpm_transport_srio_packet_send(): Error, tried to send more data than buffer size!\n");
			return -1;
		}
		else {
			memcpy(tempBuf, *buf, *len);
			Cppi_setDataLen(Cppi_DescType_HOST, desc, *len);
		}
	}
	else if (bufferOption == packet_send_LINK_BUFFER_POINTER) {
		Cppi_setData(Cppi_DescType_HOST, desc, (uint8_t *) (*buf), *len);
	}
	
	Cppi_setPacketLen(Cppi_DescType_HOST, desc, *len);

	if (addr_info->addr_type == packet_addr_type_SRIO_TYPE9) {
		to.type9.tt         = ((addr_info->addr)).srio.type9.tt;
		to.type9.id         = ((addr_info->addr)).srio.type9.id;
		to.type9.cos        = ((addr_info->addr)).srio.type9.cos;
		to.type9.streamId   = ((addr_info->addr)).srio.type9.streamId;
	}
	else if (addr_info->addr_type == packet_addr_type_SRIO_TYPE11) {
		to.type11.tt       = ((addr_info->addr)).srio.type11.tt;
		to.type11.id       = ((addr_info->addr)).srio.type11.id;
		to.type11.letter   = ((addr_info->addr)).srio.type11.letter;
		to.type11.mbox     = ((addr_info->addr)).srio.type11.mailbox;
	}
	else {
		mpm_printf (1, "mpm_transport_srio_packet_send(): packet type not supported!!\n");
		return -1;
	}

	if (Srio_sockSend (td->srioSockHnd, (Srio_DrvBuffer) desc, td->descSize, &to) < 0) {
		mpm_printf (1, "mpm_transport_srio_packet_send(): sock send failed!\n");
		return -1;
	}

	/* TODO: setup completion queue and free it instead of recycling back to FQ */

	return 0;
}

int mpm_transport_srio_packet_recv(mpm_transport_cfg_t *sp, char **buf,
								   int *len,
								   mpm_transport_packet_addr_t *src_addr,
								   mpm_transport_recv_t *cfg)
{
	mpm_transport_srio_t *td = (mpm_transport_srio_t *) sp->td;
	int32_t num_bytes;
	Srio_SockAddrInfo from;
	uint32_t count;
	uint32_t interrupt = 1;
	Cppi_Desc *desc;
	uint32_t buffLen;
	uint8_t *tempBuf;
	packet_recv_wait_opt opt;

	if (cfg == NULL) {
		opt = packet_recv_wait_DEFAULT;
	}
	else {
		opt = cfg->wait_opt;
	}

	Srio_rxCompletionIsr (td->srioHnd);
	num_bytes = Srio_sockRecv (td->srioSockHnd, (Srio_DrvBuffer*)&desc, &from);
	if (num_bytes < 0) {
		mpm_printf (1, "mpm_transport_srio_packet_recv(): sock recv failed!\n");
		return -1;
	}
	else if (num_bytes == 0) {
		if ((opt == packet_recv_wait_DEFAULT) || (opt == packet_recv_BLOCKING_INTERRUPT)) {
			if (write(td->rxFd, &interrupt, sizeof(interrupt)) != sizeof(interrupt)) {
				mpm_printf(1, "mpm_transport_srio_packet_recv(): unable to enable interrupt\n");
			}
			else if ( (read (td->rxFd, &count, sizeof(count))) != sizeof(count))
			{
				mpm_printf(1, "mpm_transport_srio_packet_recv(): unable to read on block for interrupt\n");
			}
			Srio_rxCompletionIsr (td->srioHnd);
			num_bytes = Srio_sockRecv (td->srioSockHnd, (Srio_DrvBuffer*)&desc, &from);
		}
		else {
			mpm_printf(1, "mpm_transport_srio_packet_recv(): Invalid wait option!\n");
			return -1;
		}
	}

	Cppi_getData(Cppi_DescType_HOST, desc, &tempBuf, &buffLen);

	if (*buf == NULL) {
		*len = buffLen;
		*buf = (char *) tempBuf;
		// PUSH TO RX COMPLETE Q
	}
	else {
		if (*len < buffLen) {
			mpm_printf(1, "mpm_transport_srio_packet_recv(): "
					   "Error, received %d bytes but "
					   "provided buffer only has %d bytes!\n", buffLen, *len);
			return -1;
		}
		else {
			memcpy(*buf, tempBuf, buffLen);
			*len = buffLen;
		}
	}

	if (src_addr) {
		if (src_addr->addr_type == packet_addr_type_SRIO_TYPE9) {
			((src_addr->addr)).srio.type9.tt       = from.type9.tt;
			((src_addr->addr)).srio.type9.id       = from.type9.id;
			((src_addr->addr)).srio.type9.cos      = from.type9.cos;
			((src_addr->addr)).srio.type9.streamId = from.type9.streamId;
		}
		else if (src_addr->addr_type == packet_addr_type_SRIO_TYPE11) {
			((src_addr->addr)).srio.type11.tt      = from.type11.tt;
			((src_addr->addr)).srio.type11.id      = from.type11.id;
			((src_addr->addr)).srio.type11.letter  = from.type11.letter;
			((src_addr->addr)).srio.type11.mailbox = from.type11.mbox;
		}
	}

	Srio_freeRxDrvBuffer(td->srioSockHnd, (Srio_DrvBuffer)desc);

	return 0;
}
