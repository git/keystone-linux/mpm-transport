/*
 * Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef __MPM_TRANSPORT_SRIO_H__
#define __MPM_TRANSPORT_SRIO_H__

#include "mpm_transport_qmss_interface.h"
#include "mpm_transport_cfg.h"

/* SRIO LLD include */
#include <ti/drv/srio/srio_drv.h>

typedef struct mpm_transport_srio_tag {

	/* Name for the TX free queue handle
		To be requested from RM */
	char txFqName[MPM_MAX_NAME_LENGTH];

	/* Free queue handle, to pop descriptors from
		Descriptors are used for sending */
	Qmss_QueueHnd txFqHnd;

	/* Name for the RX free queue handle
		To be requested from RM */
	char rxFqName[MPM_MAX_NAME_LENGTH];

	/* Free queue handle, to pop descriptors from
		Descriptors are used for receiving */
	Qmss_QueueHnd rxFqHnd;

	/* Receive handle, to pop descriptors from
		Will read payload if necessary */
	Qmss_QueueHnd rxqHnd;

	/* File descriptor for receive queue
		Using QPEND queues - can read this for interrupts when packet arrives */
	int rxFd;

	/* Name for the memory region info opened in JSON */
	char regionName[MPM_MAX_NAME_LENGTH];

	/* QMSS memory region information */
	Qmss_Result memReg;

	/* Maximum number of descriptors to initialize QMSS with */
	int maxDescNum;

	/* Size of tx buff */
	int txBufSize;

	/* Size of descriptors */
	int descSize;

	/* SRIO file descriptor (enables/disables the peripheral) */
	int srioFd;

	/* SRIO Handle */
	Srio_DrvHandle srioHnd;

	/* SRIO Socket Handle */
	Srio_SockHandle srioSockHnd;

	/* Number of RX Descriptors */
	int rxDescNum;

	/* Device init function */
	int (*deviceInit) (void *initCfg, void *srioAddr, uint32_t serdesAddr);

	/* Device init param */
	void *initCfg;

	/* Deinit function */
	int (*deviceDeinit) (void *deinitCfg, void *srioAddr);

	/* Device deinit param */
	void *deinitCfg;

	/* Flag for initializing QMSS */
	int qmssInit;

} mpm_transport_srio_t;

int mpm_transport_srio_init(mpm_transport_cfg_t *sp, mpm_transport_open_t *ocfg);
Qmss_QueueHnd mpm_transport_srio_init_desc(mpm_transport_cfg_t *sp, char *name, mpm_transport_qmss_desc_info_t *params);
int mpm_transport_srio_insert_mem_region(mpm_transport_cfg_t *sp, mpm_transport_qmss_mem_region_info_t *params);
int mpm_transport_srio_open(mpm_transport_cfg_t *sp, mpm_transport_open_t *ocfg);
void mpm_transport_srio_close(mpm_transport_cfg_t *sp);
int mpm_transport_srio_packet_send(mpm_transport_cfg_t *sp, char **buf, int *len, mpm_transport_packet_addr_t *addr_info, mpm_transport_send_t *cfg);
int mpm_transport_srio_packet_recv(mpm_transport_cfg_t *sp, char **buf, int *len, mpm_transport_packet_addr_t *src_addr, mpm_transport_recv_t *cfg);

#ifdef STATIC_BUILD
void Srio_freeRxDrvBuffer(Srio_SockHandle srioSock, Srio_DrvBuffer hDrvBuffer) __attribute__((weak));
int32_t Srio_sockSend(Srio_SockHandle srioSock, Srio_DrvBuffer hBuffer, uint32_t size, Srio_SockAddrInfo* to) __attribute__((weak));
int32_t Srio_close(void) __attribute__((weak));
Srio_SockHandle Srio_sockOpen(Srio_DrvHandle hSrio, Srio_SocketType type,uint16_t isBlocking) __attribute__((weak));
int32_t Srio_initCfg(Srio_InitConfig *ptr_cfg) __attribute__((weak));
void Srio_rxCompletionIsr(Srio_DrvHandle hSrioDrv) __attribute__((weak));
int32_t Srio_sockClose(Srio_SockHandle srioSock) __attribute__((weak));
Srio_DrvHandle Srio_start(Srio_DrvConfig* ptr_cfg) __attribute__((weak));
int32_t Srio_sockRecv(Srio_SockHandle srioSock, Srio_DrvBuffer* hDrvBuffer,Srio_SockAddrInfo* from) __attribute__((weak));
int32_t Srio_sockBind(Srio_SockHandle srioSock, Srio_SockBindAddrInfo* ptr_addrInfo) __attribute__((weak));
int32_t Srio_setSockOpt(Srio_SockHandle srioSock, Srio_Opt option,void* optval,int32_t optlen) __attribute__((weak));
int32_t Srio_stop(Srio_DrvHandle hSrio) __attribute__((weak));
#endif

#endif
