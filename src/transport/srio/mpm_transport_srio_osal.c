/*
 *  Copyright (c) Texas Instruments Incorporated 2015
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include "fw_mem_allocator.h"
#include "mpm_transport_srio.h"

uint32_t    Osal_srio_MallocCounter = 0;
uint32_t    Osal_srio_FreeCounter = 0;

Ptr Osal_srioMalloc (uint32_t num_bytes)
{
	Ptr ret;
	Osal_srio_MallocCounter++;
	num_bytes += (CACHE_LINESZ-1);
	ret = malloc (num_bytes);
	if(ret==NULL)
	{
		printf("\nERROR! SRIO Malloc failed!\n");
	}
	return ret;
}

void Osal_srioFree (Ptr ptr, uint32_t size)
{
	/* Increment the free counter. */
	Osal_srio_FreeCounter++;
	free(ptr);
}

void Osal_srioBeginMemAccess (void *blockPtr, uint32_t size)
{
	Osal_invalidateCache(blockPtr,size);
	return;
}

void  Osal_srioEndMemAccess (void *blockPtr, uint32_t size)
{
	Osal_writeBackCache(blockPtr,size);
	return;
}

void *Osal_srioEnterMultipleCoreCriticalSection()
{
	/* Stub Function. TBD: Would need to handle when for multi proc access
	 * To be handled using infrastructure available from Kernel
	 */
	return NULL;
}


void Osal_srioExitMultipleCoreCriticalSection (void *key)
{
	/* Stub Function. TBD: Would need to handle when for multi proc access
	 * To be handled using infrastructure available from Kernel
	 */
	return;
}

void *Osal_srioEnterSingleCoreCriticalSection()
{
	/* Stub Function. TBD: Would need to handle when for multi proc access
	 * To be handled using infrastructure available from Kernel
	 */
	return NULL;
}


void Osal_srioExitSingleCoreCriticalSection (void *key)
{
	/* Stub Function. TBD: Would need to handle when for multi proc access
	 * To be handled using infrastructure available from Kernel
	 */
	return;
}

void Osal_srioLog ( String fmt, ... )
{
}

/**
 *  @b Description
 *  @n
 *      The function is used to create a critical section.
 *
 *  @retval
 *      Semaphore Handle created
 */
void* Osal_srioCreateSem(void)
{
	return NULL;
}

/**
 *  @b Description
 *  @n
 *      The function is used to delete a critical section.
 *
 *  @param[in]  semHandle
 *      Semaphore handle to be deleted
 *
 *  @retval
 *      Not Applicable
 */
void Osal_srioDeleteSem(void* semHandle)
{
}

/**
 *  @b Description
 *  @n
 *      The function is used to pend on a semaphore
 *
 *  @param[in]  semHandle
 *      Semaphore handle on which the API will pend
 *
 *  @retval
 *      Not Applicable
 */
void Osal_srioPendSem(void* semHandle)
{
}

/**
 *  @b Description
 *  @n
 *      The function is used to post a semaphore
 *
 *  @param[in]  semHandle
 *      Semaphore handle which will be posted
 *
 *  @retval
 *      Not Applicable
 */
void Osal_srioPostSem(void* semHandle)
{
}

/**
 *  @b Description
 *  @n
 *      The function is invoked by the SRIO Driver to indicate that a
 *      descriptor is being accessed.
 *
 *  @param[in]  drvHandle
 *      Driver Instance for which descriptor is being accessed.
 *  @param[in]  ptr
 *      Pointer to the descriptor being accessed
 *  @param[in]  size
 *      Size of the descriptor (Only valid for Driver Managed)
 *
 *  @retval
 *      None
 */
void Osal_srioBeginDescriptorAccess (Srio_DrvHandle drvHandle, void* ptr, uint32_t descSize)
{
	/* In the Application all descriptor are located in LL2 memory we dont need to
	 * add any special cache invalidation hook here. However if the descriptors were located
	 * in SL2 (MSMC) this would be required. */
	return;
}

/**
 *  @b Description
 *  @n
 *      The function is invoked by the SRIO Driver to indicate that a
 *      descriptor is finished being accessed.
 *
 *  @param[in]  drvHandle
 *      Driver Instance for which descriptor is being accessed.
 *  @param[in]  ptr
 *      Pointer to the descriptor being accessed
 *  @param[in]  size
 *      Size of the descriptor (Only valid for Driver Managed)
 *
 *  @retval
 *      None
 */
void Osal_srioEndDescriptorAccess (Srio_DrvHandle drvHandle, void* ptr, uint32_t descSize)
{
	/* In the Application the descriptors are located in Core 0 Global address space
	 * If they are accessed from other cores we need to ensure that there is an MFENCE so
	 * that all the access to the descriptors are complete before we start pushing them
	 * out. */
	__sync_synchronize();
}

/**
 *  @b Description
 *  @n
 *      The function is used to allocate a data buffer of the specified
 *      size. Data buffers should always be allocated from the global
 *      address space.
 *
 *  @param[in]  numBytes
 *      Number of bytes to be allocated.
 *
 *  @retval
 *      Allocated block address
 */
void* Osal_srioDataBufferMalloc(uint32_t numBytes)
{
	return (void *) NULL;
}

/**
 *  @b Description
 *  @n
 *      The function is used to clean up a previously allocated data buffer
 *      block. All data buffers are in the global address space
 *
 *  @param[in]  ptr
 *      Pointer to the data buffer block to be cleaned up
 *  @param[in]  size
 *      Size of the data buffer
 *
 *  @retval
 *      Not Applicable
 */
void Osal_srioDataBufferFree(void* ptr, uint32_t numBytes)
{
	return;
}


