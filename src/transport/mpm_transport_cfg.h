/*
 * Copyright (C) 2013-2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef __MPMDB_H__
#define __MPMDB_H__

#include <stdio.h>
#include <stdint.h>
#include <stdarg.h>

#include "mpm_transport.h"
#include "mpm_transport_edma3.h"
#include "mpm_transport_keystone_mmap.h"
#include "mpm_transport_time_profile.h"
#include "mpm_transport_rm.h"

//#define DEBUG
//#define SYSLOG_DEBUG
//#define LOGFILE_DEBUG

#ifdef SYSLOG_DEBUG
#include "syslog.h"
#endif

#ifdef LOGFILE_DEBUG
extern FILE * debug_fp;
#endif

#define JSON_PARSER_VERSION "1.0.0.0"

#define MAX_FILE_NAME_LEN 		128
#define MPM_MAX_NAME_LENGTH		32
#define MPM_MAX_MMAP_SECTIONS	16
#define MPM_MAX_SEGMENTS		64
#define MPM_MAX_TRANSPORTS		32
#define MPM_MAX_DMA_PARAMS		16
#define MPM_MAX_DMA_PREALLOC	16
#define MPM_MAX_MPAX_PARAMS		16
#define MPM_MAX_MMAPS			16
#define MPM_MAX_SLAVES			8
#define MPM_MAX_DMA_TRANS   	4
#define MPM_MAX_EDMA_INST		5
#define MPM_MAX_MPAX_RANGES 	1

/*
 * Verbosity level 0 = no prints
 *					1 = error prints
 *					2 = debug prints
 */
#define VERBOSITY_LEVEL 0
void mpm_printf(int val, const char* str, ...);

typedef enum mpm_interface_tag {
	log_daemon,	/*stderr, stdout, syslog, default*/
	log_file,	/*<filename not matching above>*/
} mpm_interface_e;

typedef  struct mpm_mmap_tag {
	char name[MPM_MAX_NAME_LENGTH];
	char devicename[MPM_MAX_NAME_LENGTH];
	uint64_t local_addr;
	uint64_t global_addr;
	uint64_t length;
} mpm_mmap_t;

typedef enum mpm_transport_tag {
	shared_memory,
	srio,
	pcie,
	hyperlink,
	ethernet,
	qmss,
} mpm_transport_e;

typedef  enum mpm_sharing_tag {
	explicit,
	implicit,
} mpm_sharing_e;

typedef struct mpm_transport_cfg {
	char name[MPM_MAX_NAME_LENGTH];
	uint32_t core_id;
	mpm_transport_e transport;
	mpm_sharing_e sharing;
	uint32_t cluster_id;
	int num_mmap;
	mpm_mmap_t mmap[MPM_MAX_MMAPS];
	mpm_transport_trans_t ta[MPM_MAX_DMA_TRANS];
	EDMA3_DRV_Handle hEdma[MPM_MAX_EDMA_INST];
	uint32_t edma3_inst;
	uint32_t shadow_reg;
	uint32_t edma3_prealloc;
	unsigned int edma3_ch[MPM_MAX_DMA_PREALLOC];
	unsigned int edma3_tcc[MPM_MAX_DMA_PREALLOC];
	char edma3_name[MPM_MAX_NAME_LENGTH];
	keystone_mmap_handle_t  hMpax;
	keystone_mmap_handle_t  rhMpax;
	uint32_t mpax_base;
	uint32_t mpax_size;
	unsigned int mpax_index;
	unsigned int mpax_count;
	char mpax_name[MPM_MAX_NAME_LENGTH];
	Rm_ServiceHandle *rmClientServiceHandle;
	int msmc_cfg_index;
	void *td;
} mpm_transport_cfg_t;

typedef struct mmap_data_tag {
	char	*addr;
	char	*addr_usr;
	uint32_t size;
	uint32_t phy_addr;
	uint32_t logical_addr;
	uint8_t hyplnk_idx;
} mmap_data_t;

#define find_slave_p(slave_name, slave_p) \
do { \
	int count; \
	for (count = 0; count < mpm_cfg.num_slaves; count++) { \
		if (!strcmp(slave_name, mpm_slave[count].cfgp->name)) break; \
	} \
	if (count == mpm_cfg.num_slaves) { \
		slave_p = NULL; \
	} else { \
		slave_p = &mpm_slave[count]; \
	} \
} while(0)

#endif /*__MPMDB_H__*/
