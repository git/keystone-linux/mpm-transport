/*
 * Copyright (C) 2013-2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include <stdio.h>
#include "mpm_transport.h"

/*#define TEST_BASE (0x0c000000)*/
#define TEST_BASE (0xa0011110)
#define TEST_LENGTH (0x600000)

int main()
{
	int i;
	mpm_transport_h h;
	uint32_t *bufp;
	union char_int {
		char buf[4];
		uint32_t num;
	} val;
	int result = 0;

	mpm_transport_open_t ocfg = {
		.open_mode	= (O_SYNC|O_RDWR),
	};

	mpm_transport_mmap_t mcfg = {
		.mmap_prot	= (PROT_READ|PROT_WRITE),
		.mmap_flags	= MAP_SHARED,
	};

	/* Write to memory */
	printf("testing sharedmem mmap write\n");
	h = mpm_transport_open("dsp0", &ocfg);
	if (!h) {
		printf("open failed\n");
		return -1;
	}

	bufp = mpm_transport_mmap(h, TEST_BASE, TEST_LENGTH, &mcfg);
	if (!bufp) {
		printf("mmap failed\n");
		return -1;
	}
	for (i = 0; i < TEST_LENGTH; i += 4) {
		bufp[i/4] = i;
	}

	mpm_transport_close(h);
	printf("completed mmap write\n");

	/* Read and check memory */
	printf("testing sharedmem mmap read and check\n");
	h = mpm_transport_open("dsp0", &ocfg);
	if (!h) {
		printf("open failed\n");
		return -1;
	}

	bufp = mpm_transport_mmap(h, TEST_BASE, TEST_LENGTH, &mcfg);
	if (!bufp) {
		printf("mmap failed\n");
		return -1;
	}
	for (i = 0; i < TEST_LENGTH; i += 4) {
		if (bufp[i/4] != i) {
			printf("test failed at address 0x%x\n", TEST_BASE + i);
			result = -1;
			break;
		}
	}

	mpm_transport_close(h);
	printf("completed mmap read and check\n");

	/* Write to memory */
	printf("testing sharedmem write\n");
	h = mpm_transport_open("dsp0", &ocfg);
	if (!h) {
		printf("open failed\n");
		return -1;
	}
	for (i = 0; i < TEST_LENGTH; i += 4) {
		val.num = TEST_BASE + i;
		if (mpm_transport_write(h, TEST_BASE + i, 4, val.buf, NULL)) {
			printf("write failed at address 0x%x\n", TEST_BASE + i);
			result = -1;
			break;
		}
	}

	mpm_transport_close(h);
	printf("completed sharedmem write\n");

	/* Read and check memory */
	printf("testing sharedmem read and check\n");
	h = mpm_transport_open("dsp0", &ocfg);
	if (!h) {
		printf("open failed\n");
		return -1;
	}

	for (i = 0; i < TEST_LENGTH; i += 4) {
		if (mpm_transport_read(h, TEST_BASE + i, 4, val.buf, NULL)) {
			printf("read failed at address 0x%x\n", TEST_BASE + i);
			result = -1;
			break;
		}
		if (val.num != TEST_BASE + i) {
			printf("read/write did not match at address 0x%x, val.num 0x%x\n",
				TEST_BASE + i, val.num);
			result = -1;
			break;
		}
	}

	mpm_transport_close(h);
	printf("completed sharedmem read and check\n");

	if (result) {
		printf("mpm transport tests failed\n");
	} else {
		printf("mpm transport tests passed\n");
	}
	return 0;
}

