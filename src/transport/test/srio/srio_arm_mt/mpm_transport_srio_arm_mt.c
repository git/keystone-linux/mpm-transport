/*
 * Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sched.h>
#include "mpm_transport.h"

#include <ti/drv/rm/rm_server_if.h>
#include <ti/drv/rm/rm.h>
#include <ti/drv/rm/rm_transport.h>
#include <ti/drv/rm/rm_services.h>
#include "sockutils.h"

/* These are the device identifiers used used in the TEST Application */
const uint32_t DEVICE_ID1_16BIT    = 0xBEEF;
const uint32_t DEVICE_ID1_8BIT     = 0xAB;
const uint32_t DEVICE_ID2_16BIT    = 0x4560;
const uint32_t DEVICE_ID2_8BIT     = 0xCD;
const uint32_t DEVICE_ID3_16BIT    = 0x1234;
const uint32_t DEVICE_ID3_8BIT     = 0x12;
const uint32_t DEVICE_ID4_16BIT    = 0x5678;
const uint32_t DEVICE_ID4_8BIT     = 0x56;

extern int SrioDevice_init();
extern int SrioDevice_deinit();

Rm_Handle           rmClientHandle = NULL;
Rm_ServiceHandle   *rmClientServiceHandle = NULL;

/* Number of producers to test. Process 0 will be consumer. Minimum of 1 */
#define NUM_PROC 3

/* Number of packets to send in a process */
#define NUM_PACKET 10

/* Application's registered RM transport indices */
#define SERVER_TO_CLIENT_MAP_ENTRY   0

/* Maximum number of registered RM transports */
#define MAX_MAPPING_ENTRIES          1

#define RM_ERROR_CHECK(checkVal, resultVal, rmInstName, printMsg)                 \
	if (resultVal != checkVal) {                                                  \
		char errorMsgToPrint[] = printMsg;                                        \
		printf("RM Inst : %s : ", rmInstName);                                    \
		printf("%s with error code : %d, exiting\n", errorMsgToPrint, resultVal); \
		return(-1);                                                               \
	}

int coreNum;

sock_h              rmClientSocket;

typedef struct trans_map_entry_s {
	Rm_TransportHandle        transportHandle;
	sock_name_t              *remote_sock;
} Transport_MapEntry;

Transport_MapEntry  rmTransportMap[MAX_MAPPING_ENTRIES];

/* Client socket name */
#define MAX_CLIENT_SOCK_NAME 32
char                rmClientSockName[MAX_CLIENT_SOCK_NAME];
char                rmClientName[RM_NAME_MAX_CHARS];

Rm_Packet *transportAlloc(Rm_AppTransportHandle appTransport, uint32_t pktSize, Rm_PacketHandle *pktHandle)
{
	Rm_Packet *rm_pkt = NULL;

	rm_pkt = calloc(1, sizeof(*rm_pkt));
	if (!rm_pkt) {
		printf("can't malloc for RM send message (err: %s)\n", strerror(errno));
		return (NULL);
	}
	rm_pkt->pktLenBytes = pktSize;
	*pktHandle = rm_pkt;

	return(rm_pkt);
}

void transportFree (Rm_Packet *rm_pkt)
{
	if (rm_pkt) {
		free (rm_pkt);
	}
}

void transportReceive (void)
{
	int32_t             rm_result;
	int                 retval;
	int                 length = 0;
	sock_name_t         server_sock_addr;
	Rm_Packet          *rm_pkt = NULL;
	struct sockaddr_un  server_addr;

	retval = sock_wait(rmClientSocket, &length, NULL, -1);
	if (retval == -2) {
		/* Timeout */
		printf("core %d: Error socket timeout\n", coreNum);
		return;
	}
	else if (retval < 0) {
		printf("core %d: Error in reading from socket, error %d\n", coreNum, retval);
		return;
	}

	if (length < sizeof(*rm_pkt)) {
		printf("core %d: invalid RM message length %d\n", coreNum, length);
		return;
	}
	rm_pkt = calloc(1, length);
	if (!rm_pkt) {
		printf("core %d: can't malloc for recv'd RM message (err: %s)\n",
				coreNum, strerror(errno));
		return;
	}

	server_sock_addr.type = sock_addr_e;
	server_sock_addr.s.addr = &server_addr;
	retval = sock_recv(rmClientSocket, (char *)rm_pkt, length, &server_sock_addr);
	if (retval != length) {
		printf("core %d: recv RM pkt failed from socket, received = %d, expected = %d\n",
				coreNum, retval, length);
		return;
	}

	/* Provide packet to RM Client for processing */
	if ((rm_result = Rm_receivePacket(rmTransportMap[SERVER_TO_CLIENT_MAP_ENTRY].transportHandle, rm_pkt))) {
		printf("core %d: RM failed to process received packet: %d\n", coreNum, rm_result);
	}

	transportFree(rm_pkt);
}

int32_t transportSendRcv (Rm_AppTransportHandle appTransport, Rm_PacketHandle pktHandle)
{
	sock_name_t *server_sock_name = (sock_name_t *)appTransport;
	Rm_Packet   *rm_pkt = (Rm_Packet *)pktHandle;

	if (sock_send(rmClientSocket, (char *)rm_pkt, (int) rm_pkt->pktLenBytes, server_sock_name)) {
		printf("core %d: send data failed\n", coreNum);
	}

	/* Wait for response from Server */
	transportReceive();

	return (0);
}

int connection_setup(void)
{
	Rm_TransportCfg rmTransCfg;
	int32_t         rm_result;
	int             i;
	sock_name_t     sock_name;
	char            server_sock_name[] = RM_SERVER_SOCKET_NAME;

	/* Initialize the transport map */
	for (i = 0; i < MAX_MAPPING_ENTRIES; i++) {
		rmTransportMap[i].transportHandle = NULL;
	}

	if (snprintf (rmClientSockName, MAX_CLIENT_SOCK_NAME, "/tmp/var/run/rm/rm_client%d", coreNum) >= MAX_CLIENT_SOCK_NAME)
	{
		printf("error: client socket name truncated\n");
		return -1;
	}
	sock_name.type = sock_name_e;
	sock_name.s.name = rmClientSockName;

	rmClientSocket = sock_open(&sock_name);
	if (!rmClientSocket) {
		printf("connection_setup: Client socket open failed\n");
		return (-1);
	}

	rmTransportMap[SERVER_TO_CLIENT_MAP_ENTRY].remote_sock = calloc(1, sizeof(sock_name_t));
	rmTransportMap[SERVER_TO_CLIENT_MAP_ENTRY].remote_sock->type = sock_name_e;
	rmTransportMap[SERVER_TO_CLIENT_MAP_ENTRY].remote_sock->s.name = calloc(1, strlen(server_sock_name)+1);
	strncpy(rmTransportMap[SERVER_TO_CLIENT_MAP_ENTRY].remote_sock->s.name, server_sock_name, strlen(server_sock_name)+1);

	/* Register the Server with the Client instance */
	rmTransCfg.rmHandle = rmClientHandle;
	rmTransCfg.appTransportHandle = (Rm_AppTransportHandle) rmTransportMap[SERVER_TO_CLIENT_MAP_ENTRY].remote_sock;
	rmTransCfg.remoteInstType = Rm_instType_SERVER;
	rmTransCfg.transportCallouts.rmAllocPkt = transportAlloc;
	rmTransCfg.transportCallouts.rmSendPkt = transportSendRcv;
	rmTransportMap[SERVER_TO_CLIENT_MAP_ENTRY].transportHandle = Rm_transportRegister(&rmTransCfg, &rm_result);

	return(0);
}

/** ============================================================================
 *   @n@b initRm
 *
 *   @b Description
 *   @n This API initializes the RM Client for the QMSS test establishing
 *      a socket connection with the RM Server
 *
 *   @return    int32_t
 *              -1      -   Error
 *              0       -   Success
 * =============================================================================
 */
int initRm (void)
{
	Rm_InitCfg         rmInitCfg;
	int32_t            result;

	/* Initialize the RM Client - RM must be initialized before anything else in the system */
	memset(&rmInitCfg, 0, sizeof(rmInitCfg));
	if (snprintf (rmClientName, RM_NAME_MAX_CHARS, "RM_Client%d", coreNum) >= RM_NAME_MAX_CHARS)
	{
		printf("client name truncated\n");
		return -1;
	}
	rmInitCfg.instName = rmClientName;
	rmInitCfg.instType = Rm_instType_CLIENT;
	rmClientHandle = Rm_init(&rmInitCfg, &result);
	RM_ERROR_CHECK(RM_OK, result, rmClientName, "Initialization failed");

	printf("Initialized %s\n", rmClientName);

	/* Open Client service handle */
	rmClientServiceHandle = Rm_serviceOpenHandle(rmClientHandle, &result);
	RM_ERROR_CHECK(RM_OK, result, rmClientName, "Service handle open failed");
	return(connection_setup());
}

void rm_delete_val(char *name)
{
	Rm_ServiceReqInfo   rmServiceReq;
	Rm_ServiceRespInfo  rmServiceResp;

	memset((void *)&rmServiceReq, 0, sizeof(rmServiceReq));
	memset((void *)&rmServiceResp, 0, sizeof(rmServiceResp));

	rmServiceReq.type             = Rm_service_RESOURCE_UNMAP_NAME;
	rmServiceReq.resourceName     = name;
	rmServiceReq.resourceNsName   = name;
	rmServiceReq.resourceLength   = 1;
	rmServiceReq.callback.serviceCallback = NULL;

	rmClientServiceHandle->Rm_serviceHandler(rmClientServiceHandle->rmHandle, &rmServiceReq, &rmServiceResp);
}

void rm_set_val(char *name, uint32_t id)
{
	Rm_ServiceReqInfo   rmServiceReq;
	Rm_ServiceRespInfo  rmServiceResp;

	memset((void *)&rmServiceReq, 0, sizeof(rmServiceReq));
	memset((void *)&rmServiceResp, 0, sizeof(rmServiceResp));

	rmServiceReq.type             = Rm_service_RESOURCE_MAP_TO_NAME;
	rmServiceReq.resourceName     = name;
	rmServiceReq.resourceNsName   = name;
	rmServiceReq.resourceLength   = 1;
	rmServiceReq.resourceBase     = id;
	rmServiceReq.callback.serviceCallback = NULL;

	rmClientServiceHandle->Rm_serviceHandler(rmClientServiceHandle->rmHandle, &rmServiceReq, &rmServiceResp);
}

uint32_t rm_get_val(char *name)
{
	Rm_ServiceReqInfo   rmServiceReq;
	Rm_ServiceRespInfo  rmServiceResp;
	int                 succeed;

	memset((void *)&rmServiceReq, 0, sizeof(rmServiceReq));
	memset((void *)&rmServiceResp, 0, sizeof(rmServiceResp));

	rmServiceReq.type             = Rm_service_RESOURCE_GET_BY_NAME;
	rmServiceReq.resourceName     = name;
	rmServiceReq.resourceNsName   = name;
	rmServiceReq.resourceLength   = 1;
	rmServiceReq.callback.serviceCallback = NULL;

	do {
		rmClientServiceHandle->Rm_serviceHandler(rmClientServiceHandle->rmHandle, &rmServiceReq, &rmServiceResp);
		succeed = (rmServiceResp.serviceState == RM_SERVICE_APPROVED) ||
					(rmServiceResp.serviceState == RM_SERVICE_APPROVED_STATIC);
		if (!succeed)
		{
			sched_yield();
		}
	} while (!succeed);

	return rmServiceResp.resourceBase;

}

int mySrioDevice_init(void *initCfg, void *srioAddr, uint32_t serdesAddr)
{
	return (SrioDevice_init)(srioAddr, serdesAddr);
}

int mySrioDevice_deinit(void *deinitCfg, void *srioAddr)
{
	return (SrioDevice_deinit)(srioAddr);
}

int main() {
	mpm_transport_h h;
	mpm_transport_open_t ocfg;
	mpm_transport_send_t send_cfg;
	mpm_transport_packet_addr_t send_addr;
	int status, i;
	pid_t pid;
	char buf[256];
	int bufLen = 32;
	char *bufPtr = buf;
	uint16_t coreDeviceID[NUM_PROC+1];

	for (coreNum = 1; coreNum < (NUM_PROC + 1); coreNum++) {
		pid=fork();
		if (!pid) break;
	}
	if (pid) coreNum = 0;

	/* Setup RM */
	if (initRm())  {
		printf ("initRm returned error, exiting\n");
		return (-1);
	}

	/* Initialize the core Device IDs: Each core has a seperate device ID. */
	coreDeviceID[0] = DEVICE_ID1_16BIT;
	coreDeviceID[1] = DEVICE_ID2_16BIT;
#if (NUM_PROC > 2)
	coreDeviceID[2] = DEVICE_ID3_16BIT;
	coreDeviceID[3] = DEVICE_ID4_16BIT;
#endif

	/* Configure the open params */
	ocfg.open_mode                            = (O_SYNC|O_RDWR);
	ocfg.rm_info.rm_client_handle             = rmClientServiceHandle;
	ocfg.transport_info.qmss.qmss_init        = 1;
	ocfg.transport_info.srio.type             = packet_addr_type_SRIO_TYPE11;
	ocfg.transport_info.srio.type11.tt        = 1;
	ocfg.transport_info.srio.type11.id        = coreDeviceID[coreNum];
	ocfg.transport_info.srio.type11.letter    = 2;
	ocfg.transport_info.srio.type11.mailbox   = 3;
	ocfg.transport_info.srio.type11.segmap    = 0x0;

	/* Only do the device init/deinit from coreNum 0 */
	if (coreNum == 0) {
		ocfg.transport_info.srio.deviceInit   = &mySrioDevice_init;
		ocfg.transport_info.srio.initCfg      = NULL;
		ocfg.transport_info.srio.deviceDeinit = &mySrioDevice_deinit;
		ocfg.transport_info.srio.deinitCfg    = NULL;
	}
	else {
		ocfg.transport_info.srio.deviceInit   = NULL;
		ocfg.transport_info.srio.initCfg      = NULL;
		ocfg.transport_info.srio.deviceDeinit = NULL;
		ocfg.transport_info.srio.deinitCfg    = NULL;
	}

	/* Open the MPM Transport handle.
		Let coreNum 0 finish first to do SRIO device init */
	if (coreNum != 0) rm_get_val("SRIO_INIT");
	h = mpm_transport_open("arm-srio-generic", &ocfg);
	if (!h) {
		printf("!!Error: coreNum %d failed to open mpm-transport handle\n", coreNum);
		return (-1);
	}
	if (coreNum == 0) rm_set_val("SRIO_INIT", 1);

	printf("coreNum: %d, pid=%d\n", coreNum, getpid());

	/* Task: Core0 just polls for packets, all other process send */
	if (coreNum == 0) {
		for (i = 0; i< (NUM_PROC * NUM_PACKET); i++) {
			if (mpm_transport_packet_recv(h, &bufPtr, &bufLen, NULL, NULL)) {
				printf("ERROR: coreNum %d failed to receive packet number %d\n", coreNum, i);
				return (-1);
			}
			else {
				printf("coreNum: %d received packet number %d\n", coreNum, i);
				printf("\tMessage: %s\n", buf);
			}
		}
	}
	else {
		send_cfg.buf_opt = packet_send_MEMCPY_BUFFER;
		send_addr.addr_type = packet_addr_type_SRIO_TYPE11;
		send_addr.addr.srio.type11.tt = 1;
		send_addr.addr.srio.type11.id = coreDeviceID[0];
		send_addr.addr.srio.type11.letter = 2;
		send_addr.addr.srio.type11.mailbox = 3;
		for (i = 0; i < NUM_PACKET; i++) {
			snprintf(buf, 10, "-p%d, msg%d", coreNum, i);
			buf[11] = '\0';
			bufLen = strlen(buf);
			bufPtr = buf;
			if (mpm_transport_packet_send(h, &bufPtr, &bufLen, &send_addr, &send_cfg)) {
				printf("ERROR: coreNum %d failed to send packet number %d\n", coreNum, i);
				return (-1);
			}
		}
	}

	/* Exiting, let the first process wait for everyone else before tearing down hardware */
	if (coreNum == 0) {
		for (i = 0; i < NUM_PROC; i++) {
			pid = wait (&status);
			if (WIFEXITED(status))
			{
				if (WEXITSTATUS(status))
				{
					printf ("Child %d returned fail (%d)\n", pid, WEXITSTATUS(status));
				}

			}
			else
			{
				printf ("Child %d failed to exit\n", pid);
			}
		}
		rm_delete_val("SRIO_INIT");
	}
	mpm_transport_close(h);
	printf("**coreNum %d exiting!!\n", coreNum);

	if (coreNum == 0) printf("mpm_transport_srio_arm_mt.out finished!\n");
	return 0;
}
