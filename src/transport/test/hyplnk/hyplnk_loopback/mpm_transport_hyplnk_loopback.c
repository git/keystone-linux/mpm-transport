/*
 * Copyright (C) 2013-2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include "mpm_transport.h"

/* To enable the Hyperlink boundary check test, un-comment the next line */
/* #define ENABLE_HYPLNK_BOUNDARY_CHECK_TEST */

#define TEST_BASE_HYP1 (0x0c000000)
#define TEST_LENGTH_HYP1 (0x1000)
#define TEST_BASE_HYP2 (0x0c008000)
#define TEST_LENGTH_HYP2 (0x2000)
#define TEST_BASE_HYP3 (0x11800000)
#define TEST_LENGTH_HYP3 (0x2000)
#define TEST_BASE_HYP4 (0x12800000)
#define TEST_LENGTH_HYP4 (0x1400)

#define TEST_BASE_HYP5 (0x0c002000)
#define TEST_LENGTH_HYP5 (0x1000)
#define TEST_BASE_HYP6 (0x0c00A000)
#define TEST_LENGTH_HYP6 (0x2000)
#define TEST_BASE_HYP7 (0x0c004000)
#define TEST_LENGTH_HYP7 (0x1000)
#define TEST_BASE_HYP8 (0x0c00C000)
#define TEST_LENGTH_HYP8 (0x2000)

#define MAX_LINE_LENGTH 64

#ifdef ENABLE_HYPLNK_BOUNDARY_CHECK_TEST
char large_buf[TEST_LENGTH_HYP1];
#endif /* ENABLE_HYPLNK_BOUNDARY_CHECK_TEST */

int check_device_exist(char *st)
{
	int ret, fd;
	char tmp[6] = "/dev/";
	char dev[MAX_LINE_LENGTH];
	snprintf(dev, strlen(tmp) + strlen(st) + 1, "%s%s", tmp, st);
	fd = open(dev, O_SYNC);
	if (fd < 0) {
		/* errno 2 is No such file or directory. */
		if (errno == 2) ret = 0;
		else ret = errno;
	}
	else {
		close(fd);
		ret = 1;
	}
	return ret;
}

int main()
{
	int i;
	mpm_transport_h h;
	mpm_transport_h nest1;
	mpm_transport_h nest2;
	uint32_t *bufp, *bufp2;
	union char_int {
		char buf[4];
		uint32_t num;
	} val;
	int result = 0;
	int check1=0, check2=0, check3=0, check4=0;

	mpm_transport_open_t ocfg = {
		.open_mode	= (O_SYNC|O_RDWR),
		.msec_timeout = 0,
		.serdes_init = 0,
	};

	mpm_transport_mmap_t mcfg = {
		.mmap_prot	= (PROT_READ|PROT_WRITE),
		.mmap_flags	= MAP_SHARED,
	};

	/*
	 * Hyperlink 0 mmap
	*/
	if ((check1=check_device_exist("hyperlink0")) && (check2=check_device_exist("dsp0"))) {
		printf("testing hyperlink0 mmap\n");
		h = mpm_transport_open("dsp0-hyplnk", &ocfg);
		if (!h) {
			printf("open failed\n");
			return -1;
		}

		bufp = (uint32_t *) mpm_transport_mmap(h, TEST_BASE_HYP2, TEST_LENGTH_HYP2, &mcfg);
		if (!bufp) {
			printf("mmap failed\n");
			return -1;
		}
		for (i = 0; i < TEST_LENGTH_HYP2; i += 4) {
			bufp[i/4] = (uint32_t) &(bufp[i/4]);
		}

		bufp2 = (uint32_t *) mpm_transport_mmap(h, TEST_BASE_HYP1, TEST_LENGTH_HYP1, &mcfg);
		if (!bufp2) {
			printf("mmap failed\n");
			return -1;
		}
		for (i = 0; i < TEST_LENGTH_HYP1; i += 4) {
			bufp2[i/4] = i;
		}
		mpm_transport_close(h);
			printf("completed hyperlink mmap read and check\n");

		/*
		 * Hyperlink 0 read/write
		*/
		printf("testing hyperlink mpm_transport_write and mpm_transport_read\n");
		h = mpm_transport_open("dsp0-hyplnk", &ocfg);
		if (!h) {
			printf("mpm_transport_open failed: dsp0-hyplnk\n");
			return -1;
		}

#ifdef ENABLE_HYPLNK_BOUNDARY_CHECK_TEST
		printf("testing hyperlink mpm_transport_write boundary checking\n");
		if (mpm_transport_write(h, TEST_BASE_HYP1+0x3fff00, TEST_LENGTH_HYP1, large_buf, NULL)) {
			printf("write failed at address 0x%x with len=0x%x\n", (TEST_BASE_HYP1+0x3fff00), TEST_LENGTH_HYP1);
			mpm_transport_close(h);
			return -1;
		}
#else
		for (i = 0; i < TEST_LENGTH_HYP1; i += 4) {
			val.num = TEST_BASE_HYP1 - i;
			if (mpm_transport_write(h, TEST_BASE_HYP1 + i, 4, val.buf, NULL)) {
				printf("write failed at address 0x%x\n", TEST_BASE_HYP1 + i);
				result = -1;
				break;
			}
		}
		for (i = 0; i < TEST_LENGTH_HYP1; i += 4) {
			if (mpm_transport_read(h, TEST_BASE_HYP1 + i, 4, val.buf, NULL)) {
				printf("read failed at address 0x%x\n", TEST_BASE_HYP1 + i);
				result = -1;
				break;
			}
			if (val.num != TEST_BASE_HYP1 - i) {
				printf("read/write did not match at address 0x%x, val.num 0x%x\n",
					TEST_BASE_HYP1 + i, val.num);
				result = -1;
				break;
			}
		}
		for (i = 0; i < TEST_LENGTH_HYP2; i += 4) {
			val.num = TEST_BASE_HYP2 + i;
			if (mpm_transport_write(h, TEST_BASE_HYP2 + i, 4, val.buf, NULL)) {
				printf("write failed at address 0x%x\n", TEST_BASE_HYP2 + i);
				result = -1;
				break;
			}
		}
		for (i = 0; i < TEST_LENGTH_HYP2; i += 4) {
			if (mpm_transport_read(h, TEST_BASE_HYP2 + i, 4, val.buf, NULL)) {
				printf("read failed at address 0x%x\n", TEST_BASE_HYP2 + i);
				result = -1;
				break;
			}
			if (val.num != TEST_BASE_HYP2 + i) {
				printf("read/write did not match at address 0x%x, val.num 0x%x\n",
					TEST_BASE_HYP2 + i, val.num);
				result = -1;
				break;
			}
		}
#endif /* ENABLE_HYPLNK_BOUNDARY_CHECK_TEST */
		mpm_transport_close(h);
	}

	/*
	 * Hyperlink 0 read/write to dsp1
	*/
	if ((check1=check_device_exist("hyperlink0")) && (check2=check_device_exist("dsp1"))) {
		printf("testing hyperlink mpm_transport_write and mpm_transport_read on dsp1-hyplnk\n");
		h = mpm_transport_open("dsp1-hyplnk", &ocfg);
		if (!h) {
			printf("mpm_transport_open failed: dsp1-hyplnk\n");
			return -1;
		}
		for (i = 0; i < TEST_LENGTH_HYP3; i += 4) {
			val.num = TEST_BASE_HYP3 + i;
			if (mpm_transport_write(h, TEST_BASE_HYP3 + i, 4, val.buf, NULL)) {
				printf("write failed at address 0x%x\n", TEST_BASE_HYP3 + i);
				result = -1;
				break;
			}
		}
		for (i = 0; i < TEST_LENGTH_HYP3; i += 4) {
			if (mpm_transport_read(h, TEST_BASE_HYP3 + i, 4, val.buf, NULL)) {
				printf("read failed at address 0x%x\n", TEST_BASE_HYP3 + i);
				result = -1;
				break;
			}
			if (val.num != TEST_BASE_HYP3 + i) {
				printf("read/write did not match at address 0x%x, val.num 0x%x\n",
					TEST_BASE_HYP3 + i, val.num);
				result = -1;
				break;
			}
		}
		mpm_transport_close(h);
	}
	else {
		if (!check1) printf("No hyperlink0 device\n");
		if (!check2) printf("No dsp1 device\n");
		printf("Skipping test for hyperlink0 on dsp1...\n");
	}

	/*
	 * Nested hyperlink 0 test case
	*/
	if ((check1=check_device_exist("hyperlink0")) && (check2=check_device_exist("dsp0"))
		&& (check3=check_device_exist("dsp1")) && (check4=check_device_exist("dsp2"))) {
		printf("testing nested mpm_transport_open for hyperlink\n");
		h = mpm_transport_open("dsp1-hyplnk", &ocfg);
		if (!h) {
			printf("mpm_transport_open failed: dsp1-hyplnk\n");
			return -1;
		}
		for (i = 0; i < TEST_LENGTH_HYP3; i += 4) {
			val.num = TEST_BASE_HYP3 + i;
			if (mpm_transport_write(h, TEST_BASE_HYP3 + i, 4, val.buf, NULL)) {
				printf("write failed at address 0x%x\n", TEST_BASE_HYP3 + i);
				result = -1;
				break;
			}
		}
		for (i = 0; i < TEST_LENGTH_HYP3; i += 4) {
			if (mpm_transport_read(h, TEST_BASE_HYP3 + i, 4, val.buf, NULL)) {
				printf("read failed at address 0x%x\n", TEST_BASE_HYP3 + i);
				result = -1;
				break;
			}
			if (val.num != TEST_BASE_HYP3 + i) {
				printf("read/write did not match at address 0x%x, val.num 0x%x\n",
					TEST_BASE_HYP3 + i, val.num);
				result = -1;
				break;
			}
		}
		nest1 = mpm_transport_open("dsp0-hyplnk", &ocfg);
		if (!nest1) {
			printf("mpm_transport_open failed: dsp0-hyplnk\n");
			return -1;
		}
		for (i = 0; i < TEST_LENGTH_HYP1; i += 4) {
			val.num = TEST_BASE_HYP1 - i;
			if (mpm_transport_write(nest1, TEST_BASE_HYP1 + i, 4, val.buf, NULL)) {
				printf("write failed at address 0x%x\n", TEST_BASE_HYP1 + i);
				result = -1;
				break;
			}
		}
		for (i = 0; i < TEST_LENGTH_HYP1; i += 4) {
			if (mpm_transport_read(nest1, TEST_BASE_HYP1 + i, 4, val.buf, NULL)) {
				printf("read failed at address 0x%x\n", TEST_BASE_HYP1 + i);
				result = -1;
				break;
			}
			if (val.num != TEST_BASE_HYP1 - i) {
				printf("read/write did not match at address 0x%x, val.num 0x%x\n",
					TEST_BASE_HYP1 + i, val.num);
				result = -1;
				break;
			}
		}
		nest2 = mpm_transport_open("dsp2-hyplnk", &ocfg);
		if (!nest2) {
			printf("mpm_transport_open failed: dsp2-hyplnk\n");
			return -1;
		}
		for (i = 0; i < TEST_LENGTH_HYP4; i += 4) {
			val.num = TEST_BASE_HYP4 - i;
			if (mpm_transport_write(nest2, TEST_BASE_HYP4 + i, 4, val.buf, NULL)) {
				printf("write failed at address 0x%x\n", TEST_BASE_HYP4 + i);
				result = -1;
				break;
			}
		}
		for (i = 0; i < TEST_LENGTH_HYP4; i += 4) {
			if (mpm_transport_read(nest2, TEST_BASE_HYP4 + i, 4, val.buf, NULL)) {
				printf("read failed at address 0x%x\n", TEST_BASE_HYP4 + i);
				result = -1;
				break;
			}
			if (val.num != TEST_BASE_HYP4 - i) {
				printf("read/write did not match at address 0x%x, val.num 0x%x\n",
					TEST_BASE_HYP4 + i, val.num);
				result = -1;
				break;
			}
		}

		mpm_transport_close(h);
		mpm_transport_close(nest2);
		mpm_transport_close(nest1);
	}
	else {
		if (!check1) printf("No hyperlink0 device\n");
		if (!check2) printf("No dsp0 device\n");
		if (!check3) printf("No dsp1 device\n");
		if (!check4) printf("No dsp2 device\n");
		printf("Skipping nested test...\n");
	}

	/*
	 * Hyperlink1 loopback test case
	*/
	if (check1=check_device_exist("hyperlink1")) {
		printf("Hyperlink 1 loopback test\n");
		printf("testing hyperlink1 mmap\n");
		h = mpm_transport_open("arm-loopback-hyplnk-1-no-dma", &ocfg);
		if (!h) {
			printf("open failed\n");
			return -1;
		}

		bufp = (uint32_t *) mpm_transport_mmap(h, TEST_BASE_HYP5, TEST_LENGTH_HYP5, &mcfg);
		if (!bufp) {
			printf("mmap failed\n");
			return -1;
		}
		for (i = 0; i < TEST_LENGTH_HYP5; i += 4) {
			bufp[i/4] = TEST_BASE_HYP5 + i;
		}

		bufp2 = (uint32_t *) mpm_transport_mmap(h, TEST_BASE_HYP6, TEST_LENGTH_HYP6, &mcfg);
		if (!bufp2) {
			printf("mmap failed\n");
			return -1;
		}
		for (i = 0; i < TEST_LENGTH_HYP6; i += 4) {
			bufp2[i/4] = i;
		}
		mpm_transport_close(h);
		printf("completed hyperlink1 mmap read and check\n");
	}
	else {
		printf("No hyperlink1 device\n");
		printf("Skipping hyperlink1 test case\n");
	}

	/*
	 * Dual hyperlink test case
	*/
	if ((check1=check_device_exist("hyperlink0")) && (check2=check_device_exist("hyperlink1"))) {
		printf("Dual Hyperlink mmap test\n");
		h = mpm_transport_open("arm-loopback-hyplnk-1-no-dma", &ocfg);
		if (!h) {
			printf("open failed\n");
			return -1;
		}

		nest1 = mpm_transport_open("arm-loopback-hyplnk-0-no-dma", &ocfg);
		if (!nest1) {
			printf("open failed\n");
			return -1;
		}

		bufp = (uint32_t *) mpm_transport_mmap(h, TEST_BASE_HYP7, TEST_LENGTH_HYP7, &mcfg);
		if (!bufp) {
			printf("mmap failed\n");
			return -1;
		}
		for (i = 0; i < TEST_LENGTH_HYP7; i += 4) {
			bufp[i/4] = TEST_BASE_HYP7 + i;
		}
		bufp2 = (uint32_t *) mpm_transport_mmap(nest1, TEST_BASE_HYP8, TEST_LENGTH_HYP8, &mcfg);
		if (!bufp2) {
			printf("mmap failed\n");
			return -1;
		}
		for (i = 0; i < TEST_LENGTH_HYP8; i += 4) {
			bufp2[i/4] = i;
		}

		mpm_transport_close(nest1);
		mpm_transport_close(h);
	}
	else {
		if (!check1) printf("No hyperlink0 device\n");
		if (!check2) printf("No hyperlink1 device\n");
		printf("Skipping dual hyperlink port test...\n");
	}


	if (result) {
		printf("mpm transport: hyperlink loopback test failed\n");
	} else {
		printf("mpm transport: hyperlink loopback test passed\n");
	}
	return 0;
}

