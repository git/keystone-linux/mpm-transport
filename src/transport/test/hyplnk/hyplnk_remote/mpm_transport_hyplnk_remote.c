/*
 * Copyright (C) 2013-2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include <stdio.h>
#include <unistd.h>
#include <pthread.h>
#include "mpm_transport.h"

#define ENABLE_MMAP_WRITE_TEST
#define ENABLE_READ_WRITE_TEST
#define ENABLE_PUT_INITIATE_TEST
//#define ENABLE_TEST1_HYPLNK_INTERRUPT
#define ENABLE_TEST2_HYPLNK_INTERRUPT
#define ENABLE_TEST_HYPLNK_INTERRUPT_WITHTHREAD
#define ENABLE_TEST_HYPLNK_INTERRUPT_USAGE_MODEL2

//#define NODE_NAME_FOR_INTERRUPT_TEST "arm-loopback-hyplnk64-0"
#define NODE_NAME_FOR_INTERRUPT_TEST "arm-remote-hyplnk64-0"

#define TEST_BASE_HYP1 (0x0c000000)
#define TEST_LENGTH_HYP1 (0x1000)
#define TEST_BASE_HYP2 (0x0c008000)
#define TEST_LENGTH_HYP2 (0x2000)

#define TEST_DESTINATION1 0x822000000
#define TEST_SOURCE1 0x839000000
#define TEST_BUFFER_SIZE 0x400000

#define THREAD1_INTERRUPT_MASK 0x4
#define THREAD2_INTERRUPT_MASK 0x8
#define THREAD3_INTERRUPT 0x2
#define THREAD3_INTERRUPT_MASK 0x3


pthread_t tid1, tid2, tid3;
volatile int interrupt_occurred_flag[3]={0, 0, 0};
void *interrupt_thread_function(void *arg)
{
	mpm_transport_h handle=arg;
	uint32_t reported_event_bitmap;
	int result = 0;

	printf("\n Thread: Waiting for interrupt.3..");fflush(stdout);
	result = mpm_transport_wait_for_interrupt_event(handle, THREAD1_INTERRUPT_MASK, NULL, &reported_event_bitmap);
	if (result < 0) {
		printf("\nThread: mpm transport: interrupt control failed");
		return(NULL);
	}
	printf("\n Thread: Wait for interrupt 3 complete , event bitmap %x result: %d",
		reported_event_bitmap, result);
	interrupt_occurred_flag[0] = 1;
	return(NULL);
}
void *interrupt_thread_function2(void *arg)
{
	mpm_transport_h handle=arg;
	uint32_t reported_event_bitmap;
	int result = 0;

	printf("\n Thread: Waiting for interrupt.3 A..");fflush(stdout);
	result = mpm_transport_wait_for_interrupt_event(handle, THREAD2_INTERRUPT_MASK, NULL, &reported_event_bitmap);
	if (result < 0) {
		printf("\nThread: mpm transport: interrupt control failed");
		return(NULL);
	}
	printf("\n Thread: Wait for interrupt 3 A complete , event bitmap %x result: %d",
		reported_event_bitmap, result);
	interrupt_occurred_flag[1] = 1;
	return(NULL);
}
void *interrupt_thread_function3(void *arg)
{
	mpm_transport_h handle=arg;
	uint32_t reported_event_bitmap;
	int result = 0;

	printf("\n Thread: Waiting for interrupt.3 B..");fflush(stdout);
	result = mpm_transport_wait_for_interrupt_event(handle, THREAD3_INTERRUPT_MASK, NULL, &reported_event_bitmap);
	if (result < 0) {
		printf("\nThread: mpm transport: interrupt control failed");
		return(NULL);
	}
	printf("\n Thread: Wait for interrupt 3 B complete , event bitmap %x result: %d",
		reported_event_bitmap, result);
	interrupt_occurred_flag[2] = 1;
	return(NULL);
}


int main()
{
	int i;
	mpm_transport_h h;
	uint32_t *bufp, *bufp2;
	union char_int {
		char buf[4];
		uint32_t num;
	} val;
	int result = 0;
	uint32_t reported_event_bitmap;
	struct timeval tv;
	int trigger_interrupt_flag=1;

	mpm_transport_open_t ocfg = {
		.open_mode	= (O_SYNC|O_RDWR),
		.msec_timeout = 5000,
		.serdes_init = 0,
	};

	mpm_transport_mmap_t mcfg = {
		.mmap_prot	= (PROT_READ|PROT_WRITE),
		.mmap_flags	= MAP_SHARED,
	};
#ifdef ENABLE_MMAP_WRITE_TEST

	/* Write to memory */
	printf("testing hyperlink mmap write\n");
	h = mpm_transport_open("arm-remote-hyplnk-0-no-dma", &ocfg);
	if (!h) {
		printf("open failed\n");
		return -1;
	}

	bufp = (uint32_t *) mpm_transport_mmap(h, TEST_BASE_HYP2, TEST_LENGTH_HYP2, &mcfg);
	if (!bufp) {
		printf("mmap failed\n");
		return -1;
	}
	for (i = 0; i < TEST_LENGTH_HYP2; i += 4) {
		bufp[i/4] = (uint32_t) &(bufp[i/4]);
	}

	bufp2 = (uint32_t *) mpm_transport_mmap(h, TEST_BASE_HYP1, TEST_LENGTH_HYP1, &mcfg);
	if (!bufp2) {
		printf("mmap failed\n");
		return -1;
	}
	for (i = 0; i < TEST_LENGTH_HYP1; i += 4) {
		bufp2[i/4] = i;
	}

	sleep(1);
	mpm_transport_close(h);
	printf("completed hyperlink mmap read and check\n");
#endif

#ifdef ENABLE_MMAP_WRITE_TEST
	/* testing read/write */
	printf("testing hyperlink mpm_transport_write and mpm_transport_read\n");
	h = mpm_transport_open("arm-remote-hyplnk-0-no-dma", &ocfg);

	for (i = 0; i < TEST_LENGTH_HYP1; i += 4) {
		val.num = TEST_BASE_HYP1 - i;
		if (mpm_transport_write(h, TEST_BASE_HYP1 + i, 4, val.buf, NULL)) {
			printf("write failed at address 0x%x\n", TEST_BASE_HYP1 + i);
			result = -1;
			break;
		}
	}
	for (i = 0; i < TEST_LENGTH_HYP1; i += 4) {
		if (mpm_transport_read(h, TEST_BASE_HYP1 + i, 4, val.buf, NULL)) {
			printf("read failed at address 0x%x\n", TEST_BASE_HYP1 + i);
			result = -1;
			break;
		}
		if (val.num != TEST_BASE_HYP1 - i) {
			printf("read/write did not match at address 0x%x, val.num 0x%x\n",
				TEST_BASE_HYP1 + i, val.num);
			result = -1;
			break;
		}
	}
	for (i = 0; i < TEST_LENGTH_HYP2; i += 4) {
		val.num = TEST_BASE_HYP2 + i;
		if (mpm_transport_write(h, TEST_BASE_HYP2 + i, 4, val.buf, NULL)) {
			printf("write failed at address 0x%x\n", TEST_BASE_HYP2 + i);
			result = -1;
			break;
		}
	}
	for (i = 0; i < TEST_LENGTH_HYP2; i += 4) {
		if (mpm_transport_read(h, TEST_BASE_HYP2 + i, 4, val.buf, NULL)) {
			printf("read failed at address 0x%x\n", TEST_BASE_HYP2 + i);
			result = -1;
			break;
		}
		if (val.num != TEST_BASE_HYP2 + i) {
			printf("read/write did not match at address 0x%x, val.num 0x%x\n",
				TEST_BASE_HYP2 + i, val.num);
			result = -1;
			break;
		}
	}

	sleep(1);
	mpm_transport_close(h);
#endif

#ifdef ENABLE_PUT_INITIATE_TEST
	printf("Testing remote 36-bit put\n");
	h = mpm_transport_open("arm-remote-hyplnk64-0", &ocfg);
	if (!h) {
		printf("open failed for arm-remote-hyplnk64-0\n");
		return -1;
	}
	mpm_transport_put_initiate64(h, TEST_DESTINATION1, TEST_SOURCE1, TEST_BUFFER_SIZE, true, NULL);
	sleep(2);

	mpm_transport_close(h);
#endif

#if (defined(ENABLE_TEST1_HYPLNK_INTERRUPT) \
	|| defined(ENABLE_TEST2_HYPLNK_INTERRUPT) \
	|| defined(ENABLE_TEST_HYPLNK_INTERRUPT_WITHTHREAD) \
	|| defined(ENABLE_TEST_HYPLNK_INTERRUPT_USAGE_MODEL2))

	printf("\nTesting remote interrupt\n");
	/* Open mpm transport for the interface */
	/* Note: The interrupt in the peripheral is enabled during open
		based on config in JSON */
	h = mpm_transport_open(NODE_NAME_FOR_INTERRUPT_TEST, &ocfg);
	if (!h) {
		printf("\nopen failed for  %s %x\n",
			NODE_NAME_FOR_INTERRUPT_TEST, h);
		return -1;
	}
#endif
	/* This is Beginning of Usage model 1 */
#ifdef ENABLE_TEST1_HYPLNK_INTERRUPT
	printf("\n Waiting for interrupt.1..with indefinite timeout");fflush(stdout);

	/* This is blocking call till interrupt happens */
	result = mpm_transport_wait_for_interrupt_event(h, 0x3, NULL, &reported_event_bitmap);
	printf("\n mpm_transport: Wait for interrupt 1 complete , event bitmap %x result: %d",
		reported_event_bitmap, result);
#endif
#ifdef ENABLE_TEST2_HYPLNK_INTERRUPT
	printf("\n Waiting for interrupt.2..with 5 sec timeout");fflush(stdout);

	/* Time out set to five seconds. */
	tv.tv_sec = 5;
	tv.tv_usec = 0;

	/* This is blocking call till interrupt happens or time out */
	result = mpm_transport_wait_for_interrupt_event(h, 0xc, &tv, &reported_event_bitmap);

	if (result < 0) {
		printf("\nmpm transport: interrupt control failed");
		goto error;
	}
	if( result == 0 )
		printf("\n wait for interrupt 2 timed out ");
	else
		printf("\n mpm_transport: Wait for interrupt 2 complete , event bitmap %x result: %d",
			reported_event_bitmap, result);
#endif
	/* This is End of Usage model 1 */

	/* This is Beginning of Usage model 1 :
	 with interrupt handled in thread*/
#ifdef ENABLE_TEST_HYPLNK_INTERRUPT_WITHTHREAD
{
	int err;
	/* Create a thread to handle interrupts */
	err = pthread_create(&tid1, NULL, &interrupt_thread_function, h);
	if (err != 0)
		printf("\ncan't create thread :%s", strerror(err));
	else
		printf("\n Thread 1 created successfully\n");
	fflush(stdout);

	/* Create a thread to handle interrupts */
	err = pthread_create(&tid2, NULL, &interrupt_thread_function2, h);
	if (err != 0)
		printf("\ncan't create thread :%s", strerror(err));
	else
		printf("\n Thread 2 created successfully\n");
	fflush(stdout);

	/* Create a thread to handle interrupts */
	err = pthread_create(&tid3, NULL, &interrupt_thread_function3, h);
	if (err != 0)
		printf("\ncan't create thread :%s", strerror(err));
	else
		printf("\n Thread 3 created successfully\n");
	fflush(stdout);

	/* The following is emulation of background code.
		It waits for interrupt flag to be set by interrupt handling thread */
	while(!interrupt_occurred_flag[0]) {
		printf("\n Background loop 1 executing...");fflush(stdout);
		sleep(5);
		if(trigger_interrupt_flag) {
			printf("\n Generating test interrupt from background ");fflush(stdout);
			/* This is just test code to generate interrupt locally  */
			result = mpm_transport_generate_interrupt_event(h, THREAD1_INTERRUPT_MASK| THREAD2_INTERRUPT_MASK);
			if(result < 0) {
				printf("\n Error generating interrupt");
				goto error;
			}
			trigger_interrupt_flag = 0;
		}
	}
	while(!interrupt_occurred_flag[1]) {
		printf("\n Background loop 2 executing...");fflush(stdout);
		sleep(5);
	}
	/* This is just test code to generate interrupt locally  */
	result = mpm_transport_generate_interrupt_event(h, THREAD3_INTERRUPT);
	if(result < 0) {
		printf("\n Error generating interrupt");
		goto error;
	}

	while(!interrupt_occurred_flag[2]) {
		printf("\n Background loop 3 executing...");fflush(stdout);
		sleep(5);
	}
}
#endif
	/* This is End of Usage model 1 :
	 with interrupt handled in thread*/

	/* This is Beginning of Usage model 2 */
#ifdef ENABLE_TEST_HYPLNK_INTERRUPT_USAGE_MODEL2
{
	int event_fd;
	uint32_t value;
	fd_set fdset;

	printf("\n Testing remote interrupt: using model 2\n");fflush(stdout);

	/* Get the event fd */
	event_fd = mpm_transport_get_event_fd(h, 0xffffffff);
	if(event_fd < 0) {
		printf("\n Error getting event fd");
		result = -1;
		goto error;
	}
	/* Enable interrupt */
	value = 1;
	result = write(event_fd, &value, sizeof(uint32_t));
	if( result < 0 ) {
		printf("\n Error writing to fd");
		goto error;
	}
	FD_ZERO(&fdset);
	FD_SET(event_fd, &fdset);

	/* Time out set to five seconds. */
	tv.tv_sec = 5;
	tv.tv_usec = 0;
	printf("\n Waiting for interrupt.4..with 5 sec timeout");fflush(stdout);

	result = select(event_fd+1, &fdset, NULL, NULL, &tv);

	if( result == 0 ) {
		printf("\n Interrupt 4 wait timed out ");
	} else {
		printf("\n mpm_transport: Wait for interrupt 4 complete : result: %d",
		result);

		/* Read and ack interrupt event in the peripheral*/
		result = mpm_transport_read_and_ack_interrupt_event(h,
				0xffffffff, &reported_event_bitmap);
		if (result < 0 ) {
			printf("\n mpm_transport_wait_for_interrupt_event: read and ack interrupt failed");
			goto error;
		}

		/* Read the event fd to clear interrupt */
		printf("\n reported event bitmap 0x%x", reported_event_bitmap);
		result = read(event_fd, &value, sizeof(uint32_t));
		if(result == 0 ) {
			printf("\n read  failed");
			result= -1;
		} else
			result=0;
	}
}
	/* This is End of Usage model 2 */
#endif
#if (defined(ENABLE_TEST1_HYPLNK_INTERRUPT) \
	|| defined(ENABLE_TEST2_HYPLNK_INTERRUPT) \
	|| defined(ENABLE_TEST_HYPLNK_INTERRUPT_WITHTHREAD) \
	|| defined(ENABLE_TEST_HYPLNK_INTERRUPT_USAGE_MODEL2))
	mpm_transport_close(h);
#endif

error:
	if (result) {
		printf("\n mpm transport: hyperlink remote test failed %d\n", result);
	} else {
		printf("\nmpm transport: hyperlink remote test passed\n");
	}
	return 0;
}
