/*
 * Copyright (C) 2013-2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <errno.h>
#include <unistd.h>
#include <time.h>
#include <ti/sdo/edma3/drv/edma3_drv.h>
#include "mpm_transport.h"

#define TEST_DESTINATION (0xa0000800)
#define TEST_DESTINATION2 (0xa0000848)
#define TEST_DESTINATION3 (0xa0100000)
#define TEST_DESTINATION4 (0xa0200000)
#define TEST_SOURCE (0x0c000000)

#define TEST_GET_DESTINATION (0xa0400800)

#define TEST_BUFFER_SIZE		0x20064
#define TEST_BUFFER_SIZE_LARGE	0x102244

#define TEST_LINKED 1

#ifdef TEST_LINKED
uint32_t src[] = {0x0c001000, 0x0c002000, 0x0c100000, 0x0c300000};
uint32_t dst[] = {0xa0001000, 0xa0002000, 0xa0200000, 0xa1000000};
uint32_t length[] = {0x1000, 0x4000, 0x100000, 0x200000};
int num_links = 4;
uint32_t src2[] = {0x0c001000, 0x0c002000, 0x0c050000, 0x0c150000, 0x0c300000};
uint32_t dst2[] = {0xa0010000, 0xa0020000, 0xa0400000, 0xa0500000, 0xa2000000};
uint32_t length2[] = {0x1000, 0x4000, 0x100000, 0x102244, 0x100000};
int num_links2 = 5;
#endif

#define MAX_LINE_LENGTH 64

int check_device_exist(char *st)
{
	int ret, fd;
	char tmp[6] = "/dev/";
	char dev[MAX_LINE_LENGTH];
	snprintf(dev, strlen(tmp) + strlen(st) + 1, "%s%s", tmp, st);
	fd = open(dev, O_SYNC);
	if (fd < 0) {
		/* errno 2 is No such file or directory. */
		if (errno == 2) ret = 0;
		else ret = errno;
	}
	else {
		close(fd);
		ret = 1;
	}
	return ret;
}

int dev_mem_fd;
void * my_mmap(uint32_t addr, uint32_t size)
{
	void *virt_addr;
	uint32_t page_size, mapaddr, offset;
	page_size = getpagesize();
	mapaddr = addr & (~(page_size - 1));
	offset = addr - mapaddr;
	virt_addr = mmap(0, size + offset, (PROT_READ|PROT_WRITE), MAP_SHARED, dev_mem_fd, (off_t)mapaddr);
	return virt_addr + offset;
}

int verify_data(uint32_t dst, uint32_t length, uint32_t val, uint32_t val_offset)
{
	uint32_t *dstBuff;
	uint32_t i;
	printf("->Verifying data for test destination 0x%08x... \t",dst);
	dstBuff = (uint32_t *) my_mmap(dst, length);
	for (i = 0; i<length/4; i++) {
		if ( *(dstBuff+i) != (val+(i*val_offset))) {
			printf("\n!! Data verification failed for address 0x%08x\n",dst+(i*4));
			printf("\tExpected: 0x%08x, got: 0x%08x\n",(val+(i*val_offset)),*(dstBuff+i));
			return -1;
		}
	}
	printf(" PASSED!\n");
	return 0;
}

int verify_data_reverse(uint32_t dst, uint32_t length, uint32_t val, uint32_t val_offset)
{
	uint32_t *dstBuff;
	uint32_t i;
	printf("->Verifying data for test destination 0x%08x... \t",dst);
	dstBuff = (uint32_t *) my_mmap(dst, length);
	for (i = length/4 - 1; i!=0xFFFFFFFF; i--) {
		if ( *(dstBuff+i) != (val+(i*val_offset))) {
			printf("\n!! Data verification failed for address 0x%08x\n",dst+(i*4));
			printf("\tExpected: 0x%08x, got: 0x%08x\n",(val+(i*val_offset)),*(dstBuff+i));
			return -1;
		}
	}
	printf(" PASSED!\n");
	return 0;
}

int main()
{
	// MPM Transport vars
	mpm_transport_h h = NULL;
	mpm_transport_h h2 = NULL;
	mpm_transport_trans_h th;
	mpm_transport_trans_h th2;
	mpm_transport_trans_h th3;
	uint32_t *srcBuff;
	uint32_t *dstBuff;
	uint32_t i, j;
	int ret = 0;
	int check1, check2;
	union char_int {
		char buf[4];
		uint32_t num;
	} val;
	val.num = 0xaabbccdd;

	mpm_transport_open_t ocfg = {
		.open_mode	= (O_SYNC|O_RDWR),
		.msec_timeout = 0,
		.serdes_init = 0,
	};

	// Fill in some dummy data...
	dev_mem_fd = open("/dev/mem", (O_RDWR|O_SYNC));
	srcBuff = (uint32_t *) my_mmap(TEST_SOURCE, TEST_BUFFER_SIZE_LARGE);
	for (i = 0; i<TEST_BUFFER_SIZE_LARGE/4; i++)
	{
		*(srcBuff+i) = 0xFEEDBEEF;
	}

	if (check1=check_device_exist("hyperlink0")) {
		h = mpm_transport_open("arm-loopback-hyplnk-0", &ocfg);
		if (!h) {
		printf("h open failed\n");
		return -1;
		}

		if (mpm_transport_write(h, TEST_DESTINATION3, 4, val.buf, NULL) != 0) {
			printf("Error in using basic mpm_transport_write\n");
			goto end;
		}

		printf("About to do put/get initiates\n");

		th = mpm_transport_put_initiate(h, TEST_DESTINATION, TEST_SOURCE, TEST_BUFFER_SIZE, false, NULL);
		th2 = mpm_transport_put_initiate(h, TEST_DESTINATION2, TEST_SOURCE, TEST_BUFFER_SIZE, false, NULL);
		th3 = mpm_transport_put_initiate(h, TEST_DESTINATION3, TEST_SOURCE, TEST_BUFFER_SIZE_LARGE, false, NULL);
		while(mpm_transport_transfer_check(h, th) != 1);
		while(mpm_transport_transfer_check(h, th2) != 1);
		while(mpm_transport_transfer_check(h, th3) != 1);

		printf("Nested put initiates done for hyperlink0, verifying...\n");
		ret = verify_data(TEST_DESTINATION, TEST_BUFFER_SIZE, 0xFEEDBEEF, 0);
		if (ret == -1) goto end;
		ret = verify_data(TEST_DESTINATION2, TEST_BUFFER_SIZE, 0xFEEDBEEF, 0);
		if (ret == -1) goto end;
		ret = verify_data(TEST_DESTINATION3, TEST_BUFFER_SIZE_LARGE, 0xFEEDBEEF, 0);
		if (ret == -1) goto end;
		mpm_transport_put_initiate(h, TEST_DESTINATION3, TEST_SOURCE, TEST_BUFFER_SIZE_LARGE, true, NULL);

		for (i = 0; i<TEST_BUFFER_SIZE_LARGE/4; i++)
		{
			*(srcBuff+i) = 0xBABEFACE;
		}
		th = mpm_transport_get_initiate(h, TEST_SOURCE, TEST_GET_DESTINATION, TEST_BUFFER_SIZE_LARGE, false, NULL);
		while(mpm_transport_transfer_check(h, th) != 1);

		printf("Get initiate done, verifying...\n");
		ret = verify_data(TEST_GET_DESTINATION, TEST_BUFFER_SIZE_LARGE, 0xBABEFACE, 0);
		if (ret == -1) goto end;
		mpm_transport_close(h);
	}
	else {
		printf("No hyperlink0 device!\n");
		printf("Skipping dma transfers on hyperlink0...\n");
	}
	if (check2=check_device_exist("hyperlink1")) {
		h2 = mpm_transport_open("arm-loopback-hyplnk-1", &ocfg);
		if (!h2) {
		printf("h2 open failed\n");
		return -1;
		}

		for (i = 0; i<TEST_BUFFER_SIZE_LARGE/4; i++)
		{
			*(srcBuff+i) = 0xFEEDBEEF;
		}

		th = mpm_transport_put_initiate(h2, TEST_DESTINATION4, TEST_SOURCE, TEST_BUFFER_SIZE, false, NULL);
		while(mpm_transport_transfer_check(h2, th) != 1);

		printf("Nested put initiates done for hyperlink1, verifying...\n");
		ret = verify_data(TEST_DESTINATION4, TEST_BUFFER_SIZE, 0xFEEDBEEF, 0);
		if (ret == -1) goto end;

		mpm_transport_close(h2);
	}
	else {
		printf("No hyperlink1 device!\n");
		printf("Skipping dma transfers on hyperlink1...\n");
	}

	printf("Finished put/get initiates\n");

	if (!check1 || !check2) {
		printf("Skipping nested dual hyperlink linked transfer test...\n");
		goto result;
	}
#ifdef TEST_LINKED
	printf("====================================================\n");
	printf("About to do linked transfers\n");
	h = mpm_transport_open("arm-loopback-hyplnk-0", &ocfg);
	if (!h) {
		printf("h open failed\n");
		return -1;
	}
	h2 = mpm_transport_open("arm-loopback-hyplnk-1", &ocfg);
	if (!h2) {
		printf("h2 open failed\n");
		return -1;
	}

	// Dummy data...
	for (j = 0; j<num_links; j++)
	{
		srcBuff = (uint32_t *) my_mmap(src[j], length[j]);
		dstBuff = (uint32_t *) my_mmap(dst[j], length[j]);
		for (i = 0; i<length[j]/4; i++)
		{
			*(srcBuff+i) = src[j]+i*num_links;
			*(dstBuff+i) = 0xFFFFFFFF;
		}
		munmap(srcBuff,length[j]);
		munmap(dstBuff,length[j]);
	}
	th = mpm_transport_put_initiate_linked(h, num_links, dst, src, length, false, NULL);
	while(mpm_transport_transfer_check(h, th) != 1);

	printf("Hyplnk0 Finished linked put transfer, verifying...\n");

	for (j = num_links-1; j!=0xFFFFFFFF; j--)
	{
		ret = verify_data_reverse(dst[j], length[j], src[j], num_links);
		if (ret == -1) goto end;
	}

	// Dummy data 2
	for (j = 0; j<num_links2; j++)
	{
		srcBuff = (uint32_t *) my_mmap(src2[j], length2[j]);
		dstBuff = (uint32_t *) my_mmap(dst2[j], length2[j]);
		for (i = 0; i<length2[j]/4; i++)
		{
			*(srcBuff+i) = src2[j]+i*num_links2;
			*(dstBuff+i) = 0xFFFFFFFF;
		}
		munmap(srcBuff,length2[j]);
	}

	th2 = mpm_transport_put_initiate_linked(h2, num_links2, dst2, src2, length2, false, NULL);
	while(mpm_transport_transfer_check(h2, th2) != 1);
	printf("Hyplnk1 Finished linked put transfer, verifying...\n");
	for (j = num_links2-1; j!=0xFFFFFFFF; j--)
	{
		ret = verify_data_reverse(dst2[j], length2[j], src2[j], num_links2);
		if (ret == -1) goto end;
	}


#endif

end:
	printf("====================================================\n");
	if (h) mpm_transport_close(h);
	if (h2) mpm_transport_close(h2);
	close(dev_mem_fd);
result:
	if (ret == -1)
		printf("mpm transport hyperlink loopback with DMA test FAILED!!\n");
	else
		printf("mpm transport hyperlink loopback with DMA test finished.\n");
	return 0;
}
