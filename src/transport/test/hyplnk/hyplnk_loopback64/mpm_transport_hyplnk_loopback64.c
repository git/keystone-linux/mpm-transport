/*
 * Copyright (C) 2013-2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <time.h>

#include <sys/mman.h>
#include <errno.h>
#include <unistd.h>
#include <pthread.h>
#include <semaphore.h>
#include <ti/sdo/edma3/drv/edma3_drv.h>
#include "mpm_transport.h"
static int64_t clock_diff (struct timespec *start, struct timespec *end)
{
    return (end->tv_sec - start->tv_sec) * 1000000000 
             + end->tv_nsec - start->tv_nsec;
}

#define CONVERT_ULL_TO_ALIAS(x) (0x80000000 + (x - 0x800000000))

#define TEST_DESTINATION1 0x822000000
#define TEST_DESTINATION2 0xa0000000
#define TEST_DESTINATION3 0xa1000000
#define TEST_SOURCE1 0x839000000
#define TEST_SOURCE2 0x830000000
#define TEST_SOURCE3 0x838000000
#define TEST_BUFFER_SIZE 0x400000

#define TEST_GET_DESTINATION (0x820400000)

#define MAX_LINE_LENGTH 64

#define NUM_REPETITIONS 20
int read_write_size = TEST_BUFFER_SIZE;
uint32_t *srcBuff;
uint32_t *testBuf, *testBuf1;
uint32_t *mapped_ptr;

pthread_t tid1, tid2, tid3;
volatile int task_finished_flag[3]={0, 0, 0};

int check_device_exist(char *st)
{
	int ret, fd;
	char tmp[6] = "/dev/";
	char dev[MAX_LINE_LENGTH];
	snprintf(dev, strlen(tmp) + strlen(st) + 1, "%s%s", tmp, st);
	fd = open(dev, O_SYNC);
	if (fd < 0) {
		/* errno 2 is No such file or directory. */
		if (errno == 2) ret = 0;
		else ret = errno;
	}
	else {
		close(fd);
		ret = 1;
	}
	return ret;
}

int dev_mem_fd;
void * my_mmap(uint32_t addr, uint32_t size)
{
	void *virt_addr;
	uint32_t page_size, mapaddr, offset;
	page_size = getpagesize();
	mapaddr = addr & (~(page_size - 1));
	offset = addr - mapaddr;
	virt_addr = mmap(0, size + offset, (PROT_READ|PROT_WRITE), MAP_SHARED, dev_mem_fd, (off_t)mapaddr);
	return virt_addr + offset;
}

int verify_data(uint32_t dst, uint32_t length, uint32_t val)
{
	uint32_t *dstBuff;
	uint32_t i;
	printf("->Verifying data for test destination 0x%08x... \t",dst);
	dstBuff = (uint32_t *) my_mmap(dst, length);
	for (i = 0; i<length/4; i++) {
		if ( *(dstBuff+i) != val) {
			printf(" FAILED!\n");
			printf("!! Data verification failed for address 0x%08x\n",dst+i);
			printf("!! Index %d:  Expected %x : got %x \n", i, val, *(dstBuff+i));
			return -1;
		}
	}
	printf(" PASSED!\n");
	return 0;
}
int verify_buffer_data(uint32_t *dstBuff, uint32_t length, uint32_t val)
{
	uint32_t i;
	printf("->Verifying data for test destination 0x%08x... \t",dstBuff);
	for (i = 0; i<length/4; i++) {
		if ( *(dstBuff+i) != val) {
			printf(" FAILED!\n");
			printf("!! Data verification failed for address 0x%08x\n",dstBuff+i);
			printf("!! Index %d: Expected %x : got %x \n", i, val, *(dstBuff+i));
			return -1;
		}
	}
	printf(" PASSED!\n");
	return 0;
}
int verify_buffer_data1(uint32_t *dstBuff, uint32_t length, uint32_t val)
{
	uint32_t i;
	uint32_t diff_time;
	struct timespec tp_start, tp_end;
	printf("->Verifying data for test destination 0x%08x... \t",dstBuff);

	clock_gettime(CLOCK_MONOTONIC, &tp_start);

	for (i = 0; i<length/4; i++) {
		if ( *(dstBuff+i) != val) {
			printf(" FAILED!\n");
			printf("!! Data verification failed for address 0x%08x\n",dstBuff+i);
			printf("!! Index %d: Expected %x : got %x \n", i, val, *(dstBuff+i));
			return -1;
		}
	}
	clock_gettime(CLOCK_MONOTONIC, &tp_end);
	printf(" PASSED!\n");
	diff_time = clock_diff (&tp_start, &tp_end);
	printf(" verify mmap64 diff time  %d ns \n",diff_time);

	return 0;
}
void *test_thread_function1(void *arg)
{
	mpm_transport_h handle=arg;
	int result = 0;
	int j, ret;
	struct timespec tp_start, tp_end, tp_end1, tp_end2;
	uint32_t diff_time;

	for ( j=0; j< NUM_REPETITIONS; j++) {
		/* mpm transport repeated write 64 test */
		printf("Iteration %d: testing mpm_transport write64 36-bit addr \n", j);
		clock_gettime(CLOCK_MONOTONIC, &tp_start);
		ret = mpm_transport_write64(handle, TEST_DESTINATION1, read_write_size, (char *)testBuf, NULL);
		clock_gettime(CLOCK_MONOTONIC, &tp_end);
		if(ret < 0) {
			printf(" Error in mpm transport write64\n"); 
			goto end;
		}
		printf("Iteration %d: mpm_transport write64 complete \n", j);

		ret = verify_data(CONVERT_ULL_TO_ALIAS(TEST_DESTINATION1), read_write_size, 0xDEAFDEAF);
		if (ret == -1) goto end;
		diff_time = clock_diff (&tp_start, &tp_end);
		printf("Iteration %d: write64 diff time  %d ns \n", j, diff_time);
	}

	printf("\n Finished thread 1 successfully\n");
	task_finished_flag[0] = 1;

	return(NULL);

end:
	printf("\n Finished thread 1 with error at iteration %d\n", j);
	task_finished_flag[0] = 1;

	return(NULL);
}
void *test_thread_function2(void *arg)
{
	mpm_transport_h handle=arg;
	int result = 0;
	int j, ret;
	struct timespec tp_start, tp_end, tp_end1, tp_end2;
	uint32_t diff_time;

	for ( j=0; j< NUM_REPETITIONS; j++) {
		/* mpm transport read 64 test */
		printf("Iteration %d: testing mpm_transport read64 36-bit addr \n", j);
		clock_gettime(CLOCK_MONOTONIC, &tp_start);
		ret = mpm_transport_read64(handle, TEST_SOURCE2, read_write_size, (char *)testBuf1, NULL);
		clock_gettime(CLOCK_MONOTONIC, &tp_end);
		printf("Iteration %d: mpm_transport read64 complete \n", j);
		ret = verify_buffer_data(testBuf1, read_write_size, 0xBABEBABE);
		if (ret == -1) goto end;
		diff_time = clock_diff (&tp_start, &tp_end);
		printf("Iteration %d: read64 diff time  %d ns \n",j, diff_time);
	}

	printf("\n Finished thread 2 successfully\n");
	task_finished_flag[1] = 1;

	return(NULL);
end:
	printf("\n Finished thread 2 with error at iteration %d\n", j);
	task_finished_flag[1] = 1;

	return(NULL);
}
void *test_thread_function3(void *arg)
{
	mpm_transport_h handle=arg;
	int result = 0;
	int j, ret;
	struct timespec tp_start, tp_end, tp_end1, tp_end2;
	uint32_t diff_time;

	for ( j=0; j< NUM_REPETITIONS; j++) {
		clock_gettime(CLOCK_MONOTONIC, &tp_start);
		mapped_ptr = mpm_transport_mmap64(handle, TEST_SOURCE3, read_write_size, NULL);
		clock_gettime(CLOCK_MONOTONIC, &tp_end);
		if ( testBuf1 == MAP_FAILED ) {
			printf("\n Error mmap64 "); 
			goto end;
		}
		diff_time = clock_diff (&tp_start, &tp_end);
		printf(" mmap64 diff time  %d ns \n",diff_time);

		ret = verify_buffer_data1(mapped_ptr, read_write_size, 0xDEADDEAD);
		if (ret == -1) goto end;
		printf(" Testing mpm-transport munmap64 \n");
		clock_gettime(CLOCK_MONOTONIC, &tp_start);
		ret = mpm_transport_munmap64(handle, mapped_ptr, read_write_size);
		clock_gettime(CLOCK_MONOTONIC, &tp_end);
		if( ret < 0 ) {
			printf("\n Error unmap64 ");
			goto end;
		}
		diff_time = clock_diff (&tp_start, &tp_end);
		printf(" munmap64 diff time  %d ns \n",diff_time);

	}
	printf("\n Finished thread successfully 3\n");
	task_finished_flag[2] = 1;

	return(NULL);
end:
	printf("\n Finished thread 3 with error at iteration %d\n", j);
	task_finished_flag[2] = 1;

	return(NULL);
}
int main(int argc, char *argv[])
{
	int i, j, ret =0;
	int check1, check2;
	mpm_transport_h h = NULL;
	mpm_transport_h h2 = NULL;
	mpm_transport_trans_h th;
	mpm_transport_trans_h th2;
	uint32_t diff_time;
	struct timespec tp_start, tp_end, tp_end1, tp_end2;
	int err;

	mpm_transport_open_t ocfg = {
		.open_mode	= (O_SYNC|O_RDWR),
		.msec_timeout = 0,
		.serdes_init = 0,
	};

	if( argc >= 2 )
		sscanf (argv[1], "%x", &read_write_size);

	printf("\n Test read write size 0x%x \n", read_write_size);
	testBuf = malloc(read_write_size*sizeof(uint32_t));
	if (testBuf == NULL) {
		printf("\n Error allocating test buffer *");
		exit(0);
	}
	testBuf1 = malloc(read_write_size*sizeof(uint32_t));
	if (testBuf1 == NULL) {
		printf("\n Error allocating test buffer1 *");
		exit(0);
	}
	// Fill in some dummy data...
	dev_mem_fd = open("/dev/mem", (O_RDWR|O_SYNC));
	srcBuff = (uint32_t *) my_mmap(CONVERT_ULL_TO_ALIAS(TEST_SOURCE1), read_write_size );
	for (i = 0; i<read_write_size /4; i++) {
		*(srcBuff+i) = 0xFEEDFEED;
	}
	srcBuff = (uint32_t *) my_mmap(CONVERT_ULL_TO_ALIAS(TEST_SOURCE2), read_write_size );
	for (i = 0; i<read_write_size /4; i++) {
		*(srcBuff+i) = 0xBABEBABE;
	}
	srcBuff = (uint32_t *) my_mmap(CONVERT_ULL_TO_ALIAS(TEST_SOURCE3), read_write_size );
	for (i = 0; i<read_write_size /4; i++) {
		*(srcBuff+i) = 0xDEADDEAD;
	}

	for (i = 0; i<read_write_size /4; i++) {
		*(testBuf+i) = 0xDEAFDEAF;
	}
	for (i = 0; i<read_write_size /4; i++) {
		*(testBuf1+i) = 0xDEAFDEAF;
	}
	if (check1=check_device_exist("hyperlink0")) {

		/* Open mpm transport */
		printf("\nOpening mpmtransport with arm-loopback-hyplnk64-0\n");
		h = mpm_transport_open("arm-loopback-hyplnk64-0",&ocfg);
		if (!h) {
		printf("h open failed\n");
		ret = -1;
		goto end;
		}
		printf("Opened mpm transport arm-loopback-hyplnk64-0\n");

		/* mpm transport first time dummy write 64 test */
		printf("testing mpm_transport write64 36-bit addr \n");
		clock_gettime(CLOCK_MONOTONIC, &tp_start);
		ret = mpm_transport_write64(h, TEST_DESTINATION1, read_write_size, (char *)testBuf, NULL);
		clock_gettime(CLOCK_MONOTONIC, &tp_end);
		if(ret < 0) {
			printf(" Error in mpm transport write64\n"); 
			goto end;
		}
		printf("mpm_transport write64 complete \n");
		diff_time = clock_diff (&tp_start, &tp_end);
		printf(" Dummy write64 diff time  %d ns \n",diff_time);

		/* mpm transport write 64 test */
		printf("testing mpm_transport write64 36-bit addr \n");
		clock_gettime(CLOCK_MONOTONIC, &tp_start);
		ret = mpm_transport_write64(h, TEST_DESTINATION1, read_write_size, (char *)testBuf, NULL);
		clock_gettime(CLOCK_MONOTONIC, &tp_end);
		if(ret < 0) {
			printf(" Error in mpm transport write64\n"); 
			goto end;
		}
		printf("mpm_transport write64 complete \n");

		ret = verify_data(CONVERT_ULL_TO_ALIAS(TEST_DESTINATION1), read_write_size, 0xDEAFDEAF);
		if (ret == -1) goto end;
		diff_time = clock_diff (&tp_start, &tp_end);
		printf(" write64 diff time  %d ns \n",diff_time);

		for ( j=0; j< NUM_REPETITIONS; j++) {
			/* mpm transport repeated write 64 test */
			printf("Iteration %d: testing mpm_transport write64 36-bit addr \n", j);
			clock_gettime(CLOCK_MONOTONIC, &tp_start);
			ret = mpm_transport_write64(h, TEST_DESTINATION1, read_write_size, (char *)testBuf, NULL);
			clock_gettime(CLOCK_MONOTONIC, &tp_end);
			if(ret < 0) {
				printf(" Error in mpm transport write64\n"); 
				goto end;
			}
			printf("Iteration %d: mpm_transport write64 complete \n", j);

			ret = verify_data(CONVERT_ULL_TO_ALIAS(TEST_DESTINATION1), read_write_size, 0xDEAFDEAF);
			if (ret == -1) goto end;
			diff_time = clock_diff (&tp_start, &tp_end);
			printf("Iteration %d: write64 diff time  %d ns \n", j, diff_time);
		}
		/* mpm transport mmap 64 test */
		printf(" Testing mpm-transport mmap64 \n");

		clock_gettime(CLOCK_MONOTONIC, &tp_start);
		mapped_ptr = mpm_transport_mmap64(h, TEST_DESTINATION1, read_write_size, NULL);
		clock_gettime(CLOCK_MONOTONIC, &tp_end);
		if ( mapped_ptr == MAP_FAILED ) {
			printf("\n Error mmap64 "); 
			goto end;
		}

		diff_time = clock_diff (&tp_start, &tp_end);
		printf(" mmap64 diff time  %d ns \n",diff_time);

		ret = verify_buffer_data1(mapped_ptr, read_write_size, 0xDEAFDEAF);
		if (ret == -1) goto end;

		printf(" Testing mpm-transport munmap64 \n");
		clock_gettime(CLOCK_MONOTONIC, &tp_start);
		ret = mpm_transport_munmap64(h, mapped_ptr, read_write_size);
		clock_gettime(CLOCK_MONOTONIC, &tp_end);
		if( ret < 0 ) {
			printf("\n Error unmap64 ");
			goto end;
		}
		diff_time = clock_diff (&tp_start, &tp_end);
		printf(" munmap64 diff time  %d ns \n",diff_time);

		/* mpm transport read 64 test */
		printf("testing mpm_transport read64 36-bit addr \n");
		clock_gettime(CLOCK_MONOTONIC, &tp_start);
		ret = mpm_transport_read64(h, TEST_DESTINATION1, read_write_size, (char *)testBuf, NULL);
		clock_gettime(CLOCK_MONOTONIC, &tp_end);
		printf("mpm_transport read64 complete \n");
		ret = verify_buffer_data(testBuf, read_write_size, 0xDEAFDEAF);
		if (ret == -1) goto end;
		diff_time = clock_diff (&tp_start, &tp_end);
		printf(" read64 diff time  %d ns \n",diff_time);

		/* Create a thread1 for multitask test */
		err = pthread_create(&tid1, NULL, &test_thread_function1, h);
		if (err != 0)
			printf("\ncan't create thread :%s", strerror(err));
		else
			printf("\n Thread 1 created successfully\n");

		/* Create a thread2 for multitask test */
		err = pthread_create(&tid2, NULL, &test_thread_function2, h);
		if (err != 0)
			printf("\ncan't create thread :%s", strerror(err));
		else
			printf("\n Thread 2 created successfully\n");

		/* Create a thread3 for multitask test */
		err = pthread_create(&tid3, NULL, &test_thread_function3, h);
		if (err != 0)
			printf("\ncan't create thread :%s", strerror(err));
		else
			printf("\n Thread 3 created successfully\n");

		while(!task_finished_flag[0]) {
			printf("\n Background loop 1 executing...");fflush(stdout);
			sleep(1);
		}
		while(!task_finished_flag[1]) {
			printf("\n Background loop 2 executing...");fflush(stdout);
			sleep(1);
		}
		while(!task_finished_flag[2]) {
			printf("\n Background loop 3 executing...");fflush(stdout);
			sleep(1);
		}
		/* mpm transport hyplnk put initate 64 test */
		printf("testing hyperlink put_initiate 36-bit addr to remote 36-bit addr\n");

		printf("Writing to 0x%09llx, from 0x%09llx...\n", TEST_DESTINATION1, TEST_SOURCE1);
		clock_gettime(CLOCK_MONOTONIC, &tp_start);
		th = mpm_transport_put_initiate64(h, TEST_DESTINATION1, TEST_SOURCE1, read_write_size, false, NULL);
		clock_gettime(CLOCK_MONOTONIC, &tp_end);
		while(mpm_transport_transfer_check(h, th) != 1);
		clock_gettime(CLOCK_MONOTONIC, &tp_end1);
		diff_time = clock_diff (&tp_start, &tp_end);
		printf(" put_initiate 36-bit addr to remote 36-bit addr diff time  %d ns \n",diff_time);
		diff_time = clock_diff (&tp_end, &tp_end1);
		printf(" put_initiate transfer check diff time  %d ns \n",diff_time);

		printf("Verifying data...\n");
		ret = verify_data(CONVERT_ULL_TO_ALIAS(TEST_DESTINATION1), read_write_size, 0xFEEDFEED);

		if (ret == -1) goto end;
		mpm_transport_close(h);

		/* Open mpm transport second test */
		printf("Opening mpm transport second time\n");
		h = mpm_transport_open("arm-loopback-hyplnk64-0",&ocfg);
		if (!h) {
		printf("h open failed\n");
		ret = -1;
		goto end;
		}

		/* mpm transport hyplnk put initate 64 test2 */
		printf("testing hyperlink put_initiate 36-bit addr to remote 32-bit addr\n");
		clock_gettime(CLOCK_MONOTONIC, &tp_start);
		mpm_transport_put_initiate64(h, TEST_DESTINATION2, TEST_SOURCE2, read_write_size, true, NULL);
		clock_gettime(CLOCK_MONOTONIC, &tp_end);
		printf("Verifying data...\n");
		ret = verify_data(TEST_DESTINATION2, read_write_size, 0xBABEBABE);
		if (ret == -1) goto end;
		diff_time = clock_diff (&tp_start, &tp_end);
		printf(" put_initiate 36-bit addr to remote 32-bit diff time  %d ns \n",diff_time);

		/* Testing multiple calls to : mpm transport hyplnk put initate 64  */
		for ( j=0; j < NUM_REPETITIONS; j++) {
			printf("Iteration %d: testing hyperlink put_initiate 36-bit addr to remote 32-bit addr\n",
				j);
			clock_gettime(CLOCK_MONOTONIC, &tp_start);
			th2 = mpm_transport_put_initiate64(h, TEST_DESTINATION2, TEST_SOURCE1, read_write_size, true, NULL);
			if ( th2 == MPM_TRANSPORT_TRANS_FAIL) {
				printf(" ERROR: mpm_transport_put_initiate64 failed\n");
				goto end;
			}
			clock_gettime(CLOCK_MONOTONIC, &tp_end);
			printf("Iteration %d: Verifying data...\n", j);
			ret = verify_data(TEST_DESTINATION2, read_write_size, 0xFEEDFEED);
			if (ret == -1) goto end;
			diff_time = clock_diff (&tp_start, &tp_end);
			printf("Iteration %d:  put_initiate 36-bit addr to remote 32-bit diff time  %d ns \n",
				j, diff_time);
		}

		/* mpm transport hyplnk get initate 64 test */
		printf("testing hyperlink get_initiate 36-bit addr to remote 36-bit addr\n");
		clock_gettime(CLOCK_MONOTONIC, &tp_start);
		th2 = mpm_transport_get_initiate64(h, TEST_SOURCE1, TEST_GET_DESTINATION, read_write_size, false, NULL);
		if ( th2 == NULL) {
			printf(" Error in mpm_transport_get_initiate64 : transaction handle NULL\n");
			goto end;
		}
		clock_gettime(CLOCK_MONOTONIC, &tp_end);
		while(mpm_transport_transfer_check(h, th2) != 1);
		clock_gettime(CLOCK_MONOTONIC, &tp_end1);
		ret = verify_data(CONVERT_ULL_TO_ALIAS(TEST_GET_DESTINATION), read_write_size, 0xFEEDFEED);
		if (ret == -1) goto end;
		clock_gettime(CLOCK_MONOTONIC, &tp_end1);
		diff_time = clock_diff (&tp_start, &tp_end);
		printf(" get_initiate 36-bit addr to remote 36-bit add diff time  %d ns \n",diff_time);
		diff_time = clock_diff (&tp_end, &tp_end1);
		printf(" get_initiate transfer check diff time  %d ns \n",diff_time);

		if (check2=check_device_exist("hyperlink1")) {
			/* mpm transport hyplnk 1 test */
			h2 = mpm_transport_open("arm-loopback-hyplnk64-1", &ocfg);
			if (!h2) {
			printf("h2 open failed\n");
			ret = -1;
			goto end;
			}

			/* mpm transport hyplnk put initate 64 test with hyperlink1 */
			printf("testing hyperlink 1 put_initiate 36-bit addr to remote 32-bit addr\n");
			clock_gettime(CLOCK_MONOTONIC, &tp_start);
			mpm_transport_put_initiate64(h2, TEST_DESTINATION3, TEST_SOURCE3, read_write_size, true, NULL);
			clock_gettime(CLOCK_MONOTONIC, &tp_end);
			ret = verify_data(TEST_DESTINATION3, read_write_size, 0xDEADDEAD);
			diff_time = clock_diff (&tp_start, &tp_end);
			printf(" put_initiate hyperlink 1 36-bit addr to remote 32-bit diff time  %d ns \n",diff_time);

			mpm_transport_close(h2);
		}
		else {
			printf("No hyperlink1 device!\n");
			printf("Skipping hyperlink1 put_initiate64() test...\n");
		}
	}
	else {
		printf("No hyperlink0 device!\n");
		printf("Skipping hyperlink0 put_initiate64() test...\n");
	}

end:
	free(testBuf);
	free(testBuf1);
	if 	(h && check1) mpm_transport_close(h);
	if (ret == -1)
		printf("mpm transport64: hyperlink loopback test failed!!\n");
	else
		printf("mpm transport64: hyperlink loopback test finished!\n");
	return 0;
}
