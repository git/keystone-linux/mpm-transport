/*
 * Copyright (C) 2013-2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include <mpm_transport_cfg.h>
#include "mpm_transport_keystone_mmap.h"

keystone_mmap_handle_t mpm_transport_keystone_mmap_open(uint32_t base, 
	uint32_t size, unsigned int index, unsigned int count,
	uint32_t msmc_cfg, int fd, int priv)
{
  keystone_mmap_init_config_t mmap_cfg;
  keystone_mmap_address_range_t range[] = {
	{base, size},
	{0,0}
  };
  if (msmc_cfg)
	mmap_cfg.msmcRegs = (void *) msmc_cfg;
  else
	mmap_cfg.msmcRegs = (void *) CSL_MSMC_CFG_REGS;

  if (fd)
	mmap_cfg.fd = fd;
  else
  {
	mmap_cfg.fd = open("/dev/mpax", (O_RDWR | O_SYNC));
	if ( mmap_cfg.fd < 0 )
	{
		mpm_printf(1, "Error opening /dev/mpax \n");
		return (void *) -1;
	}
  }

  mmap_cfg.addr_range = range;
  mmap_cfg.privID = priv;
  mmap_cfg.xmcRegs = (void *) NULL;
  mmap_cfg.cgemRegs = (void *) NULL;
  mmap_cfg.xmcXmpaxRegCount = 0;
  mmap_cfg.xmcXmpaxRegStart = 0;
  mmap_cfg.sesMpaxRegsPerPrividCount = count;
  mmap_cfg.sesMpaxRegsPerPrividStart = index;
  return keystone_mmap_init(&mmap_cfg);
}

uint32_t mpm_transport_keystone_mmap(keystone_mmap_handle_t h, uint64_t addr, uint32_t size)
{
  void *ret;
  ret = keystone_mmap ( h, addr, size, KEYSTONE_MMAP_PROT_READ|KEYSTONE_MMAP_PROT_WRITE, 0);
  if (ret == KEYSTONE_MMAP_MAP_FAILED)
	return 0;
  else
	return (uint32_t) ret;
}

int mpm_transport_keystone_unmap(keystone_mmap_handle_t h, uint32_t addr, uint32_t size)
{
  keystone_munmap(h, (uint32_t *)addr,size);
  return 0;
}

int mpm_transport_keystone_free(keystone_mmap_handle_t h)
{
  keystone_mmap_free(h);
  return 0;
}
