/*
 *  Copyright (c) Texas Instruments Incorporated 2015
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include "fw_mem_allocator.h"

/* Lock to be used for critical section */
pthread_mutex_t fw_mutex_lock;

void fw_osalInit()
{
	pthread_mutex_init(&fw_mutex_lock, NULL);
	return;
}

void fw_osalshutdown()
{
	pthread_mutex_destroy(&fw_mutex_lock);
	return;
}

static inline void fw_osalEnterCS()
{
#if 0
	pthread_mutex_lock(&mutex_lock);
#endif
	return;
}

static inline void fw_osalLeaveCS()
{

#if 0
	pthread_mutex_unlock(&mutex_lock);
#endif
	return;
}

/*****************************************************************************
 * FUNCTION PURPOSE: Cache Invalidation Routine
 *****************************************************************************
 * DESCRIPTION: Cache Invalidation Routine
 *****************************************************************************/
void Osal_invalidateCache (void *blockPtr, uint32_t size)
{
	/* Stub Function. TBD: Would need to handle when cache is enabled for ARM */
	return;
}

/*****************************************************************************
 * FUNCTION PURPOSE: Cache Writeback Routine
 *****************************************************************************
 * DESCRIPTION: Cache Invalidation Routine
 *****************************************************************************/
void Osal_writeBackCache (void *blockPtr, uint32_t size)
{
	/* Stub Function. TBD: Would need to handle when cache is enabled for ARM */
	return;
}

void Osal_fwCsEnter (uint32_t *key)
{

	/* Stub Function. TBD: Would need to handle when for multi proc access
	 * To be handled using infrastructure available from Kernel
	 */
	return;
}

void Osal_fwCsExit (uint32_t key)
{

	/* Stub Function. TBD: Would need to handle when for multi proc access
	 * To be handled using infrastructure available from Kernel
	 */
	return;
}
