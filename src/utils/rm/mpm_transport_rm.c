/*
 * Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include "mpm_transport_rm.h"

/**
 *  @b Description
 *  @n
 *      Utility to feed a command to rm
 *
 *  @param[in]  name
 *      Name to query
 *  @param[in]  val
 *      return value or value to set
 *  @param[in]  type
 *      transaction type
 *  @param[in]  name
 *      retry until success
 *
 *  @retval
 *      Value
 */
int32_t mpm_transport_rm_cmd(Rm_ServiceHandle *service_h, const char *name, int32_t *val,
						Rm_ServiceType type, int retry)
{
	Rm_ServiceReqInfo  rmServiceReq;
	Rm_ServiceRespInfo rmServiceResp;
	int                succeed;

	memset((void *)&rmServiceReq, 0, sizeof(rmServiceReq));
	memset((void *)&rmServiceResp, 0, sizeof(rmServiceResp));

	rmServiceReq.type           = type;
	rmServiceReq.resourceName   = name;
	rmServiceReq.resourceNsName = name;
	rmServiceReq.resourceLength = 1;
	if (val) {
		rmServiceReq.resourceBase = *val;
	}

	/* RM will block until resource is returned since callback is NULL */
	rmServiceReq.callback.serviceCallback = NULL;
	do {
		service_h->Rm_serviceHandler(service_h->rmHandle, &rmServiceReq,
									 &rmServiceResp);
		succeed = (rmServiceResp.serviceState == RM_SERVICE_APPROVED) ||
					(rmServiceResp.serviceState == RM_SERVICE_APPROVED_STATIC);
		if (retry && (!succeed)) {
			sched_yield();
		}
	} while (retry && (!succeed));

	if (succeed) {
		if ((type == Rm_service_RESOURCE_GET_BY_NAME) && (val)) {
			*val = rmServiceResp.resourceBase;
		}
	} else {
		mpm_printf(1, "mpm_transport_rm_cmd(): Failed rm transaction %d %s %d (%d)\n", type, name,
				val ? *val : 0, rmServiceResp.serviceState);
	}

	return(rmServiceResp.serviceState);
}


/**
 *  @b Description
 *  @n
 *      Utility to look up a name in RM
 *
 *  @param[in]  name
 *      Name to query
 *
 *  @retval
 *      Value
 */
int32_t mpm_transport_rm_name_lookup (Rm_ServiceHandle *rmClientServiceHandle, const char *name, int32_t *val)
{
	*val = RM_RESOURCE_BASE_UNSPECIFIED;
	return(mpm_transport_rm_cmd(rmClientServiceHandle, name, val, Rm_service_RESOURCE_GET_BY_NAME, 1));
}

/**
 *  @b Description
 *  @n
 *      Utility to set a name in RM
 *
 *  @param[in]  name
 *      Name to query
 *
 *  @param[in]  val
 *      Value
 *
 *  @retval
 *      None
 */
int32_t mpm_transport_rm_name_set (Rm_ServiceHandle *rmClientServiceHandle, const char *name, int32_t *val)
{
	return(mpm_transport_rm_cmd(rmClientServiceHandle, name, val, Rm_service_RESOURCE_MAP_TO_NAME, 0));
}

/**
 *  @b Description
 *  @n
 *      Utility to delete a name from RM
 *
 *  @param[in]  name
 *      Name to delete
 *
 *  @retval
 *      None
 */
int32_t mpm_transport_rm_name_del (Rm_ServiceHandle *rmClientServiceHandle, const char *name)
{
	return(mpm_transport_rm_cmd(rmClientServiceHandle, name, NULL, Rm_service_RESOURCE_UNMAP_NAME, 0));
}
