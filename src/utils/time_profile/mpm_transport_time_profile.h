/*
 * Copyright (C) 2013-2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#define TIME_PROFILING 0
#if TIME_PROFILING
#define TIME_PROFILE_HYPLNK_PUT_INITIATE				0
#define TIME_PROFILE_HYPLNK_GET_INITIATE				0
#define TIME_PROFILE_HYPLNK_PUT_INITIATE_LINKED			0
#define TIME_PROFILE_HYPLNK_GET_INITIATE_LINKED			0
#define TIME_PROFILE_HYPLNK_TRANSFER_CHECK				0
#define TIME_PROFILE_HYPLNK_WINDOW_MAP					0
#define TIME_PROFILE_HYPLNK_GET_WINDOW					0
#define TIME_PROFILE_EDMA3_COPY1D1D						1
#define TIME_PROFILE_EDMA3_COPY1D1D_LINKED				1
#define TIME_PROFILE_EDMA3_TRANSFER_LINKED				0
#include <time.h>
static int64_t clock_diff (struct timespec *start, struct timespec *end)
{
	return (end->tv_sec - start->tv_sec) * 1000000000
			 + end->tv_nsec - start->tv_nsec;
}
#endif
