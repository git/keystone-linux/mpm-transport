/*
 * Copyright (C) 2013-2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>
#include <dirent.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>

#include "uioutils.h"

#define MAX_FILE_NAME_LENGTH	128
#define MAX_LINE_LENGTH			128
#define MAX_NAME_LENGTH			64
#define MAX_LABEL_LENGTH		16

int endian_check() {
	int check = 1;
	char ret = *(char *) &check;
	return ret;
}

int byte_swap(int x) {
	return ( ((x>>0) & 0xff) << 24)
		 | ( ((x>>8) & 0xff) << 16)
		 | ( ((x>>16) & 0xff) << 8)
		 | ( ((x>>24) & 0xff) << 0);
}

int uioutil_get_param_int(char *uio_name, char *param_name, int *buf)
{
	char filename[MAX_LINE_LENGTH];
	FILE *fp;
	int ret = -1;
	int size = 35 + strlen(uio_name) + strlen(param_name);
	if (size > MAX_LINE_LENGTH) return -1;
	snprintf(filename, size, "/proc/device-tree/soc/%s/cfg-params/%s", uio_name, param_name);
	fp = fopen(filename, "rb");
	if (fp == NULL) return -1;
	if (fread(&ret, sizeof(int), 1, fp) <= 0) {
		if (fp) fclose(fp);
		return -1;
	}
	if (endian_check()) ret = byte_swap(ret);
	if (fp) fclose(fp);
	*buf = ret;
	return 0;
}

int uioutil_get_param_int_array(char *uio_name, char *param_name, int array_size, int *buf)
{
	char filename[MAX_LINE_LENGTH];
	FILE *fp;
	int ret = -1;
	int size = 35 + strlen(uio_name) + strlen(param_name);
	int i;

	if (size > MAX_LINE_LENGTH) return -1;
	if (buf == NULL) return -1;
	snprintf(filename, size, "/proc/device-tree/soc/%s/cfg-params/%s", uio_name, param_name);
	fp = fopen(filename, "rb");
	if (fp == NULL) return -1;
	for (i=0; i<array_size; i++)
	{
		if (fread(&ret, sizeof(int), 1, fp) <= 0) {
			if (fp) fclose(fp);
			return -1;
		}
		if (endian_check()) ret = byte_swap(ret);
		buf[i] = ret;
	}
	if (fp) fclose(fp);
	return 0;
}

int uioutil_get_param_str(char *uio_name, char *param_name, char *buf) 
{
	char st[MAX_LINE_LENGTH];
	FILE *fp;
	char filename[MAX_LINE_LENGTH] = "";
	int size = 35 + strlen(uio_name) + strlen(param_name);
	if (size > MAX_LINE_LENGTH) return -1;
	snprintf(filename, size, "/proc/device-tree/soc/%s/cfg-params/%s", uio_name, param_name);
	fp = fopen(filename, "r");
	if (fp==NULL) return -1;
	if (fscanf(fp, "%s", st) == EOF) {
		if (fp) fclose(fp);
		return -1;
	}
	if (fp) fclose(fp);
	strncpy(buf,st, MAX_LINE_LENGTH);
	return 0;
}

int uioutil_check_device_exist(char *st) 
{
	int ret, fd;
	char tmp[6] = "/dev/";
	char dev[MAX_LINE_LENGTH];
	snprintf(dev, strlen(tmp) + strlen(st) + 1, "%s%s", tmp, st);
	fd = open(dev, O_SYNC);
	if (fd < 0) {
		/* errno 2 is No such file or directory. */
		if (errno == 2) ret = 0;
		else ret = errno;
	}
	else {
		close(fd);
		ret = 1;
	}
	return ret;
}

int uioutil_get_string (char *filename, char *str, int str_len)
{
	FILE *fpr = 0;
	int ret_val = -1;

	if (!filename || !str || !str_len) {
		goto close_n_exit;
	}

	fpr = fopen(filename, "r");
	if (!fpr) {
		goto close_n_exit;
	}
	if (!fgets(str, str_len, fpr)) {
		goto close_n_exit;
	}

	/* Terminate string at new line or carriage return if any */
	str[strcspn(str, "\n\r")] = '\0';

	ret_val = 0;

close_n_exit:
	if (fpr) fclose(fpr);
	return ret_val;
}

int uioutil_get_int (char *filename, int *val)
{
	char line[MAX_LINE_LENGTH];

	if (!filename || !val)
		return -1;

	if (uioutil_get_string(filename, line, MAX_LINE_LENGTH) < 0)
		return -1;

	*val = strtoul(line, NULL, 0);
	return 0;
}

char  *remove_postfix_from_device_name(char *name, int max_length)
{
  int index;

  index=0;

  /* strip anything after . in the name */
  while(name[index] != '\0')
  {
	if(name[index] == '.')
	{
		name[index] = '\0';
		index++;
		return(&name[0]);
	}
	if(index > max_length)
		break;
	index++;
  }
  return(name);
}

char  *remove_prefix_from_device_name(char *name, int max_length)
{ 
  int index;

  index=0;
  
  /* strip anything before . in the name */
  while(name[index] != '\0')
  {
	if(name[index] == '.')
	{ 
		index++;
		return(&name[index]);
	}
	if(index > max_length)
		break;
	index++;
  }
  return(name);
}
int uioutil_get_device (char *uio_name, char *class_name, int class_name_length)
{
	struct dirent *entry = 0;
	char filename[MAX_FILE_NAME_LENGTH];
	char name[MAX_NAME_LENGTH];
	int ret_val = -1;
	DIR *dir = 0;
		char *name_ptr;

	if (!uio_name || !class_name || !strlen(uio_name)) {
		goto close_n_exit;
	}

	dir = opendir("/sys/class/uio");
	if (!dir) {
		perror("readdir /sys/class/uio");
		goto close_n_exit;
	}

	while ((entry = readdir(dir)) != NULL) {
		if (strstr (entry->d_name, "uio") == NULL) {
			continue;
		}

		snprintf(filename, MAX_FILE_NAME_LENGTH, "/sys/class/uio/%s/name", entry->d_name);

		if (uioutil_get_string(filename, name, MAX_NAME_LENGTH) < 0)
			goto close_n_exit;
				name_ptr = remove_prefix_from_device_name(name, MAX_NAME_LENGTH);
		if (!strncmp (uio_name, name_ptr, strlen(uio_name))) {
			ret_val = 0;
			strncpy (class_name, entry->d_name, class_name_length);
			goto close_n_exit;
		}
	}

close_n_exit:
	if (dir) closedir(dir);

	return ret_val;
}

int uioutil_get_peripheral_device (char *uio_name, char *class_name, int class_name_length)
{
	struct dirent *entry = 0;
	char filename[MAX_FILE_NAME_LENGTH];
	char name[MAX_NAME_LENGTH];
	int ret_val = -1;
	DIR *dir = 0;
		char *name_ptr;

	if (!uio_name || !class_name || !strlen(uio_name)) {
		goto close_n_exit;
	}

	dir = opendir("/sys/class/uio");
	if (!dir) {
		perror("readdir /sys/class/uio");
		goto close_n_exit;
	}

	while ((entry = readdir(dir)) != NULL) {
		if (strstr (entry->d_name, "uio") == NULL) {
			continue;
		}

		snprintf(filename, MAX_FILE_NAME_LENGTH, "/sys/class/uio/%s/name", entry->d_name);

		if (uioutil_get_string(filename, name, MAX_NAME_LENGTH) < 0)
			goto close_n_exit;
				name_ptr = remove_postfix_from_device_name(name, MAX_NAME_LENGTH);
		if (!strncmp (uio_name, name_ptr, strlen(uio_name))) {
			ret_val = 0;
			strncpy (class_name, entry->d_name, class_name_length);
			goto close_n_exit;
		}
	}

close_n_exit:
	if (dir) closedir(dir);

	return ret_val;
}
