/*
 * Copyright (C) 2013-2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <errno.h>
#include <unistd.h>
#include <pthread.h>
#include <semaphore.h>

#include "ti/sdo/edma3/drv/edma3_drv.h"
#include "ti/sdo/edma3/rm/edma3_rm.h"

/* OPT Field specific defines */
#define OPT_SYNCDIM_SHIFT                   (0x00000002u)
#define OPT_TCC_MASK                        (0x0003F000u)
#define OPT_TCC_SHIFT                       (0x0000000Cu)
#define OPT_ITCINTEN_SHIFT                  (0x00000015u)
#define OPT_TCINTEN_SHIFT                   (0x00000014u)

#define NUM_EDMA3_INSTANCES         5u

typedef struct mpm_transport_trans_tag {
	uint32_t key;
	uint32_t chan_id;
	uint32_t tcc_no;
	uint32_t trans_addr;
	uint32_t local_logical_addr;
	char is_active;
	void *chain;
} mpm_transport_trans_t;

int mpm_transport_edma3_init_cfg(int inst);
EDMA3_DRV_Handle mpm_transport_edma3_setup(uint32_t inst, uint32_t shreg);
int mpm_transport_edma3_transfer(EDMA3_DRV_Handle hEdma, uint32_t src, uint32_t dst, uint32_t length, bool is_blocking, mpm_transport_trans_t *tp);
int mpm_transport_edma3_close(EDMA3_DRV_Handle hEdma, int inst);
int mpm_transport_edma3_checkAndClearTcc(EDMA3_DRV_Handle hEdma, uint32_t tccNo);
int mpm_transport_edma3_free_channel(EDMA3_DRV_Handle hEdma, uint32_t chId);
int mpm_transport_edma3_prealloc_ch(EDMA3_DRV_Handle hEdma, uint32_t index, unsigned int *chId, unsigned int *tcc);
int mpm_transport_edma3_transfer_linked(EDMA3_DRV_Handle hEdma, unsigned int *ch_avail, uint32_t ch_num, unsigned int tcc, int num_links, uint32_t src[], uint32_t dst[], uint32_t length[], bool is_blocking, mpm_transport_trans_t *tp);

extern EDMA3_DRV_GblConfigParams sampleEdma3GblCfgParams[NUM_EDMA3_INSTANCES];
extern EDMA3_DRV_InstanceInitConfig sampleInstInitConfig[NUM_EDMA3_INSTANCES][EDMA3_MAX_REGIONS];
