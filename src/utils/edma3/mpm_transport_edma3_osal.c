/*
 * Copyright (C) 2013-2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */


#include "mpm_transport_edma3.h"
 
sem_t mutex;

void edma3OsProtectEntry (unsigned int edma3InstanceId, int level, unsigned int *intState)
{
}

void edma3OsProtectExit (unsigned int edma3InstanceId,
						int level, unsigned int intState) {}

EDMA3_DRV_Result Edma3_CacheInvalidate(unsigned int mem_start_ptr,
							unsigned int    num_bytes) {return EDMA3_DRV_SOK;}

EDMA3_DRV_Result Edma3_CacheFlush(unsigned int mem_start_ptr,
						unsigned int num_bytes) {return EDMA3_DRV_SOK;}

EDMA3_DRV_Result edma3OsSemCreate() 
{
	sem_init(&mutex, 0, 1);
	return EDMA3_DRV_SOK;
}

EDMA3_DRV_Result edma3OsSemDelete() 
{
	sem_destroy(&mutex);
	return EDMA3_DRV_SOK;
}

EDMA3_DRV_Result edma3OsSemTake(EDMA3_OS_Sem_Handle hSem, int32_t mSecTimeout) 
{
	sem_post(&mutex);
	return EDMA3_DRV_SOK;
}

EDMA3_DRV_Result edma3OsSemGive(EDMA3_OS_Sem_Handle hSem) 
{
	sem_wait(&mutex);
	return EDMA3_DRV_SOK;
}

