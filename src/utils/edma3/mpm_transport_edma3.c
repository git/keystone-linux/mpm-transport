/*
 * Copyright (C) 2013-2015 Texas Instruments Incorporated - http://www.ti.com/
 *
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include <stdlib.h>
#include "mpm_transport_edma3.h"
#include "mpm_transport_time_profile.h"

#include "uio_module_drv.h"

uint32_t edma3_mmap(uint32_t addr, uint32_t offset, uint32_t size, int fd)
{
	uint32_t virt_addr;
	uint32_t page_size, pg_offset, base_correction, mmap_length;

	page_size = getpagesize();
	base_correction = addr & (page_size -1);
	pg_offset = offset & (~((page_size<< UIO_MODULE_DRV_MAP_OFFSET_SHIFT) - 1));
	mmap_length = size + offset - pg_offset + base_correction;


	/* Map with index 0 */
	virt_addr = (uint32_t) mmap(NULL, mmap_length, (PROT_READ|PROT_WRITE), MAP_SHARED, fd,
		pg_offset);
	if (virt_addr == -1)
	{
			mpm_printf(1, "EDMA3 mmap failed for address 0x%08x, size 0x%08x!\n",addr,size);
			return 0;
	}
	return virt_addr + (offset - pg_offset) + base_correction;
}

int setupEdmaConfig(EDMA3_DRV_GblConfigParams *cfg)
{
	int i;
	int edma_fd;
	uint32_t mmap_base_dma_addr;

	if((edma_fd = open("/dev/edma3", (O_RDWR))) == -1)
	{
		mpm_printf(1, "Failed to open \"dev/edma3\" err=%s\n",
				strerror(errno));
		return -1;
	}
	mmap_base_dma_addr = (uint32_t)cfg->globalRegs;
	cfg->globalRegs = (uint32_t *)edma3_mmap(mmap_base_dma_addr, 0, 0x8000, edma_fd);
	if (cfg->globalRegs == 0) {
		if (edma_fd) close(edma_fd);
		return -1;
	}
	for (i = 0; i < EDMA3_MAX_TC; i++)
	{
		if (cfg->tcRegs[i] != NULL)
		{
			cfg->tcRegs[i] =(uint32_t *)edma3_mmap((uint32_t)cfg->tcRegs[i],
					 ((uint32_t)cfg->tcRegs[i] - mmap_base_dma_addr),
					 0x1000, edma_fd);
			if (cfg->tcRegs[i] == 0) {
				if (edma_fd) close(edma_fd);
				return -1;
			}
		}
	}
	if (edma_fd) close(edma_fd);
	return 1;
}

int edma3_copy1d1d(
									EDMA3_DRV_Handle hEdma,
									void *   src,
									void *   dst,
									unsigned int num_bytes,
									bool is_blocking)
	{
#if TIME_PROFILE_EDMA3_COPY1D1D
	struct timespec tp_start, tp_end;
#endif


	EDMA3_DRV_Result result = EDMA3_DRV_SOK;
	EDMA3_DRV_PaRAMRegs paramSet = {0,0,0,0,0,0,0,0,0,0,0,0};
	EDMA3_RM_EventQueue evtQueue = 0;
	unsigned int chId = 0;
	unsigned int tcc = 0;
	unsigned int acnt, bcnt, ccnt=1, r, acnt_r, n=0;
	int srcbidx = 0, desbidx = 0;
	int srccidx = 0, descidx = 0;
	char *c_src = (char *)src, *c_dst = (char *)dst;

	/* Setup for Channel 1*/
	tcc = EDMA3_DRV_TCC_ANY;
	chId = EDMA3_DRV_DMA_CHANNEL_ANY;

#if TIME_PROFILE_EDMA3_COPY1D1D
	clock_gettime(CLOCK_MONOTONIC, &tp_start);
#endif
	/* Request any DMA channel and any TCC */
	result = EDMA3_DRV_requestChannel ( hEdma, &chId, &tcc, evtQueue,
										NULL, NULL);
	if (result != EDMA3_DRV_SOK) {
		mpm_printf(1, "EDMA3 DRV request channel failed for %d, result is %d !\n",chId, result);
		return -1;
	}
#if TIME_PROFILE_EDMA3_COPY1D1D
	clock_gettime(CLOCK_MONOTONIC, &tp_end);
	mpm_printf(1, "[TIME PROFILE: edma3_copy1d1d] Time taken for requestChannel(): %lld\n",clock_diff (&tp_start, &tp_end));
#endif

#if TIME_PROFILE_EDMA3_COPY1D1D
	clock_gettime(CLOCK_MONOTONIC, &tp_start);
#endif
	acnt = num_bytes;
	/*
	* This abstracts an effective 1D1D transfer which can transfer more than the 16-bit limit set by the HW.
	*
	* This is done by splitting the transfer into 2 transfers:
	*   - One 2D1D transfer.
	*   - One 1D1D transfer for the remainder.
	*/

	while ( acnt > 0xFFFF )
	{
		acnt >>= 1;
		n++;
	}
	r = num_bytes - (acnt << n);
#if TIME_PROFILE_EDMA3_COPY1D1D
	clock_gettime(CLOCK_MONOTONIC, &tp_end);
	mpm_printf(1, "[TIME PROFILE: edma3_copy1d1d] Time taken for calculating remainder: %lld\n",clock_diff (&tp_start, &tp_end));
#endif

#if TIME_PROFILE_EDMA3_COPY1D1D
	clock_gettime(CLOCK_MONOTONIC, &tp_start);
#endif
	/* One 1D1D transfer for the remainder */
	if (r > 0) {
		src = (void *)c_src;
		dst = (void *)c_dst;

		acnt_r = r;
		bcnt = 1;

		/* Setting up the SRC/DES Index */
		srcbidx = (int)acnt_r;
		desbidx = (int)acnt_r;

		/* Fill the PaRAM Set with transfer specific information */
		paramSet.srcAddr    = (unsigned int)(src);
		paramSet.destAddr   = (unsigned int)(dst);
		paramSet.srcBIdx    = srcbidx;
		paramSet.destBIdx   = desbidx;
		paramSet.srcCIdx    = srccidx;
		paramSet.destCIdx   = descidx;
		paramSet.aCnt       = acnt_r;
		paramSet.bCnt       = bcnt;
		paramSet.cCnt       = ccnt;
		paramSet.bCntReload = paramSet.bCnt;
		paramSet.linkAddr   = 0xFFFFu;

		paramSet.opt &= 0xFFFFFFFCu;
		paramSet.opt |= ((tcc << OPT_TCC_SHIFT) & OPT_TCC_MASK);
		paramSet.opt |= (1 << OPT_TCINTEN_SHIFT);
		paramSet.opt &= 0xFFFFFFFBu;

		/* Now, write the PaRAM Set. */
		result = EDMA3_DRV_setPaRAM(hEdma, chId, &paramSet);

		if (result != EDMA3_DRV_SOK) {
			mpm_printf(1, "EDMA3 DRV set PaRAM failed !\n");
			return -1;
		}

		/*Enable the Transfer  */
		result = EDMA3_DRV_enableTransfer (hEdma, chId,
											EDMA3_DRV_TRIG_MODE_MANUAL);
		if (result != EDMA3_DRV_SOK)
			{
			mpm_printf(1, "edma3_test: EDMA3_DRV_enableTransfer " \
								"Failed, error code: %d\r\n", result);
			return -1;
			}

		/* Wait for transfer to be completed */
		EDMA3_DRV_waitAndClearTcc(hEdma,chId);

		c_src += r;
		c_dst += r;
	}
#if TIME_PROFILE_EDMA3_COPY1D1D
	clock_gettime(CLOCK_MONOTONIC, &tp_end);
	mpm_printf(1, "[TIME PROFILE: edma3_copy1d1d] Time taken for transfer of remainder %lld\n",clock_diff (&tp_start, &tp_end));
#endif

	/* One 2D1D transfer */
	{
		src = (void *)c_src;
		dst = (void *)c_dst;

		/* Setting up the SRC/DES Index */
		srcbidx = (int)(1<<n);
		desbidx = (int)(1<<n);

		/* Fill the PaRAM Set with transfer specific information */
		paramSet.srcAddr    = (unsigned int)(src);
		paramSet.destAddr   = (unsigned int)(dst);
		paramSet.srcBIdx    = srcbidx;
		paramSet.destBIdx   = desbidx;
		paramSet.srcCIdx    = srccidx;
		paramSet.destCIdx   = descidx;
		paramSet.aCnt       = (1<<n);
		paramSet.bCnt       = acnt;
		paramSet.cCnt       = ccnt;
		paramSet.bCntReload = paramSet.bCnt;
		paramSet.linkAddr   = 0xFFFFu;

		paramSet.opt &= 0xFFFFFFFCu;
		paramSet.opt |= ((tcc << OPT_TCC_SHIFT) & OPT_TCC_MASK);
		paramSet.opt |= (1 << OPT_TCINTEN_SHIFT);
		paramSet.opt |= (1 << OPT_SYNCDIM_SHIFT);

#if TIME_PROFILE_EDMA3_COPY1D1D
	clock_gettime(CLOCK_MONOTONIC, &tp_start);
#endif
		/* Now, write the PaRAM Set. */
		result = EDMA3_DRV_setPaRAM(hEdma, chId, &paramSet);

		if (result != EDMA3_DRV_SOK) {
			mpm_printf(1, "EDMA3 DRV set PaRAM failed !\n");
			return -1;
		}
#if TIME_PROFILE_EDMA3_COPY1D1D
	clock_gettime(CLOCK_MONOTONIC, &tp_end);
	mpm_printf(1, "[TIME PROFILE: edma3_copy1d1d] Time taken for setPaRAM() %lld\n",clock_diff (&tp_start, &tp_end));
#endif

#if TIME_PROFILE_EDMA3_COPY1D1D
	clock_gettime(CLOCK_MONOTONIC, &tp_start);
#endif
		/*Enable the Transfer  */
		result = EDMA3_DRV_enableTransfer (hEdma, chId,
											EDMA3_DRV_TRIG_MODE_MANUAL);
		if (result != EDMA3_DRV_SOK)
			{
			mpm_printf(1, "edma3_test: EDMA3_DRV_enableTransfer " \
								"Failed, error code: %d\r\n", result);
			return -1;
			}
#if TIME_PROFILE_EDMA3_COPY1D1D
	clock_gettime(CLOCK_MONOTONIC, &tp_end);
	mpm_printf(1, "[TIME PROFILE: edma3_copy1d1d] Time taken for enableTransfer() %lld\n",clock_diff (&tp_start, &tp_end));
#endif

	/* Wait for transfer to be completed */
	if (is_blocking)
		EDMA3_DRV_waitAndClearTcc(hEdma,chId);
	}

	/* Free the previously allocated channel. */
	if (is_blocking)
	{
		result = EDMA3_DRV_freeChannel (hEdma, chId);
		if (result != EDMA3_DRV_SOK)
			{
			mpm_printf(1, "edma3_test: EDMA3_DRV_freeChannel() FAILED, " \
								"error code: %d\r\n", result);
			}
	}
	return tcc;
}

int mpm_transport_edma3_transfer(EDMA3_DRV_Handle hEdma, uint32_t src, uint32_t dst, uint32_t length, bool is_blocking, mpm_transport_trans_t *tp)
{
	int tcc;
	int ret = 0;
	tcc = edma3_copy1d1d(hEdma, (void *) src, (void *) dst, length, is_blocking);
	if (tcc < 0)
		return -1;
	tp->tcc_no = tcc;
	return ret;
}

int mpm_transport_edma3_init_cfg(int inst)
{
	EDMA3_DRV_GblConfigParams *globalConfig = NULL;
	globalConfig = &sampleEdma3GblCfgParams[inst];
	if (setupEdmaConfig(globalConfig) == -1)
	{
		mpm_printf(1, "Failed to setup sampleEdma3GblCfgParams[%d]\n",inst);
		return 0;
	}
	else
		return 1;
}

EDMA3_DRV_Handle mpm_transport_edma3_setup(uint32_t inst, uint32_t shreg)
{
	EDMA3_DRV_Result edma3Result = EDMA3_DRV_E_INVALID_PARAM;
	EDMA3_DRV_GblConfigParams *globalConfig = NULL;
	EDMA3_DRV_InstanceInitConfig *instanceConfig = NULL;
	EDMA3_DRV_InitConfig initCfg;
	EDMA3_RM_MiscParam miscParam;
	unsigned int edma3Id = inst;
	unsigned int shadow_region = shreg;

	globalConfig = &sampleEdma3GblCfgParams[edma3Id];
	edma3Result = EDMA3_DRV_create(edma3Id, globalConfig, (void *) &miscParam);
	initCfg.drvSemHandle = (void *) 1;
	/* configuration structure for the Driver */
	instanceConfig = &sampleInstInitConfig[edma3Id][shadow_region];
	initCfg.isMaster = TRUE;
	/* Choose shadow region according to the DSP# */
	initCfg.regionId = (EDMA3_RM_RegionId)shadow_region;
	/* Driver instance specific config NULL */
	initCfg.drvInstInitConfig = instanceConfig;
	initCfg.gblerrCb = NULL;
	initCfg.gblerrData = NULL;
	/* Open the Driver Instance */
	return EDMA3_DRV_open (edma3Id, (const EDMA3_DRV_InitConfig *) &initCfg, &edma3Result);
}

int mpm_transport_edma3_checkAndClearTcc(EDMA3_DRV_Handle hEdma, uint32_t tccNo)
{
	uint16_t status;
	EDMA3_DRV_Result result;
	result = EDMA3_DRV_checkAndClearTcc(hEdma, tccNo, &status);
	if( result != EDMA3_DRV_SOK) {
		mpm_printf(1, "EDMA3 checkAndClearTcc failed: error %d", result);
		return(-1);
	}
	if (status)
		return 1;
	else
		return 0;
}

int mpm_transport_edma3_free_channel(EDMA3_DRV_Handle hEdma, uint32_t chId)
{
	EDMA3_DRV_freeChannel (hEdma, chId);
	return 0;
}

int mpm_transport_edma3_close(EDMA3_DRV_Handle hEdma, int inst)
{
	EDMA3_RM_MiscParam miscParam;
	EDMA3_DRV_close(hEdma, (void *) &miscParam);
	return EDMA3_DRV_delete(inst, &miscParam);
}

int mpm_transport_edma3_prealloc_ch(EDMA3_DRV_Handle hEdma, uint32_t index, unsigned int *chId, unsigned int *tcc)
{

	EDMA3_DRV_Result result = EDMA3_DRV_SOK;
	EDMA3_RM_EventQueue evtQueue = 0;
	//unsigned int tcc = 0;
	*tcc = EDMA3_DRV_TCC_ANY;
	if (index == 0) {
		*chId = EDMA3_DRV_QDMA_CHANNEL_ANY;
		*tcc = 4;
	}
	else
		*chId = EDMA3_DRV_LINK_CHANNEL;
	result = EDMA3_DRV_requestChannel ( hEdma, chId, tcc, evtQueue, NULL, NULL);
	if (result != EDMA3_DRV_SOK) {
		mpm_printf(1, "EDMA3 DRV prealloc request failed for index %d, result %d !\n", index, result);
		return -1;
	}
	if (index == 0)
		EDMA3_DRV_setQdmaTrigWord (hEdma, *chId, EDMA3_RM_QDMA_TRIG_CCNT);
	return 0;
}

int mpm_transport_edma3_transfer_linked(EDMA3_DRV_Handle hEdma, unsigned int *ch_avail, uint32_t ch_num, unsigned int tcc, int num_links, uint32_t src[], uint32_t dst[], uint32_t length[], bool is_blocking, mpm_transport_trans_t *tp)
{
	EDMA3_DRV_Result result = EDMA3_DRV_SOK;
	EDMA3_DRV_PaRAMRegs paramSet = {0,0,0,0,0,0,0,0,0,0,0,0};
	int i;
	uint32_t size;
	int srcbidx = 0, desbidx = 0;
	int srccidx = 0, descidx = 0;
	unsigned int acnt, bcnt, ccnt=1, r, acnt_r, n=0;
	unsigned int tc_opt = 0;
	unsigned int ch_cnt = ch_num-1;
	uint32_t c_src, c_dst;
	unsigned int tcc_no = 0;

	mpm_transport_trans_t *trans = tp;
	tp->tcc_no = tcc;

	for (i=num_links-1; i>=0; i--) {
		n=0;
		paramSet.srcAddr    = 0;
		paramSet.destAddr   = 0;
		paramSet.srcBIdx    = 0;
		paramSet.destBIdx   = 0;
		paramSet.srcCIdx    = 0;
		paramSet.destCIdx   = 0;
		paramSet.aCnt       = 0;
		paramSet.bCnt       = 0;
		paramSet.cCnt       = 0;
		paramSet.bCntReload = 0;
		paramSet.opt = 0;
		c_src = (uint32_t) src[i];
		c_dst = (uint32_t) dst[i];

		size = (uint32_t) length[i];
		acnt = size;
		while ( acnt > 0xFFFF )
		{
			acnt >>= 1;
			n++;
		}
		r = size - (acnt << n);
		//mpm_printf(1, "size is %d, acnt is %d, n is %d, acnt<<n is %d, r is %lu\n",size, acnt, n, acnt<<n, r);
		if (r>0) {
			acnt_r = r;
			bcnt = 1;
			srcbidx = (int)acnt_r;
			desbidx = (int)acnt_r;
			//mpm_printf(1, "EDMA3 DBG R: src 0x%08x, dst 0x%08x, acnt %d, bcnt %d, ccnt %d\n",c_src,c_dst,acnt_r,bcnt,ccnt);
			paramSet.srcAddr    = (unsigned int)(c_src);
			paramSet.destAddr   = (unsigned int)(c_dst);
			paramSet.srcBIdx    = srcbidx;
			paramSet.destBIdx   = desbidx;
			paramSet.srcCIdx    = srccidx;
			paramSet.destCIdx   = descidx;
			paramSet.aCnt       = acnt_r;
			paramSet.bCnt       = bcnt;
			paramSet.cCnt       = ccnt;
			paramSet.bCntReload = paramSet.bCnt;
			paramSet.opt &= 0xFFFFFFFCu;
			paramSet.opt |= ((tcc << OPT_TCC_SHIFT) & OPT_TCC_MASK);
			paramSet.opt &= ~(1 << OPT_ITCINTEN_SHIFT);
			paramSet.opt &= ~(1 << 3);
			paramSet.opt |= (tc_opt << OPT_TCINTEN_SHIFT);
			paramSet.opt &= 0xFFFFFFFBu;
			paramSet.linkAddr = ((ch_avail[ch_cnt-1] << 5) & 0xFFFFu);

			result = EDMA3_DRV_setPaRAM(hEdma, ch_avail[ch_cnt], &paramSet);
			if (result != EDMA3_DRV_SOK) {
				mpm_printf(1, "EDMA3 DBG [1]: EDMA3 DRV set PaRAM failed !\n");
				mpm_printf(1, "ch_cnt: %d, channel: %d, iteration %d, num_links %d, result %d\n",ch_cnt,ch_avail[ch_cnt],i, num_links, result);
				return -1;
			}
			trans->chan_id = ch_avail[ch_cnt];
			trans->chain = (mpm_transport_trans_t *) malloc(sizeof(mpm_transport_trans_t));
			trans = trans->chain;
			trans->key = 0x00aa00bb;
			c_src += r;
			c_dst += r;
			ch_cnt--;
		}

		{
			srcbidx = (int)(1<<n);
			desbidx = (int)(1<<n);
			//mpm_printf(1, "EDMA3 DBG: src 0x%08x, dst 0x%08x, acnt %d, bcnt %d, ccnt %d\n",c_src,c_dst,(1<<n),acnt,ccnt);
			paramSet.srcAddr    = (unsigned int)(c_src);
			paramSet.destAddr   = (unsigned int)(c_dst);
			paramSet.srcBIdx    = srcbidx;
			paramSet.destBIdx   = desbidx;
			paramSet.srcCIdx    = srccidx;
			paramSet.destCIdx   = descidx;
			paramSet.aCnt       = (1<<n);
			paramSet.bCnt       = acnt;
			paramSet.cCnt       = ccnt;
			paramSet.bCntReload = paramSet.bCnt;
			paramSet.opt &= 0xFFFFFFFCu;
			paramSet.opt &= ~(1 << OPT_ITCINTEN_SHIFT);
			paramSet.opt &= ~(1 << 3);
			paramSet.opt |= (1 << OPT_SYNCDIM_SHIFT);
			if (i==1) {
				paramSet.linkAddr = 0xFFFFu;
				paramSet.opt |= (1 << 3);
				tc_opt = 1;
				tcc_no = tcc;
			}
			else {
				if (i==0){
					tc_opt = 0;
					tcc_no = 0;
					ch_cnt = 0;
					paramSet.linkAddr = ((ch_avail[ch_num-1] << 5) & 0xFFFFu);
				}
				else
					paramSet.linkAddr = ((ch_avail[ch_cnt-1] << 5) & 0xFFFFu);
			}
			paramSet.opt |= (tc_opt << OPT_TCINTEN_SHIFT);
			paramSet.opt |= ((tcc_no << OPT_TCC_SHIFT) & OPT_TCC_MASK);
			result = EDMA3_DRV_setPaRAM(hEdma, ch_avail[ch_cnt], &paramSet);
			if (result != EDMA3_DRV_SOK) {
				mpm_printf(1, "EDMA3 DBG [2]: EDMA3 DRV set PaRAM failed !\n");
				mpm_printf(1, "ch_cnt: %d, channel: %d, iteration %d, num_links %d, result %d\n",ch_cnt,ch_avail[ch_cnt],i, num_links, result);
				return -1;
			}
			trans->chan_id = ch_avail[ch_cnt];
			trans->chain = (mpm_transport_trans_t *) malloc(sizeof(mpm_transport_trans_t));
			trans = trans->chain;
			trans->key = 0x00aa00bb;
			ch_cnt--;
		}
		if (i==0) {
			//mpm_printf(1, "GOT HERE ch_cnt: %d, channel: %d, iteration %d, num_links %d, result %d\n",ch_cnt,ch_avail[ch_cnt],i, num_links, result);
			trans->chain = NULL;
		}
	}
	return 1;
}
